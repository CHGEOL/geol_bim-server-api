
import logging

from flask import request, make_response, jsonify, current_app as app
from flasgger import SwaggerView  # SwaggerView inherits flask.views.MethodView
from psycopg2 import sql

from ..shared import (HttpStatus, CRUD, CreateQueryBuilder, ReadQueryBuilder,
                      DeleteQueryBuilder)
from ..shared import authorization
from ..schemas import UserApplicationSchema

__all__ = ['UserApplication', 'UserApplicationCollection']


class UserApplication(SwaggerView):
    """
    /user/user_applications/<int:uap_usr_id>/<string:uap_role_code>
    """

    logger = logging.getLogger('apisrv')
    schema = UserApplicationSchema()
    db = CRUD()

    # A Role is required OR you are the user (no decorator for this case)
    @app.auth.login_required
    def delete(self, uap_usr_id: int, uap_role_code: str):
        """
        Delete the mapping of a user to the application.
        The application-admin can delete any mapping and a user can delete his mapping.
        ---
        operationId: user_application-id-delete
        tags:
          - user
        parameters:
          - in: path
            name: uap_usr_id
            schema:
              type: integer
              example: 1
            required: true
            description: Filtering criterion
          - in: path
            name: uap_role_code
            schema:
              type: string
              example: "ros-aadm"
            required: true
            description: Filtering criterion
        responses:
          200:
            $ref: '#/components/responses/delete_ok'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          404:
            $ref: '#/components/responses/not_found'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.not_found_404.value
        usr = app.auth.current_user()

        # for selection
        attr_filter = {'uap_usr_id': uap_usr_id,
                       'uap_role_code': uap_role_code}

        # Authorization (select usr, check)
        if not usr.usr_app_admin:
            authorization.select_usr_org_role_id(self.db, 'user_application', attr_filter)
            authorization.check_usr_id(uap_usr_id, usr)

        try:
            qb = DeleteQueryBuilder()
            qb.delete_sql = sql.SQL('DELETE FROM usr.user_application')
            qb.build_where_sql(attr_filter)  # uap_usr_id AND uap_role_code

            query, values = qb.delete_query()

            message, code = self.db.delete(query, values)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(jsonify(message), code)


class UserApplicationCollection(SwaggerView):
    """
    /user/user_applications
    """

    logger = logging.getLogger('apisrv')
    schema = UserApplicationSchema()
    db = CRUD()

    # Project-Admin can see all, the user only himself
    @app.auth.login_required
    def get(self):
        """
        Get the mappings of users to the application.
        The application-admins have access to all mappings
        and a user has access to all his mappings.
        ---
        operationId: user_application-get
        tags:
          - user
        parameters:
          - in: query
            name: uap_usr_id
            required: false
            schema:
              type: integer
              example: 1
            required: false
            description: Filtering criteria, must be the logged-in user or app-admin
          - in: query
            name: uap_role_code
            required: false
            schema:
              type: string
            required: false
            description: Filtering criteria, e.g. filtering by the role 'ros-aadm'
        responses:
          200:
            description: Multiple objects (user_application)
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/UserApplicationSchema'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.bad_request_400.value
        usr = app.auth.current_user()

        request_data = dict(request.args)  # Arguments from URL

        # Valid Parameter: attr_filter
        request_params = self.schema.parse_filter_param(request_data, attr_filter=True)
        attr_filter = request_params.values

        # Authorization: no restrictions for app-admin
        if attr_filter:  # all filtering attributes are foreign keys
            authorization.select_usr_org_role_id(self.db, 'user_application', attr_filter)
        if not usr.usr_app_admin:
            if 'uap_usr_id' in attr_filter:
                authorization.select_usr_org_role_id(self.db, 'user',
                                                     {'usr_id': attr_filter['uap_usr_id']})
                authorization.check_usr_id(attr_filter['uap_usr_id'], usr)
            else:
                err = app.config['DEFAULT_MESSAGES']['INP_VAL_REQ_FLD']
                err.details = {'uap_usr_id': 'Missing data for required field.'}
                return make_response(jsonify(err.to_dict()), err.code)

        try:
            qb = ReadQueryBuilder()
            qb.select_sql = sql.SQL(
                'SELECT uap.*,'
                ' usr.usr_name_first AS uap_usr_name_first, usr.usr_name AS uap_usr_name')
            qb.from_sql = sql.SQL(
                'FROM usr.user_application as uap'
                ' LEFT JOIN usr."user" AS usr ON usr.usr_id = uap.uap_usr_id')
            # The app-admin can get all data
            if usr.usr_app_admin:
                qb.build_where_sql(attr_filter)
            # A user can only get his data
            else:
                attr_filter['auth_usr_id']: usr.usr_id
                qb.build_where_sql(attr_filter)
            qb.order_sql = sql.SQL(
                'ORDER BY uap.uap_usr_id')

            query, values = qb.read_query()

            message, code = self.db.read(query, values, many=True)

            if code != 200:
                message = jsonify(message)
            else:
                message = self.schema.jsonify(message, many=True)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(message, code)

    # Only app-admins can add new admins (app-admin) or connect users (app-connect)
    @app.auth.login_required
    @authorization.application_role_required(role='app-admin')
    def post(self):
        """
        Create a new mapping of a user to the application.
        The application-admin can create this mapping.
        ---
        operationId: user_application-post
        tags:
          - user
        requestBody:
          required: true
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserApplicationSchema'
              example: {"uap_usr_id": "1",
                        "uap_role_code": "ros-aadm"}
        responses:
          201:
            description: Created.
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/ID'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.bad_request_400.value

        # Get the JSON from the request body and validate the data
        request_data = request.get_json()
        request_params = self.schema.parse_value_param(request_data)
        values = request_params.values

        try:  # Insert One Object
            qb = CreateQueryBuilder()
            qb.build_insert_sql(sql.SQL('usr.user_application'), values)
            qb.build_values_sql(values)
            qb.returning_sql = sql.SQL('RETURNING uap_usr_id, uap_role_code')

            query, values = qb.create_query()

            message, code = self.db.create(query,
                                           ['uap_usr_id', 'uap_role_code'],
                                           values)
            message = jsonify(message)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(message, code)
