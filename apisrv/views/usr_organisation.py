
import logging

from flask import request, make_response, jsonify, current_app as app
from flasgger import SwaggerView  # SwaggerView inherits flask.views.MethodView
from psycopg2 import sql

from ..shared import (HttpStatus, CRUD, CreateQueryBuilder, ReadQueryBuilder,
                      UpdateQueryBuilder)
from ..shared import authorization
from ..schemas import OrganisationSchema

__all__ = ['Organisation', 'OrganisationCollection']


class Organisation(SwaggerView):
    """
    /user/organisations/<int:org_id>
    """

    logger = logging.getLogger('apisrv')
    schema = OrganisationSchema()
    db = CRUD()

    @app.auth.login_required
    def get(self, org_id: int):
        """
        Get one single organisation.
        Get data about one single object (filtering parameter: org_id).

        * users have access to all organisations with any org-role (pers, emp, pl, adm)
        * app-admin can access all organisations

        ---
        operationId: organisation-id-get
        tags:
          - user
        parameters:
          - in: path
            name: org_id
            schema:
              type: integer
              example: 1
            required: true
            description: Filtering criterion
        responses:
          200:
            description: One single object (organisation)
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/OrganisationSchema'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          404:
            $ref: '#/components/responses/not_found'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.not_found_404.value
        usr = app.auth.current_user()

        attr_filter = {self.schema.pk_column: org_id}

        # Authorization: select org_id, check
        auth_ids = authorization.select_usr_org_role_id(self.db, 'organisation',
                                                        {'org_id': org_id})
        if not usr.usr_app_admin:
            roles = ['org-emp', 'org-pers', 'org-pl', 'org-admin']
            authorization.check_organisation_roles_or(roles,
                                                      auth_ids['org_id'][0], usr)

        try:
            qb = ReadQueryBuilder()
            qb.select_sql = sql.SQL(
                'SELECT org.*, '
                'CASE WHEN '
                '(SELECT uog.uog_role_code '
                'FROM usr.user_organisation uog '
                "WHERE uog.uog_org_id = org.org_id and uog.uog_role_code = 'ros-oprs'"
                ') IS NOT NULL THEN TRUE '
                'ELSE FALSE '
                'END org_pers')
            qb.from_sql = sql.SQL(
                'FROM usr.organisation org')
            qb.build_where_sql(filter_equal=attr_filter)

            query, values = qb.read_query()
            message, code = self.db.read(query, values)
            if code != 200:
                message = jsonify(message)
            else:
                message = self.schema.jsonify(message[0], many=False)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(message, code)

    @app.auth.login_required
    def patch(self, org_id: int):
        """
        Update one single organisation.
        Update data of one single object (filtering parameter: org_id).
        Only valid attributes can be updated.

        Only organisation-admins can update their organisation.
        ---
        operationId: organisation-id-patch
        tags:
          - user
        parameters:
          - in: path
            name: org_id
            schema:
              type: integer
              example: 1
            required: true
            description: Filtering criterion
        requestBody:
          required: true
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OrganisationSchema'
              example: {"org_desc": "Test"}
        responses:
          204:
            description: No content, updating the object was successful.
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          404:
            $ref: '#/components/responses/not_found'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.not_found_404.value
        usr = app.auth.current_user()

        # Get the JSON from the request body and validate the data
        request_data = request.get_json()
        request_params = self.schema.parse_value_param(request_data, partial=True)
        attr_values = request_params.values

        attr_filter = {self.schema.pk_column: org_id}  # for selection

        # Authorization: select org_id, check
        auth_ids = authorization.select_usr_org_role_id(self.db, 'organisation',
                                                        {'org_id': org_id})
        authorization.check_organisation_role('org-admin', auth_ids['org_id'][0], usr)

        try:
            attr_values['org_change_user'] = usr.usr_id

            qb = UpdateQueryBuilder()
            qb.update_sql = sql.SQL('UPDATE usr.organisation')
            qb.build_set_sql(attr_values)
            qb.build_where_sql(attr_filter, filter_in={'org_id': usr.usr_org_admin})

            query, values = qb.update_query()

            message, code = self.db.update(query, values)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(jsonify(message), code)

    @app.auth.login_required
    def delete(self, org_id: int):
        """
        Delete one single organisation and all related roles (user_organisation).

        Only organisation-admins can delete their organisation.
        ---
        operationId: organisation-id-delete
        tags:
          - user
        parameters:
          - in: path
            name: org_id
            schema:
              type: integer
              example: 1
            required: true
            description: Filtering criterion
        responses:
          200:
            $ref: '#/components/responses/delete_ok'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          404:
            $ref: '#/components/responses/not_found'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.not_found_404.value
        usr = app.auth.current_user()

        # Authorization: select org_id, check
        auth_ids = authorization.select_usr_org_role_id(self.db, 'organisation',
                                                        {'org_id': org_id})
        authorization.check_organisation_role('org-admin', auth_ids['org_id'][0], usr)

        try:
            query = sql.SQL(
                'DELETE FROM usr.user_organisation WHERE uog_org_id={} AND uog_org_id=ANY({}); '
                'DELETE FROM usr.organisation WHERE org_id={} AND org_id=ANY({});'
            ).format(sql.Placeholder(), sql.Placeholder(), sql.Placeholder(), sql.Placeholder())
            values = [org_id, usr.usr_org_admin, org_id, usr.usr_org_admin]

            message, code = self.db.delete(query, values)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(jsonify(message), code)


class OrganisationCollection(SwaggerView):
    """
    /user/organisations
    """

    logger = logging.getLogger('apisrv')
    schema = OrganisationSchema()
    db = CRUD()

    @app.auth.login_required
    def get(self):
        """
        Get all organisations.
        Get a list of all objects.

        * users see all organisations with org-admin
        * application admin can access all organisations (also personal orgs)
        * no app-admin or no org-admin: Unauthorized
        ---
        operationId: organisation-get
        tags:
          - user
        responses:
          200:
            description: Multiple objects (organisation)
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/OrganisationSchema'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.bad_request_400.value
        usr = app.auth.current_user()

        request_data = dict(request.args)  # Arguments from URL

        # Valid Parameter: attr_filter
        request_params = self.schema.parse_filter_param(request_data, attr_filter=True)

        attr_filter = request_params.values

        # Authorization in the where-sql for the org-admins
        if not usr.usr_app_admin:
            if not usr.usr_org_admin:
                # no app-admin and no org-admin
                err = app.config['DEFAULT_MESSAGES']['AUTH_ORG']
                return make_response(jsonify(err), err.code)

        try:
            qb = ReadQueryBuilder()

            qb.select_sql = sql.SQL(
                "SELECT org.*, "
                "CASE WHEN "
                "(SELECT uog.uog_role_code "
                "FROM usr.user_organisation uog "
                "WHERE uog.uog_org_id = org.org_id and uog.uog_role_code = 'ros-oprs'"
                ") IS NOT NULL THEN TRUE "
                "ELSE FALSE "
                "END org_pers"
            )
            qb.from_sql = sql.SQL(
                'FROM usr.organisation org')
            if usr.usr_app_admin:
                qb.build_where_sql(filter_equal=attr_filter)
            else:
                qb.build_where_sql(filter_equal=attr_filter,
                                   filter_in={'org_id': usr.usr_org_admin})
            qb.order_sql = sql.SQL(
                'ORDER BY org_id')

            query, values = qb.read_query()

            # Read organisations
            message, code = self.db.read(query, values, many=True)

            if code != 200:
                message = jsonify(message)
            else:
                message = self.schema.jsonify(message, many=True)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(message, code)

    # only app-admins use this endpoint. Maybe an organisation is created on registering
    @app.auth.login_required
    @authorization.application_role_required('app-admin')
    def post(self):
        """
        Create a new organisation.
        Creating a new object will automatically produce a new id. The attributes must be valid.

        Only app-admins can post a new organisation. Roles are NOT created automatically.
        ---
        operationId: organisation-post
        tags:
          - user
        requestBody:
          required: true
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OrganisationSchema'
              example: {"org_name": "Test1"}
        responses:
          201:
            description: Created.
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/ID'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.bad_request_400.value
        usr = app.auth.current_user()

        # Get the JSON from the request body and validate the data
        request_data = request.get_json()
        request_params = self.schema.parse_value_param(request_data, partial=('org_id',))
        values = request_params.values

        try:  # Insert One Object
            # Todo: Assign roles to the organisation?
            values['org_create_user'] = usr.usr_id

            qb = CreateQueryBuilder()
            # INSERT INTO usr.organisation (col1, col2, ...)
            qb.build_insert_sql(sql.SQL('usr.organisation'), values)
            # VALUES (Placeholder1, Placeholder2, ...)
            qb.build_values_sql(values)
            qb.returning_sql = sql.SQL('RETURNING org_id')

            query, values = qb.create_query()

            message, code = self.db.create(query, 'org_id', values)
            message = jsonify(message)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(message, code)
