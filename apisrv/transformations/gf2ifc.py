
import logging
import time
import re

import pandas as pd 
from io import TextIOWrapper
from .reader.dxf import DXFReader
from .reader.obj import OBJReader
from .reader.stl import STLReader
from .reader.ts import TSReader

from io import BytesIO

from .common import *
from ..ifc import V40IfcWriter, V41IfcWriter, V43IfcWriter


__all__ = ['gfCfgFileNames', 'gf2ifc']


GF_DF_NAME = 'GeologicFeature'

GF_ID_CFG_KEY = 'GeoID'
GF_FILE_CFG_KEY = 'GeoFileName'
# GF_BASE_TYPE_CFG_KEY .. GF_DESC_CFG_KEY from common
GF_R_CFG_KEY = 'R'
GF_G_CFG_KEY = 'G'
GF_B_CFG_KEY = 'B'
GF_CFG_SET = {
    GF_ID_CFG_KEY, GF_FILE_CFG_KEY, 
    GF_BASE_TYPE_CFG_KEY, GF_PERSPECTIVE_CFG_KEY, GF_SUB_TYPE_CFG_KEY, GF_CLASS_CFG_KEY, 
    GF_OBS_MTH_CFG_KEY, GF_OBS_MTH_GEO_CFG_KEY, GF_PURPOSE_CFG_KEY, GF_DESC_CFG_KEY,
    GF_R_CFG_KEY, GF_G_CFG_KEY, GF_B_CFG_KEY
}

def colorFromXlsx(row, rowNo, rKey, gKey, bKey, warnings):
    def readOne(key):
        val = row[key]
        if pd.isna(val):
            warnings.append(GroupableError('Im Excel Blatt {} ist in Zeile {} für {} kein Wert angegeben.', GF_DF_NAME, rowNo, key))
            return 0
        if re.sub('.0$', '', str(val)) != str(int(val)):
            warnings.append(GroupableError('Im Excel Blatt {} ist in Zeile {} für {} kein ganzzahliger Wert angegeben sondern {}. {} wird angenommen.', GF_DF_NAME, rowNo, key, val, int(val)))
        val = int(val)
        if val < 0:
            warnings.append(GroupableError('Im Excel Blatt {} ist in Zeile {} für {} ein zu kleiner Wert {} angegeben.', GF_DF_NAME, rowNo, key, val))
            return 0
        if 255 < val:
            warnings.append(GroupableError('Im Excel Blatt {} ist in Zeile {} für {} ein zu grosser Wert {} (> 255) angegeben.', GF_DF_NAME, rowNo, key, val))
            return 0
        return val
    return [readOne(rKey),readOne(gKey),readOne(bKey)]

def writeGeometryToProxy(ifcw, dta, cartesianPointList, triangulatedFaceSet, filename, rgb):
    
    ifcw.nttCartesianPointList3D(tuple(cartesianPointList))
    ifcw.nttTriangulatedFaceSet(ifcw.lastCartesianPointList3DId, '$', None, tuple(triangulatedFaceSet), '$')
    faceSetId = ifcw.lastTriangulatedFaceSetId

    # Colour geometries
    ifcw.nttColourRgb(None,rgb[0],rgb[1],rgb[2])
    ifcw.nttSurfaceStyleShading(ifcw.lastColourRgbId, None)
    ifcw.nttSurfaceStyle(None, 'BOTH', '(#{})'.format(ifcw.lastSurfaceStyleShadingId))
    ifcw.nttStyledItem(faceSetId, '(#{})'.format(ifcw.lastSurfaceStyleId), None)

    ifcw.nttShapeRepresentation(ifcw.contextId, 'Body', 'SweptSolid', [faceSetId])
    ifcw.nttProductDefinitionShape(None, None, [ifcw.lastShapeRepresentationId])
    productId = ifcw.lastProductDefinitionShapeId

    spacialPlacementId = ifcw.externalSpatialElementPlacementId
    ifcw.ncpLocalPlacement(relativeToId=spacialPlacementId)
    placementId = ifcw.lastLocalPlacementId

    thisId = createGf(ifcw, dta.name, dta.desc, dta.baseType, dta.perspective, dta.objType, dta.tag, placementId, productId)

    createIfcProperties(dta.pts, ifcw, [thisId])

    return thisId

def gfCfgFileNames(xlsxStream):
    return cfgFileNames(xlsxStream, GF_DF_NAME, GF_FILE_CFG_KEY)

def gf2ifc(ifcw, origShift, xlsxStream, filesDic):

    logger = logging.getLogger('apisrv')

    logger.info('Converting units and structures data to IFC.')
    startTime = time.time()

    dfGf = pd.read_excel(xlsxStream,sheet_name=GF_DF_NAME, skiprows=[0,2],converters={GF_ID_CFG_KEY:str})

    pty2ptySetName = readPtyToPtySetName(xlsxStream, GF_DF_NAME, 13) # does no validation

    # #################################
    # Do various validations and checks

    # It is not too inefficient to open the same project config again, since it is a small tab.
    dfProMeta = pd.read_excel(xlsxStream,sheet_name=PRO_DF_NAME, index_col=None, header=None)
    proCfg = readProjectConfig(dfProMeta) # does also validate

    grantColumnsPresent(GF_DF_NAME, dfGf, GF_CFG_SET)
    
    grantColumnValuesUnique(GF_DF_NAME, dfGf, GF_FILE_CFG_KEY)
    for i, row in dfGf.iterrows():
        id = row[GF_ID_CFG_KEY]
        fileName = row[GF_FILE_CFG_KEY]
        if not fileName in filesDic:
            raise ValueError(f"""Die im Excel für GeologicFeature {id} angegebene Datei {fileName} ist nicht vorhanden.""")

    grantColumnValuesNumericMany(GF_DF_NAME, dfGf, {GF_R_CFG_KEY, GF_G_CFG_KEY, GF_B_CFG_KEY}, rowNamingOffset=4)

    # ######################
    # Start writing the file

    assemblyName = proCfg[PRO_NAME_CFG_KEY] # It is not correct, but we do not have any other source for the name.
    assemblyId = createAssemblyWithProps(ifcw, assemblyName, '3DModel', proCfg)

    gfEltIds = []
    # Read each feature file
    for i, row in dfGf.iterrows():
        warns = []
        id = row[GF_ID_CFG_KEY]
        fileName = row[GF_FILE_CFG_KEY]
        rgb = colorFromXlsx(row, i+4, GF_R_CFG_KEY, GF_G_CFG_KEY, GF_B_CFG_KEY, warns)

        dta = GfDta()
        dta.setRow(row, i+4, pty2ptySetName) # does assignGfProps
        warns += dta.warnings

        # comment warnings of this GF
        if hasErrorsSoHandle(ifcw, dta.errors,
                GroupableError('Im Excel Blatt {} in Zeile {} wurde Geologic Feature "{}" ignoriert, weil {} Fehler bestanden.', GF_DF_NAME, i+4, id, len(dta.errors)), True
            ): continue
        hasWarningsSoHandle(ifcw, warns)

        # Derive reader type from file extension.
        lfn = fileName.lower()
        if lfn.endswith('.stl'):
            rdr = STLReader()
        elif lfn.endswith('.dxf'):
            rdr = DXFReader()
        elif lfn.endswith('.obj'):
            rdr = OBJReader()
        elif lfn.endswith('.ts'):
            rdr = TSReader()
        else:
            # TODO do that earlier already on upload
            # warns.append(GroupableError(f'', GF_DF_NAME, GF_FILE_CFG_KEY, i+2, id, fileName))
            continue

        try:
            bio = BytesIO(filesDic[fileName])
            strm = TextIOWrapper(bio) # , 'utf-8'
            origVertices, faces = rdr.read(strm) # cartesianPointList, triangulatedFaceSet
        except:
            raise ValueError(f'Fehler beim Lesen des Geologic Feature "{id}" aus der Datei {fileName}')

        if origShift[0] != 0.0 or origShift[1] != 0.0 or origShift[2] != 0.0:
            vertices = []
            for vtx in origVertices:
                vertices.append((vtx[0]-origShift[0],vtx[1]-origShift[1],vtx[2]-origShift[2]))
        else:
            vertices = origVertices

        newId = writeGeometryToProxy(ifcw, dta, vertices, faces, fileName, rgb)
        gfEltIds.append(newId)

    ifcw.nttRelAggregates(ifcw.createGuid(), ifcw.ownerHistoryId, 'Assembly Attachement', None, assemblyId, gfEltIds)
    containerId = ifcw.externalSpatialElementId
    ifcw.nttRelAggregates(ifcw.createGuid(), ifcw.ownerHistoryId, 'Spatial Attachement', None, containerId, [assemblyId])
            
    logger.info('Successfully done!')

    # time duration to calculate segments
    def timecalc(calculatedSeconds):
        secondsInDay = 60 * 60 * 24
        secondsInHour = 60 * 60
        secondsInMinute = 60
        days = calculatedSeconds // secondsInDay
        hours = (calculatedSeconds - (days * secondsInDay)) // secondsInHour
        minutes = (calculatedSeconds - (days * secondsInDay) - (hours * secondsInHour)) // secondsInMinute
        return(minutes)
    calcSecs = time.time() - startTime
    logger.info('The script finished in {} minutes'.format(timecalc(calcSecs)))

