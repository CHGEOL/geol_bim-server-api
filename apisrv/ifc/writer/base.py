
from distutils.file_util import write_file
import uuid
import string
import time
from math import radians, sin, cos
from abc import ABC, abstractmethod
import json
from urllib.parse import unquote, urlencode  # Send/Receive swisstopo-REST
from urllib.request import urlopen

import pandas as pd # TODO only one occurance - try to avoid import

CREATE_GUID_CHARS = string.digits + string.ascii_uppercase + string.ascii_lowercase + "_$"
APP_VERSION = '0.1'
ZERO_LOCATION = (0., 0., 0.)
X_LOCATION = (1., 0., 0.)  # Direction of X-Axis
Y_LOCATION = (0., 1., 0.)  # Direction of Y-Axis, for True North
Z_LOCATION = (0., 0., 1.)  # Direction of Z-Axis

class IfcWriter(ABC):

    def __init__(self):
        self.strs = []
        self.projectGuid = IfcWriter.createGuid()
        self.commenting = False

    @abstractmethod
    def getVersion(self):
        """Abstract method for getting the IFC version"""

    def beginning(self, filename, author, organization, description, 
            projectName, projectDescription, phase,
            epsgCode,
            authorization='', georeferencing='', origin=ZERO_LOCATION, rotation=0., appName='GEOLBIM Transformation Library', appVariant=''):
        self.grantNotCommenting()

        locZero = ZERO_LOCATION
        locX = X_LOCATION
        locY = Y_LOCATION
        tm = time.time()
        timeStr = time.strftime("%Y-%m-%dT%H:%M:%S", time.localtime(tm))  # yyyy-mm-ddThh:mm:ss
        appVersion = APP_VERSION
        xDirection = IfcWriter.rotation2xdir(rotation)
        trueNorth = IfcWriter.rotation2ydir(rotation)
        proGuid = self.projectGuid
        schemaVersion = self.getVersion() # 'IFC'+self.getVersion().replace('.0', '').replace('.', 'x')

        str = f"""ISO-10303-21;
HEADER;
FILE_DESCRIPTION(
/* description */ ('{description}'),
/* implementation_level */ '2;1');
FILE_NAME(
/* name */ '{filename}',
/* time_stamp */ '{timeStr}',
/* author */ ('{author}'),
/* organization */ ('{organization}'),
/* preprocessor_version */ '{appName}{appVariant}',
/* originating_system */ '{appName}',
/* authorization */ '{authorization}');
FILE_SCHEMA(('{schemaVersion}'));
ENDSEC;
"""
        # do all the encoding
        author = IfcWriter.encStr(author)
        organization = IfcWriter.encStr(organization)
        appVersion = IfcWriter.encStr(appVersion)
        appName = IfcWriter.encStr(appName)
        appVariant = IfcWriter.encStr(appVariant)
        projectName = IfcWriter.encStr(projectName)
        projectDescription = IfcWriter.encStr(projectDescription)
        phase = IfcWriter.encStr(phase)

        self.strs.append(str)
        str = f"""DATA;
#1=IFCPERSON($,$,'{author}',$,$,$,$,$);
#2=IFCORGANIZATION($,'{organization}',$,$,$);
#3=IFCPERSONANDORGANIZATION(#1,#2,$);
#4=IFCAPPLICATION(#2,'{appVersion}','{appName}{appVariant}','');
#5=IFCOWNERHISTORY(#3,#4,$,.ADDED.,{int(tm)},#3,#4,{int(tm)});
#6=IFCDIMENSIONALEXPONENTS(0,0,0,0,0,0,0);
#7=IFCSIUNIT(*,.LENGTHUNIT.,$,.METRE.);
#8=IFCSIUNIT(*,.AREAUNIT.,$,.SQUARE_METRE.);
#9=IFCSIUNIT(*,.VOLUMEUNIT.,$,.CUBIC_METRE.);
#10=IFCSIUNIT(*,.PLANEANGLEUNIT.,$,.RADIAN.);
#11=IFCMEASUREWITHUNIT(IFCPLANEANGLEMEASURE(0.017453292519943295),#10);
#12=IFCCONVERSIONBASEDUNIT(#6,.PLANEANGLEUNIT.,'DEGREE',#11);
#13=IFCUNITASSIGNMENT((#7,#8,#9,#12));
"""
        self.strs.append(str)
        self.ownerHistoryId = 5

        if georeferencing == 'IfcGeometricRepresentationContext':
            # Georeferencing (Translation, Rotation with the placement of the GeometricRepresentationContext
            # If no georef needed, x_direction=X, origin=O, true_north=Z[0:2]
            str = f"""#14=IFCDIRECTION({xDirection});
#15=IFCDIRECTION((0.,0.,1.));
#16=IFCCARTESIANPOINT({origin});
#17=IFCAXIS2PLACEMENT3D(#16,#15,#14);
#18=IFCDIRECTION({trueNorth[0:2]});
#19=IFCGEOMETRICREPRESENTATIONCONTEXT($,'Model',3,1.E-05,#17,#18);
#20=IFCPROJECT('{proGuid}',#5,'{projectName}','{projectDescription}',$,$,'{phase}',(#19),#13);
"""
            self.liNo = 21
        elif georeferencing == 'IfcMapConversion':
            str = f"""#14=IFCDIRECTION({locX});
#15=IFCDIRECTION((0.,0.,1.));
#16=IFCCARTESIANPOINT({locZero});
#17=IFCAXIS2PLACEMENT3D(#16,#15,#14);
#18=IFCDIRECTION({trueNorth[0:2]});
#19=IFCGEOMETRICREPRESENTATIONCONTEXT($,'Model',3,1.E-05,#17,#18);
#20=IFCPROJECT('{proGuid}',#5,'{projectName}','{projectDescription}',$,$,'{phase}',(#19),#13);
#21=IFCPROJECTEDCRS('EPSG:{epsgCode}','CH1903+ / LV95 -- Swiss CH1903+ / LV95','CH1903+','LN02','CH1903+ / LV95',$,#7);
#22=IFCMAPCONVERSION(#19,#21,{origin[0]},{origin[1]},{origin[2]}, {xDirection[0]},{xDirection[1]},$);
""" # TODO grant IFCMAPCONVERSION origin are real literals not just numbers -> append .0
            self.liNo = 23
        else:
            str = f"""#14=IFCDIRECTION({locX});
#15=IFCDIRECTION((0.,0.,1.));
#16=IFCCARTESIANPOINT({locZero});
#17=IFCAXIS2PLACEMENT3D(#16,#15,#14);
#18=IFCDIRECTION({locY[0:2]});
#19=IFCGEOMETRICREPRESENTATIONCONTEXT($,'Model',3,1.E-05,#17,#18);
#20=IFCPROJECT('{proGuid}',#5,'{projectName}','{projectDescription}',$,$,'{phase}',(#19),#13);
"""
            self.liNo = 21
        self.strs.append(str)
        self.contextId = 19
        self.projectId = 20

    def ncpSite(self, siteName, georeferencing='', origin=ZERO_LOCATION, rotation=0.):
        self.grantNotCommenting()

        # In our case georeferencing is either IfcMapConversion or IfcGeometricRepresentationContext - but never IfcSite

        try:
            lat, lng = IfcWriter.lv95towgs84(origin[0], origin[1])
        except URLError:
            lat = '$'
            lng = '$'

        elevation = origin[2]

        histId = self.ownerHistoryId
        proId = self.projectId
        ctxId = self.contextId
        i = self.liNo

        str = f"""#{i}=IFCCARTESIANPOINT((0.,0.,0.));
#{i+1}=IFCGEOMETRICCURVESET((#{i}));
#{i+2}=IFCSHAPEREPRESENTATION(#{ctxId},'ProductBasePoint','GeometricCurveSet',(#{i+1}));
#{i+3}=IFCPRODUCTDEFINITIONSHAPE('Georeferenzierung',$,(#{i+2}));
"""
        self.strs.append(str)
        prodShapeId = i+3
        i = i + 4
        self.liNo = i

        # Site placement, all other entities should be relative to the site placement
        if georeferencing == 'IfcSite':  # Do georeferencing with IfcSite
            self.ncpLocalPlacement(point=origin, dirZ=Z_LOCATION, dirX=IfcWriter.rotation2xdir(rotation))
        else:  # No Georef or Done with IfcMapConversion OR IfcGeometricRepresentationContext
            self.ncpLocalPlacement()
        plmtId = self.lastLocalPlacementId
        self.sitePlacementId = plmtId

        siteGuid = IfcWriter.createGuid()
        relAggrGuid = IfcWriter.createGuid()

        i = self.liNo

        # The lat, lng are quadrupls like e.g. (8, 57, 58, 370064), (47, 37, 59, 71019)
        str = f"""#{i}=IFCSITE('{siteGuid}',#{histId},'{IfcWriter.encStr(siteName)}','ProjectBasePoint = SurveyPoint',$,#{plmtId},#{prodShapeId},$,.ELEMENT.,{lat},{lng},{elevation},$,$);
#{i+1}=IFCRELAGGREGATES('{relAggrGuid}',#{histId},'Project Contianment',$,#{proId},(#{i}));
"""
        self.siteId = i
        self.strs.append(str)
        i = i + 2

        self.liNo = i

    def ncpExternalSpatialElement(self, name, predefinedType):
        self.grantNotCommenting()

        # EXTERNAL, EXTERNAL_EARTH, EXTERNAL_FIRE, EXTERNAL_WATER, NOTDEFINED, USERDEFINED
        predefType = IfcWriter.tval(predefinedType) # IfcExternalSpatialElement

        histId = self.ownerHistoryId
        proId = self.projectId
        ctxId = self.contextId
        i = self.liNo

        str = f"""#{i}=IFCCARTESIANPOINT((0.,0.,0.));
#{i+1}=IFCGEOMETRICCURVESET((#{i}));
#{i+2}=IFCSHAPEREPRESENTATION(#{ctxId},'ProductBasePoint','GeometricCurveSet',(#{i+1}));
#{i+3}=IFCPRODUCTDEFINITIONSHAPE('Georeferenzierung',$,(#{i+2}));
"""
        self.strs.append(str)
        prodShapeId = i+3
        i = i + 4
        self.liNo = i

        self.ncpLocalPlacement()
        plmtId = self.lastLocalPlacementId
        self.externalSpatialElementPlacementId = plmtId

        guid = IfcWriter.createGuid()
        relAggrGuid = IfcWriter.createGuid()

        i = self.liNo

        str = f"""#{i}=IFCEXTERNALSPATIALELEMENT('{guid}',#{histId},'{IfcWriter.encStr(name)}','ProjectBasePoint = SurveyPoint',$,#{plmtId},#{prodShapeId},$,{predefType});
#{i+1}=IFCRELAGGREGATES('{relAggrGuid}',#{histId},'Project Containment',$,#{proId},(#{i}));
"""
        self.externalSpatialElementId = i
        self.strs.append(str)
        i = i + 2

        self.liNo = i

    def ncpLocalPlacement(self, point=ZERO_LOCATION, dirZ=Z_LOCATION, dirX=X_LOCATION, relativeToId=None):

        self.ncpAxis2placement3D(point, dirZ, dirX)
        axisId = self.lastAxis2placement3DId

        relTo = '$'
        if relativeToId is not None:
            relTo = f"""#{relativeToId}"""
        
        i = self.liNo

        str = f"""#{i}=IFCLOCALPLACEMENT({relTo},#{axisId});
"""
        self.lastLocalPlacementId = i
        self.strs.append(str)
        i = i + 1

        self.liNo = i

    def ncpAxis2placement3D(self, point, dirZ, dirX):
        self.lastAxis2placement3DId = self._ncpAxis2placement('IFCAXIS2PLACEMENT3D', point, dirZ, dirX)

    def nttCartesianPointList2D(self, points):
        self.lastCartesianPointList2DId = self._writeOneLinerOneArg('IFCCARTESIANPOINTLIST2D', points)

    def nttCartesianPointList3D(self, points):
        self.lastCartesianPointList3DId = self._writeOneLinerOneArg('IFCCARTESIANPOINTLIST3D', points)

    def nttCartesianPoint(self, point):
        self.lastCartesianPointId = self._writeOneLinerOneArg('IFCCARTESIANPOINT', point)

    def nttPolyline(self, pointIds):
        pntIds = IfcWriter.ids(pointIds)
        self.lastPolylineId = self._writeOneLinerOneArg('IFCPOLYLINE', pntIds)

    def ncpPolyline(self, points):
        pointIds = []
        for point in points:
            self.nttCartesianPoint(point)
            pointIds.append(self.lastCartesianPointId)
        self.nttPolyline(pointIds)
    
    def nttPolyLoop(self, pointIds):
        pntIds = IfcWriter.ids(pointIds)
        self.lastPolyLoopId = self._writeOneLinerOneArg('IFCPOLYLOOP', pntIds)

    def nttFaceOuterBound(self, polyLoopId, orientation):
        plId = IfcWriter.id(polyLoopId)
        ori = IfcWriter.bool(orientation)
        self.lastFaceOuterBoundId = self._writeOneLinerOneArg(
            'IFCFACEOUTERBOUND', 
            f"""{plId},{ori}"""
        )

    def nttFace(self, faceBoundIds):
        bndIds = IfcWriter.ids(faceBoundIds)
        self.lastFaceId = self._writeOneLinerOneArg('IFCFACE', bndIds)

    def nttClosedShell(self, faceIds):
        fIds = IfcWriter.ids(faceIds)
        self.lastClosedShellId = self._writeOneLinerOneArg('IFCCLOSEDSHELL', fIds)

    def nttFacetedBrep(self, outerId):
        oId = IfcWriter.id(outerId)
        self.lastFacetedBrepId = self._writeOneLinerOneArg('IFCFACETEDBREP', oId)

    def tArcIndex(self, point):
        return f"""IFCARCINDEX({point})"""

    def nttIndexedPolyCurve(self, cartesianPointListId, segments, selfIntersect=False):
        cplId = IfcWriter.id(cartesianPointListId)
        segs = IfcWriter.types(segments)
        selfIsec = IfcWriter.bool(selfIntersect)
        self.lastIndexedPolyCurveId = self._writeOneLinerOneArg(
            'IFCINDEXEDPOLYCURVE', 
            f"""{cplId},{segs},{selfIsec}"""
        )

    def nttArbitraryClosedProfileDef(self, profileType, profileName, outerCurveId):
        proType = IfcWriter.tval(profileType)
        proName = IfcWriter.str(profileName)
        ocId = IfcWriter.id(outerCurveId)
        self.lastArbitraryClosedProfileDefId = self._writeOneLinerOneArg(
            'IFCARBITRARYCLOSEDPROFILEDEF', 
            f"""{proType},{proName},{ocId}"""
        )

    def nttDirection(self, point):
        self.lastDirectionId = self._writeOneLinerOneArg('IFCDIRECTION', point)

    def nttExtrudedAreaSolid(self, sweptAreaId, positionId, extrudedDirectionId, depth):
        saId = IfcWriter.id(sweptAreaId)
        posId = IfcWriter.id(positionId)
        eDirId = IfcWriter.id(extrudedDirectionId)
        self.lastExtrudedAreaSolidId = self._writeOneLinerOneArg(
            'IFCEXTRUDEDAREASOLID', 
            f"""{saId},{posId},{eDirId},{depth}"""
        )

    def nttShapeRepresentation(self, contextOfItemsId, representationIdentifier, representationType, itemIds):
        # See ENTITY IfcRepresentation for args.
        ctxId = IfcWriter.id(contextOfItemsId)
        repIdent = IfcWriter.str(representationIdentifier)
        repTypeName = IfcWriter.str(representationType)
        itIds = IfcWriter.ids(itemIds)
        self.lastShapeRepresentationId = self._writeOneLinerOneArg(
            'IFCSHAPEREPRESENTATION', 
            f"""{ctxId},{repIdent},{repTypeName},{itIds}"""
        )

    def nttProductDefinitionShape(self, name, description, representationIds):
        # See ENTITY IfcProductRepresentation for args.
        nm = IfcWriter.str(name)
        desc = IfcWriter.str(description)
        reprIds = IfcWriter.ids(representationIds)
        self.lastProductDefinitionShapeId = self._writeOneLinerOneArg(
            'IFCPRODUCTDEFINITIONSHAPE', 
            f"""{nm},{desc},{reprIds}"""
        )

    def nttSpace(self, globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, longName, compositionType , predefinedType, elevationWithFlooring):
        # See ENTITY IfcSpace, IfcSpatialStructureElement, IfcSpatialElement, IfcProduct, IfcObject and IfcRoot for args.
        guid = IfcWriter.str(globalUid) # IfcRoot
        histId = IfcWriter.id(ownerHistoryId) # IfcRoot
        nm = IfcWriter.str(name) # IfcRoot
        desc = IfcWriter.str(description) # IfcRoot
        objType = IfcWriter.str(objectType) # IfcObject
        objId = IfcWriter.id(objectPlacementId)  # IfcProduct
        reprId = IfcWriter.id(representationId) # IfcProduct
        longNm = IfcWriter.str(longName) # IfcSpatialElement
        compType = IfcWriter.tval(compositionType) # IfcSpatialStructureElement
        predefType = IfcWriter.tval(predefinedType) # IfcSpace
        elev = IfcWriter.num(elevationWithFlooring) # IfcSpace
        self.lastSpaceId = self._writeOneLinerOneArg(
            'IFCSPACE', 
            f"""{guid},{histId},{nm},{desc},{objType},{objId},{reprId},{longNm},{compType},{predefType},{elev}"""
        )

    def nttPropertySingleValue(self, name, specification, nominalValueX, unitId):
        # See ENTITY IfcPropertySingleValue, IfcProperty for args.
        nm = IfcWriter.str(name) # IfcProperty
        spec = IfcWriter.str(specification) # IfcProperty
        untId = IfcWriter.id(unitId) # IfcPropertySingleValue
        self.lastPropertySingleValueId = self._writeOneLinerOneArg(
            'IFCPROPERTYSINGLEVALUE', 
            f"""{nm},{spec},{nominalValueX},{untId}"""
        )

    def nttPropertySet(self, globalUid, ownerHistoryId, name, description, hasPropertyIds):
        # See ENTITY IfcPropertySet and IfcRoot for args.
        guid = IfcWriter.str(globalUid) # IfcRoot
        histId = IfcWriter.id(ownerHistoryId) # IfcRoot
        nm = IfcWriter.str(name) # IfcRoot
        desc = IfcWriter.str(description) # IfcRoot
        pptIds = IfcWriter.ids(hasPropertyIds) # IfcPropertySet
        self.lastPropertySetId = self._writeOneLinerOneArg(
            'IFCPROPERTYSET', 
            f"""{guid},{histId},{nm},{desc},{pptIds}"""
        )

    def nttRelDefinesByProperties(self, globalUid, ownerHistoryId, name, description, relatedObjectIds, relatingPropertyDefinitionId):
        # See ENTITY IfcRelDefinesByProperties and IfcRoot for args.
        guid = IfcWriter.str(globalUid) # IfcRoot
        histId = IfcWriter.id(ownerHistoryId) # IfcRoot
        nm = IfcWriter.str(name) # IfcRoot
        desc = IfcWriter.str(description) # IfcRoot
        relObjIds = IfcWriter.ids(relatedObjectIds) # IfcRelDefinesByProperties
        rpdId = IfcWriter.id(relatingPropertyDefinitionId) # IfcRelDefinesByProperties
        self.lastRelDefinesByPropertiesId = self._writeOneLinerOneArg(
            'IFCRELDEFINESBYPROPERTIES', 
            f"""{guid},{histId},{nm},{desc},{relObjIds},{rpdId}"""
        )
    
    def nttRelAggregates(self, globalUid, ownerHistoryId, name, description, relatingObjectId, relatedObjectIds):
        self.lastRelAggregatesId = self._nttRel('IFCRELAGGREGATES',
            globalUid, ownerHistoryId, name, description, relatingObjectId, relatedObjectIds)
    
    def nttRelDefinesByType(self, globalUid, ownerHistoryId, name, description, relatedObjectIds, relatingTypeId):
        # See ENTITY IfcRelDefinesByType or others and IfcRoot for args.
        # Order of args opposite to IfcRelAggregates !
        guid = IfcWriter.str(globalUid) # IfcRoot
        histId = IfcWriter.id(ownerHistoryId) # IfcRoot
        nm = IfcWriter.str(name) # IfcRoot
        desc = IfcWriter.str(description) # IfcRoot
        relObjIds = IfcWriter.ids(relatedObjectIds) # IfcRelDefinesByType
        relTypeId = IfcWriter.id(relatingTypeId) # IfcRelDefinesByType
        self.lastRelDefinesByTypeId = self._writeOneLinerOneArg(
            'IFCRELDEFINESBYTYPE', 
            f"""{guid},{histId},{nm},{desc},{relObjIds},{relTypeId}"""
        )
    
    def nttDistanceExpression(self, distanceAlong , offsetLateral , offsetVertical , offsetLongitudinal, alongHorizontal=False):
        # See ENTITY IfcDistanceExpression for args.
        distAlg = IfcWriter.num(distanceAlong)
        offLat = IfcWriter.num(offsetLateral)
        offVrt = IfcWriter.num(offsetVertical)
        offLng = IfcWriter.num(offsetLongitudinal)
        algH = IfcWriter.bool(alongHorizontal)
        self.lastDistanceExpressionId = self._writeOneLinerOneArg(
            'IFCDISTANCEEXPRESSION',
            f"""{distAlg},{offLat},{offVrt},{offLng},{algH}"""
        )

    def nttSectionedSolidHorizontal(self, directrixId, crossSectionIds, crossSectionPositionIds, fixedAxisVertical):
        # See ENTITY IfcSectionedSolidHorizontal and IfcSectionedSolid for args.
        dirxId = IfcWriter.id(directrixId) # IfcSectionedSolid
        csIds = IfcWriter.ids(crossSectionIds) # IfcSectionedSolid
        cspIds = IfcWriter.ids(crossSectionPositionIds) # IfcSectionedSolidHorizontal
        fAxis = IfcWriter.bool(fixedAxisVertical) # IfcSectionedSolidHorizontal, IFC 4.3 does no longer support fixedAxisVertical
        self.lastSectionedSolidHorizontalId = self._writeOneLinerOneArg(
            'IFCSECTIONEDSOLIDHORIZONTAL',
            f"""{dirxId},{csIds},{cspIds},{fAxis}"""
        )

    def nttExternalSpatialElement(self, globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, longName, predefinedType):
        self.lastExternalSpatialElementId = self._nttElementPlusPredefType('IFCEXTERNALSPATIALELEMENT', globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag, predefinedType)

    def nttElement(self, globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag):
        self.lastElementId = self._nttElement('IFCELEMENT', globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag)

    def nttGeographicElement(self, globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag, predefinedType):
        self.lastGeographicElementId = self._nttElementPlusPredefType('IFCGEOGRAPHICELEMENT', globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag, predefinedType)

    def nttRelContainedInSpatialStructure(self, globalUid, ownerHistoryId, name, description, relatedElementIds, relatingStructureId):
        # See ENTITY IfcRelContainedInSpatialStructure and IfcRoot for args.
        guid = IfcWriter.str(globalUid) # IfcRoot
        histId = IfcWriter.id(ownerHistoryId) # IfcRoot
        nm = IfcWriter.str(name) # IfcRoot
        desc = IfcWriter.str(description) # IfcRoot
        relEltIds = IfcWriter.ids(relatedElementIds) # IfcRelContainedInSpatialStructure
        relStructId = IfcWriter.id(relatingStructureId) # IfcRelContainedInSpatialStructure
        self.lastRelContainedInSpatialStructureId = self._writeOneLinerOneArg(
            'IFCRELCONTAINEDINSPATIALSTRUCTURE',
            f"""{guid},{histId},{nm},{desc},{relEltIds},{relStructId}"""
        )

    def nttColourRgb(self, name, red, green, blue):
        # See IfcColourRgb and IfcColourSpecification for args.
        nm = IfcWriter.str(name) # IfcColourSpecification
        r = IfcWriter.num(red) # IfcColourRgb
        g = IfcWriter.num(green) # IfcColourRgb
        b = IfcWriter.num(blue) # IfcColourRgb
        self.lastColourRgbId = self._writeOneLinerOneArg(
            'IFCCOLOURRGB',
            f"""{nm},{r},{g},{b}"""
        )

    def nttSurfaceStyleShading(self, surfaceColourId, transparency):
        colorId = IfcWriter.id(surfaceColourId)
        t = IfcWriter.num(transparency)
        self.lastSurfaceStyleShadingId = self._writeOneLinerOneArg(
            'IFCSURFACESTYLESHADING',
            f"""{colorId},{t}"""
        )

    def nttSurfaceStyle(self, name, side, styles):
        # See IfcSurfaceStyle and IfcPresentationStyle for args.
        nm = IfcWriter.str(name) # IfcPresentationStyle
        sd = IfcWriter.tval(side) # IfcSurfaceStyle
        self.lastSurfaceStyleId = self._writeOneLinerOneArg(
            'IFCSURFACESTYLE',
            f"""{nm},{sd},{styles}"""
        )

    def nttStyledItem(self, representationId, styles, name):
        reprId = IfcWriter.id(representationId)
        nm = IfcWriter.str(name)
        self.lastSurfaceStyleId = self._writeOneLinerOneArg(
            'IFCSTYLEDITEM',
            f"""{reprId},{styles},{nm}"""
        )

    def nttBuildingElementProxy(self, globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag, predefinedType):
        self.lastBuildingElementProxyId = self._nttElementPlusPredefType('IFCBUILDINGELEMENTPROXY', globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag, predefinedType)

    def nttPipeSegment(self, globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag, predefinedType):
        self.lastPipeSegmentId = self._nttElementPlusPredefType('IFCPIPESEGMENT', globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag, predefinedType)
       
    def nttTriangulatedFaceSet(self, coordinatesId, normals, closed, coordIndex, pnIndex):
        # See IfcTriangulatedFaceSet and IfcTessellatedFaceSet for args.
        crdsId = IfcWriter.id(coordinatesId) # IfcTessellatedFaceSet
        # normals IfcTriangulatedFaceSet
        clsd = IfcWriter.bool(closed) # IfcTriangulatedFaceSet
        # coordIndex IfcTriangulatedFaceSet
        # pnIndex IfcTriangulatedFaceSet
        self.lastTriangulatedFaceSetId = self._writeOneLinerOneArg(
            'IFCTRIANGULATEDFACESET',
            f"""{crdsId},{normals},{clsd},{coordIndex},{pnIndex}"""
        )

    def nttSystem(self, globalUid, ownerHistoryId, name, description, objectType):
        # See ENTITY IfcObject and IfcRoot for args.
        guid = IfcWriter.str(globalUid) # IfcRoot
        histId = IfcWriter.id(ownerHistoryId) # IfcRoot
        nm = IfcWriter.str(name) # IfcRoot
        desc = IfcWriter.str(description) # IfcRoot
        objType = IfcWriter.str(objectType) # IfcObject
        self.lastSystemId = self._writeOneLinerOneArg(
            'IFCSYSTEM', 
            f"""{guid},{histId},{nm},{desc},{objType}"""
        )

    def nttAnnotation(self, globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId):
        # See ENTITY IfcProduct, IfcObject and IfcRoot for args.
        guid = IfcWriter.str(globalUid) # IfcRoot
        histId = IfcWriter.id(ownerHistoryId) # IfcRoot
        nm = IfcWriter.str(name) # IfcRoot
        desc = IfcWriter.str(description) # IfcRoot
        objType = IfcWriter.str(objectType) # IfcObject
        objId = IfcWriter.id(objectPlacementId)  # IfcProduct
        reprId = IfcWriter.id(representationId) # IfcProduct
        self.lastAnnotationId = self._writeOneLinerOneArg(
            'IFCANNOTATION', 
            f"""{guid},{histId},{nm},{desc},{objType},{objId},{reprId}"""
        )

    def ending(self):
        self.grantNotCommenting()
        str = """ENDSEC;
END-ISO-10303-21;
"""
        self.strs.append(str)
        self.liNo = self.liNo + 2
    
    def content(self):
        return ''.join(self.strs)

    def _writeOneLinerOneArg(self, name, arg):
        self.grantNotCommenting()
        i = self.liNo
        str = f"""#{i}={name}({arg});
"""
        id = i
        self.strs.append(str)
        i = i + 1
        self.liNo = i
        return id

    def _nttElement(self, entityName, globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag):
        # See ENTITY IfcElement, IfcProduct, IfcObject and IfcRoot for args.
        guid = IfcWriter.str(globalUid) # IfcRoot
        histId = IfcWriter.id(ownerHistoryId) # IfcRoot
        nm = IfcWriter.str(name) # IfcRoot
        desc = IfcWriter.str(description) # IfcRoot
        objType = IfcWriter.str(objectType) # IfcObject
        objId = IfcWriter.id(objectPlacementId)  # IfcProduct
        reprId = IfcWriter.id(representationId) # IfcProduct
        tg = IfcWriter.str(tag) # IfcElement
        return self._writeOneLinerOneArg(
            entityName, 
            f"""{guid},{histId},{nm},{desc},{objType},{objId},{reprId},{tg}"""
        )

    def _nttElementPlusPredefType(self, entityName, globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag, predefinedType):
        # See ENTITY IfcElement, IfcProduct, IfcObject and IfcRoot for args.
        guid = IfcWriter.str(globalUid) # IfcRoot
        histId = IfcWriter.id(ownerHistoryId) # IfcRoot
        nm = IfcWriter.str(name) # IfcRoot
        desc = IfcWriter.str(description) # IfcRoot
        objType = IfcWriter.str(objectType) # IfcObject
        objId = IfcWriter.id(objectPlacementId)  # IfcProduct
        reprId = IfcWriter.id(representationId) # IfcProduct
        tg = IfcWriter.str(tag) # IfcElement
        predefType = IfcWriter.tval(predefinedType)
        return self._writeOneLinerOneArg(
            entityName, 
            f"""{guid},{histId},{nm},{desc},{objType},{objId},{reprId},{tg},{predefType}"""
        )

    def _nttRel(self, entityName, globalUid, ownerHistoryId, name, description, relatingObjectId, relatedObjectIds):
        # See ENTITY IfcRelAggregates or others and IfcRoot for args.
        guid = IfcWriter.str(globalUid) # IfcRoot
        histId = IfcWriter.id(ownerHistoryId) # IfcRoot
        nm = IfcWriter.str(name) # IfcRoot
        desc = IfcWriter.str(description) # IfcRoot
        relObjId = IfcWriter.id(relatingObjectId) # IfcRelAggregates
        relObjIds = IfcWriter.ids(relatedObjectIds) # IfcRelAggregates
        return self._writeOneLinerOneArg(
            entityName, 
            f"""{guid},{histId},{nm},{desc},{relObjId},{relObjIds}"""
        )

    def _ncpAxis2placement(self, entityName, point=ZERO_LOCATION, dirZ=Z_LOCATION, dirX=X_LOCATION):
        self.grantNotCommenting()
        # Code was written for IfcAxis2Placement3D, abstracted and also used for IfcAxis2PlacementLinear
        # Creates an IfcAxis2Placement3D from Location, Axis and RefDirection specified as Python tuples
        # :param ifcfile: Instance of ifcopenshell
        # :param point: tuple, (x,y,z), Location
        # :param dirZ: tuple, Direction of Z-axis
        # :param dirX: tuple, Direction of X-Axis
        # :return: IfcAxis2Placement3D

        # Convert integer coordinates to float
        point = tuple(float(i) for i in point)
        dirZ = tuple(float(i) for i in dirZ)
        dirX = tuple(float(i) for i in dirX)

        i = self.liNo

        str = f"""#{i}=IFCCARTESIANPOINT({point});
"""
        pntId = i
        self.strs.append(str)
        i = i + 1

        if dirZ == (0., 0., 1.) and dirX == (1., 0., 0.):  # creates less IfcCartesianPoints
            str = f"""#{i}={entityName}(#{pntId},$,$);
"""
            self.strs.append(str)
            resLineId = i
            i = i + 1
        else:
            str = f"""#{i}=IFCDIRECTION({dirZ});
#{i+1}=IFCDIRECTION({dirX});
#{i+2}={entityName}(#{pntId},#{i},#{i+1});
"""
            self.strs.append(str)
            resLineId = i+2
            i = i + 3

        self.liNo = i
        return resLineId

    def grantNotCommenting(self):
        if self.commenting:
            raise ValueError(f'Comment was started but tried to write content in line {self.liNo}.')

    def startComment(self, txt):
        if self.commenting:
            raise ValueError(f'Comment already started but tried to start comment again in line {self.liNo}.')
        self.commenting = True
        self.strs.append(f"""/* {txt}\n""")

    def inComment(self, txt):
        if not self.commenting:
            raise ValueError(f'Comment not started but tried to write comment "{txt}" in line {self.liNo}.')
        self.strs.append(f'{txt}\n')

    def endComment(self, txt):
        if not self.commenting:
            raise ValueError(f'Comment not started but tried to end comment in line {self.liNo}.')
        self.strs.append(f"""   {txt} */\n""")
        self.commenting = False

    @staticmethod
    def str(strOrNone):
        if not pd.isna(strOrNone):
            return '\'' + IfcWriter.encStr(strOrNone) + '\''
        else:
            return '$'

    @staticmethod
    def bool(boolOrNone):
        if boolOrNone is not None:
            return '.T.' if boolOrNone else '.F.'
        else:
            return '$'

    @staticmethod
    def num(numOrNone):
        if numOrNone is not None:
            return f"""{numOrNone}"""
        else:
            return '$'

    @staticmethod
    def tval(valOrNone):
        if valOrNone is not None:
            return '.' + valOrNone.upper() + '.'
        else:
            return '$'

    @staticmethod
    def id(idOrNone):
        if idOrNone is not None:
            return '#' + str(idOrNone)
        else:
            return '$'

    @staticmethod
    def ids(idListOrNone):
        if idListOrNone is not None:
            return '(' + ','.join(list(map(IfcWriter.id, idListOrNone))) + ')'
        else:
            return '$'

    @staticmethod
    def types(typeListOrNone):
        if typeListOrNone is not None:
            return '(' + ','.join(typeListOrNone) + ')'
        else:
            return '$'

    @staticmethod
    def encStr(str):
        if pd.isna(str):
            return None
        res = ''
        maxOrd = 0
        for ch in str:
            n = ord(ch)
            maxOrd = max(maxOrd, n)
        startStr = '\\X2\\'
        if 0xffff < maxOrd:
            startStr = '\\X4\\'
        isExt = False
        for ch in str:
            n = ord(ch)
            withinSpfRange = 0x20 <= n and n <= 0x7e
            if isExt and withinSpfRange:
                res += '\\X0\\'
            elif not isExt and not withinSpfRange:
                res += startStr
            if withinSpfRange:
                res += ch
                if ch == '\\' or ch == '\'':
                    res += ch
            else:
                res += "{0:0{1}X}".format(n,4) # 0x00B0 <- "{0:#0{1}x}"
            isExt = not withinSpfRange
        if isExt:
            res += '\\X0\\'
        return res

    @staticmethod
    def lv95towgs84(easting, northing):
        """
        Convert LV95 to WGS84 (Degrees, Minutes, Seconds, Millionthseconds)
        Send Request to Reframe-REST-API
        For georeferencing IfcSite (RefLatitude, RefLongitude
        :param easting: in LV95 (2714805)
        :param northing: in LV95 (1276941)
        :return: lon, lat; e.g. (8, 57, 58, 370064), (47, 37, 59, 71019)
        """
        params = {'easting': easting,
                'northing': northing,
                'format': 'json'}
        uri = 'http://geodesy.geo.admin.ch/reframe/lv95towgs84?' + unquote(urlencode(params))

        wgs84 = urlopen(uri)
        data = wgs84.read()
        wgs_84_dd = json.loads(data)
        lon_dms = IfcWriter.decdeg2dms(float(wgs_84_dd['easting']))
        lat_dms = IfcWriter.decdeg2dms(float(wgs_84_dd['northing']))

        return lat_dms, lon_dms

    @staticmethod
    def decdeg2dms(dd):
        """
        Conversion DecimalDegrees to DegreeMinuteSecondMillionthsecond
        For georeferencing IfcSite (RefLatitude, RefLongitude
        :param dd: Coordinate in DecimalDegree, e.g. 8.966213906595765
        :return: degrees, minutes, seconds, millionthseconds, e.g. (8, 57, 58, 370064)
        Adapted from:
        https://stackoverflow.com/questions/2579535/convert-dd-decimal-degrees-to-dms-degrees-minutes-seconds-in-python
        """
        # If degrees are negative (not the case in Switzerland)
        is_positive = dd >= 0
        dd = abs(dd)

        minutes, seconds = divmod(dd*3600, 60)
        seconds, millionthseconds = divmod(seconds*1000000, 1000000)
        degrees, minutes = divmod(minutes, 60)
        degrees = degrees if is_positive else -degrees
        if is_positive:
            return int(degrees), int(minutes), int(seconds), int(round(millionthseconds))
        else:
            return -int(degrees), -int(minutes), -int(seconds), -int(round(millionthseconds))

    @staticmethod
    def createGuid():
        g = uuid.uuid1().hex
        bs = [int(g[i : i + 2], 16) for i in range(0, len(g), 2)]
        def b64(v, l=4):
            return "".join([CREATE_GUID_CHARS[(v // (64 ** i)) % 64] for i in range(l)][::-1])
        return "".join([b64(bs[0], 2)] + [b64((bs[i] << 16) + (bs[i + 1] << 8) + bs[i + 2]) for i in range(1, 16, 3)])
    
    @staticmethod
    def rotation2xdir(rot, mode='deg'):
        """
        Creates a 3D coordinate-tuple of the X-direction
        :param rot: Rotation (counterclockwise)
        :param mode: 'deg' or 'rad'
        :return: Coordinate-tuple of x-direction, (1,0,0) for rot=0
        """
        if mode == 'deg':
            rotation = radians(rot)
        elif mode == 'rad':
            rotation = rot
        else:
            print("Rotation: in degrees ('deg') or radians ('rad')")
            raise ValueError
        # Cosmetics: if no rotation -> 1,0,0
        if rot == 0:
            xdirection = (1., 0., 0.)
        else:
            xdirection = (round(cos(rotation), 5), round(sin(rotation), 5), 0.)
        return xdirection

    @staticmethod
    def rotation2ydir(rot, mode='deg'):
        """
        Creates a 3D Coordinate-Tuple of the Y-Direction
        Only used to calculate True North (for Axis2Placement, X-/Z-Directions are used)
        :param rot: Rotation (counterclockwise)
        :param mode: 'deg' or 'rad'
        :return: Coordinate-tuple of Y-direction (1,0,0) for rot=0
        """
        if mode == 'deg':
            rotation = radians(rot)
        elif mode == 'rad':
            rotation = rot
        else:
            print("Rotation: in degrees ('deg') or radians ('rad')")
            raise ValueError
        # Cosmetics: if no rotation -> 0,1,0
        if rot == 0:
            tn = (0., 1., 0.)
        else:
            tn = (round(sin(rotation), 5), round(cos(rotation), 5), 0.)
        return tn

