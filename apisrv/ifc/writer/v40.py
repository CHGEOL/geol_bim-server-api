
from .base import IfcWriter

class V40IfcWriter(IfcWriter):

    def getVersion(self):
        return "IFC4"