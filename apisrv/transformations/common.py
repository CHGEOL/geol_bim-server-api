
from datetime import datetime
import pandas as pd # TODO only one occurance - try to avoid import
# from io import BytesIO

from apisrv.ifc.writer.base import IfcWriter 
from ..ifc import V40IfcWriter, V41IfcWriter, V43IfcWriter

NUMERIC_EXACT = 0.01 # allow this error whan compare equality or normal values (also coordinate height)
NUMERIC_EXACT_COORDS = 0.00001 # allow this error whan compare coordinate x or y (lat or long) values


PRO_DF_NAME = 'Projectdata_Metadata'
SUR_DF_NAME = 'SurveyPoints'

PRO_NAME_CFG_KEY = 'IfcProjectName'
PRO_DESC_CFG_KEY = 'IfcProjectDescription'
SITE_NAME_CFG_KEY = 'IfcSiteName'
PRO_ORIG_X_CFG_KEY = 'ProjectOriginXCoord'
PRO_ORIG_Y_CFG_KEY = 'ProjectOriginYCoord'
PRO_ORIG_Z_CFG_KEY = 'ProjectOriginZCoord'
LO_GEO_REF_CFG_KEY = 'LoGeoRef'
EPSG_CODE_CFG_KEY = 'EPSG-Code'
TRANS_COORD_CFG_KEY = 'ApplyTranslateCoord'
#ROTATION_CFG_KEY = 'Rotation'
IFC_VERSION_CFG_KEY = 'IfcVersion'
GEOMETRY_TYPE_CFG_KEY = 'GeometryType'

PURPOSE_CFG_KEY = 'Purpose'
PHASE_CFG_KEY = 'Phase'
VERSION_CFG_KEY = 'Version'
DELIVERY_PARTY_PERSON_CFG_KEY = 'DeliveryPartyPerson'
DELIVERY_PARTY_ORG_CFG_KEY = 'DeliveryPartyOrganisation'
DATE_KNOWLEDGE_CFG_KEY = 'DateKnowledge'

TYPE_CFG_KEY = 'Type'
DATE_MODEL_CFG_KEY = 'DateModel'
DATE_PROCESSING_CFG_KEY = 'DateProcessing'
CUSTOMER_CFG_KEY = 'Customer'
CONSULTANT_CFG_KEY = 'Consultant'
MODEL_VERSION_CFG_KEY = 'ModelVersion'
MODEL_TYPE_CFG_KEY = 'ModelType'
MODEL_SOFTWARE_CFG_KEY = 'ModelSoftware'

PRO_CFG_SET = {
    PRO_NAME_CFG_KEY, PRO_DESC_CFG_KEY, SITE_NAME_CFG_KEY, 
    PRO_ORIG_X_CFG_KEY, PRO_ORIG_Y_CFG_KEY, PRO_ORIG_Z_CFG_KEY, 
    LO_GEO_REF_CFG_KEY, EPSG_CODE_CFG_KEY, TRANS_COORD_CFG_KEY, 
    IFC_VERSION_CFG_KEY, GEOMETRY_TYPE_CFG_KEY,
    PURPOSE_CFG_KEY, PHASE_CFG_KEY, VERSION_CFG_KEY, DELIVERY_PARTY_PERSON_CFG_KEY, DELIVERY_PARTY_ORG_CFG_KEY, DATE_KNOWLEDGE_CFG_KEY,
    TYPE_CFG_KEY, DATE_MODEL_CFG_KEY, DATE_PROCESSING_CFG_KEY, CUSTOMER_CFG_KEY, CONSULTANT_CFG_KEY, MODEL_VERSION_CFG_KEY, MODEL_TYPE_CFG_KEY, MODEL_SOFTWARE_CFG_KEY
}
PRO_DATES_CFG_SET = {
    DATE_KNOWLEDGE_CFG_KEY, DATE_MODEL_CFG_KEY, DATE_PROCESSING_CFG_KEY
}

SUR_ID_CFG_KEY = 'SurveyPointID'
SUR_TYPE_CFG_KEY = 'Typ'
SUR_X_CFG_KEY = 'XCoord'
SUR_Y_CFG_KEY = 'YCoord'
SUR_Z_CFG_KEY = 'ZCoord'
SUR_CFG_SET = {
    SUR_ID_CFG_KEY, SUR_TYPE_CFG_KEY, SUR_X_CFG_KEY, SUR_Y_CFG_KEY, SUR_Z_CFG_KEY
}

GF_BASE_TYPE_CFG_KEY = 'BaseType'
GF_PERSPECTIVE_CFG_KEY = 'Perspective'
GF_SUB_TYPE_CFG_KEY = 'SubType'
GF_CLASS_CFG_KEY = 'Classifier'
GF_OBS_MTH_CFG_KEY = 'ObservationMethod'
GF_OBS_MTH_GEO_CFG_KEY = 'ObservationMethodGeometry'
GF_PURPOSE_CFG_KEY = 'Purpose'
GF_DESC_CFG_KEY = 'Description'

IFC_VERSION_VALUE_SET = {'IFC4', 'IFC4x1', 'IFC4x3'}

PREFP_KEY = 'PRefP'
PREFK_KEY = 'PRefK'
PREF_TYPE_VALUE_SET = {PREFP_KEY, PREFK_KEY}

GEO_REF_PTY_SET = 'PSet_GeoRef'
COMMON_PRO_PTY_SET = 'CHGLG_ProjectCommon'
COMMON_OWN_ASBL_PTY_SET = 'CHGLG_GeotechnicalAssemblyCommon'
COMMON_OWN_GTEC_PTY_SET = 'CHGLG_GeotechnicalStratumCommon'
GF_OWN_PTY_SET = 'CHGLG_GeologicFeature'
OBS_OWN_PTY_SET = 'CHGLG_Observation'


UNIT_KEY = 'Unit'
STRUCT_KEY = 'Structure'
GEOLOGY_KEY = 'Geology'
GEOTECH_KEY = 'Geotechnic'
HYDRO_KEY = 'Hydrogeology'
GF_TYPE_VALUE_SET = {UNIT_KEY, STRUCT_KEY}
GF_PERSPECTIVE_VALUE_SET = {GEOLOGY_KEY, GEOTECH_KEY, HYDRO_KEY}

EXCEL_DOC_NAME = 'Excel'
PRESUMED_VALUE_TXT = 'Im {} in Spalte {} Zeile {} wurde {} angenommen als Wert für "{}".'
VALUE_RANGE_TXT = 'Im {} muss in Spalte {} Zeile {} ein korrekter Wert gesetzt sein. Erlaubte Werte sind: {}'
VALUE_NUMBER_TXT = 'Im {} muss in Spalte {} Zeile {} eine Zahl sein.'
INVALID_VALUE_TXT = 'Der Wert "{}" ist ungültig.'

MAX_ERR_EXAMPLE_VALUES = 10

class GroupableError(ValueError):
    def __init__(self, msgStr, *msgArgs):
        super().__init__(msgStr.format(*msgArgs))
        self.msgStr = msgStr
        self.msgArgs = msgArgs # no need to = list(msgArgs)

    @staticmethod
    def groupErrors(errors):
        res = []
        msg2Errs = {}
        for err in errors:
            if err.msgStr not in msg2Errs:
                msg2Errs[err.msgStr] = []
            msg2Errs[err.msgStr].append(err)
        
        for msgStr, errs in msg2Errs.items():
            if 1 < len(errs):
                xpl = errs[0]
                argRanges = [{} for i in range(len(xpl.msgArgs))]
                for err in errs:
                    for i in range(len(err.msgArgs)):
                        arg = err.msgArgs[i]
                        if arg not in argRanges[i]:
                            argRanges[i][arg] = 0
                        argRanges[i][arg] += 1
                # group if variation of only one arg is more than 1 or
                # if variation of several args is more than 1 and identical in number
                argRngMagn = [len(argRng) for argRng in argRanges]
                firstGt1Mag = None
                allIdxIdentical = []
                for idx in range(len(argRngMagn)):
                    mag = argRngMagn[idx]
                    if 1 < mag:
                        if 0 < len(allIdxIdentical):
                            if mag == firstGt1Mag:
                                allIdxIdentical.append(idx)
                            else:    
                                allIdxIdentical = []
                                break
                        else:
                            firstGt1Mag = mag
                            allIdxIdentical.append(idx)
                nofIdxIdt = len(allIdxIdentical)
                if 0 < nofIdxIdt:
                    magMax = min(MAX_ERR_EXAMPLE_VALUES, firstGt1Mag) - 1
                    vals = [[] for idx in allIdxIdentical]
                    for err in errs:
                        thisMax = 0
                        for i in range(len(allIdxIdentical)):
                            idx = allIdxIdentical[i]
                            val = err.msgArgs[idx]
                            if val not in vals[i]:
                                vals[i].append(val)
                                thisMax = max(thisMax, len(vals[i]))
                        if magMax < thisMax:
                            # consider nor more errors (we have enough example values)
                            break
                    valStrs = []
                    for explVals in vals:
                        # explValStrs = [str(v) for v in explVals]
                        explValStrs = []
                        for v in explVals:
                            vs = str(v)
                            explValStrs.append(vs)
                        jstr = ', '.join(explValStrs)
                        if MAX_ERR_EXAMPLE_VALUES < firstGt1Mag:
                            jstr += ', ..'
                        valStrs.append(jstr)
                    err = errs[0] # just any example error
                    args = []
                    for idx in range(len(err.msgArgs)):
                        if idx in allIdxIdentical:
                            i = allIdxIdentical.index(idx)
                            vstr = valStrs[i]
                            args.append(vstr)
                        else:
                            args.append(err.msgArgs[idx])
                    newErr = GroupableError(err.msgStr, *args)
                    res.append(newErr)
                else:
                    # not groupable
                    res += errs
            else:
                # There is only one error of this kind.
                res.append(errs[0])
        return res

def createAssemblyWithProps(ifcw, name, objType, proCfg):
    if ifcw.getVersion() != 'IFC4x3':
        ifcw.nttBuildingElementProxy(ifcw.createGuid(), ifcw.ownerHistoryId, name, None, objType, None, None, None, None) # No placement nor representation
        assemblyId = ifcw.lastBuildingElementProxyId # This id does the spacial attachment and gets all the properties attached.
    else:
        ifcw.nttGeomodel(ifcw.createGuid(), ifcw.ownerHistoryId, name, None, objType, None, None, None) # No placement nor representation
        assemblyId = ifcw.lastGeomodelId

    pts = {}
    pts[COMMON_OWN_ASBL_PTY_SET] = {}
    p = pts[COMMON_OWN_ASBL_PTY_SET]
    p['Consultant'] = proCfg[CONSULTANT_CFG_KEY]
    p['Customer'] = proCfg[CUSTOMER_CFG_KEY]
    p['DateModel'] = proCfg[DATE_MODEL_CFG_KEY]
    p['ModelSoftware'] = proCfg[MODEL_SOFTWARE_CFG_KEY]
    p['ModelType'] = proCfg[MODEL_TYPE_CFG_KEY]
    p['ModelVersion'] = proCfg[MODEL_VERSION_CFG_KEY]

    createIfcProperties(pts, ifcw, [assemblyId])

    return assemblyId

def createGf(ifcw, name, desc, baseType, perspective, objType, tag, placementId, productId):
        if ifcw.getVersion() != 'IFC4x3':
            ifcw.nttGeographicElement(ifcw.createGuid(), ifcw.ownerHistoryId, name, desc, objType, placementId, productId, tag, None)
            return ifcw.lastGeographicElementId
        else:
            # ifcw.nttGeotechnicalStratum(ifcw.createGuid(), ifcw.ownerHistoryId, name, desc, objType, placementId, productId, None)
            # proxyId = ifcw.lastGeotechnicalStratumId
            if (perspective != HYDRO_KEY):
                ifcw.nttSolidStratum(ifcw.createGuid(), ifcw.ownerHistoryId, name, desc, objType, placementId, productId, tag)
                return ifcw.lastSolidStratumId
            else:
                ifcw.nttWaterStratum(ifcw.createGuid(), ifcw.ownerHistoryId, name, desc, objType, placementId, productId, tag)
                return ifcw.lastWaterStratumId
        # ifcw.nttSpace(ifcw.createGuid(), ifcw.ownerHistoryId, bhId, 'BOREHOLE_SEGMENT', 'BoreholeSegment', 
        #     spacePlacementId, productId, None, 'ELEMENT', 'USERDEFINED', None)
        # return ifcw.lastSpaceId

def createPref(ifcw, contPlcId, type, name, desc, point):

    ifcw.ncpLocalPlacement(relativeToId=contPlcId)
    placementId = ifcw.lastLocalPlacementId

    a = point # a tuple
    if type == PREFK_KEY:
        # 4 top points and a as the base one (pyramid pointing down)
        offset = 1.0
    else: # PREFP_KEY
        # 4 base points and a as the top one (pyramid pointing up)
        offset = -1.0
    ifcw.nttCartesianPoint((a[0]-0.5,a[1]-0.5,a[2]+offset))
    bp0 = ifcw.lastCartesianPointId
    ifcw.nttCartesianPoint((a[0]+0.5,a[1]-0.5,a[2]+offset))
    bp1 = ifcw.lastCartesianPointId
    ifcw.nttCartesianPoint((a[0]+0.5,a[1]+0.5,a[2]+offset))
    bp2 = ifcw.lastCartesianPointId
    ifcw.nttCartesianPoint((a[0]-0.5,a[1]+0.5,a[2]+offset))
    bp3 = ifcw.lastCartesianPointId
    ifcw.nttCartesianPoint((a[0],a[1],a[2]))
    pref = ifcw.lastCartesianPointId

    # 4 side triangles and a square
    ifcw.nttPolyLoop((bp0, bp1, pref))
    r0 = ifcw.lastPolyLoopId
    ifcw.nttPolyLoop((bp1, bp2, pref))
    r1 = ifcw.lastPolyLoopId
    ifcw.nttPolyLoop((bp2, bp3, pref))
    r2 = ifcw.lastPolyLoopId
    ifcw.nttPolyLoop((bp3, bp0, pref))
    r3 = ifcw.lastPolyLoopId
    ifcw.nttPolyLoop((bp0, bp1, bp2, bp3))
    bs = ifcw.lastPolyLoopId
    # 5 outer bound faces
    ifcw.nttFaceOuterBound(r0, True)
    fob0 = ifcw.lastFaceOuterBoundId
    ifcw.nttFaceOuterBound(r1, True)
    fob1 = ifcw.lastFaceOuterBoundId
    ifcw.nttFaceOuterBound(r2, True)
    fob2 = ifcw.lastFaceOuterBoundId
    ifcw.nttFaceOuterBound(r3, True)
    fob3 = ifcw.lastFaceOuterBoundId
    ifcw.nttFaceOuterBound(bs, True)
    fobb = ifcw.lastFaceOuterBoundId
    # 5 according faces
    ifcw.nttFace((fob0,))
    f0 = ifcw.lastFaceId
    ifcw.nttFace((fob1,))
    f1 = ifcw.lastFaceId
    ifcw.nttFace((fob2,))
    f2 = ifcw.lastFaceId
    ifcw.nttFace((fob3,))
    f3 = ifcw.lastFaceId
    ifcw.nttFace((fobb,))
    fb = ifcw.lastFaceId
    ifcw.nttClosedShell((f0, f1, f2, f3, fb))
    ifcw.nttFacetedBrep(ifcw.lastClosedShellId)
    facetedBrepId = ifcw.lastFacetedBrepId

    ifcw.nttShapeRepresentation(ifcw.contextId, 'Body', 'Brep', [facetedBrepId])
    ifcw.nttProductDefinitionShape(None, None, [ifcw.lastShapeRepresentationId])
    productId = ifcw.lastProductDefinitionShapeId

    ifcw.nttGeographicElement(ifcw.createGuid(), ifcw.ownerHistoryId, name, desc, 'PROJECT_REFERENCE_POINT', placementId, productId, None, None)
    return ifcw.lastGeographicElementId

def warnErrors(ifcw, errs):
    ifcw.startComment('WARNINGS')
    for err in errs:
        msg = err.args[0]
        ifcw.inComment(msg)
    ifcw.endComment('WARNINGS')

def hasWarningsSoHandle(ifcw, errs, err=None):
    return hasErrorsSoHandle(ifcw, errs, err, True)

def hasErrorsSoHandle(ifcw, errs, err=None, onlyWarn=False):
    if 0 < len(errs):
        if err is not None:
            errs.append(err)
        errs = GroupableError.groupErrors(errs)
        if err is None and len(errs) == 1 and not onlyWarn:
            raise errs[0]
        if ifcw is None:
            raise errs[0] # just the first one
        warnErrors(ifcw, errs)
        return True
    return False

def hasCoordErrors(liNo, row, ifcw, name, xKey, yKey, zKey, err=None):
    errs = []
    a = (row[xKey], row[yKey], row[zKey])
    valNo(a[0], GroupableError(VALUE_NUMBER_TXT + ' ' + INVALID_VALUE_TXT, name, xKey, liNo, a[0]), errs)
    valNo(a[1], GroupableError(VALUE_NUMBER_TXT + ' ' + INVALID_VALUE_TXT, name, yKey, liNo, a[1]), errs)
    valNo(a[2], GroupableError(VALUE_NUMBER_TXT + ' ' + INVALID_VALUE_TXT, name, zKey, liNo, a[2]), errs)
    return hasErrorsSoHandle(ifcw, errs, err)

def readProjectBasics(proCfg):

    scVup = proCfg[IFC_VERSION_CFG_KEY].upper()
    if scVup == 'IFC4' or scVup == 'IFC4X0' or scVup == '4' or scVup == '4X0':
        ifcw = V40IfcWriter()
    elif scVup == 'IFC4X1' or scVup == '4X1':
        ifcw = V41IfcWriter()
    elif scVup == 'IFC4X3' or scVup == '4X3':
        ifcw = V43IfcWriter()
    else:
        raise ValueError('Fehlerhafte Konfiguration für {} mit "{}". Erlaubte Werte sind: {}'.format(IFC_VERSION_CFG_KEY, proCfg[IFC_VERSION_CFG_KEY], ', '.join(IFC_VERSION_VALUE_SET)))

    geoRef = None
    if proCfg[LO_GEO_REF_CFG_KEY] == 30:
        geoRef = 'IfcMapConversion' 
    elif proCfg[LO_GEO_REF_CFG_KEY] == 50:
        geoRef = 'IfcGeometricRepresentationContext' 
    else: 
        raise ValueError('Ungültige Stufe der Georeferenzierung {}. Erlaubte Werte sind 30 oder 50.'.format(proCfg[LO_GEO_REF_CFG_KEY]))
    
    epsgCode = proCfg[EPSG_CODE_CFG_KEY]

    proOrig = [proCfg[PRO_ORIG_X_CFG_KEY], proCfg[PRO_ORIG_Y_CFG_KEY], proCfg[PRO_ORIG_Z_CFG_KEY]]
    if proCfg[TRANS_COORD_CFG_KEY] == 'True':
        transCoord = True
        origShift = proOrig
    else:
        transCoord = False
        origShift = [0, 0, 0]

    return ifcw, geoRef, epsgCode, proOrig, transCoord, origShift

# This function is not used for the main (first) config
def validateProjectConfig(xlsxStream, cfgFileName, commonCfgFileName, commonIfcw, commonGeoRef, commonEpsgCode, commonProOrig, commonApplyTranslate, commonOrigShift):

    dfProMeta = pd.read_excel(xlsxStream,sheet_name=PRO_DF_NAME, index_col=None, header=None)
    proCfg = readProjectConfig(dfProMeta) # does also validate 
    # We actually validate too much, since we only need IFC_VERSION_CFG_KEY, LO_GEO_REF_CFG_KEY,
    # PRO_ORIG_?_CFG_KEY and TRANS_COORD_CFG_KEY here.

    ifcw, geoRef, epsgCode, proOrig, transCoord, origShift = readProjectBasics(proCfg)

    if commonIfcw.getVersion() != ifcw.getVersion():
        raise ValueError(f"""In der Excel Konfigurations Datei {cfgFileName} entspricht die IFC Version {ifcw.getVersion()} nicht der der Haupt-Konfigurations Datei {commonCfgFileName}, wo sie {commonIfcw.getVersion()} ist.""")

    if commonGeoRef != geoRef:
        raise ValueError(f"""In der Excel Konfigurations Datei {cfgFileName} entspricht die GEO Referenzierung {LO_GEO_REF_CFG_KEY} {geoRef} nicht der der Haupt-Konfigurations Datei {commonCfgFileName}, wo sie {commonGeoRef} ist.""")

    if str(commonGeoRef) == '50' and commonEpsgCode != epsgCode:
        raise ValueError(f"""In der Excel Konfigurations Datei {cfgFileName} entspricht der {EPSG_CODE_CFG_KEY} {epsgCode} nicht dem der Haupt-Konfigurations Datei {commonCfgFileName}, wo er {commonEpsgCode} ist.""")

    if len(commonProOrig) != len(proOrig) or commonProOrig[0] != proOrig[0] or commonProOrig[1] != proOrig[1] or commonProOrig[2] != proOrig[2]:
        raise ValueError(f"""In der Excel Konfigurations Datei {cfgFileName} entspricht der Koordinatenursprung {proOrig} nicht dem der Haupt-Konfigurations Datei {commonCfgFileName}, wo er {commonProOrig} ist.""")

    if commonApplyTranslate != transCoord:
        raise ValueError(f"""In der Excel Konfigurations Datei {cfgFileName} entspricht {TRANS_COORD_CFG_KEY} {transCoord} nicht dem der Haupt-Konfigurations Datei {commonCfgFileName}, wo es {commonApplyTranslate} ist.""")
    
    # The values of origShift and commonOrigShift are unused since this is the same as comparing geoRef and transCoord.


# This function is used for the main (first) config
def createWriterAndBegin(xlsxStream):

    dfProMeta = pd.read_excel(xlsxStream,sheet_name=PRO_DF_NAME, index_col=None, header=None)
    proCfg = readProjectConfig(dfProMeta) # does also validate

    ifcw, geoRef, epsgCode, proOrig, transCoord, origShift = readProjectBasics(proCfg)

    rotat = 0 # No longer configurable. Always 0.

    ifcw.beginning(proCfg[PRO_NAME_CFG_KEY]+'.ifc', 
        proCfg[DELIVERY_PARTY_PERSON_CFG_KEY], proCfg[DELIVERY_PARTY_ORG_CFG_KEY], 
        proCfg[PRO_DESC_CFG_KEY], 
        proCfg[PRO_NAME_CFG_KEY], proCfg[PRO_DESC_CFG_KEY], proCfg[PHASE_CFG_KEY],
        epsgCode,
        authorization='', georeferencing=geoRef,
        origin=proOrig, rotation=rotat, appName='GEOLBIM WebApp')

    # ifcw.ncpSite(proCfg[SITE_NAME_CFG_KEY], georeferencing=geoRef, origin=proOrig, rotation=rotat)
    # spatialContainerId = ifcw.lastSiteId
    # containerPlacementId = ifcw.sitePlacementId
    ifcw.ncpExternalSpatialElement(proCfg[SITE_NAME_CFG_KEY], 'EXTERNAL')

    pts = {}
    pts[COMMON_PRO_PTY_SET] = {}
    p = pts[COMMON_PRO_PTY_SET]
    p['DateKnowledge'] = proCfg[DATE_KNOWLEDGE_CFG_KEY]
    p['DateProcessing'] = proCfg[DATE_PROCESSING_CFG_KEY]
    dpStr = proCfg[DELIVERY_PARTY_PERSON_CFG_KEY]
    if not pd.isna(dpStr) and 1 < len(str(dpStr)):
        dpStr = str(dpStr) + ' - ' + str(proCfg[DELIVERY_PARTY_ORG_CFG_KEY])
    else:
        dpStr = str(proCfg[DELIVERY_PARTY_ORG_CFG_KEY])
    p['DeliveryParty'] = dpStr
    p['Purpose'] = proCfg[PURPOSE_CFG_KEY]
    p['Version'] = proCfg[VERSION_CFG_KEY]
    createIfcProperties(pts, ifcw, [ifcw.projectId])


    return ifcw, geoRef, epsgCode, proOrig, transCoord, origShift

# This function is used for the main (first) config
def writeAndValidateSurveyPoints(xlsxStream, fileName, ifcw, origShift, mainFileName = None, mainSpId2Row = None):

    dfSurPnts = pd.read_excel(xlsxStream,sheet_name=SUR_DF_NAME, skiprows=[1],converters={SUR_ID_CFG_KEY:str})
    grantColumnsPresent(SUR_DF_NAME, dfSurPnts, SUR_CFG_SET)
    grantColumnValuesUnique(SUR_DF_NAME, dfSurPnts, SUR_ID_CFG_KEY)
    grantColumnValuesNumericMany(SUR_DF_NAME, dfSurPnts, {SUR_X_CFG_KEY, SUR_Y_CFG_KEY, SUR_Z_CFG_KEY}, rowNamingOffset=3)

    skippedSurIds = []
    for i, row in dfSurPnts.iterrows():
        surId = row[SUR_ID_CFG_KEY]
        surType = row[SUR_TYPE_CFG_KEY]

        errs = []
        wrns = []
        surType = validateSurveyType(errs, wrns, SUR_TYPE_CFG_KEY, i+2, surType, SUR_DF_NAME)
        # TODO not sure the error/warnings handling is correct like that
        if hasErrorsSoHandle(ifcw, errs): 
            skippedSurIds.append(surId)
            continue
        if hasWarningsSoHandle(ifcw, wrns):
            skippedSurIds.append(surId)
            continue

        # No need to validate here since grantColumnValuesNumericMany is used
        if hasCoordErrors(i+2, row, ifcw, SUR_DF_NAME, SUR_X_CFG_KEY, SUR_Y_CFG_KEY, SUR_Z_CFG_KEY, 
                GroupableError('Im {} (von {}) in Zeile {} wurde {} {} ignoriert, weil die Koordinaten ungültig sind.', SUR_DF_NAME, fileName, i+2, surType, surId)
            ):
            skippedSurIds.append(surId)
            continue

    if mainSpId2Row is not None:
        # Validate dfSurPnts by comparing them to mainSpId2Row (id must match)
        wrns = []
        for i, row in dfSurPnts.iterrows():
            surId = row[SUR_ID_CFG_KEY]
            surType = row[SUR_TYPE_CFG_KEY]
            if surId in skippedSurIds: continue

            if surId not in mainSpId2Row:
                wrns.append(GroupableError('Im {} (von {}) in Zeile {} wurde {} {} ignoriert, weil die ID in {} nicht vorkommt.', SUR_DF_NAME, fileName, i+2, surType, surId, mainFileName))
                skippedSurIds.append(surId)
                continue

            mainRow = mainSpId2Row[surId]

            if surType != mainRow[SUR_TYPE_CFG_KEY]:
                wrns.append(GroupableError('Im {} (von {}) in Zeile {} wurde {} {} ignoriert, weil der Typ unterschiedlich ist zu dem in {}.', SUR_DF_NAME, fileName, i+2, surType, surId, mainFileName))
                skippedSurIds.append(surId)
                continue

            a = (row[SUR_X_CFG_KEY], row[SUR_Y_CFG_KEY], row[SUR_Z_CFG_KEY])

            if not ( mainRow[SUR_X_CFG_KEY] * (1.0 - NUMERIC_EXACT_COORDS) < a[0] and a[0] < mainRow[SUR_X_CFG_KEY] * (1.0 + NUMERIC_EXACT_COORDS) ):
                wrns.append(GroupableError('Im {} (von {}) in Zeile {} wurde {} {} ignoriert, weil die X-Koordinate {} nicht mit {} in {} übereinstimmt.', SUR_DF_NAME, fileName, i+2, surType, surId, a[0], mainRow[SUR_X_CFG_KEY], mainFileName))
                skippedSurIds.append(surId)
                continue

            if not ( mainRow[SUR_Y_CFG_KEY] * (1.0 - NUMERIC_EXACT_COORDS) < a[1] and a[1] < mainRow[SUR_Y_CFG_KEY] * (1.0 + NUMERIC_EXACT_COORDS) ):
                wrns.append(GroupableError('Im {} (von {}) in Zeile {} wurde {} {} ignoriert, weil die Y-Koordinate {} nicht mit {} in {} übereinstimmt.', SUR_DF_NAME, fileName, i+2, surType, surId, a[1], mainRow[SUR_Y_CFG_KEY], mainFileName))
                skippedSurIds.append(surId)
                continue

            if not ( mainRow[SUR_Z_CFG_KEY] * (1.0 - NUMERIC_EXACT) < a[2] and a[2] < mainRow[SUR_Z_CFG_KEY] * (1.0 + NUMERIC_EXACT) ):
                wrns.append(GroupableError('Im {} (von {}) in Zeile {} wurde {} {} ignoriert, weil die Z-Koordinate {} nicht mit {} in {} übereinstimmt.', SUR_DF_NAME, fileName, i+2, surType, surId, a[2], mainRow[SUR_Z_CFG_KEY], mainFileName))
                skippedSurIds.append(surId)
                continue
        
        hasWarningsSoHandle(ifcw, wrns)

    # Create survey points
    containerId = ifcw.externalSpatialElementId
    contPlcId = ifcw.externalSpatialElementPlacementId
    for i, row in dfSurPnts.iterrows():
        surId = row[SUR_ID_CFG_KEY]
        surType = row[SUR_TYPE_CFG_KEY]
        if surId in skippedSurIds: continue

        a = (row[SUR_X_CFG_KEY], row[SUR_Y_CFG_KEY], row[SUR_Z_CFG_KEY])
        coord = (a[0]-origShift[0], a[1]-origShift[1], a[2]-origShift[2])
        pts = {}
        pts[GEO_REF_PTY_SET] = {}
        p = pts[GEO_REF_PTY_SET]
        p['Name'] = surType
        p['LocalX'] = 0.0
        p['LocalY'] = 0.0
        p['LocalZ'] = 0.0
        p['Easting'] = a[0]
        p['Northing'] = a[1]
        p['OrthogonalHeight'] = a[2]
        theId = createPref(ifcw, contPlcId, surType, surId, f'Projektreferenzpunkt - {surType} - {surId}', coord)

        createIfcProperties(pts, ifcw, [theId])

        ifcw.nttRelAggregates(ifcw.createGuid(), ifcw.ownerHistoryId, 'Spatial Attachement', None, containerId, [theId])

    if mainSpId2Row is None:
        spId2Row = {}
        for i, row in dfSurPnts.iterrows():
            surId = row[SUR_ID_CFG_KEY]
            spId2Row[surId] = row
        return spId2Row
    else:
        return None

def cfgFileNames(xlsxStream, dfName, fileCfgKey):
    # Fits for both xlsx tabs GeologicFeature and Voxel_GeologicFeature.
    res = []
    df = pd.read_excel(xlsxStream, sheet_name=dfName, skiprows=[0,2])
    for i, row in df.iterrows():
        fileName = row[fileCfgKey]
        if not pd.isna(fileName) and not fileName in res:
            res.append(fileName)
    return res


def readProjectConfig(dfProMeta):
    proCfg = {}
    for i, row in dfProMeta.iterrows():
        key = row[0]
        value = row[2]
        if key in PRO_CFG_SET:
            proCfg[key] = value
    missingKeys = []
    for key in PRO_CFG_SET:
        if not key in proCfg:
            missingKeys.append(key)
    if 0 < len(missingKeys):
        if len(missingKeys) == 1:
            key = missingKeys[0]
            raise ValueError(f"""Im Excel Blatt {PRO_DF_NAME} fehlt die Zeile {key}.""")
        else:
            missingKeysStr = ' und '.join(', '.join(missingKeys).rsplit(', ', 1))
            raise ValueError(f"""Im Excel Blatt {PRO_DF_NAME} fehlen Zeilen für {missingKeysStr}.""")
    for i, row in dfProMeta.iterrows():
        rowNo = i+1
        key = row[0]
        value = row[2]
        if key in PRO_DATES_CFG_SET and not pd.isna(value) and not isinstance(value, datetime):
            raise ValueError(f"""Im Excel in Blatt {PRO_DF_NAME} ist in der Zeile {key} ({rowNo}) der Wert "{value}" keine gültige Datums- und/oder Zeitangabe.""")
    return proCfg

def readPtyToPtySetName(xlsxStream, sheetName, startIdx=0):
    dfItvGrp = pd.read_excel(xlsxStream,sheet_name=sheetName, index_col=None, header=None, nrows=2)
    nofCols = len(dfItvGrp.columns)
    lastNotNoneValue = None
    keyToGroup = {}
    for i in range(startIdx, nofCols): # in v8 we must do range(11, nofCols) to get only custom attibutes of Interval_GeologicFeature
        key = dfItvGrp[i][1]
        value = dfItvGrp[i][0]
        if not pd.isna(value):
            lastNotNoneValue = value
        keyToGroup[key] = lastNotNoneValue
        #logger.info(f"""{key}:{lastNotNoneValue}""")
    return keyToGroup

def grantColumnsPresent(dfName, df, nameSet):
    missingNames = []
    for name in nameSet:
        if name not in df or len(df[name]) < 1:
            missingNames.append(name)
    if 0 < len(missingNames):
        missingNamesStr = ', '.join(missingNames)
        if len(missingNames) == 1:
            raise ValueError(f"""Im Excel fehlt in Blatt {dfName} die {missingNamesStr} Spalte.""")
        else:
            missingNamesStr = ' und '.join(missingNamesStr.rsplit(', ', 1))
            raise ValueError(f"""Im Excel fehlen in Blatt {dfName} die Spalten {missingNamesStr}.""")

def grantColumnValuesUnique(dfName, df, name, rowNamingOffset=1, orNone=False):
    already = {}
    i = rowNamingOffset
    for value in df[name]:
        if not orNone and value is None:
            raise ValueError(f"""Im Excel in Blatt {dfName} ist in der Spalte {name} der Wert in Zeile {i} nicht gesetzt. Das ist ungültig.""")
        if not value is None:
            if not value in already:
                already[value] = i
            else:
                firstIndex = already[value]
                secondIndex = i
                raise ValueError(f"""Im Excel in Blatt {dfName} sind in der Spalte {name} die beiden Werte in Zeile {firstIndex} und {secondIndex} identisch. Das ist ungültig.""")
        i += 1

def grantColumnValuesWithin(dfName, df, name, valueSet, rowNamingOffset=1):
    i = rowNamingOffset
    for value in df[name]:
        if not str(value) in valueSet:
            valueSetStr = ' und '.join(', '.join(valueSet).rsplit(', ', 1))
            if 200 < len(valueSetStr):
                valueSetStr = valueSetStr[:160] # less than 200 for a near cut is not ridiculous
                valueSetStr = valueSetStr.rsplit(' ', 1)[0] + ' .. (Es sind sehr viele möglich)'
            raise ValueError(f"""Im Excel in Blatt {dfName} ist in der Spalte {name} in Zeile {i} der Wert "{value}" ungültig. Es muss einer der folgenden Werte sein: {valueSetStr}.""")
        i += 1

def grantColumnValuesNumeric(dfName, df, name, rowNamingOffset=1):
    i = rowNamingOffset
    for value in df[name]:
        if not(type(value) is float or type(value) is int):
            raise ValueError(f"""Im Excel in Blatt {dfName} ist in der Spalte {name} in Zeile {i} der Wert "{value}" keine gültige Zahl.""")
        i += 1

def grantColumnValuesNumericMany(dfName, df, nameSet, rowNamingOffset=1):
    for name in nameSet:
        grantColumnValuesNumeric(dfName, df, name, rowNamingOffset)

def grantColumnValuesDatetime(dfName, df, name, rowNamingOffset=1):
    i = rowNamingOffset
    for value in df[name]:
        if not pd.isna(value) and not isinstance(value, datetime):
            raise ValueError(f"""Im Excel in Blatt {dfName} ist in der Spalte {name} in Zeile {i} der Wert "{value}" keine gültige Datums- und/oder Zeitangabe.""")
        i += 1

def grantColumnValuesDatetimeMany(dfName, df, nameSet, rowNamingOffset=1):
    for name in nameSet:
        grantColumnValuesDatetime(dfName, df, name, rowNamingOffset)

def createIfcProperties(setNameToPts, ifcw, segmentBelongToIds):
    for setName, values in setNameToPts.items():
        propertySingleValues = [] # list with SingleValues, values fromexample 59=IfcPropertySingleValue('volume_factor','volume_factor',IfcText('80.39329512'),$)
        for ptyName, ptyValue in values.items():
            if not pd.isna(ptyValue): 
                segValueStr = IfcWriter.encStr(str(ptyValue))
            else:
                segValueStr = ''
            # TODO for label do right encoding here (?)
            ifcw.nttPropertySingleValue(ptyName, ptyName, f"""IFCTEXT('{segValueStr}')""", None)
            propertySingleValues.append(ifcw.lastPropertySingleValueId)
        ifcw.nttPropertySet(ifcw.createGuid(), ifcw.ownerHistoryId, setName, None, propertySingleValues)
        ifcw.nttRelDefinesByProperties(ifcw.createGuid(), ifcw.ownerHistoryId, None, None, segmentBelongToIds, ifcw.lastPropertySetId)

# def baseTypeAndPerspectiveToObjectType(baseType, perspective):
#     objType = ''
#     if (perspective == GEOLOGY_KEY):
#         objType += 'GEOLOGIC'
#     if (perspective == GEOTECH_KEY):
#         objType += 'GEOTECHNIC'
#     if (perspective == HYDRO_KEY):
#         objType += 'HYDROGEOLOGIC'
#     if (baseType == UNIT_KEY):
#         objType += 'UNIT'
#     if (baseType == STRUCT_KEY):
#         objType += 'STRUCTURE'
#     return objType

def nn(val):
    if not pd.isna(val):
        return val
    else:
        return None

def valNo(val, error, errors=None):
    isErr = False
    try:
        val = float(val)
        isErr = pd.isna(val)
    except ValueError:
        isErr = True
    if isErr:
        if errors is None:
            raise error
        else:
            errors.append(error)
        val = 0 # just any digestable value
    return val

def validateSurveyType(errors, warnings, ptyName, rowNo, val, docName):

    if pd.isna(val) or len(val.strip()) == 0:
        errors.append(GroupableError(VALUE_RANGE_TXT, docName, ptyName, rowNo, ', '.join(PREF_TYPE_VALUE_SET)))
        return val

    valStp = val.strip()
    valLow = valStp.lower()
    if valLow == PREFP_KEY.lower():
        return PREFP_KEY
    elif valLow == PREFK_KEY.lower():
        return PREFK_KEY
    elif valLow.endswith('p'):
        warnings.append(GroupableError(PRESUMED_VALUE_TXT, docName, ptyName, rowNo, PREFP_KEY, valStp))
        return PREFP_KEY
    elif valLow.endswith('k'):
        warnings.append(GroupableError(PRESUMED_VALUE_TXT, docName, ptyName, rowNo, PREFK_KEY, valStp))
        return PREFK_KEY
    else:
        errors.append(GroupableError(VALUE_RANGE_TXT + ' ' + INVALID_VALUE_TXT, docName, ptyName, rowNo, ', '.join(PREF_TYPE_VALUE_SET), valStp))
        return val

class EltData:

    def __init__(self):
        self.pts = {}
        self.errors = []
        self.warnings = []

    def actValue(self, docName, ptyName, row, rowNo):
        val = row[ptyName]
        val = self.validateIfBasePty(ptyName, rowNo, val, docName)
        if 0 < len(self.errors): # raise errors immediately
            raise self.errors[0]
        return val

    def assignGfProps(self, docName, row, rowNo, pty2ptySetName):
        self.baseType = self.actValue(docName, GF_BASE_TYPE_CFG_KEY, row, rowNo)
        self.perspective = self.actValue(docName, GF_PERSPECTIVE_CFG_KEY, row, rowNo)

        self.name = self.actValue(docName, GF_CLASS_CFG_KEY, row, rowNo) # classifier
        self.desc = nn(self.actValue(docName, GF_DESC_CFG_KEY, row, rowNo))
        # objType = baseTypeAndPerspectiveToObjectType(self.baseType, self.perspective)
        self.objType = self.actValue(docName, GF_SUB_TYPE_CFG_KEY, row, rowNo) # subType
        self.tag = None

        self.setPty(COMMON_OWN_GTEC_PTY_SET, 'observationMethod', self.actValue(docName, GF_OBS_MTH_CFG_KEY, row, rowNo))
        self.setPty(COMMON_OWN_GTEC_PTY_SET, 'observationMethodGeometry', self.actValue(docName, GF_OBS_MTH_GEO_CFG_KEY, row, rowNo))
        self.setPty(COMMON_OWN_GTEC_PTY_SET, 'purpose', self.actValue(docName, GF_PURPOSE_CFG_KEY, row, rowNo))
        for ptyName, ptySetName in pty2ptySetName.items():
            self.setPty(ptySetName, ptyName, self.actValue(docName, ptyName, row, rowNo))

    def setPty(self, setName, name, val):
        if not pd.isna(val):
            if not setName in self.pts:
                self.pts[setName] = {}
            ptySet = self.pts[setName]
            ptySet[name] = val

    def validateIfBasePty(self, ptyName, rowNo, val, docName):
        if ptyName == GF_BASE_TYPE_CFG_KEY:
            return self.validateBaseType(ptyName, rowNo, val, docName)
        elif ptyName == GF_PERSPECTIVE_CFG_KEY:
            return self.validatePerspective(ptyName, rowNo, val, docName)
        else:
            return val
    
    def validateBaseType(self, ptyName, rowNo, val, docName):

        if pd.isna(val) or len(val.strip()) == 0:
            self.errors.append(GroupableError(VALUE_RANGE_TXT, docName, ptyName, rowNo, ', '.join(GF_TYPE_VALUE_SET)))
            return val

        valStp = val.strip()
        valLow = valStp.lower()
        if valLow == UNIT_KEY.lower():
            return UNIT_KEY
        elif valLow == STRUCT_KEY.lower():
            return STRUCT_KEY
        elif valLow.startswith('u'):
            self.warnings.append(GroupableError(PRESUMED_VALUE_TXT, docName, ptyName, rowNo, UNIT_KEY, valStp))
            return UNIT_KEY
        elif valLow.startswith('s'):
            self.warnings.append(GroupableError(PRESUMED_VALUE_TXT, docName, ptyName, rowNo, STRUCT_KEY, valStp))
            return STRUCT_KEY
        else:
            self.errors.append(GroupableError(VALUE_RANGE_TXT + ' ' + INVALID_VALUE_TXT, docName, ptyName, rowNo, ', '.join(GF_TYPE_VALUE_SET), valStp))
            return val

    def validatePerspective(self, ptyName, rowNo, val, docName):

        if pd.isna(val) or len(val.strip()) == 0:
            self.errors.append(GroupableError(VALUE_RANGE_TXT, docName, ptyName, rowNo, ', '.join(GF_PERSPECTIVE_VALUE_SET)))
            return val

        valStp = val.strip()
        valLow = valStp.lower()
        if valLow == GEOLOGY_KEY.lower():
            return GEOLOGY_KEY
        elif valLow == GEOTECH_KEY.lower():
            return GEOTECH_KEY
        elif valLow == HYDRO_KEY.lower():
            return HYDRO_KEY
        elif valLow.startswith('geol'):
            self.warnings.append(GroupableError(PRESUMED_VALUE_TXT, docName, ptyName, rowNo, GEOLOGY_KEY, valStp))
            return GEOLOGY_KEY
        elif valLow.startswith('geot'):
            self.warnings.append(GroupableError(PRESUMED_VALUE_TXT, docName, ptyName, rowNo, GEOTECH_KEY, valStp))
            return GEOTECH_KEY
        elif valLow.startswith('h'):
            self.warnings.append(GroupableError(PRESUMED_VALUE_TXT, docName, ptyName, rowNo, HYDRO_KEY, valStp))
            return HYDRO_KEY
        else:
            self.errors.append(GroupableError(VALUE_RANGE_TXT + ' ' + INVALID_VALUE_TXT, docName, ptyName, rowNo, ', '.join(GF_PERSPECTIVE_VALUE_SET), valStp))
            return val

class GfDta(EltData):

    def setRow(self, row, rowNo, pty2ptySetName):
        self.assignGfProps(EXCEL_DOC_NAME, row, rowNo, pty2ptySetName)

