
import logging

from flask import request, make_response, jsonify, current_app as app
from flasgger import SwaggerView  # SwaggerView inherits flask.views.MethodView

from ..shared import (HttpStatus)


__all__ = ['Info']


class Info(SwaggerView):
    """
    /status/info
    """

    logger = logging.getLogger('apisrv')

    # @app.auth.login_required so far no issue
    def get(self):
        """
        Get info about the api state.
        ---
        operationId: info
        tags:
          - status
        responses:
          200:
            description: The info about the api state.
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/InfoSchema'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.bad_request_400.value

        try:
            apiVersion = app.config['VERSION']
            dic = {'api_version': apiVersion}
            message = jsonify(dic)
            code = 200

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(message, code)
