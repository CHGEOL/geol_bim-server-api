
import logging
from collections import OrderedDict
from typing import Dict, Tuple, List, Optional, Union

from psycopg2 import sql


class QueryBuilder:
    """
    Base-Class for generating SQL Query-Strings. The Query-Strings for the separate SQL blocks
    and the values are stored as class attributes.
    """
    logger: 'logging.Logger' = logging.getLogger('apisrv')

    values: Union[Dict, 'OrderedDict'] = OrderedDict()  # order of keys is always kept

    # Used in Read (optional), Update (required), Delete (required)
    where_sql: Union['sql.Composable', None] = None

    def build_where_sql(self, filter_equal: Dict = None, filter_in: Dict = None):
        """
        Builds the WHERE block with placeholders for the values.
        The generated SQL-statement and the matching values are stored as class attributes.

        :param filter_equal: Dictionary with attribute-names and their values to filter
        :param filter_in: Dict with attribute-names and their filtering list; {'pro_id': [1, 2]}
        """
        # Build IN-String for filtering
        in_sql, equal_sql = None, None

        if filter_in:
            # Rename keys for named placeholders, that they are different than the other keys
            in_values = {f'in_{k}': v for k, v in filter_in.items()}

            # WHERE asdf IN () throws error, asdf = any(ARRAY[]) is equivalent

            in_ = zip(map(sql.Identifier, filter_in), map(sql.Placeholder, in_values))
            in_sql = sql.SQL('AND ').join(
                [sql.SQL("{} = ANY ({}) ").format(col, val) for col, val in in_])
            self.values.update(in_values)

        # Todo: string -> %like%, list -> IN, other -> =
        if filter_equal:
            # Rename keys for named placeholders, that they are different than the other keys
            equal_values = {f'eq_{k}': v for k, v in filter_equal.items()}
            equal = zip(map(sql.Identifier, filter_equal), map(sql.Placeholder, equal_values))
            equal_sql = sql.SQL('AND ').join(
                [sql.SQL("{}={} ").format(col, val) for col, val in equal])
            self.values.update(equal_values)

        if in_sql or equal_sql:
            where_sql = sql.SQL("WHERE {in_expr}{and_}{equal_expr}").format(
                in_expr=in_sql if in_sql else sql.SQL(''),
                and_=sql.SQL('AND ') if in_sql and equal_sql else sql.SQL(''),
                equal_expr=equal_sql if equal_sql else sql.SQL(''))
            # return SQL with named Placeholders and the dictionary with the values
            self.where_sql = where_sql


class CreateQueryBuilder(QueryBuilder):
    """
    Generating SQL-String for creating tuples. The SQL-Statements can be written by hand or
    using the functions to replace column names and values with placeholders.

    Example::

        # Example with raw SQL
        qb = CreateQueryBuilder()
        qb.insert_sql = sql.SQL(
            'INSERT INTO trf.project (pro_name, pro_org_id)')
        qb.values_sql = sql.SQL(
            'VALUES ({}, {})').format(
            sql.Placeholder('pro_name'), sql.Placeholder('pro_org_id'))
        qb.returning_sql = sql.SQL(
            'RETURNING pro_id')
        qb.values = dict(pro_name='Test1', pro_org_id=1)
        query, values = qb.create_query()

        # Example using functions for generating the placeholders
        # (matching the input data)
        qb = CreateQueryBuilder()
        values = dict(pro_name='Test1', pro_org_id=1)
        qb.build_insert_sql(sql.SQL('trf.project'), values)
        qb.build_values_sql(values)
        qb.returning_sql = sql.SQL('RETURNING pro_id')
        query, values = qb.create_query()

    """
    # CREATE
    insert_sql: 'sql.Composable' = None
    values_sql: 'sql.Composable' = sql.SQL('')
    returning_sql: 'sql.Composable' = None  # Has to be primary keys (see CRUD,create)

    def __init__(self):
        # No inheritance of values to parent object
        self.values = OrderedDict()

    def create_query(self) -> Tuple['sql.Composable', 'OrderedDict']:
        """
        Builds the SQL-Query for creating a new tuple with the class attributes.
        The function returns the parametrized SQL-Query and the matching values-Dictionary.

        :return: Parametrized SQL-Query, values
        :rtype: psycopg2.sql.Composable, OrderedDict
        """
        # Validation
        if not self.insert_sql:  # required
            message = 'No insert statement.'
            self.logger.error(message)
            raise ValueError(message)
        if not self.returning_sql:  # required for CRUD.create
            message = 'No returning statement.'
            self.logger.error(message)
            raise ValueError(message)

        create = sql.SQL(
            "{insert} {values} {returning};").format(
                insert=self.insert_sql,
                values=self.values_sql,
                returning=self.returning_sql)

        if not self.values:
            self.values = {}

        return create, self.values

    def build_insert_sql(self, table: 'sql.Composable', values: Dict):
        """
        Builds the INSERT block with column-names matching the values.
        The generated SQL-statement are stored as class attributes.

        :param table: Tablename for inserting the tuple
        :type table: psycopg2.sql.Composable
        :param values: Values for the new tuple
        :type values: Dict
        """
        # If the insert is not for all columns

        columns_sql = sql.SQL(', ').join(map(sql.Identifier, values))

        self.insert_sql = sql.SQL('INSERT INTO {table} ({columns})').format(
            table=table, columns=columns_sql)

    def build_values_sql(self, values: Dict):
        """
        Builds the VALUES block with named placeholders matching the values.
        The generated SQL-statement and the matching values are stored as class attributes.

        :param values: Values for the new tuple
        :type values: Dict
        """

        val_sql = sql.SQL(', ').join(map(sql.Placeholder, values))
        self.values_sql = sql.SQL('VALUES ({val})').format(val=val_sql)
        if self.values:
            self.values.update(values)
        else:
            self.values = OrderedDict(values)


class ReadQueryBuilder(QueryBuilder):
    """
    Generating SQL-String for selecting tuples. The SQL-Statements can be written by hand or
    using the functions to replace values with placeholders.

    Example::

        # Example with raw SQL
        qb = ReadQueryBuilder()
        qb.select_sql = sql.SQL(
            'SELECT trf.project.*,'
            ' usr.organisation.org_name AS pro_org_name')
        qb.from_sql = sql.SQL(
            'FROM trf.project '
            'LEFT JOIN usr.organisation'
            ' ON usr.organisation.org_id = trf.project.pro_org_id')
        qb.where_sql = sql.SQL(
            'WHERE pro_id = {}').format(sql.Placeholder('pro_id'))
        qb.order_sql = sql.SQL(
            'ORDER BY pro_name')
        qb.values = dict(pro_id=1)
        query, values = qb.create_query()

        # Example using functions for generating the placeholders
        # (matching the input data)
        qb = ReadQueryBuilder()
        qb.build_select_sql([
            sql.SQL('pro.*'),
            sql.SQL('org.org_name AS pro_org_name')])
        qb.build_from_sql([
            sql.SQL('trf.project AS pro'),
            sql.SQL('LEFT JOIN usr.organisation AS org'
                    ' ON org.org_id = pro.pro_org_id')])
        qb.build_where_sql(dict(pro_id=1))
        qb.build_order_sql([
            sql.SQL('pro_name'), ])
        query, values = qb.create_query()

    """

    # SELECT
    select_sql: 'sql.Composable' = None  # required
    from_sql: 'sql.Composable' = None  # required
    group_sql: 'sql.Composable' = sql.SQL('')  # optional
    order_sql: 'sql.Composable' = sql.SQL('')  # optional

    def __init__(self):
        # No inheritance of values to parent object
        self.where_sql = sql.SQL('')
        self.values = OrderedDict()

    def read_query(self) -> Tuple['sql.Composable', 'OrderedDict']:
        """
        Builds the SQL-Query for creating a new tuple with the class attributes.
        The function returns the parametrized SQL-Query and the matching values-Dictionary.

        :return: Parametrized SQL-Query, values
        :rtype: psycopg2.sql.Composable, OrderedDict
        """
        # validation
        if not self.select_sql:  # required
            message = 'No select statement.'
            self.logger.error(message)
            raise ValueError(message)
        if not self.from_sql:  # required
            message = 'No from statement.'
            self.logger.error(message)
            raise ValueError(message)

        read = sql.SQL('{select} {fro} {where} {group} {order};').format(
            select=self.select_sql, fro=self.from_sql, where=self.where_sql,
            group=self.group_sql, order=self.order_sql)

        if not self.values:
            self.values = {}

        return read, self.values

    def build_select_sql(self, sql_exprs: Optional[List['sql.Composable']] = None,
                         with_expr: Optional['sql.Composable'] = None):
        """
        Builds a Select-Statement from a list of SQL-composables

        :param sql_exprs: List of sql-expressions
        :type sql_exprs: [psycopg2.sql.Composable]
        :param with_expr: with-expression
        :type with_expr: psycopg2.sql.Composable
        """
        if sql_exprs:
            # Remove duplicates
            sql_exprs = [expr for n, expr in enumerate(sql_exprs) if expr not in sql_exprs[:n]]
            # Chain the expressions
            select_sql = sql.SQL(', ').join(sql_exprs)
            if with_expr:
                self.select_sql = sql.SQL('{with_sql} SELECT {select}').format(
                    with_sql=with_expr, select=select_sql)
            else:
                self.select_sql = sql.SQL('SELECT {select}').format(select=select_sql)

    def build_from_sql(self, sql_exprs: Optional[List['sql.Composable']] = None):
        """
        Builds a FROM-statement from a list of sql-expressions

        :param sql_exprs: List of sql-expressions
        :type sql_exprs: [psycopg2.sql.Composable]
        """

        if sql_exprs:
            # Remove duplicates
            sql_exprs = [expr for n, expr in enumerate(sql_exprs) if expr not in sql_exprs[:n]]
            # Chain the expressions
            from_sql = sql.SQL(' ').join(sql_exprs)
            self.from_sql = sql.SQL('FROM {fro}').format(fro=from_sql)

    def build_order_sql(self, sql_exprs: Optional[List['sql.Composable']] = None):
        """
        Builds a ORDER-BY-statement from a list of sql-expressions

        :param sql_exprs: List of sql-expressions
        :type sql_exprs: [psycopg2.sql.Composable]
        """
        if sql_exprs:
            # Remove duplicates
            sql_exprs = [expr for n, expr in enumerate(sql_exprs) if expr not in sql_exprs[:n]]
            # Chain the expressions
            order_sql = sql.SQL(', ').join(sql_exprs)
            self.order_sql = sql.SQL('ORDER BY {ord}').format(ord=order_sql)
        else:
            self.order_sql = sql.SQL('')


class UpdateQueryBuilder(QueryBuilder):
    """
    Generating SQL-String for updating tuples. The SQL-Statements can be written by hand or
    using the functions to replace values with placeholders.

    Example::

        # Example with raw SQL
        qb = UpdateQueryBuilder()
        qb.update_sql = sql.SQL(
            'UPDATE trf.project')
        qb.set_sql = sql.SQL(
            'SET "pro_desc" = {}').format(sql.Placeholder('pro_desc'))
        qb.where_sql = sql.SQL(
            'WHERE pro_id = {}').format(sql.Placeholder('pro_id'))
        qb.values = dict(pro_desc='Test1', pro_id=1)
        query, values = qb.update_query()

        # Example using functions for generating the placeholders
        # (matching the input data)
        qb = UpdateQueryBuilder()
        qb.update_sql = sql.SQL('UPDATE trf.project')
        qb.build_set_sql(dict(pro_desc='Test123'))
        qb.build_where_sql(dict(pro_id=1))
        query, values = qb.update_query()

    """

    # UPDATE
    update_sql: 'sql.Composable' = None  # required
    set_sql: 'sql.Composable' = None  # required

    def __init__(self):
        # No inheritance of values to parent object
        self.where_sql = None  # required, only update of a single tuple allowed
        self.values = OrderedDict()

    def update_query(self) -> Tuple['sql.Composable', 'OrderedDict']:
        """
        Builds the SQL-Query for updating a tuple.
        The function returns the parametrized SQL-Query and the matching values-Dictionary.

        :return: Parametrized SQL-Query, values
        :rtype: psycopg2.sql.Composable, OrderedDict
        """

        if not self.update_sql:  # required
            message = 'No update statement.'
            self.logger.error(message)
            raise ValueError(message)
        if not self.set_sql:  # required
            message = 'No set statement.'
            self.logger.error(message)
            raise ValueError(message)
        if not self.where_sql:  # required
            message = 'No where statement.'
            self.logger.error(message)
            raise ValueError(message)

        update = sql.SQL("{update} {sets} {where};").format(
            update=self.update_sql,
            sets=self.set_sql,
            where=self.where_sql)

        if not self.values:
            self.values = {}

        return update, self.values

    def build_set_sql(self, values: Dict):
        """
        Builds the SET block with identifiers and named placeholders matching the values.
        The generated SQL-statement and the matching values are stored as class attributes.

        :param values: Values for the new tuple
        :type values: Dict
        """

        val_placeholder = zip(map(sql.Identifier, values), map(sql.Placeholder, values))
        values_sql = sql.SQL(",").join(
            [sql.SQL("{} = {}").format(col, val) for col, val in val_placeholder])
        self.set_sql = sql.SQL('SET {val}').format(val=values_sql)
        if self.values:
            self.values.update(values)
        else:
            self.values = OrderedDict(values)


class DeleteQueryBuilder(QueryBuilder):
    """
    Generating SQL-String for deleting tuples. The SQL-Statements can be written by hand or
    using the functions to replace values with placeholders.

    Example::

        # Example with raw SQL
        qb = DeleteQueryBuilder()
        qb.delete_sql = sql.SQL(
            'DELETE FROM trf.project')
        qb.where_sql = sql.SQL(
            'WHERE pro_id = {}').format(sql.Placeholder('pro_id'))
        qb.values = dict(pro_id=1)
        query, values = qb.delete_query()

        # Example using functions for generating the placeholders
        # (matching the input data)
        qb = DeleteQueryBuilder()
        qb.delete_sql = sql.SQL('DELETE FROM trf.project')
        qb.build_where_sql(dict(pro_id=1))
        query, values = qb.delete_query()

    """

    # DELETE
    delete_sql: 'sql.Composable' = None  # required

    def __init__(self):
        # No inheritance of values to parent object
        self.where_sql = None  # required, no deletion of all tuples allowed
        self.values = OrderedDict()

    def delete_query(self) -> Tuple['sql.Composable', 'OrderedDict']:
        """
        Builds the SQL-Query for deleting a tuple.
        The function returns the parametrized SQL-Query and the matching values-Dictionary.

        :return: Parametrized SQL-Query, values
        :rtype: psycopg2.sql.Composable, OrderedDict
        """
        if not self.delete_sql:  # required
            message = 'No delete statement.'
            self.logger.error(message)
            raise ValueError(message)
        if not self.where_sql:  # required
            message = 'No where statement.'
            self.logger.error(message)
            raise ValueError(message)

        delete = sql.SQL("{delete} {where};").format(
            delete=self.delete_sql,
            where=self.where_sql)

        if not self.values:
            self.values = {}

        return delete, self.values
