
from .base import IfcWriter

class V43IfcWriter(IfcWriter):

    def getVersion(self):
        return "IFC4x3"

    def nttDistanceExpression(self, distanceAlong , offsetLateral , offsetVertical , offsetLongitudinal, alongHorizontal=False):
        raise ValueError('IFC4x3 does no longer support IfcDistanceExpression.')

    def nttSectionedSolidHorizontal(self, directrixId, crossSectionIds, crossSectionPositionIds, fixedAxisVertical=None):
        # See ENTITY IfcSectionedSolidHorizontal and IfcSectionedSolid for args.
        if fixedAxisVertical is not None:
            raise ValueError('IFX4x3 does no longer support IfcSectionedSolidHorizontal.FixedAxisVertical .')
        dirxId = IfcWriter.id(directrixId) # IfcSectionedSolid
        csIds = IfcWriter.ids(crossSectionIds) # IfcSectionedSolid
        cspIds = IfcWriter.ids(crossSectionPositionIds) # IfcSectionedSolidHorizontal
        self.lastRelAggregatesId = self._writeOneLinerOneArg(
            'IFCSECTIONEDSOLIDHORIZONTAL', 
            f"""{dirxId},{csIds},{cspIds}"""
        )

    def nttGeotechnicalAssembly(self, globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag):
        self.lastGeotechnicalAssemblyId = self._nttElement('IFCGEOTECHNICALASSEMBLY', globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag)

    def nttBorehole(self, globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag):
        self.lastBoreholeId = self._nttElement('IFCBOREHOLE', globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag)

    def nttGeoslice(self, globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag):
        self.lastGeosliceId = self._nttElement('IFCGEOSLICE', globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag)

    def nttGeomodel(self, globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag):
        self.lastGeomodelId = self._nttElement('IFCGEOMODEL', globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag)

    def nttGeotechnicalStratum(self, globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag):
        self.lastGeotechnicalStratumId = self._nttElement('IFCGEOTECHNICALSTRATUM', globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag)

    def nttSolidStratum(self, globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag):
        self.lastSolidStratumId = self._nttElement('IFCSOLIDSTRATUM', globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag)

    def nttWaterStratum(self, globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag):
        self.lastWaterStratumId = self._nttElement('IFCWATERSTRATUM', globalUid, ownerHistoryId, name, description, objectType, objectPlacementId, representationId, tag)

    def ncpAxis2placementLinear(self, point, dirZ, dirX):
        self.lastAxis2placementLinearId = self._ncpAxis2placement('IFCAXIS2PLACEMENTLINEAR', point, dirZ, dirX)
