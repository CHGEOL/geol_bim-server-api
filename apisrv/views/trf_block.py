import logging
import string
import random

from flask import request, make_response, jsonify, current_app as app
from flasgger import SwaggerView  # SwaggerView inherits flask.views.MethodView
from psycopg2 import sql, Binary

from io import BytesIO

from ..shared import (HttpStatus, CRUD, CreateQueryBuilder, ReadQueryBuilder,
                      UpdateQueryBuilder, DeleteQueryBuilder, ErrorMessage)
from ..shared import authorization
from ..schemas import BlockSchema

#from apisrv.transformations.vxl2ifc import vxl2ifc
from ..transformations import (createWriterAndBegin, validateProjectConfig, writeAndValidateSurveyPoints, vxlCfgFileNames, gfCfgFileNames, vxl2ifc, bhl2ifc, gf2ifc)


__all__ = ['Block', 'BlockCollection', 'BlockData']


class Block(SwaggerView):
    """
    /transform/blocks/<int:blk_id>
    """

    logger = logging.getLogger('apisrv')
    schema = BlockSchema()
    db = CRUD()

    @app.auth.login_required
    def get(self, blk_id: int):
        """
        Get one single block
        Get data about one single object (filtering parameter: blk_id).
        ---
        operationId: block-id-get
        tags:
        - transform
        parameters:
          - in: path
            name: blk_id
            schema:
              type: integer
              example: 1
            required: true
            description: Filtering criterion
          - in: query
            name: woData
            schema:
              type: boolean
            required: false
            description: Get block with all attributes but data
        responses:
          200:
            description: One single object (block)
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/BlockSchema'
          400:
              $ref: '#/components/responses/bad_request'
          401:
              $ref: '#/components/responses/unauthorized'
          404:
              $ref: '#/components/responses/not_found'
          default:
              $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.not_found_404.value

        request_data = dict(request.args)
        woData = 'woData' in request_data and request_data['woData'] == 'true'

        attr_filter = {self.schema.pk_column: blk_id}

        # Authorize: pro-read
        pro_id = authorization.select_related_pro_id(self.db, {'blk_id': blk_id})
        authorization.check_project_role('pro-read', pro_id)

        try:
            qb = ReadQueryBuilder()

            if woData == False:
              qb.select_sql = sql.SQL(
                  'SELECT blk.*'
              )
            else:
              # Use schema.fields https://marshmallow.readthedocs.io/en/stable/api_reference.html#marshmallow.Schema.fields
              selectStr = ','.join(['blk.'+f.name for f in BlockSchema().shortFieldsList()])
              qb.select_sql = sql.SQL(
                  'SELECT ' + selectStr
              )
            qb.from_sql = sql.SQL(
                'FROM trf.block AS blk'
            )
            qb.where_sql = sql.SQL(
                'WHERE blk_id = {}'
            ).format(sql.Placeholder('blk_id'))

            qb.values = attr_filter
            query, values = qb.read_query()
            message, code = self.db.read(query, values)
            if code != 200:
                message = jsonify(message)
            else:
                message = self.schema.jsonify(message[0], many=False)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements, Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(message, code)

    @app.auth.login_required
    def patch(self, blk_id: int):
        """
        Update one single block
        Update data of one single object (filtering parameter: blk_id).
        Only valid attributes can be updated.

        Type codes:
        * type-pcf: Point config data
        * type-pnt: Point data
        * type-lin: Line data
        * type-acf: Area config data
        * type-ara: Area data
        * type-hdr: Header text
        * type-div: Division text
        State codes:
        * state-ini: Initial state
        * state-upl: Uploading state
        * state-trf: Transforming state
        * state-rdy: Data ready state
        * state-upf: Uploading failed state
        * state-tff: Transforming failed state
        ---
        operationId: block-id-patch
        tags:
          - transform
        parameters:
          - in: path
            name: blk_id
            schema:
              type: integer
              example: 1
            required: true
            description: Filtering criterion
        requestBody:
          required: true
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BlockSchema'
              example: {"blk_hdr": "Test"}
        responses:
          204:
            description: No content, updating the object was successful.
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          404:
            $ref: '#/components/responses/not_found'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.not_found_404.value
        usr = app.auth.current_user()

        # Get the JSON from the request body and validate the data
        request_data = request.get_json()
        request_params = self.schema.parse_value_param(request_data, partial=True)

        attr_values = request_params.values
        pk_dict = request_params.pk_dict
        attr_filter = {self.schema.pk_column: blk_id}  # for selection

        # Authorize: pro-admin
        pk_dict['blk_id'] = blk_id
        pro_id = authorization.select_related_pro_id(self.db, pk_dict)
        authorization.check_project_role('pro-admin', pro_id, usr)

        attr_values['blk_change_user'] = usr.usr_id

        try:
            # Side effect to create IFC
            blockType = attr_values['blk_type_code']
            if blockType in {'bkt-pcf', 'bkt-lin', 'bkt-acf'}:
              # self.sdFxCreatePws(usr, 3)
              # self.sdFxCreateUsers(usr)
              message, code = self.sdFxCreateIfc(usr, pro_id, blk_id, attr_values) # may change attr_values as side fx 

            qb = UpdateQueryBuilder()
            qb.update_sql = sql.SQL('UPDATE trf.block')
            qb.build_set_sql(attr_values)
            qb.build_where_sql(attr_filter)
            query, values = qb.update_query()
            message, code = self.db.update(query, values)

        except Exception as err:  # pragma: no cover

            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = message.to_dict()

        finally:
            return make_response(jsonify(message), code)

    @app.auth.login_required
    def delete(self, blk_id: int):
        """
        Delete one single block.
        Deletes all data of one single object (filtering parameter: blk_id).

        ---
        operationId: block-id-delete
        tags:
          - transform
        parameters:
          - in: path
            name: blk_id
            schema:
              type: integer
              example: 1
            required: true
            description: Filtering criterion
        responses:
          200:
            $ref: '#/components/responses/delete_ok'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          404:
            $ref: '#/components/responses/not_found'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.not_found_404.value

        attr_filter = {self.schema.pk_column: blk_id}

        # Authorize: pro-admin
        pro_id = authorization.select_related_pro_id(self.db, {'blk_id': blk_id})
        authorization.check_project_role('pro-admin', pro_id)

        usr = app.auth.current_user()

        try:

            pro_attr_values = {'pro_output': None, 'pro_change_user': usr.usr_id}
            pro_attr_filter = {'pro_id': pro_id}
            qb = UpdateQueryBuilder()
            qb.update_sql = sql.SQL('UPDATE trf.project')
            qb.build_set_sql(pro_attr_values)
            qb.build_where_sql(pro_attr_filter)
            query, values = qb.update_query()
            message, code = self.db.update(query, values)

            qb = DeleteQueryBuilder()
            qb.delete_sql = sql.SQL('DELETE FROM trf.block')
            qb.where_sql = sql.SQL('WHERE blk_id = {}').format(sql.Placeholder('blk_id'))
            qb.values = attr_filter

            query, values = qb.delete_query()

            message, code = self.db.delete(query, values)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(jsonify(message), code)

    def setUpListOfConfigBlocks(self, message):
      cfgBlocks = []
      for b in message:
        if b['blk_type_code'] == 'bkt-pcf' or b['blk_type_code'] == 'bkt-lin' or b['blk_type_code'] == 'bkt-acf':
          cfgBlocks.append(b)
      return cfgBlocks

    def setUpConfigBlockToListOfDataBlocks(self, cfgBlocks, message):
      mentionedFileName2CfgBlkId = {}
      for cfgBlk in cfgBlocks:
        cfgBlkId = cfgBlk['blk_id']
        cfgFileNames = []
        if cfgBlk['blk_type_code'] == 'bkt-pcf':
          # TODO this opening here can be very expensive if a lot of pure voxel data is in the config xlsx itself
          xlsxStream = BytesIO(cfgBlk['blk_data'])
          cfgFileNames = vxlCfgFileNames(xlsxStream) # xlsx
        elif cfgBlk['blk_type_code'] == 'bkt-acf':
          xlsxStream = BytesIO(cfgBlk['blk_data'])
          cfgFileNames = gfCfgFileNames(xlsxStream) # xlsx
        for fn in cfgFileNames:
          mentionedFileName2CfgBlkId[fn] = cfgBlkId
      cfgBlkId2Blocks = {}
      for b in message: # must iterate over message to keep order of blk_seq
        if b['blk_type_code'] == 'bkt-pnt' or b['blk_type_code'] == 'bkt-ara':
          fn = b['blk_heading']
          if fn in mentionedFileName2CfgBlkId:
            cfgBlkId = mentionedFileName2CfgBlkId[fn]
            if cfgBlkId in cfgBlkId2Blocks:
              cfgBlkId2Blocks[cfgBlkId].append(b)
            else:
              cfgBlkId2Blocks[cfgBlkId] = [b]
          else:
            blockTypeStr = 'GF'
            if b['blk_type_code'] == 'bkt-pnt':
              blockTypeStr = 'Voxel'
            raise ValueError(f"""Die Datei {fn} des {blockTypeStr}-Block wird in keiner Excel Konfiguration erwähnt.""")
      return cfgBlkId2Blocks
    
    def setAllConfigBlocksToState(self, pro_id, cfgBlocks, state):
      if 0 < len(cfgBlocks):
        qb = UpdateQueryBuilder()
        qb.update_sql = sql.SQL('UPDATE trf.block')
        qb.build_set_sql({'blk_state_code': state})
        qb.build_where_sql(
            filter_equal={'blk_pro_id': pro_id},
            filter_in={'blk_type_code': ['bkt-pcf', 'bkt-lin', 'bkt-acf']}
          )
        query, values = qb.update_query()
        message, code = self.db.update(query, values)

    def blockList2FileNameToDataDic(self, blocks):
      blocksDic = {}
      for b in blocks:
        fn = b['blk_heading']
        bData = b['blk_data']
        blocksDic[fn] = bData
      return blocksDic

    def transform2ifc(self, usr, pro_id):

      # Load all blocks of this project
      qb = ReadQueryBuilder()
      qb.select_sql = sql.SQL('SELECT blk.*')
      qb.from_sql = sql.SQL('FROM trf.block AS blk')
      qb.build_where_sql(
          filter_equal={'blk_pro_id': pro_id},
          filter_in={'blk_type_code': ['bkt-pcf', 'bkt-pnt', 'bkt-lin', 'bkt-acf', 'bkt-ara']}
        )
      qb.order_sql = sql.SQL('ORDER BY blk_seq')
      query, values = qb.read_query()
      message, code = self.db.read(query, values, many=True)
      # filter them TODO later this not necessary any more since there are no more title blocks
      if code == 200:

        cfgBlocks = self.setUpListOfConfigBlocks(message)
        if len(cfgBlocks) == 0:
          message = ErrorMessage(code=HttpStatus.bad_request_400.value,
                                     message='Es hat keinen Block mit einer Konfiguration für die Umwandlung nach IFC.',
                                     internal_code='TRF_NO_CONFIG')
          code = message.code
          return message, code
        self.setAllConfigBlocksToState(pro_id, cfgBlocks, 'bks-trf')
        cfgBlkId2Blocks = self.setUpConfigBlockToListOfDataBlocks(cfgBlocks, message)
        ifcw = None
        mainSpId2Row = None
        origShift = None
        mainCfgFn = None
        for cfgBlk in cfgBlocks:
          cfgBlkId = cfgBlk['blk_id']
          blkType = cfgBlk['blk_type_code'] # only possible values: 'bkt-pcf', 'bkt-lin', 'bkt-acf'
          cfgFn = cfgBlk['blk_heading'] # name of the xlsx file
          try:
            proCfgStream = BytesIO(cfgBlk['blk_data']) # xlsx data from db
            if not ifcw:
              mainCfgFn = cfgFn
              # Only the project config of the first config block is used for the IFC file header (and setup)
              ifcw, geoRef, epsgCode, proOrig, applyTranslate, origShift = createWriterAndBegin(proCfgStream)
              mainSpId2Row = writeAndValidateSurveyPoints(proCfgStream, cfgFn, ifcw, origShift)
            else:
              validateProjectConfig(proCfgStream, cfgFn, mainCfgFn, ifcw, geoRef, epsgCode, proOrig, applyTranslate, origShift)
              writeAndValidateSurveyPoints(proCfgStream, cfgFn, ifcw, origShift, mainCfgFn, mainSpId2Row)

            if blkType == 'bkt-pcf':
              dataBlocks = [] # for bkt-pcf filenames may lack if there is only pure voxel data directly in the xlsx
              if cfgBlkId in cfgBlkId2Blocks:
                dataBlocks = cfgBlkId2Blocks[cfgBlkId]
              blocksDic = self.blockList2FileNameToDataDic(dataBlocks) # csv (not xlsx)
              vxl2ifc(ifcw, origShift, proCfgStream, blocksDic)
            elif blkType == 'bkt-lin':
              bhl2ifc(ifcw, origShift, proCfgStream) # proCfg contains also data in the borehole case
            else: # blkType == 'bkt-acf'
              if cfgBlkId not in cfgBlkId2Blocks:
                raise ValueError(f"""In der Excel Konfigurations Datei {cfgFn} des GF-Block sind keine Dateinamen von Daten erwähnt.""")
              blocksDic = self.blockList2FileNameToDataDic(cfgBlkId2Blocks[cfgBlkId]) # dxf, obj, stl or ts (not xlsx)
              gf2ifc(ifcw, origShift, proCfgStream, blocksDic)

          except Exception as err:
            msg = 'Allgemeiner Fehler bei der Umwandlung'
            if err.args and err.args[0]:
              msg = err.args[0]

            # Reset to none transforming state at any rate
            self.setAllConfigBlocksToState(pro_id, cfgBlocks, 'bks-rdy')
            # And the specific config block set to transform failed state
            attr_values = {}
            attr_values['blk_state_code'] = 'bks-tff'
            attr_values['blk_state_message'] = msg
            attr_values['blk_change_user'] = usr.usr_id
            attr_filter = {self.schema.pk_column: cfgBlkId}
            qb = UpdateQueryBuilder()
            qb.update_sql = sql.SQL('UPDATE trf.block')
            qb.build_set_sql(attr_values)
            qb.build_where_sql(attr_filter)
            query, values = qb.update_query()
            message, code = self.db.update(query, values)

            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message.message = msg

            return message, code, cfgBlkId
        
        ifcw.ending()
        ifcStr = ifcw.content()
        self.setAllConfigBlocksToState(pro_id, cfgBlocks, 'bks-rdy')

        return message, code, ifcStr

    def createUser(self, usr, firstName, lastName, email, pwHash, orgName=None):

      if orgName is None:
        orgName = f'Organisation von {firstName} {lastName}'
      if 50 < len(orgName):
        orgName = orgName[:50]

      values = {
        'usr_name': lastName,
        'usr_name_first': firstName,
        'usr_email': email.lower(),
        'usr_pw': pwHash, # app.pwd_context.hash(pw)
        'usr_status': 'active',
        'usr_confirmed_agb': False,
        'usr_confirmed_query': False
      }
      qb = CreateQueryBuilder()
      qb.build_insert_sql(sql.SQL('usr.user'), values)
      qb.build_values_sql(values)
      qb.returning_sql = sql.SQL('RETURNING usr_id')
      query, values = qb.create_query()
      conn, cur = self.db.get_cursor()
      message, code = self.db._create(cur, query, 'usr_id', values)
      userId = message['usr_id']

      qb = ReadQueryBuilder()
      qb.select_sql = sql.SQL('SELECT org.org_id, org.org_name')
      qb.from_sql = sql.SQL('FROM usr.organisation AS org')
      qb.build_where_sql(
          filter_equal={'org_name': orgName}
        )
      query, values = qb.read_query()
      message, code = self.db.read(query, values, many=True)
      if code == 200 and 0 < len(message): # actually len(message) == 1
        orgId = message[0]['org_id']
      else:
        values = {
          'org_name': orgName
        }
        qb = CreateQueryBuilder()
        qb.build_insert_sql(sql.SQL('usr.organisation'), values)
        qb.build_values_sql(values)
        qb.returning_sql = sql.SQL('RETURNING org_id')
        query, values = qb.create_query()
        conn, cur = self.db.get_cursor()
        message, code = self.db._create(cur, query, 'org_id', values)
        orgId = message['org_id']

      values = {
        'uap_usr_id': userId,
        'uap_role_code': 'ros-acon'
      }
      qb = CreateQueryBuilder()
      qb.build_insert_sql(sql.SQL('usr.user_application'), values)
      qb.build_values_sql(values)
      qb.returning_sql = sql.SQL('RETURNING uap_usr_id, uap_role_code')
      query, values = qb.create_query()
      conn, cur = self.db.get_cursor()
      message, code = self.db._create(cur, query, ['uap_usr_id', 'uap_role_code'], values)

      values = {
        'uog_usr_id': userId,
        'uog_org_id': orgId,
        'uog_role_code': 'ros-opl'
      }
      qb = CreateQueryBuilder()
      qb.build_insert_sql(sql.SQL('usr.user_organisation'), values)
      qb.build_values_sql(values)
      qb.returning_sql = sql.SQL('RETURNING uog_usr_id, uog_org_id, uog_role_code')
      query, values = qb.create_query()
      conn, cur = self.db.get_cursor()
      message, code = self.db._create(cur, query, ['uog_usr_id', 'uog_org_id', 'uog_role_code'], values)

      self.logger.info(f'created user {userId} ({firstName}, {lastName})')

      return userId

    # Use this function to create n password and hash pairs (and insert them to the xlsx).
    def sdFxCreatePws(self, usr, n):
      if usr.usr_id == 4:
        i = 0
        while i < n:
          pw = ''.join(random.choice(string.ascii_letters+string.digits) for i in range(8))
          hash = app.pwd_context.hash(pw)
          self.logger.info(f'{pw} {hash}')
          i += 1

    # Use a template like this in xlsx to create the code and paste it into this method:
    # ="self.createUser(usr, '" & A2 & "', '" & B2 & "', '" & C2 & "', '" & G2 & "', '" & D2 & "')"
    def sdFxCreateUsers(self, usr):
      if usr.usr_id == 4:
        # self.createUser(usr, 'Eins', 'Dummy', 'dummy01@fhnw.ch', '...', 'FHNW')
        return

    def sdFxCreateIfc(self, usr, pro_id, blk_id, attr_values):

      # blockType = attr_values['blk_type_code']
      #CFG_BLOCK_TYPES = {'bkt-pcf', 'bkt-lin', 'bkt-acf'}
      # if blockType in CFG_BLOCK_TYPES: # TODO why not do it here? Why do we get a TypeError('cannot unpack non-iterable NoneType object') when checking a set contains a string?
      
        qb = ReadQueryBuilder()
        select_expr = [
            sql.SQL('blk.blk_id'),
            sql.SQL('blk.blk_data'),
            sql.SQL('blk.blk_state_code')]
        from_expr = [sql.SQL('trf.block AS blk')]
        attr_id_filter = {'blk_id': blk_id}
        qb.build_select_sql(sql_exprs=select_expr)
        qb.build_from_sql(sql_exprs=from_expr)
        qb.build_where_sql(attr_id_filter)
        query, values = qb.read_query()
        # Read block
        message, code = self.db.read(query, values)
        if code == 200:

          prevStateCode = message[0]['blk_state_code']
          currentStateCode = attr_values['blk_state_code']

          if prevStateCode != 'bks-trf' and currentStateCode == 'bks-trf':

            message, code, ifcStrOrErrCfgBlkId = self.transform2ifc(usr, pro_id)

            if code == 200:
              ifcStr = ifcStrOrErrCfgBlkId
              outData = ifcStr.encode("ascii", "ignore")
              pro_attr_values = {'pro_output': Binary(outData), 
                              'pro_change_user': usr.usr_id}
              pro_attr_filter = {'pro_id': pro_id}
              qb = UpdateQueryBuilder()
              qb.update_sql = sql.SQL('UPDATE trf.project')
              qb.build_set_sql(pro_attr_values)
              qb.build_where_sql(pro_attr_filter)
              query, values = qb.update_query()
              message, code = self.db.update(query, values)

              attr_values.clear() # attr_values = {}
              attr_values['blk_state_code'] = 'bks-rdy' # on the db already like that, but this is a side effect
              attr_values['blk_state_message'] = None
            else:
              if blk_id == ifcStrOrErrCfgBlkId:
                attr_values.clear() # attr_values = {}
                attr_values['blk_state_code'] = 'bks-tff'
                attr_values['blk_state_message'] = message.message
                message = message.to_dict()
  
            message = jsonify(message)
  
        else: # code != 200
            # Reading the current state of the block failed
            message = jsonify(message)

        return message, code

class BlockData(SwaggerView):
    """
    /transform/blocks/<int:blk_id>/dta
    """

    logger = logging.getLogger('apisrv')
    schema = BlockSchema()
    db = CRUD()

    @app.auth.login_required
    def patch(self, blk_id: int):
        """
        Block-Data upload
        To store an input in the database, the file must be provided.
        The input must not be bigger than 400 KB. The file can be sent with the mimetype multipart/form-data blk_data.

        To remove an input, use the endpoint PATCH /transform/blocks/<id>
        ---
        operationId: block-id-dta-patch
        tags:
          - block
        parameters:
          - in: path
            name: blk_id
            schema:
              type: integer
              example: 1
            required: true
            description: Block-ID
        requestBody:
          required: true
          content:
            multipart/form-data:
              schema:
                type: object
                properties:
                  blk_data:
                    type: string
                    format: binary
        responses:
          204:
            description: No content, updating the object was successful.
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          404:
            $ref: '#/components/responses/not_found'
          default:
            $ref: '#/components/responses/unexpected_error'
        """

        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.bad_request_400.value
        usr = app.auth.current_user()

        # Authorize: select, check usr (usr_org_id is dump_only)
        authorization.select_usr_org_role_id(self.db, 'user', {'usr_id': usr.usr_id})
        if not usr.usr_app_admin:
            authorization.check_usr_id(usr.usr_id)

        # Get config file
        # block_type = .. many lines starting with qb = ReadQueryBuilder() to read the block
        ipt_data = self.schema.get_data_from_request(dta_attr='blk_data')
        fileName = request.form['blk_heading']
        mimeType = request.form['blk_mime_type']
        # The request.get_json() does not work and is None

        # Validate file and get data type
        # TODO do something like: data_type = self.schema.validate_data(block_type, ipt_data)

        try:
          # Build Query with the new values and filter by user-id
          attr_values = {'blk_data': Binary(ipt_data), 
                          'blk_change_user': usr.usr_id}
          attr_filter = {'blk_id': blk_id}
          qb = UpdateQueryBuilder()
          qb.update_sql = sql.SQL('UPDATE trf.block')
          qb.build_set_sql(attr_values)
          qb.build_where_sql(attr_filter)
          query, values = qb.update_query()
          message, code = self.db.update(query, values)

          self.sdFxHandleUpload(usr, blk_id, fileName, ipt_data)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(jsonify(message), code)
    
    def sdFxHandleUpload(self, usr, blockId, fileName, data):
      qb = ReadQueryBuilder()
      select_expr = [
          sql.SQL('blk.blk_id'),
          sql.SQL('blk.blk_pro_id'),
          sql.SQL('blk.blk_seq'),
          sql.SQL('blk.blk_state_code'),
          sql.SQL('blk.blk_type_code')]
      from_expr = [sql.SQL('trf.block AS blk')]
      attr_id_filter = {'blk_id': blockId}
      qb.build_select_sql(sql_exprs=select_expr)
      qb.build_from_sql(sql_exprs=from_expr)
      qb.build_where_sql(attr_id_filter)
      query, values = qb.read_query()
      # Read block
      message, code = self.db.read(query, values)
      if code == 200:
        cfgBlk = message[0]
        pro_id = cfgBlk['blk_pro_id']

        pro_attr_values = {'pro_output': None, 'pro_change_user': usr.usr_id}
        pro_attr_filter = {'pro_id': pro_id}
        qb = UpdateQueryBuilder()
        qb.update_sql = sql.SQL('UPDATE trf.project')
        qb.build_set_sql(pro_attr_values)
        qb.build_where_sql(pro_attr_filter)
        query, values = qb.update_query()
        message, code = self.db.update(query, values)

        attr_values = {}
        attr_values['blk_state_code'] = 'bks-rdy'
        attr_values['blk_heading'] = fileName
        attr_values['blk_state_message'] = ''
        attr_values['blk_state_percent'] = 0
        attr_values['blk_change_user'] = usr.usr_id
        attr_filter = {self.schema.pk_column: blockId}
        qb = UpdateQueryBuilder()
        qb.update_sql = sql.SQL('UPDATE trf.block')
        qb.build_set_sql(attr_values)
        qb.build_where_sql(attr_filter)
        query, values = qb.update_query()
        message, code = self.db.update(query, values)

        # Use data to create more blocks in bkt-acf and bkt-pcf case according to what is mentioned in xlsx.
        cfgFileNames = []
        if cfgBlk['blk_type_code'] == 'bkt-pcf':
          # TODO this opening here can be very expensive if a lot of pure voxel data is in the config xlsx itself
          xlsxStream = BytesIO(data)
          cfgFileNames = vxlCfgFileNames(xlsxStream) # xlsx
          dtaTypeCode = 'bkt-pnt'
        elif cfgBlk['blk_type_code'] == 'bkt-acf':
          xlsxStream = BytesIO(data)
          cfgFileNames = gfCfgFileNames(xlsxStream) # xlsx
          dtaTypeCode = 'bkt-ara'

        nofFiles = len(cfgFileNames)
        if 0 < nofFiles:
          # Load all blocks of this project
          qb = ReadQueryBuilder()
          qb.select_sql = sql.SQL('SELECT blk.blk_id, blk.blk_seq, blk.blk_type_code, blk.blk_heading, blk.blk_state_message')
          qb.from_sql = sql.SQL('FROM trf.block AS blk')
          qb.build_where_sql(
              filter_equal={'blk_pro_id': pro_id},
              filter_in={'blk_type_code': ['bkt-pcf', 'bkt-pnt', 'bkt-lin', 'bkt-acf', 'bkt-ara']}
            )
          # filter them TODO later this not necessary any more since there are no more title blocks
          qb.order_sql = sql.SQL('ORDER BY blk_seq')
          query, values = qb.read_query()
          message, code = self.db.read(query, values, many=True)
          if code == 200:
            fnExists = set()
            msgExists = set()
            for blk in message:
              if blk['blk_heading'] is not None and 0 < len(blk['blk_heading']):
                fnExists.add(blk['blk_heading'])
              if blk['blk_state_message'] is not None and 0 < len(blk['blk_state_message']):
                msgExists.add(blk['blk_state_message'])
            missingFileNames = []
            for fn in cfgFileNames:
              msg = BlockData.createMsg(fn)
              if fn not in fnExists and msg not in msgExists:
                missingFileNames.append(fn)
            if 0 < len(missingFileNames):
              lastOneIsThisCfg = False
              nextAfterCfgBlk = None
              for blk in message:
                if lastOneIsThisCfg:
                  nextAfterCfgBlk = blk
                  break
                if blk['blk_id'] == cfgBlk['blk_id']:
                  lastOneIsThisCfg = True
              range = 1000
              if nextAfterCfgBlk:
                range = nextAfterCfgBlk['blk_seq'] - cfgBlk['blk_seq']
              seqStep = range // (nofFiles + 1)
              blkSeq = cfgBlk['blk_seq']
              for fn in missingFileNames:
                blkSeq += seqStep
                self.createBlockForFileName(usr, pro_id, dtaTypeCode, fn, blkSeq)

    @staticmethod
    def createMsg(fileName):
      return f'{fileName} hochladen'

    def createBlockForFileName(self, usr, proId, typeCode, fileName, seq):
      attr_values = {}
      attr_values['blk_pro_id'] = proId
      attr_values['blk_type_code'] = typeCode
      attr_values['blk_state_code'] = 'bks-ini'
      attr_values['blk_state_message'] = BlockData.createMsg(fileName) # not blk_heading
      attr_values['blk_state_percent'] = 0
      attr_values['blk_create_user'] = usr.usr_id
      attr_values['blk_change_user'] = usr.usr_id
      attr_values['blk_seq'] = seq
      # attr_values['blk_change_user'] = usr.usr_id
      qb = CreateQueryBuilder()
      qb.build_insert_sql(sql.SQL('trf.block'), attr_values)
      qb.build_values_sql(attr_values)
      qb.returning_sql = sql.SQL('RETURNING blk_id')
      query, values = qb.create_query()
      message, code = self.db.create(query, 'blk_id', values)


class BlockCollection(SwaggerView):
    """
    /transform/blocks
    """

    logger = logging.getLogger('apisrv')
    schema = BlockSchema()
    db = CRUD()

    @app.auth.login_required
    def get(self):
        """
        Get all blocks of a project.
        Blocks can be filtered with parameter [attr_filter], e.g. attributename=value.
        By default all blocks come without data when loaded in a row.
        If no filtering criteria are defined, then all objects are returned.
        Related activities can be queried as nested objects.
        ---
        operationId: block-get
        tags:
          - transform
        parameters:
          - in: query
            name: blk_pro_id
            required: false
            schema:
              type: integer
            description: Filtering criteria, e.g. filtering by the project id
        responses:
          200:
            description: Multiple objects (blocks)
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/BlockSchema'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.bad_request_400.value
        usr = app.auth.current_user()

        request_data = dict(request.args)  # Arguments from URL

        # Valid Parameter: attr_filter
        request_params = self.schema.parse_filter_param(
            request_data, attr_filter=True, required_pk=True)

        attr_filter = request_params.values
        pk_dict = request_params.pk_dict

        # Authorize: pro-read (pk/fk required)
        pro_id = authorization.select_related_pro_id(self.db, pk_dict)
        authorization.check_project_role('pro-read', pro_id, usr)

        try:
            qb = ReadQueryBuilder()

            #'SELECT blk.*'
            # Use schema.fields https://marshmallow.readthedocs.io/en/stable/api_reference.html#marshmallow.Schema.fields
            selectStr = ','.join(['blk.'+f.name for f in BlockSchema().shortFieldsList()])
            qb.select_sql = sql.SQL(
                'SELECT ' + selectStr
            )
            qb.from_sql = sql.SQL(
                'FROM trf.block AS blk'
            )
            qb.build_where_sql(filter_equal=attr_filter,
                               filter_in={'blk_pro_id': usr.usr_pro_read})
            qb.order_sql = sql.SQL(
                'ORDER BY blk_seq')

            query, values = qb.read_query()

            # Read blocks
            message, code = self.db.read(query, values, many=True)

            if code != 200:
                message = jsonify(message)
            else:
                message = self.schema.jsonify(message, many=True)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(message, code)

    @app.auth.login_required
    def post(self):
        """
        Create a new block.
        Creating a new object will automatically produce a new id. The attributes must be valid.

        Type codes:
        * type-pcf: Point config data
        * type-pnt: Point data
        * type-lin: Line data
        * type-acf: Area config data
        * type-ara: Area data
        * type-hdr: Header text
        * type-div: Division text
        State codes:
        * state-ini: Initial state
        * state-upl: Uploading state
        * state-trf: Transforming state
        * state-rdy: Data ready state
        * state-upf: Uploading failed state
        * state-tff: Transforming failed state
        ---
        operationId: block-post
        tags:
          - transform
        requestBody:
          required: true
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BlockSchema'
              example: {"blk_type_code": "bkt-lin", "blk_pro_id": 1, "blk_state_code": "bks-ini"}
        responses:
          201:
            description: Created.
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/ID'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.bad_request_400.value
        usr = app.auth.current_user()

        # Get the JSON from the request body and validate the data
        request_data = request.get_json()
        request_params = self.schema.parse_value_param(request_data, partial=('blk_id',))
        values = request_params.values
        pk_dict = request_params.pk_dict

        # Authorize: pro-admin
        pro_id = authorization.select_related_pro_id(self.db, pk_dict)
        authorization.check_project_role('pro-admin', pro_id, usr)

        try:  # Insert One Object
            values['blk_create_user'] = usr.usr_id

            qb = CreateQueryBuilder()
            # INSERT
            qb.build_insert_sql(sql.SQL('trf.block'), values)
            # VALUES
            qb.build_values_sql(values)
            qb.returning_sql = sql.SQL('RETURNING blk_id')

            query, values = qb.create_query()

            message, code = self.db.create(query, 'blk_id', values)
            message = jsonify(message)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(message, code)
