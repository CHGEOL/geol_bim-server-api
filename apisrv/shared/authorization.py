
from functools import wraps
from typing import Callable, TYPE_CHECKING, Dict, List, Any, Union, Tuple, Set
import logging

from flask import current_app as app
import werkzeug.exceptions
from psycopg2 import sql


if TYPE_CHECKING:
    # Import only for the type-checking, not on runtime
    from ..models import CurrentUserModel
    from .crud import CRUD


logger = logging.getLogger('apisrv')


def _get_current_usr() -> 'CurrentUserModel':
    """
    Get the current user. If there is no logged-in user, it raises an InternalServerError.

    .. warning::
           Use this function only for authorized endpoints
           (protected with the login-required decorator).

    :return: The current logged-in user.
    """
    # Load the current-user
    current_user = app.auth.current_user()
    if not current_user:
        logger.error("A logged-in user is required: "
                     "Use the login-decorator to protect the resource.")
        err = app.config['DEFAULT_MESSAGES']['INT_ERR']
        raise werkzeug.exceptions.InternalServerError(err)
    return current_user


def check_application_role(role: str,
                           current_user: 'CurrentUserModel' = None) -> 'CurrentUserModel':
    """
    Compares the required role to the role of the currently logged-in user.
    If the User does not have the required role, an Unauthorized-Error is raised.
    If no current user is given, it will be fetched from the app-context.

    :param role: The required permission: one of ``'app-admin', 'app-conn'``
    :param current_user: Currently authenticated user as CurrentUserModel
    :return: CurrentUserModel
    """
    # Load the current-user
    if not current_user:
        current_user = _get_current_usr()

    if role == 'app-admin':
        if not current_user.usr_app_admin:
            # Not authorized to access this resource (project)
            err = app.config['DEFAULT_MESSAGES']['AUTH_APP']
            raise werkzeug.exceptions.Unauthorized(err)
    elif role == 'app-conn':
        if not current_user.usr_app_conn:
            # Not authorized to access this resource (project)
            err = app.config['DEFAULT_MESSAGES']['AUTH_APP']
            raise werkzeug.exceptions.Unauthorized(err)
    else:
        logger.error("The role must be one of ['app-admin', 'app-conn']")
        err = app.config['DEFAULT_MESSAGES']['INT_ERR']
        raise werkzeug.exceptions.InternalServerError(err)

    return current_user


def application_role_required(role: str) -> Callable:
    """
    *Decorator:* Compares the required role to the role of the currently logged-in user
    with the function
    :func:`check_application_role() <apisrv.shared.authorization.check_application_role>`

    Example::

        @app.auth.login_required
        @authorization.application_role_required(role='app-admin')
        def post(self):
            ...

    :param role: One role, ``'app-admin', 'app-conn'``
    :return: Continues with the decorated function
    """
    # used for the decorator with arguments
    def role_required_internal(func: Callable) -> Callable:
        @wraps(func)
        def decorated_function(*args, **kwargs) -> Callable:

            # Check the role, it raises an Unauthorized-Error on fail
            check_application_role(role)
            # Continue with the function
            return func(*args, **kwargs)
        return decorated_function
    return role_required_internal


def _list_of_dict2dict_of_list(list_of_dicts: List[Dict[str, Any]]) -> Dict[str, List[Any]]:
    """
    Merges the attributes of a collection of objects to a dictionary with tuples of the values.
    [{k1: v11, k2: v21}, {k1: v21, k2: v22}] to {k1: [v11, v21], k2: [v21, v22]}

    :param list_of_dicts: List of dictionaries
    :return: dictionary with lists of the values
    """
    # Find all keys
    dict_of_lists = {k: list() for obj in list_of_dicts for k in obj}

    # Append values to the lists
    for obj in list_of_dicts:
        for k in dict_of_lists:
            # add new values to list (skip multiple)
            if k in obj and obj[k] not in dict_of_lists[k]:
                dict_of_lists[k].append(obj[k])

    return dict_of_lists


def select_usr_org_role_id(db: 'CRUD', table: str, pk_dict: Dict,
                           fk_only: bool = False) -> Dict[str, List[int]]:
    """
    Select the primary keys in one table (``table``) for the given conditions (``pk_dict``)
    and returns a dictionary with the pk's as keys and a list of unique values as values
    (e.g. ``{'uog_usr_id': [11, 12], 'uog_org_id': [1]}``).
    The function is used for authorizing on the following tables:

    * project
    * organisation
    * user (uap_role_code='ros-acon' automatically added)
    * user_organisation
    * user_project
    * user_application

    :param db: Database-connection
    :param table: table to select from, one of the listed above
    :param pk_dict: Dictionary with the name and values of the primary keys to check
    :param fk_only: If only fk's are selected, the error-code is
                    BE_ITG_FK if no related object was found (else, it is NF_OBJ
    :return: dictionary with the resulting values
    :raises NF_OBJ: NotFount (NF_OBJ): If no tuples were found and ``fk_only=False``
    :raises BE_ITG_FK: BadRequest (BE_ITG_FK): If no tuples were found and ``fk_only=True``
    """
    pks = pk_dict.copy()
    # Only select connected users
    if table == 'user':
        # Only connected users (only to be shure)
        pks['role_code'] = 'ros-acon'

    if not pks:
        err = app.config['DEFAULT_MESSAGES']['AUTH_ORG']
        raise werkzeug.exceptions.Unauthorized(err)

    # Build the SQL-Query
    filt = sql.SQL(' AND ').join(  # Join the conditions for all given pk
        [sql.SQL("{}={}").format(
            sql.Identifier(k), sql.Placeholder()
        ) for k, _ in pks.items()])

    # map the SQL-Query (value) to the table (key)
    table_dict = {
        'project': sql.SQL(
            'SELECT pro.pro_id FROM trf.project AS pro WHERE {};'
        ).format(filt),
        'organisation': sql.SQL(
            'SELECT org.org_id FROM usr.organisation AS org WHERE {};'
        ).format(filt),
        'user': sql.SQL(
            'SELECT usr_id FROM usr.v_user_roles WHERE {};'
        ).format(filt),
        'user_organisation': sql.SQL(
            'SELECT uog.uog_usr_id, uog.uog_org_id FROM usr.user_organisation AS uog WHERE {};'
        ).format(filt),
        'user_project': sql.SQL(
            'SELECT upr.upr_usr_id, upr.upr_pro_id FROM usr.user_project AS upr WHERE {};'
        ).format(filt),
        'user_application': sql.SQL(
            'SELECT uap.uap_usr_id FROM usr.user_application AS uap WHERE {};'
        ).format(filt)
    }
    if table not in table_dict:
        logger.error("The table must be one of 'project', 'user', 'organisation', "
                     "'user_organisation', 'user_project', 'user_application'.")
        err = app.config['DEFAULT_MESSAGES']['INT_ERR']
        raise werkzeug.exceptions.InternalServerError(err)

    query = table_dict[table]
    values = list(pks.values())
    message, code = db.read(query, values)

    if code == 200:  # good
        return _list_of_dict2dict_of_list(message)

    else:
        if fk_only:
            err = app.config['DEFAULT_MESSAGES']['BE_ITG_FK']
            raise werkzeug.exceptions.BadRequest(description=err)
        else:
            err = app.config['DEFAULT_MESSAGES']['NF_OBJ']
            raise werkzeug.exceptions.NotFound(err)


def check_organisation_role(role: str, organisation_id: int,
                            current_user: 'CurrentUserModel' = None) -> 'CurrentUserModel':
    """
    Compares the required role to the roles of the currently logged-in user.
    If no current user is given, it will be fetched from the app-context.
    The required role can be given as a string (only one role is valid).
    It is not intended, that multiple roles are required (AND)
    or one of a list of roles is required (OR).

    If the User does not have the required role, an Unauthorized-Error is raised.

    Example::

        usr = CurrentUserModel(usr_org_admin=[1,], usr_org_pl=[1,], usr_org_emp=[1, ])
        # The user has the admin permission
        check_organisation_role('org-admin', 1, usr)
        # The user has the project leader permission
        check_organisation_role('org-pl', 1, usr)
        # Raises an error, not an employee
        check_organisation_role('org-emp', 666, usr)

    :param role: The required permission: one of ``'org-admin', 'org-pl', 'org-emp', org-pers'``
    :param organisation_id: The id of the organisation
    :param current_user: Currently authenticated user as CurrentUserModel
    :return: CurrentUserModel
    """
    # Load the current-user
    if not current_user:
        current_user = _get_current_usr()
    if role == 'org-admin':
        if int(organisation_id) not in current_user.usr_org_admin:
            # Not authorized to access this resource (project)
            err = app.config['DEFAULT_MESSAGES']['AUTH_ORG']
            raise werkzeug.exceptions.Unauthorized(err)

    elif role == 'org-pl':
        if int(organisation_id) not in current_user.usr_org_pl:
            # Not authorized to access this resource (project)
            err = app.config['DEFAULT_MESSAGES']['AUTH_ORG']
            raise werkzeug.exceptions.Unauthorized(err)

    elif role == 'org-emp':
        if int(organisation_id) not in current_user.usr_org_emp:
            # Not authorized to access this resource (project)
            err = app.config['DEFAULT_MESSAGES']['AUTH_ORG']
            raise werkzeug.exceptions.Unauthorized(err)

    elif role == 'org-pers':
        if int(organisation_id) not in current_user.usr_org_pers:
            # Not authorized to access this resource (project)
            err = app.config['DEFAULT_MESSAGES']['AUTH_ORG']
            raise werkzeug.exceptions.Unauthorized(err)

    else:
        logger.error("The role must be one of ['org-admin', 'org-pl', 'org-emp']")
        err = app.config['DEFAULT_MESSAGES']['INT_ERR']
        raise werkzeug.exceptions.InternalServerError(err)

    return current_user


def check_organisation_roles_or(roles: List[str], organisation_id: int,
                                current_user: 'CurrentUserModel' = None) -> 'CurrentUserModel':
    """
    Compares the required roles to the roles of the currently logged-in user.
    The required roles are checked with the roles of the current logged-in user.
    The current logged-in user need at minimum one of the required roles (OR).

    If the User does not have any of the required roles, an Unauthorized-Error is raised.

    Example::

        usr = CurrentUserModel(usr_org_pers=1, usr_org_pl=[1,])
        # The user has the personal organisation permission
        check_organisation_roles_or(['org-pers', 'org-emp'], 1, usr)

    :param roles: The required roles: many of ``'org-admin', 'org-pl', 'org-emp', org-pers'``
    :param organisation_id: The id of the organisation
    :param current_user: Currently authenticated user as CurrentUserModel
    :return: CurrentUserModel
    """
    granted = False
    for role in roles:
        try:
            if granted:
                return current_user
            check_organisation_role(role, organisation_id, current_user)
            granted = True
        except werkzeug.exceptions.Unauthorized:
            pass  # catch and check the next role
    if granted:
        return current_user
    else:
        # Not authorized to access this resource (project)
        err = app.config['DEFAULT_MESSAGES']['AUTH_ORG']
        raise werkzeug.exceptions.Unauthorized(err)


def select_related_pro_id(db: 'CRUD', pk_dict: Dict[str, Union[int, List, Tuple, Set]],
                          many: bool = False) -> Union[int, List]:
    """
    Selects and returns the unique project-id for the given primary keys of any table,
    related to the project (transform-schema and the organisation).

    1. Select the related project id to the given primary key(s)
    2. Only one related project allowed: all given keys must belong to the same project
       (except ``many=True`` on special endpoints)
    3. Return the project-ID or raise an Error (Not Found)

    :param db: Database-connection
    :param pk_dict: Dictionary with the name and values of the primary keys to check
    :param many: If many project-ids are valid. Only used in special cases (get projects)
    :return: ID of the project
    :raises Unauthorized (AUTH_PRO_MULT): If multiple project-ids were found
    :raises NotFount (NF_OBJ): If no project-id was found

    """
    if not pk_dict:
        err = app.config['DEFAULT_MESSAGES']['AUTH_PRO']
        raise werkzeug.exceptions.Unauthorized(err)

    # Map the primary keys to the view or table
    pk_to_view = {
        'pro_id': sql.Identifier('trf', 'project'),
        'org_id': sql.Identifier('trf', 'v_auth_organisation'),
        'blk_id': sql.Identifier('trf', 'v_auth_block')
    }

    # Build the SQL-Query
    q_list = []
    values = []
    for pk, val in pk_dict.items():
        if not val:  # skip None values (optional fk)
            pass
        elif isinstance(val, (int, str)):
            q = sql.SQL(
                'SELECT pro_id FROM {view} WHERE {pk_column}={pk}'
            ).format(view=pk_to_view[pk], pk_column=sql.Identifier(pk), pk=sql.Placeholder())
            q_list.append(q)
            values.append(val)
        elif isinstance(val, (list, tuple, set)):
            q = sql.SQL(
                'SELECT pro_id FROM {view} WHERE {pk_column} = ANY ({pk})'
            ).format(view=pk_to_view[pk], pk_column=sql.Identifier(pk), pk=sql.Placeholder())
            q_list.append(q)
            values.append(list(val))
        else:
            logger.error('Invalid type (int/str OR List/Tuple/Set) as values in the pk_dict.')
            raise werkzeug.exceptions.InternalServerError()
    # Combine the queries with UNION
    query = sql.SQL(' UNION ').join(q_list)

    message, code = db.read(query, values)

    if code == 200:
        # related project(s) found for the foreign key(s)
        # the UNION in the query merges the result to unique values
        pro_ids = list(d['pro_id'] for d in message)
        if many:
            return pro_ids
        # 1 Project ID -> good
        if len(pro_ids) == 1:
            # check_project_role(role, pro_ids[0], current_user)
            return int(pro_ids[0])
        # Multiple Project ID -> not good
        else:
            err = app.config['DEFAULT_MESSAGES']['AUTH_PRO_MULT']
            raise werkzeug.exceptions.Unauthorized(err)

    else:  # No Project ID found -> pk / fk does not exist
        err = app.config['DEFAULT_MESSAGES']['NF_OBJ']
        raise werkzeug.exceptions.NotFound(err)


def check_project_role(role: str, project_id: int,
                       current_user: 'CurrentUserModel' = None) -> 'CurrentUserModel':
    """
    Compares the required permission for a specific project
    to the projects assigned to the currently logged-in user.
    If no current user is given, it will be fetched from the app-context.

    If the User does not have the permission, an Unauthorized-Error is raised.

    Example::

        usr = CurrentUserModel(usr_pro_read=[1, 2, 3], usr_pro_write=[1,], usr_pro_admin=[])
        # The user has the reading permission
        check_project_role('pro-read', 1, usr)
        # The user has the writing permission
        check_project_role('pro-write', 1, usr)
        # Raises an error, not a project admin
        check_project_role('pro-admin', 666, usr)

    :param role: The required role: one of ``'pro-read', 'pro-write', 'pro-admin'``
    :param project_id: The id of the project
    :param current_user: Currently authenticated user as CurrentUserModel
    :return: CurrentUserModel
    :raises Unauthorized (AUTH_PRO): If the user has not the permission
    :raises InternalServerError (INT_ERR): Invalid role name
    """
    # Load the current-user
    if not current_user:
        current_user = _get_current_usr()

    if role == 'pro-read':
        if project_id not in current_user.usr_pro_read:
            # Not authorized to access this resource (project)
            err = app.config['DEFAULT_MESSAGES']['AUTH_PRO']
            raise werkzeug.exceptions.Unauthorized(description=err)

    elif role == 'pro-write':
        if project_id not in current_user.usr_pro_write:
            # Not authorized to access this resource (project)
            err = app.config['DEFAULT_MESSAGES']['AUTH_PRO']
            raise werkzeug.exceptions.Unauthorized(description=err)

    elif role == 'pro-admin':
        if project_id not in current_user.usr_pro_admin:
            # Not authorized to access this resource (project)
            err = app.config['DEFAULT_MESSAGES']['AUTH_PRO']
            raise werkzeug.exceptions.Unauthorized(description=err)

    else:
        logger.error("The role must be one of ['pro-read', 'pro-write', 'pro-admin]")
        err = app.config['DEFAULT_MESSAGES']['INT_ERR']
        raise werkzeug.exceptions.InternalServerError(description=err)
    return current_user


def check_usr_id(usr_id: int, current_user: 'CurrentUserModel' = None) -> 'CurrentUserModel':
    """
    Compares the required user-id to the id of the currently logged-in user.
    If no current user is given, it will be fetched from the app-context.
    It returns the current-user or raises an Exception, if the ids don't match.

    :param usr_id: User-ID to check
    :param current_user: Currently authenticated user as CurrentUserModel
    :return: CurrentUserModel
    """
    # Load the current-user
    if not current_user:
        current_user = _get_current_usr()
    if usr_id == current_user.usr_id:
        return current_user
    else:
        # Not authorized to access this resource (wrong user)
        err = app.config['DEFAULT_MESSAGES']['AUTH_USR']
        raise werkzeug.exceptions.Unauthorized(description=err)
