
import logging
from datetime import datetime, timedelta
from typing import Dict, List, Tuple, TYPE_CHECKING, Union, Optional, Set

from flask import request, make_response, jsonify, current_app as app
from flasgger import SwaggerView  # SwaggerView inherits flask.views.MethodView
from psycopg2 import sql, Binary
import psycopg2
from marshmallow import EXCLUDE
import werkzeug.exceptions
import random

from ..shared import (HttpStatus, NestedSchema, CRUD, CreateQueryBuilder, ReadQueryBuilder,
                      UpdateQueryBuilder)
from ..shared import authorization
from ..schemas import ProjectSchema, ProjectCopySchema, BlockSchema

if TYPE_CHECKING:
    # Import only for the type-checking, not on runtime
    import psycopg2.extensions

__all__ = ['Project', 'ProjectCollection', 'ProjectCopy']


ARCHIVE_AFTER_DAYS = 30

class Project(SwaggerView):
    """
    /transform/projects/<int:pro_id>
    """

    logger = logging.getLogger('apisrv')
    schema = ProjectSchema()
    db = CRUD()

    @app.auth.login_required
    def get(self, pro_id: int):
        """
        Get one single project.
        Get data about one single object (filtering parameter: pro_id).

        operationId: project-id-get
        tags:
          - transform
        parameters:
          - in: path
            name: pro_id
            schema:
              type: integer
              example: 1
            required: true
            description: Filtering criterion
          - in: query
            name: nested
            schema:
              type: string
              example: 'blocks'
            required: false
            description: Nesting objects in an (optional) attribute. \
              Valid is 'blocks' and the nested blocks all come without the data field populated.
        responses:
          200:
            description: One single object (project)
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/ProjectSchema'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          404:
            $ref: '#/components/responses/not_found'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.not_found_404.value
        usr = app.auth.current_user()

        request_data = dict(request.args)  # Arguments from URL

        # Valid Parameter: none
        request_params = self.schema.parse_filter_param(request_data, nested=True)

        nested = request_params.nested

        attr_filter = {'pro_id': pro_id}

        # Authorize: pro-read
        pro_id = authorization.select_related_pro_id(self.db, {'pro_id': pro_id})
        authorization.check_project_role('pro-read', pro_id, usr)

        try:
            nested_schemas = list()  # If there are nested objects

            qb = ReadQueryBuilder()

            # Prepare iterables, sql-expressions
            # Select a from list of sql-expressions
            select_expr = [
                sql.SQL('pro.*'),
                sql.SQL('org.org_name AS pro_org_name'),
                sql.SQL('CASE WHEN '
                        '(SELECT uog.uog_role_code FROM usr.user_organisation uog '
                        "WHERE uog.uog_org_id = org.org_id and uog.uog_role_code = 'ros-oprs') "
                        'IS NOT NULL THEN TRUE ELSE FALSE END pro_pers')]
            # From a list of sql-expressions
            from_expr = [
                sql.SQL('trf.project AS pro'),
                sql.SQL('LEFT JOIN usr.organisation AS org ON org.org_id = pro.pro_org_id')]
            order_expr = [
                sql.SQL('pro_name')]

            if 'blocks' in nested:
                blkScm = BlockSchema()
                # Add information for dumping
                nested_schemas.append(NestedSchema(child_fk='blk_pro_id',
                                                   nested_schema=blkScm,
                                                   nested_field_name='pro_blk_nested'))
                # Add select-expressions
                selectStr = ','.join(['blk.'+f.name for f in blkScm.shortFieldsList()])
                select_expr.append(sql.SQL(selectStr))

                # join to trf.block
                from_expr.append(sql.SQL(
                    'LEFT JOIN trf.block AS blk ON blk.blk_pro_id = pro.pro_id'))

                order_expr.append(
                    sql.SQL('blk_seq'))

            # build sql
            qb.build_select_sql(sql_exprs=select_expr)
            qb.build_from_sql(sql_exprs=from_expr)
            qb.build_where_sql(filter_equal=attr_filter)
            qb.build_order_sql(sql_exprs=order_expr)
            query, values = qb.read_query()

            # Read Projects
            message, code = self.db.read(query, values)

            if code != 200:
                message = jsonify(message)

            else:
                if nested_schemas:
                    # one-to-many relation on blocks
                    message = self.schema.dump_nested_collection(message, nested_schemas)
                    # Do not use schema.jsonify, datetime serialization causes trouble
                    message = jsonify(message[0])
                else:
                    message = self.schema.jsonify(message[0], many=False)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(message, code)

    @app.auth.login_required
    def patch(self, pro_id: int):
        """
        Update one single project.
        Update data of one single object. Only valid attributes can be updated.

        Only project-admins can update a project.
        Updating the foreign key to the organisation is only possible for application admins.
        ---
        operationId: project-id-patch
        tags:
          - transform
        parameters:
          - in: path
            name: pro_id
            schema:
              type: integer
              example: 1
            required: true
            description: Filtering criterion
        requestBody:
          required: true
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProjectSchema'
              example: {"pro_desc": "Test"}
        responses:
          204:
            description: No content, updating the object was successful.
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          404:
            $ref: '#/components/responses/not_found'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.not_found_404.value
        usr = app.auth.current_user()

        # Get the JSON from the request body and validate the data
        request_data = request.get_json()
        request_params = self.schema.parse_value_param(request_data, partial=True)
        attr_values = request_params.values

        attr_filter = {self.schema.pk_column: pro_id}  # for selection

        # Authorize: pro-admin
        pro_id = authorization.select_related_pro_id(self.db, {'pro_id': pro_id})
        authorization.check_project_role('pro-admin', pro_id, usr)

        # Only app-admins can move projects from one organisation to another
        # or change the reference project
        if not usr.usr_app_admin:
            attr_values.pop('pro_org_id', None)
            attr_values.pop('pro_pro_id', None)
            # if no other attributes to patch -> raise error
            if not attr_values:
                err = app.config['DEFAULT_MESSAGES']['INP_VAL_ND']
                return make_response(jsonify(err), err.code)
        try:
            attr_values['pro_change_user'] = usr.usr_id

            qb = UpdateQueryBuilder()
            qb.update_sql = sql.SQL('UPDATE trf.project')
            qb.build_set_sql(attr_values)
            qb.build_where_sql(attr_filter, filter_in={'pro_id': usr.usr_pro_admin})

            query, values = qb.update_query()

            message, code = self.db.update(query, values)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(jsonify(message), code)

    @app.auth.login_required
    def delete(self, pro_id: int):
        """
        Delete one single project and all the related roles (user_project).
        Deletes all data of one single object (pro_id).

        Only project-admins can delete a project.
        ---
        operationId: project-id-delete
        tags:
          - transform
        parameters:
          - in: path
            name: pro_id
            schema:
              type: integer
              example: 1
            required: true
            description: Filtering criterion
        responses:
          200:
            $ref: '#/components/responses/delete_ok'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          404:
            $ref: '#/components/responses/not_found'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.not_found_404.value
        usr = app.auth.current_user()

        # Authorize: pro-admin
        pro_id = authorization.select_related_pro_id(self.db, {'pro_id': pro_id})
        authorization.check_project_role('pro-admin', pro_id, usr)

        try:
            query = sql.SQL(
                'DELETE FROM usr.user_project WHERE upr_pro_id = {} AND upr_pro_id = ANY({}); '
                'DELETE FROM trf.project WHERE pro_id = {} AND pro_id = ANY({});'
            ).format(sql.Placeholder(), sql.Placeholder(), sql.Placeholder(), sql.Placeholder())
            values = [pro_id, usr.usr_pro_admin, pro_id, usr.usr_pro_admin]

            message, code = self.db.delete(query, values)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(jsonify(message), code)

class ProjectCollection(SwaggerView):
    """
    /transform/projects
    """

    logger = logging.getLogger('apisrv')
    schema = ProjectSchema()
    db = CRUD()

    @app.auth.login_required
    def get(self):
        """
        Get one or multiple projects.
        Get a list of one or multiple objects.
        Objects can be filtered with parameter [attr_filter], e.g. attributename=value.
        If no filtering criteria are defined, then all objects are returned.

        Ordering: The projects are ordered by their name.
        ---
        operationId: project-get
        tags:
          - transform
        parameters:
          - in: query
            name: pro_name
            required: false
            schema:
              type: string
            required: false
            description: Filtering criteria, e.g. filtering by the project name
          - in: query
            name: nested
            schema:
              type: string
              example: 'blocks'
            required: false
            description: Nesting objects in an (optional) attribute. \
              Valid is 'blocks'
        responses:
          200:
            description: Multiple objects (project)
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/ProjectSchema'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.bad_request_400.value
        usr = app.auth.current_user()

        request_data = dict(request.args)  # Arguments from URL

        # Valid Parameter: attr_filter
        request_params = self.schema.parse_filter_param(
            request_data, attr_filter=True, statistics=True, nested=True)

        attr_filter = request_params.values
        pk_dict = request_params.pk_dict
        nested = request_params.nested

        # Authorization:
        # No pk/fk required. If one given: check if exists
        if pk_dict:  # pro_id or pro_org_id in attr_filter
            pro_ids = authorization.select_related_pro_id(self.db, pk_dict, many=True)
            # The user must have authorization on at least one project
            if not any(i in usr.usr_pro_read for i in pro_ids):
                err = app.config['DEFAULT_MESSAGES']['AUTH_PRO']
                return make_response(jsonify(err.to_dict()), err.code)

        # The authorization is within the sql-query (filter_in):

        try:
            nested_schemas = list()  # If there are nested objects

            qb = ReadQueryBuilder()

            # Prepare iterables, sql-expressions
            # Select a from list of sql-expressions
            selectStr = 'pro.*'
            # Use schema.fields https://marshmallow.readthedocs.io/en/stable/api_reference.html#marshmallow.Schema.fields
            selectStr = ','.join(['pro.'+f.name for f in ProjectSchema().shortFieldsList()])
            select_expr = [
                sql.SQL(selectStr),
                sql.SQL('org.org_name AS pro_org_name'),
                sql.SQL('CASE WHEN '
                        '(SELECT uog.uog_role_code FROM usr.user_organisation uog '
                        "WHERE uog.uog_org_id = org.org_id and uog.uog_role_code = 'ros-oprs') "
                        'IS NOT NULL THEN TRUE ELSE FALSE END pro_pers')]
            # From a list of sql-expressions
            from_expr = [
                sql.SQL('trf.project AS pro'),
                sql.SQL('LEFT JOIN usr.organisation AS org ON org.org_id = pro.pro_org_id')]
            order_expr = [
                sql.SQL('pro_name')]

            if 'blocks' in nested:
                # Add information for dumping
                nested_schemas.append(NestedSchema(child_fk='blk_pro_id',
                                                   nested_schema=BlockSchema(),
                                                   nested_field_name='pro_blk_nested'))
                # Add select-expressions
                select_expr.append(sql.SQL('blk.*'))  # trf.block.*

                # join to trf.block
                from_expr.append(sql.SQL(
                    'LEFT JOIN trf.block AS blk ON blk.blk_pro_id = pro.pro_id'))

                order_expr.append(
                    sql.SQL('blk_seq'))

            # build sql
            qb.build_select_sql(sql_exprs=select_expr)
            qb.build_from_sql(sql_exprs=from_expr)
            qb.build_where_sql(filter_equal=attr_filter,
                               filter_in={'pro_id': usr.usr_pro_read})
            qb.build_order_sql(sql_exprs=order_expr)
            query, values = qb.read_query()

            # Read Projects
            message, code = self.db.read(query, values, many=True)

            if code != 200:
                message = jsonify(message)

            else:
                self.sdFxArchiveOldJobs()
                if nested_schemas:
                    # one-to-many relation on blocks
                    message = self.schema.dump_nested_collection(message, nested_schemas)
                    message = jsonify(message)  # not schema.jsonify (datetime causes trouble)

                else:
                    message = self.schema.jsonify(message, many=True)  # jsonify: dumps+response

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(message, code)
    
    # Archive all projects that are too old.
    def sdFxArchiveOldJobs(self):

      TREAT_AT_ONCE = 10

      # Read the block count
      message, code = self.db.read(sql.SQL(
          "SELECT COUNT(blk.*) as cnt FROM trf.block AS blk "
          "WHERE blk.blk_state_code != 'bks-ini' AND blk.blk_change_date < now() - interval '30 days'" #  AND blk.blk_data IS NOT NULL
      ))
      nof = message[0]['cnt']

      lim_off_query = sql.SQL(
          "SELECT blk.blk_id, blk.blk_pro_id, blk.blk_type_code, blk.blk_state_code, blk.blk_create_date, blk.blk_change_date "
          "FROM trf.block AS blk "
          "WHERE blk.blk_state_code != 'bks-ini' AND blk.blk_change_date < now() - interval '30 days' "
          "LIMIT {lim_no} OFFSET {off_no}"
      ).format(
          lim_no=sql.Placeholder("lim_no"),
          off_no=sql.Placeholder("off_no"),
      )
      if TREAT_AT_ONCE < nof:
        offset = random.randint(0, nof - TREAT_AT_ONCE)
      else:
        offset = 0
      lim_off_values = {
        'lim_no': min(TREAT_AT_ONCE, nof),
        'off_no': offset
      }
      message, code = self.db.read(lim_off_query, lim_off_values, many=True)
      proIds = []
      for blk in message:
        if not blk['blk_pro_id'] in proIds:
          proIds.append(blk['blk_pro_id'])

      # Load all blocks of these projects
      qb = ReadQueryBuilder()
      qb.select_sql = sql.SQL('SELECT blk.blk_id, blk.blk_pro_id, blk.blk_type_code, blk.blk_state_code, blk.blk_create_date, blk.blk_change_date ')
      qb.from_sql = sql.SQL('FROM trf.block AS blk')
      qb.build_where_sql(
          filter_in={'blk_pro_id': proIds}
        )
      qb.order_sql = sql.SQL('ORDER BY blk_seq')
      query, values = qb.read_query()
      message, code = self.db.read(query, values, many=True)
      proId2Blocks = {}
      if code == 200:
        for blk in message:
          if blk['blk_pro_id'] not in proId2Blocks:
            proId2Blocks[blk['blk_pro_id']] = []
          proId2Blocks[blk['blk_pro_id']].append(blk)
      for proId, blocks in proId2Blocks.items():
        self.checkAndArchiveProject(proId, blocks)

    def checkAndArchiveProject(self, proId, blocks):
      # Load project 
      qb = ReadQueryBuilder()
      qb.select_sql = sql.SQL('SELECT pro.pro_id, pro.pro_name, pro.pro_change_date ')
      qb.from_sql = sql.SQL('FROM trf.project AS pro')
      qb.build_where_sql(filter_equal={'pro_id': proId})
      query, values = qb.read_query()
      message, code = self.db.read(query, values)
      pro = message[0]

      maxChangeDate = pro['pro_change_date'] # compare also projects change date
      anyNoneInitBlock = False
      for blk in blocks:
        maxChangeDate = max(maxChangeDate, blk['blk_change_date'])
        anyNoneInitBlock = anyNoneInitBlock or blk['blk_state_code'] != 'bks-ini'
      currentDate = datetime.now()
      archiveDelimiterDate = currentDate - timedelta(days=ARCHIVE_AFTER_DAYS)
      addWithTz = datetime(archiveDelimiterDate.year, archiveDelimiterDate.month, archiveDelimiterDate.day, archiveDelimiterDate.hour, archiveDelimiterDate.minute, archiveDelimiterDate.second, archiveDelimiterDate.microsecond, tzinfo=maxChangeDate.tzinfo)
      tooOld = maxChangeDate < addWithTz
      if tooOld and anyNoneInitBlock: # If too old and not yet archived ..
        # Archive project itself
        attr_values = {}
        attr_values['pro_output'] = None
        attr_values['pro_name'] = pro['pro_name'] + ' archiviert'
        attr_filter = {'pro_id': pro['pro_id']}
        qb = UpdateQueryBuilder()
        qb.update_sql = sql.SQL('UPDATE trf.project')
        qb.build_set_sql(attr_values)
        qb.build_where_sql(attr_filter)
        query, values = qb.update_query()
        message, code = self.db.update(query, values)
        # Archive all blocks
        query = sql.SQL(
            "UPDATE trf.block SET blk_state_code = 'bks-ini', blk_data = null, blk_state_message = concat(blk_heading, ' archiviert'::text), blk_heading = null WHERE blk_pro_id = {};"
        ).format(sql.Placeholder())
        values = [pro['pro_id']]
        conn, cur = self.db.get_cursor()
        message, code = self.db._update(cur, query, values)
        if code == 204:
          conn.commit()
        else:
          conn.rollback()
        cur.close()
        # attr_values = {}
        # attr_values['blk_state_code'] = 'bks-ini'
        # attr_values['blk_data'] = None
        # attr_filter = {'blk_pro_id': pro['pro_id']}
        # qb = UpdateQueryBuilder()
        # qb.update_sql = sql.SQL('UPDATE trf.block')
        # qb.build_set_sql(attr_values)
        # qb.build_where_sql(attr_filter)
        # query, values = qb.update_query()
        # message, code = self.db.update(query, values)

    # Authorization on organisation-project-leader is within the endpoint
    @app.auth.login_required
    def post(self):
        """
        Create a new project.
        Creating a new object will automatically produce a new id. The attributes must be valid.

        Only project-leaders of an organisation can post new projects.
        Project read, right and admin permissions are created as well.
        ---
        operationId: project-post
        tags:
          - transform
        requestBody:
          required: true
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProjectSchema'
              example: {"pro_name": "Test1", "pro_org_id": "1"}
        responses:
          201:
            description: Created.
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/ID'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.bad_request_400.value
        usr = app.auth.current_user()

        # Get the JSON from the request body and validate the data
        request_data = request.get_json()
        request_params = self.schema.parse_value_param(request_data, partial=('pro_id',))
        values = request_params.values

        # Authorization:
        # Select the organisation (foreign key)
        authorization.select_usr_org_role_id(self.db, 'organisation',
                                             {'org_id': values['pro_org_id']}, fk_only=True)
        authorization.check_organisation_role('org-pl', values['pro_org_id'], usr)

        conn, cur = self.db.get_cursor()
        try:  # Insert One Object
            values['pro_create_user'] = usr.usr_id

            qb = CreateQueryBuilder()
            # INSERT INTO trf.project (col1, col2, ...)
            qb.build_insert_sql(sql.SQL('trf.project'), values)
            # VALUES (Placeholder1, Placeholder2, ...)
            qb.build_values_sql(values)
            qb.returning_sql = sql.SQL('RETURNING pro_id')

            query, values = qb.create_query()

            message, code = self.db._create(cur, query, 'pro_id', values)

            # Roles: Project admin, reading and writing permissions
            if code == 201:
                pro_id = message['pro_id']

                query = [
                    sql.SQL(
                        'INSERT INTO usr.user_project (upr_usr_id, upr_pro_id, upr_role_code)'
                        ' VALUES ({}, {}, {}) RETURNING upr_usr_id, upr_pro_id, upr_role_code;'
                    ).format(sql.Placeholder(), sql.Placeholder(), sql.Placeholder()),
                    sql.SQL(
                        'INSERT INTO usr.user_project (upr_usr_id, upr_pro_id, upr_role_code)'
                        ' VALUES ({}, {}, {}) RETURNING upr_usr_id, upr_pro_id, upr_role_code;'
                    ).format(sql.Placeholder(), sql.Placeholder(), sql.Placeholder()),
                    sql.SQL('INSERT INTO usr.user_project (upr_usr_id, upr_pro_id, upr_role_code)'
                            ' VALUES ({}, {}, {}) RETURNING upr_usr_id, upr_pro_id, upr_role_code;'
                            ).format(sql.Placeholder(), sql.Placeholder(), sql.Placeholder())]

                values = [[usr.usr_id, pro_id, 'ros-padm'],
                          [usr.usr_id, pro_id, 'ros-prd'],
                          [usr.usr_id, pro_id, 'ros-pwr']]

                roles = []
                ros_message = None
                # Execute the queries separately to return all created roles
                for q, v in zip(query, values):
                    ros_message, ros_code = self.db._create(cur, q,
                                                            ['upr_usr_id', 'upr_pro_id',
                                                             'upr_role_code'], v)
                    code = ros_code
                    if ros_code == 201:
                        roles.append(ros_message['upr_role_code'])

                if code == 201:
                    # merge the returning id's
                    message['upr_role_code'] = roles
                    conn.commit()
                else:
                    message = ros_message
                    conn.rollback()
            else:
                conn.rollback()
            # Commit and close the cursor
            cur.close()

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())
        # Catch DB-Exceptions
        except psycopg2.Error as exc:
            self.logger.info('Rolling back.')
            conn.rollback()
            cur.close()
            err = self.db.get_error_message(exc)
            code = err.code
            message = err.to_dict()

        finally:
            return make_response(jsonify(message), code)

class ProjectCopy(SwaggerView):
    """
    /transform/projects/{pro_id}/copy
    """

    logger = logging.getLogger('apisrv')
    schema = ProjectSchema()
    db = CRUD()

    @app.auth.login_required
    def post(self, pro_id: int):
        """
        Create a new project using blocks from other project.
        The request payload will be:
        * validated against the schemas of blocks
        * checked if the logged in user has permission on these blocks

        Further, Project read, right and admin permissions \
        are created for the looged in user as well.
        ---
        operationId: project-copy-post
        tags:
          - transform
        parameters:
          - in: path
            name: pro_id
            schema:
              type: integer
              example: 1
            required: true
            description: Filtering criterion
        requestBody:
          required: true
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProjectCopySchema'
              example: >
                {
                  "pro_name": "Test JSON",
                  "pro_org_id": 1,
                  "pro_org_name": "FHNW"
                }
        responses:
          201:
            description: Created.
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/ID'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.bad_request_400.value
        usr = app.auth.current_user()

        # Get the JSON from the request body and validate the data
        # Validate each object in the nested dictionary
        request_data = request.get_json()
        pro_schema = ProjectSchema()
        blk_schema = BlockSchema()
        pro_schema.parse_value_param(request_data, partial=('pro_id', ), unknown=EXCLUDE)
        blocks = request_data.get('pro_blk_nested', [])
        for blk in blocks:
            blk_schema.parse_value_param(blk, unknown=EXCLUDE)

        project_copy_schema = ProjectCopySchema()
        # Validation errors are also nested for the nested fields
        parsed_request_parameter = project_copy_schema.parse_value_param(
            request_data, partial=('pro_id', 'pro_pro_id'), unknown=EXCLUDE)
        project_parsed = parsed_request_parameter.values  # project-data
        pro_blk_nested = project_parsed.pop('pro_blk_nested', [])  # blocks

        pk_dict = {} # result from former validate_project_data (but we have nothing to validate so far)
        # TODO must pk_dict be constructed from pro_blk_nested in order for the reading permissions to be checked?

        # Organisation Authorization
        # Project leader required
        authorization.check_organisation_role('org-pl', project_parsed['pro_org_id'], usr)
        # Reading permission on the original projects-blocks required
        if pk_dict:
            pro_ids = authorization.select_related_pro_id(self.db, pk_dict, many=True)
            for pro in pro_ids:
                authorization.check_project_role('pro-read', pro, usr)

        conn, cur = self.db.get_cursor()
        try:
            # get source project
            source_pro_query = sql.SQL(
                'SELECT pro_pro_id, pro_type_code '
                'FROM trf.project '
                'WHERE pro_id = {pro_id}').format(pro_id=sql.Placeholder('pro_id'))
            source_pro_message, source_pro_code = self.db._read(
                cur, source_pro_query, {'pro_id': pro_id})

            source_pro_pro_id = None
            source_pro_type_code = None
            if source_pro_code == 200:
                source_pro_pro_id = source_pro_message[0]['pro_pro_id']
                source_pro_type_code = source_pro_message[0]['pro_type_code']
            elif source_pro_code == 404:
                err = app.config['DEFAULT_MESSAGES']['NF_OBJ']
                raise werkzeug.exceptions.NotFound(description=err)
            else:
                err = app.config['DEFAULT_MESSAGES']['BAD_REQ']
                raise werkzeug.exceptions.BadRequest(description=err)

            if source_pro_type_code == 'pro-usr':
                project_parsed['pro_pro_id'] = source_pro_pro_id
            elif source_pro_type_code == 'pro-ref':
                project_parsed['pro_pro_id'] = pro_id
            else:
                err = app.config['DEFAULT_MESSAGES']['BAD_REQ']
                raise werkzeug.exceptions.BadRequest(description=err)

            # a copied project is always a user-project
            project_parsed['pro_type_code'] = 'pro-usr'
            # create new project
            message, code = self.create_project(cur, project_parsed, usr.usr_id)
            if code == 201:
                copy_pro_id = message['pro_id']

                # copy blocks
                self.copy_blocks(cur, pro_blk_nested, copy_pro_id, usr.usr_id)

                # Roles: Project admin, reading and writing permissions
                query = [
                    sql.SQL(
                        'INSERT INTO usr.user_project (upr_usr_id, upr_pro_id, upr_role_code)'
                        ' VALUES ({}, {}, {}) RETURNING upr_usr_id, upr_pro_id, upr_role_code;'
                    ).format(sql.Placeholder(), sql.Placeholder(), sql.Placeholder()),
                    sql.SQL(
                        'INSERT INTO usr.user_project (upr_usr_id, upr_pro_id, upr_role_code)'
                        ' VALUES ({}, {}, {}) RETURNING upr_usr_id, upr_pro_id, upr_role_code;'
                    ).format(sql.Placeholder(), sql.Placeholder(), sql.Placeholder()),
                    sql.SQL('INSERT INTO usr.user_project (upr_usr_id, upr_pro_id, upr_role_code)'
                            ' VALUES ({}, {}, {}) RETURNING upr_usr_id, upr_pro_id, upr_role_code;'
                            ).format(sql.Placeholder(), sql.Placeholder(), sql.Placeholder())]

                values = [[usr.usr_id, copy_pro_id, 'ros-padm'],
                          [usr.usr_id, copy_pro_id, 'ros-prd'],
                          [usr.usr_id, copy_pro_id, 'ros-pwr']]

                roles = []
                ros_message = None
                # Execute the queries separately to return all created roles
                for q, v in zip(query, values):
                    ros_message, ros_code = self.db._create(cur, q,
                                                            ['upr_usr_id', 'upr_pro_id',
                                                             'upr_role_code'], v)
                    code = ros_code
                    if ros_code == 201:
                        roles.append(ros_message['upr_role_code'])

                if code == 201:
                    # merge the returng id's
                    message['upr_role_code'] = roles
                    conn.commit()
                else:
                    message = ros_message
                    conn.rollback()
            else:
                conn.rollback()
            cur.close()

        except (ValueError, TypeError):  # pragma: no cover
            self.logger.info('Rolling back.')
            conn.rollback()
            cur.close()
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        except psycopg2.Error as exc:
            # Catch DB-Exceptions
            self.logger.info('Rolling back.')
            conn.rollback()
            cur.close()
            err = self.db.get_error_message(exc)
            code = err.code
            if err.details and 'project' in err.details:  # unique-constraint validated on project
                err.details['project'] = project_parsed  # add the project-object
            message = err.to_dict()

        except werkzeug.exceptions.HTTPException as exc:
            self.logger.info('Rolling back.')
            conn.rollback()
            cur.close()
            err = exc.description
            code = err.code
            message = err.to_dict()

        finally:
            return make_response(jsonify(message), code)

    def copy_blocks(self, cur: 'psycopg2.extensions.cursor',
                    ref_blocks: List[Dict],
                    project_copy_id: int, create_user_id: int) -> Tuple[Dict, Dict]:
        """
        Copy Blocks

        :param cur: DB-Cursor
        :param ref_blocks: The list of Blocks to copy
        :param project_copy_id: The id of the newly copied Project
        :param create_user_id: The id of the authenticated user to set as create_user
        """
        block_copy_mapping = {}  # empty 
        returning_ids = {'blk_id': []}

        for ref_block in ref_blocks:
            # Create block
            ref_block['blk_pro_id'] = project_copy_id
            message, code = self.create_block(cur, ref_block, create_user_id)
            returning_ids['blk_id'].append(message['blk_id'])

        return returning_ids, block_copy_mapping

    def create_block(self, cur: 'psycopg2.extensions.cursor',
                     block_values: Dict, create_user_id: int) -> Tuple[Dict, int]:
        """
        Create a Block

        :param cur: DB-Cursor
        :param block_values: The values to create the new Block
        :param create_user_id: The id of the authenticated user to set as create_user
        :return: If the code is 201, then the message contains the id of the new Block,
        otherwise throws a 'psycopg2.Error'
        :raises psycopg2.Error: If there was an database error
        """
        block_values['blk_create_user'] = create_user_id
        block_values.pop('blk_id', None)
        qb = CreateQueryBuilder()
        qb.build_insert_sql(sql.SQL('trf.block'), values=block_values)
        qb.build_values_sql(values=block_values)
        qb.returning_sql = sql.SQL('RETURNING blk_id')
        query, values = qb.create_query()
        return self.db._create(cur, query, 'blk_id', values=values)

    def create_project(self, cur: 'psycopg2.extensions.cursor',
                       project_values: Dict, create_user_id: int) -> Tuple[Dict, int]:
        """
        Creates a Project

        :param cur: DB-Cursor
        :param project_values: The values to create the new Project
        :param create_user_id: The id of the authenticated user to set as create_user
        :return: If the code is 201, then the message contains the id of the new Project,
        otherwise throws a 'psycopg2.Error'
        :raises psycopg2.Error: If there was an database error
        """
        project_values['pro_create_user'] = create_user_id
        project_values.pop('pro_id', None)
        qb = CreateQueryBuilder()
        qb.build_insert_sql(sql.SQL('trf.project'), values=project_values)
        qb.build_values_sql(values=project_values)
        qb.returning_sql = sql.SQL('RETURNING pro_id')
        query, values = qb.create_query()
        return self.db._create(cur, query, 'pro_id', values=values)

