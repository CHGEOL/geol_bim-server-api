
from typing import Tuple, Union, Dict, List, TYPE_CHECKING, Optional, Any
import re
import logging
import requests
import sys
import warnings
from io import BytesIO
import dataclasses

from flask import request, jsonify as orig_jsonify, current_app as app
import PIL
from PIL import Image
import werkzeug.exceptions
import marshmallow.exceptions
from marshmallow import Schema

logger = logging.getLogger('apisrv')
ma_logger = logging.getLogger('marshmallow')
warnings.simplefilter('error', Image.DecompressionBombWarning)

sentinel = object()  # For BaseSchema.jsonify, from marshmallow.schema

if TYPE_CHECKING:
    # Import only for the type-checking, not on runtime
    from .errormessage import ErrorMessage


@dataclasses.dataclass
class RequestParameter:
    """
    The validated request parameter are stored as attributes.
    """
    # used for filtering and value parameter
    values: Optional[Dict] = None
    pk_dict: Optional[Dict] = None
    # only for filtering parameter
    statistics: Optional[bool] = None
    nested: Optional[list] = None
    aggregated: Optional[bool] = None


class NestedSchema:
    """
    Representing a nested Object (the nested object is an attribute of its parent object).
    It is for either one nested child, or for a collection of nested children.
    """

    def __init__(self, child_fk: str, nested_schema: 'BaseSchema', nested_field_name: str,
                 pluck: str = None, nested_schemas: List['NestedSchema'] = None):
        """
        :param child_fk: Foreign key of the child (nested object)
        :param nested_schema: Schema-instance of the child (nested object)
        :param nested_field_name: str, name of the field in the parent object to insert the child
        :param pluck: Name of the attribute to pluck
        :param nested_schemas: A list of (recursive) nested schemas
        """
        self.child_fk = child_fk
        self.schema = nested_schema
        self.nested_field_name = nested_field_name
        self.pluck = pluck
        self.nested_schemas = nested_schemas


class BaseSchema(Schema):
    """
    Additional functionality of marshmallow.Schema class
    """
    # Attributes overridden in subclasses
    db_schema = str()  # SQL query: <db_schema>.<tablename>.<pk_column>
    tablename = str()
    pk_column = str()  # name for id-column f'{prefix}_id'
    prefix = str()

    col_intern = list()  # List of columns in db-table
    # Valid names for nested attributes in the query string)
    nested_attr = list()  # Overwritten in subclasses

    # Skip Null-Values when dumping data:
    SKIP_VALUES = [None, 'null', [], {}]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # List of all required attributes
        self.required = [f for f in self.declared_fields if self.declared_fields[f].required]

    def jsonify(self, obj, many=sentinel, *args, **kwargs):
        """
        Return a JSON response containing the serialized data
        (originally from flask_marshmallow.schema).

        :param obj: Object to serialize.
        :param bool many: Whether `obj` should be serialized as an instance
            or as a collection. If unset, defaults to the value of the
            `many` attribute on this Schema.
        :param kwargs: Additional keyword arguments passed to `flask.jsonify`.
        """
        # From flask_marshmallow.schema
        if many is sentinel:
            many = self.many
        data = self.dump(obj, many=many)

        return orig_jsonify(data, *args, **kwargs)

    def handle_error(self, exc, data, **kwargs):
        """
        Logs marshmallow-validation errors
        """
        ma_logger.warning(f'{exc.__class__.__name__}: {exc}')
        raise exc.__class__(exc)

    @staticmethod
    def _search(dictionary: Dict, search_str: Union[str, List, Tuple]) -> bool:
        """
        Search for a (partial) string in the dictionary values.

        :param dictionary: Search in the dictionary
        :param search_str: String of list of strings to match
        :return: True, if a match was found
        """
        for key in dictionary:
            if isinstance(search_str, str):
                if search_str in dictionary[key]:
                    return True
            elif isinstance(search_str, (list, tuple)):
                if any(s in dictionary[key] for s in search_str):
                    return True
        return False

    def _get_error_message(self, exc: Exception) -> 'ErrorMessage':
        """
        Generates an error-message-Object, based on the type of the exception (ValidationErrors).
        The Marshmallow ValidationError is parsed based on the strings of the message with
        :func:`_search() <_search>`

        :param exc: raised Exception
        :return: Object with the message and code
        """

        default_messages = app.config['DEFAULT_MESSAGES']

        if isinstance(exc, marshmallow.exceptions.ValidationError):
            # Strange datastructure in errors... cleanup
            if isinstance(exc.messages, marshmallow.exceptions.ValidationError):
                exc = exc.normalized_messages()['_schema']

            detail = exc.messages

            # only one message per field
            # {'attr': ['Not a valid string.']} -> {'attr': 'Not a valid string.'}
            detail = {k: ' '.join(v) for k, v in detail.items()}

            err_message = default_messages['INP_VAL']  # Default MA-Validation-Message
            if len(detail) == 1:
                if self._search(detail, ['minimum length', 'maximum length', 'Length must be']):
                    err_message = default_messages['INP_VAL_STR_LEN']
                elif self._search(detail, 'Field may not be null.'):
                    err_message = default_messages['INP_VAL_NULL_FLD']
                elif self._search(detail, 'Missing data for required field'):
                    err_message = default_messages['INP_VAL_REQ_FLD']
                elif self._search(detail, 'Unknown field'):
                    err_message = default_messages['INP_VAL_INV_FLD']
                elif self._search(detail, 'Not a valid email address.'):
                    err_message = default_messages['INP_VAL_MAIL']
                elif self._search(detail, 'Not a valid'):
                    err_message = default_messages['INP_VAL_TYP']
            elif len(detail) > 1:
                err_message = default_messages['INP_VAL_MULT']
            err_message.details = detail
        else:  # Any other error -> default BadRequest
            err_message = default_messages['BAD_REQ']
        return err_message

    @staticmethod
    def _raise_error(err: 'ErrorMessage'):
        """
        Raises an error based on its code. The errors are returned with
        the flask error-handler, defined in :func:`app.errorhandlers <init_errorhandler>`

        :param err: ErrorMessage
        :raises: Appropriate HTTP-Exception
        """
        code_to_error = {
            400: werkzeug.exceptions.BadRequest(description=err),  # all validation errors
            401: werkzeug.exceptions.Unauthorized(description=err),
            404: werkzeug.exceptions.NotFound(description=err),
            405: werkzeug.exceptions.MethodNotAllowed(description=err),
            500: werkzeug.exceptions.InternalServerError(description=err)
        }
        raise code_to_error[err.code]

    def _parse_nested(self, txt: str) -> List:
        """
        Helper function to parse the query string for the names of nested attributes (regex).
        It compares the string with the valid names (defined in self.nested_attr)

        :param txt: Query string, e.g. 'pset, schema' or 'psetschema'
        :return: set of valid names, e.g. ['pset', 'schema']
        """
        x = re.findall('|'.join(self.nested_attr), txt)
        return list(set(x))

    def _parse_colnames(self, txt: str) -> List:  # pragma: no cover
        """
        Parses the query string for the column names (regex).
        It compares the string with the valid names (defined in self.col_intern)

        .. warning::
           Deprecated. It was used to parse selecting-parameters (only select certain columns).
           They are not used anymore.

        :param txt: Query string, e.g. 'ips_name,ips_desc' or 'ips_nameips_desc'
        :return: set of valid names, e.g. ['pset', 'schema']
        """
        DeprecationWarning('Deprecated. Not used anymore.')
        logger.warning(
            'DeprecationWarning: BaseSchema._parse_colnames() deprecated.')
        x = re.findall('|'.join(self.col_intern), txt)
        return list(set(x))

    @staticmethod  # alternative: use webargs for parsing optional arguments like statistics, ...
    def _get_param_from_dict(data: Dict, param_name: str,
                             default: Optional[Any] = None) -> Tuple[Dict, Union[str, bool]]:
        """
        Helper-function, to fetch a parameter form a dictionary by its key.
        The function returns the dictionary without the parameter and the value of the parameter

        :param data: Dictionary with parameters
        :param param_name: Key of the specific parameter
        :param default: Default value if the parameter is not in the dictionary
        :return: reduced data-dict, parameter
        """
        # get the parameter value, remove it from the dict
        param = data.pop(param_name, default)

        # Parse boolean
        if isinstance(param, str) and param.lower() == 'true':
            param = True
        if isinstance(param, str) and param.lower() == 'false':
            param = False

        return data, param

    def _get_related_filtering_param(self, data: Dict,
                                     related_filter: Dict) -> Tuple[Dict, Dict]:
        """
        Helper-Function, to fetch the filtering parameter for related tables  from the
        parameter dictionary (``data``) according to the definitions in ``related_filter``.

        * Removes the keys from the original data
        * Replaces the the keys with their DB-name
        * returns the reduced original data and all filtering parameters in a separate dict.

        Example::

            # Filter the Activities by
            # - the name Test (normal param)
            # - related to the project 1 (related param)
            data = {'act_name': 'Test', 'act_pro_id': 1}
            # The Attribute with the name act_pro_id is
            # used in the SQL-query as pro_id
            related_filter = {'act_pro_id': 'pro_id'}

            data, rel_val = _get_related_filtering_param(data, related_filter)

            # data >>> {'act_name': 'Test'}
            # rel_val >>> {'pro_id': 1}

        :param data: Filtering parameters
        :param related_filter: The required Format is ``{'<param_name>:<db_name>}``.
                               **Example:** Filter the activities by a project id:
                               ``related_filter = {'act_pro_id': 'pro_id'}``. The parameter name
                               is ``act_pro_id`` and the name in the SQL-Query is ``pro_id``.
        :return: reduced data-dict, filtering parameter
        """
        related_filter_values = dict()

        for k, v in related_filter.items():
            if k in data:
                data, param = self._get_param_from_dict(data, k)
                related_filter_values[v] = param

        return data, related_filter_values

    def _get_pk_from_param(self, data: Dict,
                           required_pk: Optional[Union[bool, List]] = None) -> Dict:
        """
        The function parses the keys in the dictionary for primary keys and foreign keys
        and returns a dictionary with the keys and their values. The fk are returned as
        their related primary key: ``{'act_id': 1}`` for the input ``{'req_act_id': 1}``.
        There are three options for the attribute ``required_pk``:

        * False / None: If no pk is found, it returns an empty dict.
        * True: If no pk is found, a ValidationError is raised
        * List of primary keys: Only key-value pairs for the listed ones are returned.
          At least one pk of the given list must be found in the data-dict.
          Foreign keys are given in the list as the original primary key:
          ``'act_id'`` instead of ``'req_act_id'``.
          If no pk is found, a ValidationError is raised

        The function is used in
        :func:`parse_filter_param() <parse_filter_param>`
        after the validation of (optional) related filter and in
        :func:`parse_value_param() <parse_value_param>`.

        :param data: Filtering parameters
        :param required_pk: Bool or a list of required keys (OR), e.g.
                         ``['act_id', 'pty_id', 'prc_id', 'elm_id']``
        :raises: marshmallow.ValidationError (INP_VAL_NULL_FLD), if required and no pk found
        """
        # find any pk/fk in the keys and store the pk with values
        pk_dict = {}
        for key in data:
            parts = key.split('_')
            # pk or fk with 2 or three parts: 'elm'+'id' or 'pty'+'elm'+'id'
            if parts[-1] == 'id':
                if len(parts) == 2 and len(parts[0]) == 3:
                    name = key
                    pk_dict[name] = data[key]
                elif len(parts) == 3 and len(parts[1]) == 3:
                    name = '_'.join(parts[1:])  # 'elm_id' for 'pty_elm_id'
                    pk_dict[name] = data[key]

        if required_pk:
            # If a specific pk or a list of pks is given: remove others
            if isinstance(required_pk, list):
                pk_dict = {k: v for k, v in pk_dict.items() if k in required_pk}

            # required but no pk found -> ValidationError
            if not pk_dict:
                mess = ['Missing data for required field. '
                        'At least one pk or (virtual) fk must be given.']
                if isinstance(self.pk_column, str):
                    err = {self.pk_column: mess}
                else:
                    err = {self.pk_column[0]: mess}
                raise marshmallow.exceptions.ValidationError(err)

        return pk_dict

    def parse_filter_param(self, data: Dict,
                           statistics: Optional[bool] = False, nested: Optional[bool] = False,
                           aggregated: Optional[bool] = False,
                           attr_filter: Optional[bool] = False,
                           related_filter: Optional[Dict] = None,
                           required_pk: Optional[Union[bool, List]] = None,
                           partial: Optional[bool] = True
                           ) -> 'RequestParameter':
        """
        Parses the filtering parameters of a request (usually get-request)
        and raises a HTTP-Error (INP_VAL(_...)) on fail.
        See the :any:`Documentation <API-Architektur/API_description>`
        on request parameters for more information.

        * Parses and validates all parameters according to the function input
        * Raises a ValidationError (INP_VAL(_...)), if the validation fails

        :param data: Dictionary with the request-parameter
        :param statistics: If a statistic-parameter should be parsed
            (with :func:`_get_param_from_dict() <_get_param_from_dict>`)
        :param nested: If a nested-parameter should be parsed
            (with :func:`_parse_nested() <_parse_nested>`)
        :param aggregated: If a aggregated-parameter should e parsed
            (with :func:`_get_param_from_dict() <_get_param_from_dict>`)
        :param attr_filter: If filtering-parameters should be parsed (with Schema.load())
        :param related_filter: If a filtering-parameter from a related table should be parsed
            (with :func:`_get_related_filtering_param() <_get_related_filtering_param>`).
            The required Format is ``{'<param_name>:<db_name>}``.
            **Example:** Filter the activities by a project id:
            ``related_filter = {'act_pro_id': 'pro_id'}``. The parameter name
            is ``act_pro_id`` and the name in the SQL-Query is ``pro_id``.
        :param required_pk: (Optional) Bool or a list of required keys (OR), e.g.
            ``['act_id', 'pty_id', 'prc_id', 'elm_id']``
            for :func:`_get_pk_from_param <_get_pk_from_param>`
        :param partial: If true, then not the complete schema need to be there.
            This parameter is passed to Schema.load().
        :return: RequestParameter() with all values
        :raises: 400 BadRequest (INP_VAL(_...))
        """
        if not data:  # Replace None (no data) with an empty dict.
            data = dict()

        schema_filter_val = dict()  # temporary dictionary for the filters on this table
        foreign_filter_val = dict()  # temporary dictionary for the filters on related tables

        # Valid parameter as attributes
        request_parameter = RequestParameter()

        try:
            # optional parameter
            if statistics:
                data, param = self._get_param_from_dict(data, 'statistics', default=False)
                request_parameter.statistics = param
            if nested:
                param = self._parse_nested(str(data.pop('nested', '')))
                request_parameter.nested = param
            if aggregated:
                data, param = self._get_param_from_dict(data, 'aggregated', default=False)
                request_parameter.aggregated = param

            # filtering parameter:
            if related_filter:  # {'act_pro_id': 'pro_id'}
                # rename the parameter from the key to its value (act_pro_id -> pro_id)
                # and store them temporarily in the dictionary with their filtering values
                data, foreign_filter_val = self._get_related_filtering_param(data, related_filter)
            if attr_filter:
                # Validate remaining parameter: Raises marshmallow.exceptions.ValidationError
                schema_filter_val = self.load(data, many=False, partial=partial)

            if attr_filter or related_filter:
                # Concat the two filtering dictionaries
                attr_filter = {**foreign_filter_val, **schema_filter_val}
                request_parameter.values = {**foreign_filter_val, **schema_filter_val}
                # validate required pk/fk for this endpoint
                request_parameter.pk_dict = self._get_pk_from_param(attr_filter, required_pk)

        except marshmallow.exceptions.ValidationError as exc:
            # ValidationError --> raises 400
            err = self._get_error_message(exc)
            self._raise_error(err)

        # Validation passed, continue
        return request_parameter

    def parse_value_param(self, data: Dict, partial: Union[bool, Tuple] = None,
                          **kwargs) -> 'RequestParameter':
        """
        Parses the value parameters (new values for creating/updating) of a request
        and raises a HTTP-Error (INP_VAL(_...)) on fail.
        See the :any:`Documentation <API-Architektur/API_description>`
        on request parameters for more information.

        * Validation with the Schema
        * The primary key is omitted (if it is not a combination of foreign keys).
        * Raises a ValidationError (INP_VAL(_...)), if the schema-validation fails
        * Raises a ValidationError (INP_VAL_ND) if no (valid) data is given

        :param data: Dictionary with the parameters names and values.
        :param partial: Parameter for Schema.load(), if set, required attributes are skipped.
        :param kwargs: additional parameters for Schema.load(), e.g. ``unknown``.
            many=True is ignored.
        :return: RequestParameter() with all values
        :raises: 400 BadRequest (INP_VAL(_...))
        """
        kwargs.pop('many', None)

        # Valid parameter as attributes
        request_parameter = RequestParameter()

        try:  # if no valid parameters are given, it returns an error
            if data:  # Validation against the schema (skip dump_only parameters)
                values = self.load(data, many=False, partial=partial, **kwargs)
                # Remove the primary key, if given
                if isinstance(self.pk_column, str):
                    values.pop(self.pk_column, None)
                request_parameter.values = values
                request_parameter.pk_dict = self._get_pk_from_param(values, required_pk=False)

            # No (valid) data given --> raises 400
            if not data or not request_parameter.values:
                err = app.config['DEFAULT_MESSAGES']['INP_VAL_ND']
                self._raise_error(err)

        except marshmallow.exceptions.ValidationError as err:
            # ValidationError --> raises 400
            err = self._get_error_message(err)
            self._raise_error(err)

        # Validation passed, continue
        return request_parameter

    @staticmethod
    def get_data_from_request(dta_attr: str) -> bytes: # used for blk_data
        """
        Get data from the request as file must be given
        with the appropriate mimetypes. It returns the data or raises an error.

        * file: multipart/form-data

        :param dta_attr: Name of the attribute for the form-data
        :return: data, error message and code if the validation fails
        :raises: BadRequest (DTA_VAL_REQ_FLD, DTA_VAL_MTYP)
        """
        default_messages = app.config['DEFAULT_MESSAGES']

        # file upload
        # Multipart-mimetype includes binary file-data and text
        if request.mimetype == 'multipart/form-data':
            if dta_attr in request.files:  # same as with mimetype 'TODO/'
                # files['cfg_data'] has type werkzeug.datastructures.FileStorage
                dta_data = request.files[dta_attr].read()  # after reading: type binary

            else:
                # Required field missing (file)
                err = default_messages['DTA_VAL_REQ_FLD']
                raise werkzeug.exceptions.BadRequest(description=err)

        else:
            # Invalid mimetype
            err = default_messages['DTA_VAL_MTYP']
            raise werkzeug.exceptions.BadRequest(description=err)

        return dta_data

    @staticmethod
    def get_img_data_from_request(img_attr: str, url_attr: str) -> bytes:
        """
        Get image-data from the request, either the file or the url to the image must be given
        with the appropriate mimetypes. It returns the image or raises an error.

        * file: multipart/form-data
        * url: Either with mimetype application/json or multipart/form-data

        :param img_attr: Name of the attribute for the image (form-data)
        :param url_attr: Name of the attribute for the url (form-data, json)
        :return: Image Data, error message and code if the validation fails
        :raises: BadRequest (IMG_VAL_INV_URL, IMG_VAL_REQ_FLD, IMG_VAL_MTYP)
        """
        default_messages = app.config['DEFAULT_MESSAGES']

        # file upload
        # Multipart-mimetype includes binary file-data and text or json-data
        if request.mimetype == 'multipart/form-data':
            if img_attr in request.files:  # same as with mimetype 'image/'
                # files['usr_image'] has type werkzeug.datastructures.FileStorage
                img_data = request.files[img_attr].read()  # after reading: type binary

            elif url_attr in request.form:  # same as with mimetype 'application/json'
                img_url = request.form[url_attr]
                # GET image from URL
                response = requests.get(img_url)
                if response.status_code == 200:
                    img_data = response.content
                else:
                    # Invalid image-URL: No image found
                    err = default_messages['IMG_VAL_INV_URL']
                    raise werkzeug.exceptions.BadRequest(description=err)
            else:
                # Required field missing (url-field OR file)
                err = default_messages['IMG_VAL_REQ_FLD']
                raise werkzeug.exceptions.BadRequest(description=err)

        # url in json_body
        elif request.mimetype == 'application/json':
            request_data = request.get_json()

            if url_attr in request_data:
                img_url = request_data[url_attr]
                # GET image from URL
                response = requests.get(img_url)
                if response.status_code == 200:
                    img_data = response.content
                else:
                    # Invalid image-URL: No image found
                    err = default_messages['IMG_VAL_INV_URL']
                    raise werkzeug.exceptions.BadRequest(description=err)
            else:
                # Required field missing (url-field)
                err = default_messages['IMG_VAL_REQ_FLD']
                raise werkzeug.exceptions.BadRequest(description=err)
        else:
            # Invalid mimetype
            err = default_messages['IMG_VAL_MTYP']
            raise werkzeug.exceptions.BadRequest(description=err)

        return img_data

    @staticmethod
    def validate_img_data(img_data: bytes) -> str:
        """
        This function validates the image data and returns the image type.
        If the validation fails, it raises an error message.

        :param img_data: Data as a binary string
        :return: Image Type, Error message and code (if the validation fails)
        :raises: BadRequest (IMG_VAL_SIZE, IMG_VAL_TYP)
        """
        default_messages = app.config['DEFAULT_MESSAGES']

        # Validate Image-Size
        img_size = sys.getsizeof(img_data)
        max_size = app.config['MAX_IMAGE_SIZE']

        # File is too big
        if img_size > max_size:
            img_size_kb = img_size // 1024
            allowed_size_kb = max_size // 1024
            if int(img_size_kb) == int(allowed_size_kb):
                img_size_kb += 1
            err = default_messages['IMG_VAL_SIZE']
            err.message = f'The file is too big: {img_size_kb} KB. Allowed: {allowed_size_kb} KB'
            raise werkzeug.exceptions.BadRequest(description=err)

        # https://stackoverflow.com/questions/889333/how-to-check-if-a-file-is-a-valid-image-file
        try:
            # Opening the image only loads metadata
            im = Image.open(BytesIO(img_data))
            # Image-Size could be verified here with im.size (pixels)

            # verify image-type
            if im.format.lower() in app.config['IMAGE_TYPES']:
                img_type = im.format.lower()

                # verify image
                im.verify()  # Verifies image metadata
                im.close()
                im = Image.open(BytesIO(img_data))
                im.transpose(PIL.Image.FLIP_LEFT_RIGHT)  # Image manipulation for verification
                im.close()

            else:
                # Invalid Image-Type (valid types in config IMAGE_TYPES)
                err = default_messages['IMG_VAL_TYP']
                raise werkzeug.exceptions.BadRequest(description=err)

        except PIL.UnidentifiedImageError:
            # No valid Header in the image-File
            err = default_messages['IMG_VAL_TYP']
            raise werkzeug.exceptions.BadRequest(description=err)

        except (Image.DecompressionBombWarning, Image.DecompressionBombError) \
                as err:  # pragma: no cover
            logger.critical(f'DecompressionBomb detected in the request: {err}')
            err = default_messages['IMG_VAL_BOMB']
            raise werkzeug.exceptions.BadRequest(description=err)

        except (FileNotFoundError, ValueError, TypeError):  # pragma: no cover
            # Possible errors from PIL on opening an image
            err = default_messages['IMG_VAL']
            raise werkzeug.exceptions.BadRequest(description=err)

        return img_type

    def clean_missing(self, data, many='many'):
        """
        Removes keys without values from the dictionary after serialization.
        It can be used with a post_dump decorator after serialization.

        :param data: dict
        :param many: Provided for marshmallow post_dump method
        :return: dict, data without missing values
        """
        return {
            key: value for key, value in data.items()
            if value not in self.SKIP_VALUES
        }

    def dump_nested_collection(self, data: List, nested_schemas: List[NestedSchema]) -> List:
        """
        Creates nested objects from the result of a DB-Query (with Joins).

        1. Serialize all parent objects.
        2. Loop over the nested schemas. For each:
        3. Serialize all child objects
        4. Match children to their parent object with their foreign key

        It is made for direct relationships (one to many). If is is a many-to-many relationship,
        a workaround is needed

        :param data: DB-Output: List of Dictionaries
        :param nested_schemas: List of NestedSchemas with all required keys to nest the children.
        :return: Serialized, nested objects (no validation during serialization
        """
        # dumps all unique data, then matches parent with nested child
        parent_pk = self.pk_column

        # List of unique parent obj
        parent_list = self.dump(data, many=True)  # List with duplicates
        # Only unique: A (temporary) dictionary with <pk> as keys, values are the objects:
        # if the obj. is a duplicate, it replaces the previous value (obj.) to the given key (pk)
        # The unique objects are the list of values in the temporary dictionary
        parent_list = list({v[parent_pk]: v for v in parent_list}.values())  # only unique

        for nested in nested_schemas:
            # Get values for the nesting
            child_pk = nested.schema.pk_column
            schema = nested.schema
            child_fk = nested.child_fk
            nested_field_name = nested.nested_field_name
            pluck = nested.pluck

            if nested.nested_schemas:  # Recursive for nesting multiple levels
                # unique, nested children
                child_list = schema.dump_nested_collection(
                    data, nested_schemas=nested.nested_schemas)

            else:
                # List of unique nested obj (matching to many parent obj)
                child_list = schema.dump(data, many=True)  # List with duplicates
                # Only unique: A (temp) dictionary with <pk>_<fk> as keys, values are the objects:
                # The unique objects are the list of values in the temporary dictionary
                child_list = list({f"{v[child_pk]}_{v[child_fk]}": v for v in child_list}.values())

            # insert the nested collection into the parent
            for parent_dict in parent_list:
                # matching children (foreign keys in the children to the primary key of the parent)
                children = [c for c in child_list if c[child_fk] == parent_dict[parent_pk]]
                if pluck:  # Insert only a list of one attribute instead of the whole objects
                    children = [c[pluck] for c in children]
                # insert the children into the parent object at the given attribute
                parent_dict[nested_field_name] = children

        return parent_list
