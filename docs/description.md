# Overview

This API gives the possibility to get or set informations about the objects of the GEOL_BIM Server.
For every objecttype the API supports the http verbs:

- get : Retrieve informations about one or multiple objects, reading permission required.
- post : Create one new object, writing permission required.
- patch : Update an existing object, writing permission required.
- delete : Delete one or multiple objects, writing permission required.

For more details on how to use these verbs see below.

## Error codes

The following error codes are basically supported and returned:

- 200: OK.
- 201: OK. Object created
- 204: OK. No content returned
- 400: Bad request, verify to use the correct syntax.
- 404: Object not found.
- 500: Internal Error

See below for more and specific details for each resource/verb.
