"""
REST-API mit Flask
"""

from .app import create_app

__all__ = ['create_app']
