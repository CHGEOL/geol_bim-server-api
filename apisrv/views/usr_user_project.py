
import logging

from flask import request, make_response, jsonify, current_app as app
from flasgger import SwaggerView  # SwaggerView inherits flask.views.MethodView
from psycopg2 import sql
import psycopg2

from ..shared import HttpStatus, CRUD, CreateQueryBuilder, ReadQueryBuilder
from ..shared import authorization
from ..schemas import UserProjectSchema

__all__ = ['UserProject', 'UserProjectCollection']


class UserProject(SwaggerView):
    """
    /user/user_projects/<int:upr_usr_id>/<int:upr_pro_id>/<string:upr_role_code>
    """

    logger = logging.getLogger('apisrv')
    schema = UserProjectSchema()
    db = CRUD()

    # A Role is required OR you are the user (no decorator for this case)
    @app.auth.login_required
    def delete(self, upr_usr_id: int, upr_pro_id: int, upr_role_code: str):
        """
        Delete the mapping of a user to the project.
        The project-admin can delete mappings of the project and a user can delete his mapping.

        The last project-admin cannot be deleted (Returns 400 BE_ITG_AUTH).

        Codes:
        * ros-padm: project administrator
        * ros-prd:  project read
        * ros-pwr:  project write
        ---
        operationId: user_project-id-delete
        tags:
          - user
        parameters:
          - in: path
            name: upr_usr_id
            schema:
              type: integer
              example: 1
            required: true
            description: Filtering criterion
          - in: path
            name: upr_pro_id
            schema:
              type: integer
              example: 1
            required: true
            description: Filtering criterion
          - in: path
            name: upr_role_code
            schema:
              type: string
              example: "ros-prd"
            required: true
            description: Filtering criterion
        responses:
          200:
            $ref: '#/components/responses/delete_ok'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          404:
            $ref: '#/components/responses/not_found'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.not_found_404.value
        usr = app.auth.current_user()

        # for selection
        attr_filter = {'upr_usr_id': upr_usr_id,
                       'upr_pro_id': upr_pro_id,
                       'upr_role_code': upr_role_code}

        # Authorization: check User (select usr OR select org+role + check)
        if not upr_usr_id == usr.usr_id:  # not your own id -> maybe an admin
            auth_ids = authorization.select_usr_org_role_id(self.db, 'user_project', attr_filter)
            authorization.check_project_role('pro-admin', auth_ids['upr_pro_id'][0], usr)

        conn, cur = self.db.get_cursor()
        try:
            # Query for deleting a role (matching attr_filter)
            query = sql.SQL(
                "DELETE FROM usr.user_project AS upr "
                "WHERE"
                " upr.upr_usr_id = {} AND "
                " upr.upr_pro_id = {} AND"
                " upr_role_code = {};"
            ).format(sql.Placeholder('upr_usr_id'), sql.Placeholder('upr_pro_id'),
                     sql.Placeholder('upr_role_code'))

            # Not able to delete the last project-admin with this endpoint.
            # On deleting a project, the roles get deleted as well
            if upr_role_code == 'ros-padm':
                # Select the data by its filter and count all administrators for the project
                query_select = sql.SQL(
                    "SELECT "
                    " upr.upr_usr_id, upr.upr_pro_id, upr.upr_role_code,"
                    " (SELECT COUNT(*) FROM usr.user_project AS upr"
                    "  WHERE upr.upr_pro_id = {} AND upr.upr_role_code = 'ros-padm') \"count\" "
                    "FROM usr.user_project AS upr "
                    "WHERE"
                    " upr.upr_usr_id = {} AND"
                    " upr.upr_pro_id = {} AND"
                    " upr_role_code = 'ros-padm'"
                    ";").format(sql.Placeholder('upr_pro_id'), sql.Placeholder('upr_usr_id'),
                                sql.Placeholder('upr_pro_id'))
                values = {'upr_pro_id': upr_pro_id, 'upr_usr_id': upr_usr_id}
                message, code = self.db._read(cur, query_select, values)

                # 404 Not found: the tuple doesnt exist
                if code == 200:  # there are administrators
                    count = message[0]['count']
                    if count > 1:  # more than 1 admin, deleting is possible
                        message, code = self.db._delete(cur, query, attr_filter)
                    else:  # only one admin left, no deletion
                        message = app.config['DEFAULT_MESSAGES']['BE_ITG_AUTH']
                        code = message.code

            else:  # no restrictions on deleting reading / writing roles
                message, code = self.db._delete(cur, query, attr_filter)

            # Close the cursor and the connection
            cur.close()
            if code == 200:
                conn.commit()
            else:
                conn.rollback()

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = message.to_dict()
        # Catch DB-Exceptions
        except psycopg2.Error as exc:
            self.logger.info('Rolling back.')
            conn.rollback()
            cur.close()
            err = self.db.get_error_message(exc)
            code = err.code
            message = err.to_dict()

        finally:
            return make_response(jsonify(message), code)


class UserProjectCollection(SwaggerView):
    """
    /user/user_projects
    """

    logger = logging.getLogger('apisrv')
    schema = UserProjectSchema()
    db = CRUD()

    # Project-Admin can see all, the user only himself
    @app.auth.login_required
    def get(self):
        """
        Get the mappings of users to the projects.
        There must be either 'upr_usr_id' or 'upr_pro_id' as filtering parameter.
        The project-admins have access to all mappings of their projects
        and a user has access to all his mappings.

        Codes:
        * ros-padm: project administrator
        * ros-prd:  project read
        * ros-pwr:  project write
        ---
        operationId: user_project-get
        tags:
          - user
        parameters:
          - in: query
            name: upr_usr_id
            required: false
            schema:
              type: integer
            required: false
            description: Filtering criteria, must be the logged-in user or pro-admin+upr_pro_id
          - in: query
            name: upr_pro_id
            required: false
            schema:
              type: integer
              example: 1
            required: false
            description: Filtering criteria, pro-admin required
          - in: query
            name: upr_role_code
            required: false
            schema:
              type: string
            required: false
            description: Filtering criteria, e.g. filtering by the role 'ros-pwr'
        responses:
          200:
            description: Multiple objects (user_project)
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/UserProjectSchema'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.bad_request_400.value
        usr = app.auth.current_user()

        request_data = dict(request.args)  # Arguments from URL

        # Valid Parameter: attr_filter
        request_params = self.schema.parse_filter_param(
            request_data, attr_filter=True, required_pk=['usr_id', 'pro_id'])
        attr_filter = request_params.values

        # Authorization:
        # user can see his data, org-admin can see his organisation
        # only fk in attr_filter possible -> check if exists with attr_filter
        auth_ids = authorization.select_usr_org_role_id(self.db, 'user_project', attr_filter)
        if 'upr_pro_id' in attr_filter:  # all filtering attributes are foreign keys
            authorization.check_project_role('pro-admin', auth_ids['upr_pro_id'][0], usr)
        elif 'upr_usr_id' in attr_filter:  # filtering with the usr_id
            authorization.select_usr_org_role_id(self.db, 'user',
                                                 {'usr_id': attr_filter['upr_usr_id']})
            authorization.check_usr_id(attr_filter['upr_usr_id'], usr)
        else:  # not possible, 'usr_id' or 'pro_id' is required in filtering parameter
            err = app.condig['DEFAULT_MESSAGES']['AUTH']
            return make_response(jsonify(err.to_dict()), err.code)

        try:
            # Build the filtering statements 'AND upr_pro_id = %s AND upr_usr_id = %s'
            if attr_filter:
                equal = zip(map(sql.Identifier, attr_filter), map(sql.Placeholder, attr_filter))
                equal_sql = sql.SQL('AND ').join(
                    [sql.SQL("{}={} ").format(col, val) for col, val in equal])
            else:
                equal_sql = None

            qb = ReadQueryBuilder()

            qb.select_sql = sql.SQL(
                'SELECT upr.*,'
                ' usr.usr_name_first AS upr_usr_name_first, usr.usr_name AS upr_usr_name,'
                ' pro.pro_name AS upr_pro_name')
            qb.from_sql = sql.SQL(
                'FROM usr.user_project as upr'
                ' LEFT JOIN usr."user" AS usr ON usr.usr_id = upr.upr_usr_id'
                ' LEFT JOIN trf.project AS pro ON pro.pro_id = upr.upr_pro_id')
            # As a project-admin, you have access to all roles to these projects
            # As a user, you have access to all of your roles
            if attr_filter:
                qb.where_sql = sql.SQL(
                    'WHERE (upr.upr_pro_id = ANY({}) OR upr.upr_usr_id = {})'
                    ' AND {}').format(sql.Placeholder('auth_pro_id'),
                                      sql.Placeholder('auth_usr_id'),
                                      equal_sql)
            else:  # not possible, 'usr_id' or 'pro_id' is required in filtering parameter
                qb.where_sql = sql.SQL(
                    'WHERE upr.upr_pro_id = ANY({}) OR upr.upr_usr_id = {}'
                ).format(sql.Placeholder('auth_pro_id'),
                         sql.Placeholder('auth_usr_id'))
            attr_filter['auth_pro_id'] = usr.usr_pro_admin
            attr_filter['auth_usr_id'] = usr.usr_id
            qb.values = attr_filter
            qb.order_sql = sql.SQL(
                'ORDER BY upr.upr_usr_id, upr.upr_pro_id'
            )

            query, values = qb.read_query()

            message, code = self.db.read(query, values, many=True)

            if code != 200:
                message = jsonify(message)
            else:
                message = self.schema.jsonify(message, many=True)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(message, code)

    # Authorization in endpoint: project-admin / app-admin
    @app.auth.login_required
    def post(self):
        """
        Create a new mapping of  a user to a project.
        The project-admin can create a mapping to his projects.
        The application-admin can create any mapping.

        Codes:
        * ros-padm: project administrator
        * ros-prd:  project read
        * ros-pwr:  project write
        ---
        operationId: user_project-post
        tags:
          - user
        requestBody:
          required: true
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserProjectSchema'
              example: {"upr_usr_id": "1", "upr_pro_id": "2",
                        "upr_role_code": "ros-prd"}
        responses:
          201:
            description: Created.
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/ID'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.bad_request_400.value
        usr = app.auth.current_user()

        # Get the JSON from the request body and validate the data
        request_data = request.get_json()
        request_params = self.schema.parse_value_param(request_data)
        values = request_params.values

        # Authorization: project-admin required for the new project
        if not usr.usr_app_admin:
            auth_ids = authorization.select_usr_org_role_id(self.db, 'project',
                                                            {'pro_id': values['upr_pro_id']})
            authorization.check_project_role('pro-admin', auth_ids['pro_id'][0], usr)

        try:  # Insert One Object
            qb = CreateQueryBuilder()
            qb.build_insert_sql(sql.SQL('usr.user_project'), values)
            qb.build_values_sql(values)
            qb.returning_sql = sql.SQL('RETURNING upr_usr_id, upr_pro_id, upr_role_code')

            query, values = qb.create_query()

            message, code = self.db.create(query,
                                           ['upr_usr_id', 'upr_pro_id', 'upr_role_code'],
                                           values)
            message = jsonify(message)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(message, code)
