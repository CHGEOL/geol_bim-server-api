
import logging
from datetime import datetime, timedelta

from flask import current_app as app, Blueprint, request, make_response, jsonify
from psycopg2 import sql
import psycopg2

from ..schemas import UserSchema, TokenSchema, PasswordResetSchema, RegisterUserSchema
from ..shared import HttpStatus, CRUD, CreateQueryBuilder
from ..shared import authentication

auth_blueprint = Blueprint('auth', __name__)  # URL-prefix, name

logger = logging.getLogger('apisrv')


# A GET request will lead to this endpoint
@auth_blueprint.route('/')
def index():
    """
    example endpoint
    """
    return 'Welcome to the authentication, please go to /register, /login or /logout.'


@auth_blueprint.route('/login', methods=['post'])
def login():
    """
    Login
    This Endpoint returns an authentication-token and user-data for a successful login.
    ---
    operationId: login
    tags:
      - auth
    method: post
    security: []
    requestBody:
      required: true
      content:
        application/json:
          schema:
            title: UserLogin
            properties:
              email:
                type: string
                example: test@example.com
              password:
                type: string
                example: password
    responses:
      200:
        description: Login successful.
        content:
          application/json:
            schema:
              type: object
              properties:
                message:
                  type: string
                token:
                  type: string
                usr:
                  $ref: '#/components/schemas/UserSchema'
                  type: object
      400:
        $ref: '#/components/responses/bad_request'
      401:
        $ref: '#/components/responses/unauthorized'
      404:
        $ref: '#/components/responses/not_found'
      default:
        $ref: '#/components/responses/unexpected_error'
    """
    logger.info(f'{request.remote_addr} requested {request.url}')

    user_schema = UserSchema(many=False)
    db = CRUD()

    request_data = request.get_json()
    email = request_data.pop('email', None)
    password = request_data.pop('password', None)
    if not email or not password:
        err = app.config['DEFAULT_MESSAGES']['INP_VAL_MULT']
        code = err.code
        err.details = {'email': 'Missing data for required field.',
                       'password': 'Missing data for required field.'}
        message = err.to_dict()
        return make_response(message, code)

    # Select User by the email-address with the view usr.v_user_roles
    query = sql.SQL(  # IMAGE missing
        "SELECT"
        " usr_id, usr_name, usr_name_first, usr_email,"
        " usr_pw, usr_image,"
        " id, role_code, usr_org_name "
        "FROM usr.v_user_roles "
        "WHERE usr_email={}"
        ";").format(sql.Placeholder('usr_email'))
    values = dict(usr_email=email)
    message, code = db.read(query, values)

    if code == 200:  # user found --- else: 404 -> not found, everything else -> error
        usr_message = message
        # dump user (the password is excluded, load_only), dump roles, merge
        current_user = user_schema.dump(message[0])
        current_user_roles = user_schema.dump_roles(message)
        current_user = {**current_user, **current_user_roles}
        # User not connected
        if not current_user['usr_app_conn']:
            # Todo: what error-message / behavior? (Same as No user / no matching password)
            err = app.config['DEFAULT_MESSAGES']['AUTH_BE_CRED']
            return make_response(jsonify(err), err.code)

        try:
            assert len(set([m['usr_id'] for m in usr_message])) == 1  # only one user in result
            usr_id = usr_message[0]['usr_id']
            pw_hash = usr_message[0]['usr_pw']
            # Check Password
            valid, new_hash = app.pwd_context.verify_and_update(password, pw_hash)
            if valid:
                # Generate Token
                token = authentication.generate_auth_token(usr_id)
                # write token to db
                values = dict(usr_token=token.decode('utf-8'),
                              usr_login_date=datetime.now(),
                              usr_login_ip=request.remote_addr,
                              usr_id=usr_message[0]['usr_id'])

                if new_hash:  # save new hash to the db
                    sql_hash = sql.SQL(", usr_pw={}").format(sql.Placeholder('usr_pw'))
                    values['usr_pw'] = new_hash
                else:
                    sql_hash = sql.SQL("")
                query = sql.SQL(
                    'UPDATE usr.user '
                    'SET usr_token={tok}, usr_login_date={dat}, usr_login_ip={ip}{ha} '
                    'WHERE usr_id={id};').format(
                    tok=sql.Placeholder('usr_token'),
                    dat=sql.Placeholder('usr_login_date'),
                    ip=sql.Placeholder('usr_login_ip'),
                    ha=sql_hash,
                    id=sql.Placeholder('usr_id'))
                message, code = db.update(query, values)

                if code == 204:  # else: updating error: 404 / 400
                    code = HttpStatus.ok_200.value
                    # Merge the user and his roles
                    message = dict(usr=current_user,
                                   message='Login successful.', token=token.decode('utf-8'))
            else:  # Invalid password
                err = app.config['DEFAULT_MESSAGES']['AUTH_BE_CRED']
                code = err.code
                message = err.to_dict()

        except KeyError:  # pragma: no cover
            err = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = err.code
            message = err.to_dict()
    elif code == 404:
        err = app.config['DEFAULT_MESSAGES']['AUTH_BE_CRED']
        code = err.code
        message = err.to_dict()
    # else: a db-error occurs: return message / code
    return make_response(jsonify(message), code)


@auth_blueprint.route('/logout', methods=['get'])
@app.auth.login_required
def logout():
    """
    Logout
    This Endpoint requires an authentication-token to identify the user.
    ---
    operationId: logout
    tags:
      - auth
    responses:
      200:
        description: Logout successful.
        content:
          application/json:
            schema:
              type: object
              properties:
                message:
                  type: string
                code:
                  type: integer
      400:
        $ref: '#/components/responses/bad_request'
      401:
        $ref: '#/components/responses/unauthorized'
      404:
        $ref: '#/components/responses/not_found'
      default:
        $ref: '#/components/responses/unexpected_error'
    """
    logger.info(f'{request.remote_addr} requested {request.url}')
    db = CRUD()

    # Currently logged in user
    usr = app.auth.current_user()  # CurrentUserModel

    # Set Token to null
    query = sql.SQL('UPDATE usr.user SET usr_token={} WHERE usr_id={};').format(
        sql.Placeholder('usr_token'), sql.Placeholder('usr_id'))
    values = dict(usr_token=None, usr_id=usr.usr_id)
    message, code = db.update(query, values)

    if code == 204:  # else: updating error: 404 / 400
        message = f'Logout user {usr.usr_name_first} {usr.usr_name} successful.'
        code = HttpStatus.ok_200.value
        message = dict(message=message, code=code)
    else:  # pragma: no cover
        err = app.config['DEFAULT_MESSAGES']['INT_ERR']
        code = err.code
        message = err.to_dict()

    return make_response(jsonify(message), code)


@auth_blueprint.route('/register', methods=['post'])
def register():
    """
    Register a user for personal organisation.
    Sends an email with a confirmation link to the new user.
    The link will contain the provided url.
    This Endpoint creates a user, a personal organisation and the required roles.
    The required roles are ros-oprs and ros-opl, but without ros-acon.
    The role ros-acon will be added on PATCH /register.
    The newly registered user will get ros-prd permission on a default reference project.
    The default reference project is configurable.
    ---
    operationId: register-post
    tags:
      - auth
    method: post
    security: []
    requestBody:
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/RegisterUserSchema'
          example: {"usr_name": "Test1", "usr_name_first": "Kunibert",
                    "usr_pw": "Password", "usr_email": "test@example.com",
                    "usr_title": "Herr", "usr_company": "FHNW IDIBAU",
                    "usr_phone": "+41 79 123 45 67", "usr_address_street": "register street",
                    "usr_address_postcode": "1000", "usr_address_town": "Lausanne",
                    "usr_address_country": "Schweiz",
                    "usr_confirmed_agb": true, "usr_confirmed_query": true,
                    "url": "http://localhost:5000/auth/register/confirm"}
    responses:
      201:
        description: Created.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ID'
      400:
        $ref: '#/components/responses/bad_request'
      401:
        $ref: '#/components/responses/unauthorized'
      404:
        $ref: '#/components/responses/not_found'
      default:
        $ref: '#/components/responses/unexpected_error'
    """
    logger.info(f'{request.remote_addr} requested {request.url}')
    message, code = {}, HttpStatus.bad_request_400.value

    register_user_schema = RegisterUserSchema(many=False)
    db = CRUD()

    request_data = request.get_json()
    request_params = register_user_schema.parse_value_param(request_data, partial=('usr_id',))
    attr_values = request_params.values
    url = attr_values.pop('url')

    conn, cur = db.get_cursor()

    try:
        # check if mail address already exists
        query = sql.SQL(
            "SELECT"
            " usr_id, usr_email "
            "FROM usr.user "
            "WHERE usr_email={}"
            ";").format(sql.Placeholder('usr_email'))
        values = dict(usr_email=attr_values['usr_email'])
        message, code = db._read(cur, query, values)
        if code == 404:
            # email not found, ready to register
            usr_id, org_id, roles = None, None, list()
            # insert user
            qb = CreateQueryBuilder()
            qb.build_insert_sql(sql.SQL('usr.user'), attr_values)
            qb.build_values_sql(attr_values)
            qb.returning_sql = sql.SQL('RETURNING usr_id')
            query, values = qb.create_query()
            message, code = db._create(cur, query, 'usr_id', values)
            if code == 201:
                usr_id = message['usr_id']

                # insert organisation
                values = {'org_name': f'@{usr_id}'}
                qb = CreateQueryBuilder()
                qb.build_insert_sql(sql.SQL('usr.organisation'), values)
                qb.build_values_sql(values)
                qb.returning_sql = sql.SQL('RETURNING org_id')
                query, values = qb.create_query()
                message, code = db._create(cur, query, 'org_id', values)
                if code == 201:
                    org_id = message['org_id']

                    # insert roles
                    # oprs
                    query = sql.SQL(
                        'INSERT INTO usr.user_organisation'
                        ' (uog_usr_id, uog_org_id, uog_role_code) '
                        'VALUES ({}, {}, {}) RETURNING uog_usr_id;'
                    ).format(sql.Placeholder(), sql.Placeholder(), sql.Placeholder())
                    values = [usr_id, org_id, 'ros-oprs']
                    message, code = db._create(cur, query, 'uog_usr_id', values)
                    if code == 201:
                        roles.append('ros-oprs')
                        # opl
                        query = sql.SQL(
                            'INSERT INTO usr.user_organisation'
                            ' (uog_usr_id, uog_org_id, uog_role_code) '
                            'VALUES ({}, {}, {}) RETURNING uog_usr_id;'
                        ).format(sql.Placeholder(), sql.Placeholder(), sql.Placeholder())
                        values = [usr_id, org_id, 'ros-opl']
                        message, code = db._create(cur, query, 'uog_usr_id', values)
                        if code == 201:
                            roles.append('ros-opl')
                            pro_id = app.config['REGISTER_DEFAULT_REFERENCE_PROJECT_ID']
                            query = sql.SQL(
                                'INSERT INTO usr.user_project'
                                ' (upr_usr_id, upr_pro_id, upr_role_code) '
                                'VALUES ({}, {}, {}) RETURNING upr_usr_id;'
                            ).format(sql.Placeholder(), sql.Placeholder(), sql.Placeholder())
                            values = [usr_id, pro_id, 'ros-prd']
                            message, code = db._create(cur, query, 'upr_usr_id', values)
                            if code == 201:
                                roles.append('ros-prd')

                                name = f'{attr_values["usr_name_first"]} {attr_values["usr_name"]}'
                                user_mail = attr_values['usr_email']
                                # create confirmation token
                                token = authentication.generate_url_safe_token()
                                # set activation and expiration time
                                now = datetime.now()
                                token_expiration = int(app.config['PASSWORD_RESET_TOKEN_DURATION'])
                                expire = now + timedelta(seconds=token_expiration)
                                # store the token in the db
                                query = sql.SQL(
                                    "INSERT INTO usr.token "
                                    "(tok_usr_id, tok_token, tok_expire, tok_token_code) "
                                    "VALUES ({}, {}, {}, {}) RETURNING tok_id;"
                                ).format(sql.Placeholder(), sql.Placeholder(),
                                         sql.Placeholder(), sql.Placeholder())
                                values = [usr_id, token, expire, 'tok-crg']

                                tok_message, code = db._create(cur, query, 'tok_id', values)
        else:
            # email address already exists
            err = app.config['DEFAULT_MESSAGES']['REG_MAIL_DUP']
            message = err.to_dict()
            code = err.code

        if code == 201:  # everything ok code from create token
            # Send confirmation mail with a link to confirm after conn.commit()
            url = f'{url}{token}'
            try:
                authentication.send_mail('register_confirm', 'E-Mail Bestätigung', user_mail,
                                         user_name=name, url=url)
                logger.info(f'Confirmation-Token {token} sent to {user_mail}.')
                message = {'usr_id': usr_id, 'org_id': org_id, 'roles': roles}
                # DB-Operation and Send Mail is done, then commit
                conn.commit()
            except ConnectionRefusedError as exc:  # Todo: what kind of errors could occur?
                conn.rollback()
                mail_server = app.config['MAIL_SERVER']
                logger.error(f'ConnectionRefusedError:mailserver '
                             f'{mail_server} refused connection {exc}')
                err = app.config['DEFAULT_MESSAGES']['MAIL_NA']
                code = err.code
                message = err.to_dict()
            except Exception as exc:
                conn.rollback()
                logger.error(f'MAIL EXCEPTION NOT HANDLED: {type(exc.__name__)}: {exc}')
                err = app.config['DEFAULT_MESSAGES']['MAIL_NA']
                code = err.code
                message = err.to_dict()
        else:
            conn.rollback()
        cur.close()

    except (ValueError, TypeError):  # pragma: no cover
        # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
        message = app.config['DEFAULT_MESSAGES']['INT_ERR']
        code = message.code
        message = message.to_dict()
    # Catch DB-Exceptions
    except psycopg2.Error as exc:
        logger.info('Rolling back.')
        conn.rollback()
        cur.close()
        err = db.get_error_message(exc)
        code = err.code
        message = err.to_dict()
    finally:
        return make_response(jsonify(message), code)


@auth_blueprint.route('/register', methods=['patch'])
def register_confirm():
    """
    Confirm the registration of a user
    This Endpoint creates a the required roles ros-acon, that the user is able to log in.
    Doesn't check expiration of token.
    ---
    operationId: register-patch
    tags:
      - auth
    security: []
    requestBody:
      required: true
      content:
        application/json:
          schema:
            title: RegisterConfirmation
            properties:
              token:
                type: string
                example: 'unpredictable'
    responses:
      200:
        description: registration confirmation successful
        content:
          application/json:
            schema:
              type: object
              properties:
                message:
                  type: string
                code:
                  type: integer
      400:
        $ref: '#/components/responses/bad_request'
      401:
        $ref: '#/components/responses/unauthorized'
      404:
        $ref: '#/components/responses/not_found'
      default:
        $ref: '#/components/responses/unexpected_error'
    """
    tok_schema = TokenSchema()
    db = CRUD()

    # Validate the input
    request_data = request.get_json()
    token = {'tok_token': request_data.pop('token', None)}

    # Validate the token
    token = tok_schema.parse_value_param(token, partial=('tok_id', 'tok_expire'))
    token = token.values['tok_token']

    conn, cur = db.get_cursor()

    try:
        # get the token and the user from the db
        query = sql.SQL(
            "SELECT"
            " tok.tok_expire,"
            " usr.usr_id AS tok_usr_id "
            "FROM usr.token AS tok"
            " LEFT JOIN usr.\"user\" AS usr on usr.usr_id = tok.tok_usr_id "
            "WHERE tok_token = {} AND tok_token_code = {};"
        ).format(sql.Placeholder(), sql.Placeholder())
        values = [token, 'tok-crg']

        message, code = db._read(cur, query, values)

        if code == 200:
            tok = tok_schema.dump(message[0])

            # valid confirmation, add application role 'app-con' to user
            query = sql.SQL(
                'INSERT INTO usr.user_application (uap_usr_id, uap_role_code) '
                'VALUES ({}, {}) RETURNING uap_usr_id;'
            ).format(sql.Placeholder(), sql.Placeholder())
            values = [tok['tok_usr_id'], 'ros-acon']
            message, code = db._create(cur, query, 'uap_usr_id', values)
            if code == 201:
                code = HttpStatus.ok_200.value
                message = dict(roles=['ros-acon'])
                # commit
                conn.commit()
            else:
                logger.error(f'create user_application_role with {token} failed')
                err = app.config['DEFAULT_MESSAGES']['INT_ERR']
                code = err.code
                message = err.to_dict()
                conn.rollback()
        else:
            # token not found: same error as expired
            err = app.config['DEFAULT_MESSAGES']['TOK_EXP']
            code = err.code
            message = err.to_dict()
            conn.rollback()

        # close cursor
        cur.close()

    except (ValueError, TypeError):  # pragma: no cover
        # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
        message = app.config['DEFAULT_MESSAGES']['INT_ERR']
        code = message.code
        message = message.to_dict()
    # Catch DB-Exceptions
    except psycopg2.Error as exc:
        logger.info('Rolling back.')
        conn.rollback()
        cur.close()
        err = db.get_error_message(exc)
        code = err.code
        message = err.to_dict()
    finally:
        return make_response(jsonify(message), code)


@auth_blueprint.route('/password/reset', methods=['post'])
def reset_password_send_token():
    """
    Send an email with a reset link.
    Sends an email to the given address with (given) url to reset the password
    The url contains a (timed) token as 'www.example.com?token=s3cre7'.
    ---
    operationId: reset-post
    tags:
      - auth
    security: []
    requestBody:
      required: true
      content:
        application/json:
          schema:
            title: ResetPassword
            properties:
              email:
                type: string
                example: test@example.com
              url:
                type: string
                example: http://localhost:5000/auth/password/reset
    responses:
      200:
        description: Email sent to the given address.
        content:
          application/json:
            schema:
              type: object
              properties:
                message:
                  type: string
                code:
                  type: integer
      400:
        $ref: '#/components/responses/bad_request'
      401:
        $ref: '#/components/responses/unauthorized'
      404:
        $ref: '#/components/responses/not_found'
      default:
        $ref: '#/components/responses/unexpected_error'
    """
    db = CRUD()
    usr_schema = UserSchema()
    pwr_schema = PasswordResetSchema()

    # Get the JSON from the request body and validate the data
    request_data = request.get_json()
    # validate the input: returns 400 if no email or invalid email
    values = pwr_schema.parse_value_param(request_data)

    # Get the user: name, email
    usr_email = values.values['email']
    url = values.values['url']

    conn, cur = db.get_cursor()

    try:
        # Get user by email
        usr_query = sql.SQL(
            "SELECT usr_id, usr_name, usr_name_first "
            "FROM usr.\"user\" "
            "WHERE usr_email = {};"
        ).format(sql.Placeholder())
        values = [usr_email]

        usr_message, code = db._read(cur, usr_query, values)
        message = {'message': f'Es wurde eine Email an die angegebene '
                              f'Adresse gesendet '
                              f'({usr_email}). Falls sie keine Email erhalten haben, '
                              f'prüfen Sie bitte, ob die Email-Adresse korrekt ist.',
                   'code': HttpStatus.ok_200.value,
                   'details': {'usr_email': usr_email}}
        if code == 200:
            usr = usr_schema.dump(usr_message[0])
            name = f'{usr["usr_name_first"]} {usr["usr_name"]}'

            # random token
            token = authentication.generate_url_safe_token()
            # set activation and expiration time
            now = datetime.now()
            expire = now + timedelta(seconds=int(app.config['PASSWORD_RESET_TOKEN_DURATION']))

            # store the token in the db
            query = sql.SQL(
                "INSERT INTO usr.token (tok_usr_id, tok_token, tok_expire, tok_token_code) "
                "VALUES ({}, {}, {}, {}) RETURNING tok_id;"
            ).format(sql.Placeholder(), sql.Placeholder(), sql.Placeholder(), sql.Placeholder())
            values = [usr['usr_id'], token, expire, 'tok-rpw']

            tok_message, tok_code = db._create(cur, query, 'tok_id', values)

            if tok_code == 201:
                # Send the email with a link to reset the url
                url = f'{url}{token}'
                try:
                    authentication.send_mail('reset_password', 'Passwort zurücksetzen', usr_email,
                                             user_name=name, url=url)
                    logger.info(f'Password-Reset-Token {token} sent to {usr_email}.')
                    # DB-Operation and Send Mail is done, then commit
                    conn.commit()
                except ConnectionRefusedError as exc:  # Todo: what kind of errors could occur?
                    conn.rollback()
                    mail_server = app.config['MAIL_SERVER']
                    logger.error(f'ConnectionRefusedError:mailserver '
                                 f'{mail_server} refused connection {exc}')
                    err = app.config['DEFAULT_MESSAGES']['MAIL_NA']
                    code = err.code
                    message = err.to_dict()
                except Exception as exc:
                    conn.rollback()
                    logger.error(f'MAIL EXCEPTION NOT HANDLED: {type(exc.__name__)}: {exc}')
                    err = app.config['DEFAULT_MESSAGES']['MAIL_NA']
                    code = err.code
                    message = err.to_dict()

            else:  # store token in the db failed
                logger.error('Storing the token in the db failed.')
                err = app.config['DEFAULT_MESSAGES']['INT_ERR']
                code = err.code
                message = err.to_dict()

        else:  # Select user BY email failed
            # same message, as if the email was sent --> no guessing of emails
            logger.info(f'Email {usr_email} not found in DB.')
            code = HttpStatus.ok_200.value

        # close cursor
        cur.close()

    except (ValueError, TypeError):  # pragma: no cover
        # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
        message = app.config['DEFAULT_MESSAGES']['INT_ERR']
        code = message.code
        message = message.to_dict()
    # Catch DB-Exceptions
    except psycopg2.Error as exc:
        logger.info('Rolling back.')
        conn.rollback()
        cur.close()
        err = db.get_error_message(exc)
        code = err.code
        message = err.to_dict()
    finally:
        return make_response(jsonify(message), code)


@auth_blueprint.route('/password/reset', methods=['patch'])
def reset_password():
    """
    Reset the password
    The reset token is submitted in the body with the new password
    ---
    operationId: reset-patch
    tags:
      - auth
    security: []
    requestBody:
      required: true
      content:
        application/json:
          schema:
            title: ResetPassword
            properties:
              password:
                type: string
                example: 'Password'
              token:
                type: string
                example: ''
    responses:
      200:
        description: Reset successful.
        content:
          application/json:
            schema:
              type: object
              properties:
                message:
                  type: string
                code:
                  type: integer
      400:
        $ref: '#/components/responses/bad_request'
      401:
        $ref: '#/components/responses/unauthorized'
      404:
        $ref: '#/components/responses/not_found'
      default:
        $ref: '#/components/responses/unexpected_error'
    """
    tok_schema = TokenSchema()
    usr_schema = UserSchema()
    db = CRUD()

    # Validate the input
    request_data = request.get_json()
    token = {'tok_token': request_data.pop('token', None)}
    user = {'usr_pw': request_data.pop('password', None)}

    # Validate the token
    token = tok_schema.parse_value_param(token, partial=('tok_id', 'tok_expire'))
    token = token.values['tok_token']
    # Validate and hash the password
    user = usr_schema.parse_value_param(user, partial=('usr_id', 'usr_email',
                                                       'usr_name', 'usr_name_first'))
    password = user.values['usr_pw']  # password is hashed by parse_value_param

    conn, cur = db.get_cursor()

    try:

        # get the token and the user from the db
        query = sql.SQL(
            "SELECT"
            " tok.tok_id,"
            " tok.tok_expire,"
            " usr.usr_id "
            "FROM usr.token AS tok"
            " LEFT JOIN usr.\"user\" AS usr on usr.usr_id = tok.tok_usr_id "
            "WHERE tok_token = {} AND tok_token_code = {};"
        ).format(sql.Placeholder(), sql.Placeholder())
        values = [token, 'tok-rpw']

        message, code = db._read(cur, query, values)

        if code == 200:
            tok = tok_schema.dump(message[0])
            usr = usr_schema.dump(message[0])
            now = datetime.now()
            expire = datetime.fromisoformat(tok['tok_expire'])

            if expire > now:
                # store the password in the database
                query = sql.SQL(
                    "UPDATE usr.\"user\" SET usr_pw = {} WHERE usr_id = {};"
                ).format(sql.Placeholder(), sql.Placeholder())
                values = [password, usr['usr_id']]
                pw_message, code = db._update(cur, query, values)
                if code == 204:
                    # set token expire to now, so that a token can be used only once
                    query = sql.SQL(
                        "UPDATE usr.token SET tok_expire = {} WHERE tok_id = {};"
                    ).format(sql.Placeholder(), sql.Placeholder())
                    values = [now, tok['tok_id']]
                    message, code = db._update(cur, query, values)
                    if code == 204:
                        code = HttpStatus.ok_200.value
                        message = pw_message
                        # commit
                        conn.commit()
                    else:
                        # set token expire to now failed
                        logger.error(f'set token {token} expire to now failed')
                        err = app.config['DEFAULT_MESSAGES']['INT_ERR']
                        code = err.code
                        message = err.to_dict()
                        conn.rollback()
                else:
                    # Updating password failed
                    logger.error(f'Updating the password with token {token} failed.')
                    err = app.config['DEFAULT_MESSAGES']['INT_ERR']
                    code = err.code
                    message = err.to_dict()
                    conn.rollback()

            else:
                # token expired
                err = app.config['DEFAULT_MESSAGES']['TOK_EXP']
                code = err.code
                message = err.to_dict()
                conn.rollback()

        else:
            # token not found: same error as expired
            err = app.config['DEFAULT_MESSAGES']['TOK_EXP']
            code = err.code
            message = err.to_dict()
            conn.rollback()

        # close cursor
        cur.close()

    except (ValueError, TypeError):  # pragma: no cover
        # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
        message = app.config['DEFAULT_MESSAGES']['INT_ERR']
        code = message.code
        message = message.to_dict()
    # Catch DB-Exceptions
    except psycopg2.Error as exc:
        logger.info('Rolling back.')
        conn.rollback()
        cur.close()
        err = db.get_error_message(exc)
        code = err.code
        message = err.to_dict()
    finally:
        return make_response(jsonify(message), code)
