
from flask import Blueprint

from .trf_project import Project, ProjectCollection, ProjectCopy
from .trf_block import Block, BlockCollection, BlockData
from .trf_code import Code, CodeCollection

transform_blueprint = Blueprint('transform', __name__)  # URL-prefix, name


# A GET request will lead to this endpoint
@transform_blueprint.route('/')
def index():
    """
    example endpoint
    """
    return 'Welcome to the transform API of the GEOL_BIM Server.'


# Add Views to the app: register Collection first, then Single
transform_blueprint.add_url_rule('/projects',
                               view_func=ProjectCollection.as_view('project_collection_api'))
transform_blueprint.add_url_rule('/projects/<int:pro_id>',
                               view_func=Project.as_view('project_api'))
transform_blueprint.add_url_rule('/projects/<int:pro_id>/copy',
                                view_func=ProjectCopy.as_view('project_copy_api'))

transform_blueprint.add_url_rule('/blocks',
                               view_func=BlockCollection.as_view('block_collection_api'))
transform_blueprint.add_url_rule('/blocks/<int:blk_id>',
                               view_func=Block.as_view('block_api'))
transform_blueprint.add_url_rule(
    '/blocks/<int:blk_id>/dta', view_func=BlockData.as_view('blk_dta_api'))


transform_blueprint.add_url_rule('/codes',
                               view_func=CodeCollection.as_view('code_collection_api'))
transform_blueprint.add_url_rule('/codes/<string:cod_id>',
                               view_func=Code.as_view('code_api'))

