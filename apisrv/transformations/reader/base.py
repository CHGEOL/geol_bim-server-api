from abc import ABC, abstractmethod

class GeometryReader(ABC):

    def __init__(self, transform=None):
        self.transform = transform if transform else (0,0,0)
    
    @abstractmethod
    def _read(self, strm):
        """Abstract method for reading geometry"""

    def read(self, strm):
        """Read a geometry and return faces and vertices"""
        # here you can inititalize things for all GeometryReader subclasses
        res = self._read(strm)
        # maybe do something with res
        return res
