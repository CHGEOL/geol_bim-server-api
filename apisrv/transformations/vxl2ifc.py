
import logging
import time

import pandas as pd 
from io import BytesIO

from .common import *

__all__ = ['vxlCfgFileNames', 'vxl2ifc']

VXL_DF_NAME = 'Voxel_GeologicFeature'

VXL_ID_CFG_KEY = 'GeoID'
VXL_FILE_CFG_KEY = 'GeoFileName'
VXL_BASE_TYPE_CFG_KEY = GF_BASE_TYPE_CFG_KEY
VXL_PERSPECTIVE_CFG_KEY = GF_PERSPECTIVE_CFG_KEY
VXL_SUB_TYPE_CFG_KEY = GF_SUB_TYPE_CFG_KEY
VXL_CLASS_CFG_KEY = GF_CLASS_CFG_KEY
VXL_OBS_MTH_CFG_KEY = GF_OBS_MTH_CFG_KEY
VXL_OBS_MTH_GEO_CFG_KEY = GF_OBS_MTH_GEO_CFG_KEY
VXL_PURPOSE_CFG_KEY = GF_PURPOSE_CFG_KEY
VXL_DESC_CFG_KEY = GF_DESC_CFG_KEY
VXL_X_CFG_KEY = 'X'
VXL_Y_CFG_KEY = 'Y'
VXL_Z_CFG_KEY = 'Z'
VXL_EXT_X_CFG_KEY = 'ExtX'
VXL_EXT_Y_CFG_KEY = 'ExtY'
VXL_EXT_Z_CFG_KEY = 'ExtZ'
VXL_ID_N_FILE_CFG_SET = {VXL_ID_CFG_KEY, VXL_FILE_CFG_KEY}
VXL_BASE_CFG_SET = {
    VXL_BASE_TYPE_CFG_KEY, VXL_PERSPECTIVE_CFG_KEY, VXL_SUB_TYPE_CFG_KEY, VXL_CLASS_CFG_KEY,
    VXL_OBS_MTH_CFG_KEY, VXL_OBS_MTH_GEO_CFG_KEY, VXL_PURPOSE_CFG_KEY, VXL_DESC_CFG_KEY,
    VXL_X_CFG_KEY, VXL_Y_CFG_KEY, VXL_Z_CFG_KEY,
    VXL_EXT_X_CFG_KEY, VXL_EXT_Y_CFG_KEY, VXL_EXT_Z_CFG_KEY
}
VXL_CFG_SET = set.union(VXL_ID_N_FILE_CFG_SET, VXL_BASE_CFG_SET)

CSV_DOC_NAME = 'CSV'

INVALID_PTY_MAPPING_TXT = 'Im Excel wurde für {} angegeben, dass es im CSV der Spalte {} entspricht. Diese Spalte fehlt aber im CSV.'

class VxlDta(EltData):

    def __init__(self):
        super().__init__()
        self.mappedPty = {}
        self.valueForAll = {}

    def assignMappedOrNot(self, ptyName, row, rowNo):
        val = row[ptyName]
        if pd.isna(val) or not str(val).startswith(':'):
            val = self.validateIfBasePty(ptyName, rowNo, val, EXCEL_DOC_NAME)
            self.valueForAll[ptyName] = val
        else:
            # validation must happen for each
            mappedPtyName = str(val)[1:]
            self.mappedPty[ptyName] = mappedPtyName

    def setDefRow(self, row, pty2ptySetName, rowNo=1):
        for ptyName in VXL_BASE_CFG_SET:
            # The pty2ptySetName does not contain set names for these properties.
            self.assignMappedOrNot(ptyName, row, rowNo)
        for ptyName, ptySetName in pty2ptySetName.items():
            self.assignMappedOrNot(ptyName, row, rowNo)

    def validateIfBasePty(self, ptyName, rowNo, val, docName):
        if ptyName == VXL_X_CFG_KEY or ptyName == VXL_Y_CFG_KEY or ptyName == VXL_Z_CFG_KEY \
                or ptyName == VXL_EXT_X_CFG_KEY or ptyName == VXL_EXT_Y_CFG_KEY or ptyName == VXL_EXT_Z_CFG_KEY:
            #if pd.isna(val) or not(type(val) is float or type(val) is int):
            return valNo(val, GroupableError(VALUE_NUMBER_TXT + ' ' + INVALID_VALUE_TXT, docName, ptyName, rowNo, val), self.errors)
        else:
            return super().validateIfBasePty(ptyName, rowNo, val, docName)

    @property
    def isMapped(self):
        return 0 < len(self.mappedPty)

    def actValue(self, docName, ptyName, row, rowNo): # overrides the simple one without mapping
        if ptyName in self.mappedPty:
            mPtyName = self.mappedPty[ptyName]
            val = row[mPtyName]
            val = self.validateIfBasePty(ptyName, rowNo, val, docName) # not mPtyName
            # Do NOT raise errors immediately but collect and group them
            return val
        else: # must be in self.valueForAll
            # No need for row, since there is nothing in there.
            return self.valueForAll[ptyName]
    
    def validateMapping(self, row): # always called on the first CSV row
        for srcPty, destPty in self.mappedPty.items():
            if destPty not in row:
                raise ValueError(INVALID_PTY_MAPPING_TXT.format(srcPty, destPty))

    def setRow(self, row, rowNo, pty2ptySetName, docName): # Used for CSV or Excel
        self.pts = {} # reset properties (since called multiple times)
        # Derive from GfData and return super(VxlDta, self).setRow(row, pty2ptySetName) here does not help.
        self.assignGfProps(docName, row, rowNo, pty2ptySetName) # can only be used if VXL_BASE_TYPE_CFG_KEY == GF_BASE_TYPE_CFG_KEY, etc.

        # The ones typical for voxel
        self.x = self.actValue(docName, VXL_X_CFG_KEY, row, rowNo)
        self.y = self.actValue(docName, VXL_Y_CFG_KEY, row, rowNo)
        self.z = self.actValue(docName, VXL_Z_CFG_KEY, row, rowNo)
        self.xExt = self.actValue(docName, VXL_EXT_X_CFG_KEY, row, rowNo)
        self.yExt = self.actValue(docName, VXL_EXT_Y_CFG_KEY, row, rowNo)
        self.zExt = self.actValue(docName, VXL_EXT_Z_CFG_KEY, row, rowNo)
        # Grant all the 6 are numeric


def ifcCreateVoxel(ifcw, dta, proOrigCalc, containerPlacementId):

    x = float(dta.x) - proOrigCalc[0]
    y = float(dta.y) - proOrigCalc[1]
    z = float(dta.z) - proOrigCalc[2]
    dx = float(dta.xExt)
    dy = float(dta.yExt)
    dz = float(dta.zExt)

    ifcw.ncpLocalPlacement(relativeToId=containerPlacementId)
    spacePlacementId = ifcw.lastLocalPlacementId
    ifcw.ncpAxis2placement3D((x, y, z), (0.0, 0.0, 1.0), (1.0, 0.0, 0.0))
    extrusionPlacementId = ifcw.lastAxis2placement3DId
    pointListExtrusionArea = [
        (0.0-(dx/2), 0.0-(dy/2), (-dz/2)),
        (0.0+(dx/2), 0.0-(dy/2), (-dz/2)),
        (0.0+(dx/2), 0.0+(dy/2), (-dz/2)),
        (0.0-(dx/2), 0.0+(dy/2),(-dz/2)),
        (0.0-(dx/2), 0.0-(dy/2), (-dz/2)) ] #Center Point
    extrusion = dz

    ifcw.ncpPolyline(pointListExtrusionArea)
    ifcw.nttArbitraryClosedProfileDef('AREA', None, ifcw.lastPolylineId)
    closedProfileId = ifcw.lastArbitraryClosedProfileDefId
    ifcw.nttDirection((0., 0., 1.))
    ifcw.nttExtrudedAreaSolid(closedProfileId, extrusionPlacementId, ifcw.lastDirectionId, extrusion)
    ifcw.nttShapeRepresentation(ifcw.contextId, 'Body', 'SweptSolid', [ifcw.lastExtrudedAreaSolidId])
    ifcw.nttProductDefinitionShape(None, None, [ifcw.lastShapeRepresentationId])
    productId = ifcw.lastProductDefinitionShapeId
    spaceId = createGf(ifcw, dta.name, dta.desc, dta.baseType, dta.perspective, dta.objType, dta.tag, spacePlacementId, productId)
    # ifcw.nttSpace(ifcw.createGuid(), ifcw.ownerHistoryId, None, 'Voxel', 'GEOLOGY_VOXEL', 
    #     spacePlacementId, ifcw.lastProductDefinitionShapeId, None, 'ELEMENT', 'USERDEFINED', None)
    # spaceId = ifcw.lastSpaceId

    segmentSpaceIds = [spaceId]

    createIfcProperties(dta.pts, ifcw, segmentSpaceIds)
    # The createIfcProperties creates IfcText instead of IfcReal compared to the orig code but guess that's ok.
    return spaceId

def vxlCfgFileNames(xlsxStream):
    return cfgFileNames(xlsxStream, VXL_DF_NAME, VXL_FILE_CFG_KEY)

def vxl2ifc(ifcw, origShift, xlsxStream, filesDic):

    logger = logging.getLogger('apisrv')

    logger.info('Converting Voxel Data to IFC.')
    startTime = time.time()

    dfVxl = pd.read_excel(xlsxStream,sheet_name=VXL_DF_NAME, skiprows=[0,2],converters={VXL_ID_CFG_KEY:str})
    
    pty2ptySetName = readPtyToPtySetName(xlsxStream, VXL_DF_NAME, 16) # does no validation

    # #################################
    # Do various validations and checks

    # It is not too inefficient to open the same project config again, since it is a small tab.
    dfProMeta = pd.read_excel(xlsxStream,sheet_name=PRO_DF_NAME, index_col=None, header=None)
    proCfg = readProjectConfig(dfProMeta) # does also validate

    grantColumnsPresent(VXL_DF_NAME, dfVxl, VXL_CFG_SET)
    grantColumnValuesUnique(VXL_DF_NAME, dfVxl, VXL_ID_CFG_KEY)
    grantColumnValuesUnique(VXL_DF_NAME, dfVxl, VXL_FILE_CFG_KEY, orNone=True)
    id2Dta = {}
    for i, row in dfVxl.iterrows():
        id = row[VXL_ID_CFG_KEY]
        fileName = row[VXL_FILE_CFG_KEY]
        # Validate the file of fileName exists.
        # Validate either there is at least one mapped property and fileName is set or there is no mapped property and fileName is not set.
        dta = VxlDta()
        dta.setDefRow(row, pty2ptySetName, i+4)
        if not pd.isna(fileName):
            if not fileName in filesDic:
                raise ValueError(f"""Die im Excel für GeologicFeature {id} angegebene Datei {fileName} ist nicht vorhanden.""")
            if not dta.isMapped:
                raise ValueError(f"""Im Excel ist für GeologicFeature {id} zwar eine Datei {fileName} angegeben, aber keiner der Zellenwerte beschreibt einen Spaltentitel in der Datei. Solche Zellenwerte beginnen mit einem Doppelpunkt um sie von fixen Werten zu unterscheiden.""")
        else:
            if dta.isMapped:
                raise ValueError(f"""Im Excel ist für GeologicFeature {id} keine Datei {fileName} angegeben, aber es gibt Zellenwerte, welche Spaltentitel in einer möglichen Datei beschreiben. Solche Zellenwerte beginnen mit einem Doppelpunkt um sie von fixen Werten zu unterscheiden.""")
        if 0 < len(dta.errors):
            groupedErrors = GroupableError.groupErrors(dta.errors)
            raise groupedErrors[0] # raise first and ignore the others (hope its only one - a grouped one)

        id2Dta[id] = dta

    # ######################
    # Start writing the file
    containerId = ifcw.externalSpatialElementId
    containerPlacementId = ifcw.externalSpatialElementPlacementId

    assemblyName = proCfg[PRO_NAME_CFG_KEY] # It is not correct, but we do not have any other source for the name.
    assemblyId = createAssemblyWithProps(ifcw, assemblyName, '3DModel', proCfg)

    sliceEltIds = []

    # Read each feature file
    ifcNoneMappedEltNtts = []
    collectedWarns = []
    collectedErrors = []
    for i, row in dfVxl.iterrows():
        id = row[VXL_ID_CFG_KEY]
        fileName = row[VXL_FILE_CFG_KEY]
        dta = id2Dta[id] # get the prepared VxlDta (also providing x,y,z, xExt,yExt,zExt info/mapping)

        if dta.isMapped:
            try:
                csvStream = BytesIO(filesDic[fileName])
                dfCsv = pd.read_csv(csvStream, sep =",")
            except:
                raise ValueError(f'Fehler beim Laden der Datei "{fileName}" des Geologic Feature "{id}".')

            for j, csvRow in dfCsv.iterrows():
                dta.validateMapping(csvRow) # raises according error if necessary
                break # only the first one

            ifcFileEltNtts = []
            for j, csvRow in dfCsv.iterrows():

                # Reset the properties each time and reread them either mapped from the row or take the common value.
                origNofErrs = len(dta.errors)
                dta.setRow(csvRow, j+2, pty2ptySetName, CSV_DOC_NAME) # generates errors or warnings
                if origNofErrs < len(dta.errors): continue # ignore faulty voxel
                voxelNtt = ifcCreateVoxel(ifcw, dta, origShift, containerPlacementId)
                ifcFileEltNtts.append(voxelNtt)

            errsNwarns = [] + dta.errors + dta.warnings
            hasWarningsSoHandle(ifcw, errsNwarns)

            if ifcw.getVersion() != 'IFC4x3':
                ifcw.nttBuildingElementProxy(ifcw.createGuid(), ifcw.ownerHistoryId, fileName, None, 'SLICE', None, None, None, None) # No placement nor representation
                sliceId = ifcw.lastBuildingElementProxyId # This id does the spacial attachment and gets all the properties attached.
            else:
                ifcw.nttGeoslice(ifcw.createGuid(), ifcw.ownerHistoryId, fileName, None, 'SLICE', None, None, None) # No placement nor representation
                sliceId = ifcw.lastGeosliceId

            ifcw.nttRelAggregates(ifcw.createGuid(), ifcw.ownerHistoryId, fileName + ' slice aggregation', None, sliceId, ifcFileEltNtts)
            sliceEltIds.append(sliceId)

        else:
            # Set the same row as in setDefRow again to copy all static values where they should be.
            origNofErrs = len(dta.errors)
            dta.setRow(row, i+4, pty2ptySetName, EXCEL_DOC_NAME)
            if origNofErrs < len(dta.errors): continue # ignore faulty voxel
            voxelNtt = ifcCreateVoxel(ifcw, dta, origShift, containerPlacementId)
            ifcNoneMappedEltNtts.append(voxelNtt)

            collectedWarns += dta.warnings
            collectedErrors += dta.errors

    errsNwarns = [] + collectedErrors + collectedWarns
    hasWarningsSoHandle(ifcw, errsNwarns)

    if 0 < len(ifcNoneMappedEltNtts):
        if ifcw.getVersion() != 'IFC4x3':
            ifcw.nttBuildingElementProxy(ifcw.createGuid(), ifcw.ownerHistoryId, 'General', None, 'SLICE', None, None, None, None) # No placement nor representation
            sliceId = ifcw.lastBuildingElementProxyId # This id does the spacial attachment and gets all the properties attached.
        else:
            ifcw.nttGeoslice(ifcw.createGuid(), ifcw.ownerHistoryId, 'General', None, 'SLICE', None, None, None) # No placement nor representation
            sliceId = ifcw.lastGeosliceId

        ifcw.nttRelAggregates(ifcw.createGuid(), ifcw.ownerHistoryId, 'General slice aggregation', None, sliceId, ifcNoneMappedEltNtts)
        sliceEltIds.append(sliceId)

    ifcw.nttRelAggregates(ifcw.createGuid(), ifcw.ownerHistoryId, 'Assembly Attachement', None, assemblyId, sliceEltIds)
    ifcw.nttRelAggregates(ifcw.createGuid(), ifcw.ownerHistoryId, 'Spatial Attachement', None, containerId, [assemblyId])

    #logger.info(ifcstr[0:100])
    logger.info('Successfully done!')

    # time duration to calculate segments
    def timecalc(calculatedSeconds):
        secondsInDay = 60 * 60 * 24
        secondsInHour = 60 * 60
        secondsInMinute = 60
        days = calculatedSeconds // secondsInDay
        hours = (calculatedSeconds - (days * secondsInDay)) // secondsInHour
        minutes = (calculatedSeconds - (days * secondsInDay) - (hours * secondsInHour)) // secondsInMinute
        return(minutes)
    calcSecs = time.time() - startTime
    logger.info('The script finished in {} minutes'.format(timecalc(calcSecs)))

