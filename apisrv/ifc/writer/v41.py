
from .base import IfcWriter

class V41IfcWriter(IfcWriter):

    def getVersion(self):
        return "IFC4x1"