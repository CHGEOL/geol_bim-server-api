
import logging

from flask import request, make_response, jsonify, current_app as app
from flasgger import SwaggerView  # SwaggerView inherits flask.views.MethodView
from psycopg2 import sql

from ..shared import HttpStatus, CRUD, ReadQueryBuilder
from ..schemas import CodeSchema


__all__ = ['Code', 'CodeCollection']


class Code(SwaggerView):
    """
    /transform/codes/<int:cod_id>
    """

    logger = logging.getLogger('apisrv')
    schema = CodeSchema()
    db = CRUD()

    @app.auth.login_required
    def get(self, cod_id: int):
        """
        Get one single code.
        Get data about one single object (filtering parameter: cod_id).
        ---
        operationId: code-id-get
        tags:
          - transform
        parameters:
          - in: path
            name: cod_id
            schema:
              type: string
              example: 'rop-arc'
            required: true
            description: Filtering criterion
        responses:
          200:
            description: One single object (code)
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/CodeSchema'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          404:
            $ref: '#/components/responses/not_found'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.not_found_404.value

        attr_filter = {self.schema.pk_column: cod_id}

        try:
            qb = ReadQueryBuilder()

            qb.select_sql = sql.SQL(
                'SELECT cod.*, col.col_name as cod_col_name')
            qb.from_sql = sql.SQL(
                'FROM trf.code AS cod '
                'LEFT JOIN trf.codelist AS col ON col.col_id = cod.cod_col_id')
            qb.where_sql = sql.SQL(
                'WHERE cod_id = {}').format(sql.Placeholder('cod_id'))
            qb.values = attr_filter

            query, values = qb.read_query()

            # Read Activities
            message, code = self.db.read(query, values)
            if code != 200:
                message = jsonify(message)
            else:
                message = self.schema.jsonify(message[0], many=False)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(message, code)


class CodeCollection(SwaggerView):
    """
    /transform/codes
    """

    logger = logging.getLogger('apisrv')
    schema = CodeSchema()
    db = CRUD()

    @app.auth.login_required
    def get(self):
        """
        Get one or multiple codes.
        Get a list of one or multiple objects.
        Objects can be filtered with parameter [attr_filter], e.g. attributename=value.
        If no filtering criteria are defined, then all objects are returned.
        ---
        operationId: code-get
        tags:
          - transform
        parameters:
          - in: query
            name: cod_name
            required: false
            schema:
              type: string
            description: Filtering criteria, e.g. filtering by the activity name
          - in: query
            name: cod_col_id
            required: false
            schema:
              type: string
              example: 'rop'
            description: Filtering criteria, e.g. filtering by the related codelist id
          - in: query
            name: cod_col_name
            required: false
            schema:
              type: string
              example: 'role_project'
            description: Filtering criteria, e.g. filtering by the related codelist name
        responses:
          200:
            description: Multiple objects (codes)
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/CodeSchema'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          default:
            $ref: '#/components/responses/unexpected_error'
        """

        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.bad_request_400.value

        request_data = dict(request.args)  # Arguments from URL

        request_params = self.schema.parse_filter_param(
            request_data, attr_filter=True, related_filter=dict(cod_col_name='col_name'))

        attr_filter = request_params.values

        try:
            qb = ReadQueryBuilder()

            qb.select_sql = sql.SQL(
                'SELECT cod.*, col.col_name as cod_col_name')
            qb.from_sql = sql.SQL(
                'FROM trf.code AS cod '
                'LEFT JOIN trf.codelist AS col ON col.col_id = cod.cod_col_id')
            qb.build_where_sql(attr_filter)  # WHERE cod_name = sql.Placeholder
            qb.order_sql = sql.SQL(
                'ORDER BY cod_seq')

            query, values = qb.read_query()

            # Read Activities
            message, code = self.db.read(query, values, many=True)

            if code != 200:
                message = jsonify(message)
            else:
                message = self.schema.jsonify(message, many=True)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(message, code)
