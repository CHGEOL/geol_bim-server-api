
import logging

from flask import request, make_response, jsonify, current_app as app
from flasgger import SwaggerView  # SwaggerView inherits flask.views.MethodView
from psycopg2 import sql
import psycopg2

from ..shared import HttpStatus, CRUD, CreateQueryBuilder, ReadQueryBuilder
from ..shared import authorization
from ..schemas import UserOrganisationSchema

__all__ = ['UserOrganisation', 'UserOrganisationCollection']


class UserOrganisation(SwaggerView):
    """
    /user/user_organisations/<int:uog_usr_id>/<int:uog_org_id>/<string:uog_role_code>
    """

    logger = logging.getLogger('apisrv')
    schema = UserOrganisationSchema()
    db = CRUD()

    # A Role is required OR you are the user (no decorator for this case)
    @app.auth.login_required
    def delete(self, uog_usr_id: int, uog_org_id: int, uog_role_code: str):
        """
        Delete the mapping of a user to the organisation.
        The organisation-admin can delete mappings of the organisation
        and a user can delete his mappings.

        The last organisation-admin cannot be deleted (Returns 400 BE_ITG_AUTH).

        Codes:
        * ros-oadm: organisation administrator
        * ros-opl:  project leader of the organisation
        * ros-oemp: employee of the organisation
        ---
        operationId: user_organisation-id-delete
        tags:
          - user
        parameters:
          - in: path
            name: uog_usr_id
            schema:
              type: integer
              example: 1
            required: true
            description: Filtering criterion
          - in: path
            name: uog_org_id
            schema:
              type: integer
              example: 1
            required: true
            description: Filtering criterion
          - in: path
            name: uog_role_code
            schema:
              type: string
              example: "ros-oemp"
            required: true
            description: Filtering criterion
        responses:
          200:
            $ref: '#/components/responses/delete_ok'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          404:
            $ref: '#/components/responses/not_found'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.not_found_404.value
        usr = app.auth.current_user()

        # for selection
        attr_filter = {'uog_usr_id': uog_usr_id,
                       'uog_org_id': uog_org_id,
                       'uog_role_code': uog_role_code}

        # Authorization: check User (select usr OR select org+role + check)
        if not uog_usr_id == usr.usr_id:  # not your own id -> maybe an admin
            auth_ids = authorization.select_usr_org_role_id(self.db, 'user_organisation',
                                                            attr_filter)
            authorization.check_organisation_role('org-admin', auth_ids['uog_org_id'][0], usr)

        conn, cur = self.db.get_cursor()
        try:
            # Query for deleting a role (matching attr_filter)
            query = sql.SQL(
                "DELETE FROM usr.user_organisation AS uog "
                "WHERE"
                " uog.uog_usr_id = {} AND "
                " uog.uog_org_id = {} AND"
                " uog.uog_role_code = {};"
            ).format(sql.Placeholder('uog_usr_id'), sql.Placeholder('uog_org_id'),
                     sql.Placeholder('uog_role_code'))

            # Not able to delete the last organisation-admin with this endpoint.
            # On deleting an organisation, the roles get deleted as well
            if uog_role_code == 'ros-oadm':
                # Select the data by its filter and count all administrators for the organisation
                query_select = sql.SQL(
                    "SELECT uog.uog_usr_id, uog.uog_org_id, uog.uog_role_code,"
                    " (SELECT COUNT(*) FROM usr.user_organisation AS uog"
                    "  WHERE uog.uog_org_id = {} AND uog.uog_role_code = 'ros-oadm') \"count\" "
                    "FROM usr.user_organisation AS uog "
                    "WHERE"
                    " uog.uog_usr_id = {} AND"
                    " uog.uog_org_id = {} AND"
                    " uog.uog_role_code = 'ros-oadm'"
                    ";").format(sql.Placeholder('uog_org_id'), sql.Placeholder('uog_usr_id'),
                                sql.Placeholder('uog_org_id'))
                values = {'uog_org_id': uog_org_id, 'uog_usr_id': uog_usr_id}
                message, code = self.db._read(cur, query_select, values)

                # 404 Not found: the tuple doesnt exist
                if code == 200:  # there are administrators
                    count = message[0]['count']
                    if count > 1:  # more than 1 admin, deleting is possible
                        message, code = self.db._delete(cur, query, attr_filter)
                    else:  # only one admin left, no deletion
                        message = app.config['DEFAULT_MESSAGES']['BE_ITG_AUTH']
                        code = message.code

            else:  # no restrictions on deleting reading / writing roles
                message, code = self.db._delete(cur, query, attr_filter)

            # Close the cursor and the connection
            cur.close()
            if code == 200:
                conn.commit()
            else:
                conn.rollback()

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = message.to_dict()
        # Catch DB-Exceptions
        except psycopg2.Error as exc:
            self.logger.info('Rolling back.')
            conn.rollback()
            cur.close()
            err = self.db.get_error_message(exc)
            code = err.code
            message = err.to_dict()

        finally:
            return make_response(jsonify(message), code)


class UserOrganisationCollection(SwaggerView):
    """
    /user/user_organisations
    """

    logger = logging.getLogger('apisrv')
    schema = UserOrganisationSchema()
    db = CRUD()

    # Organisation-Admin can see all, the user only himself
    @app.auth.login_required
    def get(self):
        """
        Get the mappings of users to the organisations.
        There must be either 'uog_usr_id' or 'uog_org_id' as filtering parameter.
        The organisation-admins have access to all mappings of their organisation
        and a user has access to all his mappings.

        Codes:
        * ros-oadm: organisation administrator
        * ros-opl: project leader of the organisation
        * ros-oemp: employee of the organisation
        ---
        operationId: user_organisation-get
        tags:
          - user
        parameters:
          - in: query
            name: uog_usr_id
            required: false
            schema:
              type: integer
            required: false
            description: Filtering criteria, must be the logged-in user or org-admin+uog_org_id
          - in: query
            name: uog_org_id
            required: false
            schema:
              type: integer
              example: 1
            required: false
            description: Filtering criteria, org-admin required
          - in: query
            name: uog_role_code
            required: false
            schema:
              type: string
            required: false
            description: Filtering criteria, e.g. filtering by the role 'ros-oadm'
        responses:
          200:
            description: Multiple objects (user_organisation)
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/UserOrganisationSchema'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.bad_request_400.value
        usr = app.auth.current_user()

        request_data = dict(request.args)  # Arguments from URL

        # Valid Parameter: attr_filter
        request_params = self.schema.parse_filter_param(
            request_data, attr_filter=True, required_pk=['usr_id', 'org_id'])
        attr_filter = request_params.values

        # Authorization:
        # user can see his data, org-admin can see his organisation
        # only fk in attr_filter possible -> check if exists with attr_filter
        auth_ids = authorization.select_usr_org_role_id(self.db, 'user_organisation',
                                                        attr_filter)
        if 'uog_org_id' in attr_filter:
            authorization.check_organisation_role('org-admin', auth_ids['uog_org_id'][0], usr)
        elif 'uog_usr_id' in attr_filter:  # filtering with the usr_id
            # Selection already done ..._id(self.db, 'user', {'usr_id': attr_filter['uog_usr_id']})
            authorization.check_usr_id(attr_filter['uog_usr_id'], usr)
        else:  # not possible, 'usr_id' or 'org_id' is required in filtering parameter
            err = app.condig['DEFAULT_MESSAGES']['INP_VAL_MULT']
            err.details = {'uog_usr_id': 'Missing data for required field.',
                           'uog_org_id': 'Missing data for required field.'}
            return make_response(jsonify(err.to_dict()), err.code)

        try:
            # Build the filtering statements 'AND upr_pro_id = %s AND upr_usr_id = %s'
            if attr_filter:
                equal = zip(map(sql.Identifier, attr_filter), map(sql.Placeholder, attr_filter))
                equal_sql = sql.SQL('AND ').join(
                    [sql.SQL("{}={} ").format(col, val) for col, val in equal])
            else:  # never, usr_id or org_id required parameter
                equal_sql = None

            qb = ReadQueryBuilder()

            qb.select_sql = sql.SQL(
                'SELECT uog.*,'
                ' usr.usr_name_first AS uog_usr_name_first, usr.usr_name AS uog_usr_name,'
                ' org.org_name AS uog_org_name')
            qb.from_sql = sql.SQL(
                'FROM usr.user_organisation as uog'
                ' LEFT JOIN usr."user" AS usr ON usr.usr_id = uog.uog_usr_id'
                ' LEFT JOIN usr.organisation AS org ON org.org_id = uog.uog_org_id')
            # As a organisation-admin, you have access to all roles to these organisations
            # As a user, you have access to all of your roles
            if attr_filter:
                qb.where_sql = sql.SQL(
                    'WHERE (uog.uog_org_id = ANY({}) OR uog.uog_usr_id = {})'
                    ' AND {}').format(sql.Placeholder('auth_org_id'),
                                      sql.Placeholder('auth_usr_id'),
                                      equal_sql)
            else:  # not possible, 'usr_id' or 'org_id' is required in filtering parameter
                qb.where_sql = sql.SQL(
                    'WHERE uog.uog_org_id = ANY({}) OR uog.uog_usr_id = {}'
                ).format(sql.Placeholder('auth_org_id'),
                         sql.Placeholder('auth_usr_id'))

            attr_filter['auth_org_id'] = usr.usr_org_admin
            attr_filter['auth_usr_id'] = usr.usr_id

            qb.values = attr_filter
            qb.order_sql = sql.SQL(
                'ORDER BY uog.uog_usr_id, uog.uog_org_id'
            )

            query, values = qb.read_query()

            message, code = self.db.read(query, values, many=True)

            if code != 200:
                message = jsonify(message)
            else:
                message = self.schema.jsonify(message, many=True)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(message, code)

    # Authorization in endpoint: organisation-admin or application-admin
    @app.auth.login_required
    def post(self):
        """
        Create a new mapping of a user to a organisation.
        A user can only be an employee of one one organisation.
        A user can only be member of one personal organisation.

        The organisation-admin can create mappings for his organisation.
        The application-admin can create any mapping.

        Codes:
        * ros-oadm: organisation administrator
        * ros-opl: project leader of the organisation
        * ros-oemp: employee of the organisation
        ---
        operationId: user_organisation-post
        tags:
          - user
        requestBody:
          required: true
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserOrganisationSchema'
              example: {"uog_usr_id": "1", "uog_org_id": "2",
                        "uog_role_code": "ros-oemp"}
        responses:
          201:
            description: Created.
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/ID'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.bad_request_400.value
        usr = app.auth.current_user()

        # Get the JSON from the request body and validate the data
        request_data = request.get_json()
        request_params = self.schema.parse_value_param(request_data)
        values = request_params.values

        # Authorization: app-admin or organisation-admin required
        if not usr.usr_app_admin:
            auth_ids = authorization.select_usr_org_role_id(self.db, 'organisation',
                                                            {'org_id': values['uog_org_id']})
            authorization.check_organisation_role('org-admin', auth_ids['org_id'][0], usr)

        # not possible to be employee of >1 organisations
        if values['uog_role_code'] == 'ros-oemp':
            query_select = sql.SQL(
                "SELECT COUNT(*) as \"count\" "
                "FROM usr.user_organisation AS uog "
                "WHERE uog.uog_usr_id = {} AND uog.uog_role_code = 'ros-oemp';"
            ).format(sql.Placeholder('uog_usr_id'))
            values_select = {'uog_usr_id': values['uog_usr_id']}
            message, code = self.db.read(query_select, values_select)
            assert code == 200
            if message[0]['count'] >= 1:
                err = app.config['DEFAULT_MESSAGES']['AUTH_ORG_MULT']
                return make_response(jsonify(err), err.code)

        # not possible to be member of >1 personal organisations
        if values['uog_role_code'] == 'ros-oprs':
            query_select = sql.SQL(
                "SELECT COUNT(*) as \"count\" "
                "FROM usr.user_organisation AS uog "
                "WHERE uog.uog_usr_id = {} AND uog.uog_role_code = 'ros-oprs';"
            ).format(sql.Placeholder('uog_usr_id'))
            values_select = {'uog_usr_id': values['uog_usr_id']}
            message, code = self.db.read(query_select, values_select)
            assert code == 200
            if message[0]['count'] >= 1:
                err = app.config['DEFAULT_MESSAGES']['AUTH_ORGPERS_MULT']
                return make_response(jsonify(err), err.code)

        try:  # Insert One Object
            qb = CreateQueryBuilder()
            qb.build_insert_sql(sql.SQL('usr.user_organisation'), values)
            qb.build_values_sql(values)
            qb.returning_sql = sql.SQL('RETURNING uog_usr_id, uog_org_id, uog_role_code')

            query, values = qb.create_query()

            message, code = self.db.create(query,
                                           ['uog_usr_id', 'uog_org_id', 'uog_role_code'],
                                           values)
            message = jsonify(message)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(message, code)
