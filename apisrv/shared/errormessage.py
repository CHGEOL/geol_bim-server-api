
import dataclasses

from flask import make_response, jsonify, current_app as app
import werkzeug.exceptions


@dataclasses.dataclass
class ErrorMessage:
    """
    Class for the structure of the returning error-messages
    """
    code: int  # HttpStatusCode
    message: str  # public message
    internal_code: str  # internal code
    details: dict = None  # details on the error

    def to_dict(self):
        return dataclasses.asdict(self)


def init_errorhandler():
    """
    Add automatic responses on raised errors to the application.
    Often, the errors are caught and returned without raising an exception.
    If one of the following errors is raised,
    the application returns the answers defined in the errorhandlers.

    * BadRequest (400)
    * Unauthorized (401)
    * NotFound (404)
    * MethodNotAllowed (405)
    * InternalServerError (500)

    """

    @app.errorhandler(werkzeug.exceptions.BadRequest)
    def handle_bad_request(exc: 'werkzeug.exceptions.HTTPException'):
        # If the exception was raised with an ErrorMessage
        if isinstance(exc.description, ErrorMessage):
            return make_response(jsonify(exc.description.to_dict()), 400)
        else:  # Use the default message
            err = app.config['DEFAULT_MESSAGES']['BAD_REQ']
            return make_response(jsonify(err.to_dict()), 400)

    @app.errorhandler(werkzeug.exceptions.Unauthorized)
    def handle_unauthorized(exc: 'werkzeug.exceptions.HTTPException'):
        # If the exception was raised with an ErrorMessage
        if isinstance(exc.description, ErrorMessage):
            return make_response(jsonify(exc.description.to_dict()), 401)
        else:  # Use the default message
            err = app.config['DEFAULT_MESSAGES']['AUTH']
            return make_response(jsonify(err.to_dict()), 401)

    @app.errorhandler(werkzeug.exceptions.NotFound)
    def handle_not_found(exc: 'werkzeug.exceptions.HTTPException'):
        # If the exception was raised with an ErrorMessage
        if isinstance(exc.description, ErrorMessage):
            return make_response(jsonify(exc.description.to_dict()), 404)
        else:  # Use the default message
            err = app.config['DEFAULT_MESSAGES']['NF_URL']
            return make_response(jsonify(err.to_dict()), 404)

    # Default responses, if an error is raised
    @app.errorhandler(werkzeug.exceptions.MethodNotAllowed)
    def handle_method_not_allowed(exc: 'werkzeug.exceptions.HTTPException'):
        # If the exception was raised with an ErrorMessage
        if isinstance(exc.description, ErrorMessage):
            return make_response(jsonify(exc.description.to_dict()), 405)
        else:  # Use the default
            err = app.config['DEFAULT_MESSAGES']['NOT_ALL']
            return make_response(jsonify(err.to_dict()), 405)

    @app.errorhandler(werkzeug.exceptions.InternalServerError)
    def handle_internal_server_error(exc: 'werkzeug.exceptions.HTTPException'):
        # If the exception was raised with an ErrorMessage
        if isinstance(exc.description, ErrorMessage):
            return make_response(jsonify(exc.description.to_dict()), 500)
        else:  # Use the default message
            err = app.config['DEFAULT_MESSAGES']['INT_ERR']
            return make_response(jsonify(err.to_dict()), 500)


error_mapping = {
    # Class 02: No Data (this is also a warning class per the SQL standard)
    '02000': None,  # NoData DatabaseError
    '02001': None,  # NoAdditionalDynamicResultSetsReturned DatabaseError
    # Class 03: SQL Statement Not Yet Complete
    '03000': None,  # SqlStatementNotYetComplete DatabaseError
    # Class 08: Connection Exception
    '08000': None,  # ConnectionException DatabaseError
    '08001': None,  # SqlclientUnableToEstablishSqlconnection DatabaseError
    '08003': None,  # ConnectionDoesNotExist DatabaseError
    '08004': None,  # SqlserverRejectedEstablishmentOfSqlconnection DatabaseError
    '08006': None,  # ConnectionFailure DatabaseError
    '08007': None,  # TransactionResolutionUnknown DatabaseError
    '08P01': None,  # ProtocolViolation DatabaseError
    # Class 09: Triggered Action Exception
    '09000': None,  # TriggeredActionException DatabaseError
    # Class 0A: Feature Not Supported
    '0A000': None,  # FeatureNotSupported NotSupportedError
    # Class 0B: Invalid Transaction Initiation
    '0B000': None,  # InvalidTransactionInitiation DatabaseError
    # Class 0F: Locator Exception
    '0F000': None,  # LocatorException DatabaseError
    '0F001': None,  # InvalidLocatorSpecification DatabaseError
    # Class 0L: Invalid Grantor
    '0L000': None,  # InvalidGrantor DatabaseError
    '0LP01': None,  # InvalidGrantOperation DatabaseError
    # Class 0P: Invalid Role Specification
    '0P000': None,  # InvalidRoleSpecification DatabaseError
    # Class 0Z: Diagnostics Exception
    '0Z000': None,  # DiagnosticsException DatabaseError
    '0Z002': None,  # StackedDiagnosticsAccessedWithoutActiveHandler DatabaseError
    # Class 20: Case Not Found
    '20000': None,  # CaseNotFound ProgrammingError
    # Class 21: Cardinality Violation
    '21000': None,  # CardinalityViolation ProgrammingError
    # Class 22: Data Exception
    '22000': None,  # DataException DataError
    '22001': 'BE_DAT_STR_LEN',  # StringDataRightTruncation DataError
    '22002': None,  # NullValueNoIndicatorParameter DataError
    '22003': None,  # NumericValueOutOfRange DataError
    '22004': None,  # NullValueNotAllowed DataError
    '22005': None,  # ErrorInAssignment DataError
    '22007': None,  # InvalidDatetimeFormat DataError
    '22008': None,  # DatetimeFieldOverflow DataError
    '22009': None,  # InvalidTimeZoneDisplacementValue DataError
    '2200B': None,  # EscapeCharacterConflict DataError
    '2200C': None,  # InvalidUseOfEscapeCharacter DataError
    '2200D': None,  # InvalidEscapeOctet DataError
    '2200F': None,  # ZeroLengthCharacterString DataError
    '2200G': None,  # MostSpecificTypeMismatch DataError
    '2200H': None,  # SequenceGeneratorLimitExceeded DataError
    '2200L': None,  # NotAnXmlDocument DataError
    '2200M': None,  # InvalidXmlDocument DataError
    '2200N': None,  # InvalidXmlContent DataError
    '2200S': None,  # InvalidXmlComment DataError
    '2200T': None,  # InvalidXmlProcessingInstruction DataError
    '22010': None,  # InvalidIndicatorParameterValue DataError
    '22011': None,  # SubstringError DataError
    '22012': None,  # DivisionByZero DataError
    '22013': None,  # InvalidPrecedingOrFollowingSize DataError
    '22014': None,  # InvalidArgumentForNtileFunction DataError
    '22015': None,  # IntervalFieldOverflow DataError
    '22016': None,  # InvalidArgumentForNthValueFunction DataError
    '22018': None,  # InvalidCharacterValueForCast DataError
    '22019': None,  # InvalidEscapeCharacter DataError
    '2201B': None,  # InvalidRegularExpression DataError
    '2201E': None,  # InvalidArgumentForLogarithm DataError
    '2201F': None,  # InvalidArgumentForPowerFunction DataError
    '2201G': None,  # InvalidArgumentForWidthBucketFunction DataError
    '2201W': None,  # InvalidRowCountInLimitClause DataError
    '2201X': None,  # InvalidRowCountInResultOffsetClause DataError
    '22021': None,  # CharacterNotInRepertoire DataError
    '22022': None,  # IndicatorOverflow DataError
    '22023': None,  # InvalidParameterValue DataError
    '22024': None,  # UnterminatedCString DataError
    '22025': None,  # InvalidEscapeSequence DataError
    '22026': None,  # StringDataLengthMismatch DataError
    '22027': None,  # TrimError DataError
    '2202E': None,  # ArraySubscriptError DataError
    '2202G': None,  # InvalidTablesampleRepeat DataError
    '2202H': None,  # InvalidTablesampleArgument DataError
    '22030': None,  # DuplicateJsonObjectKeyValue DataError
    '22031': None,  # InvalidArgumentForSqlJsonDatetimeFunction DataError
    '22032': None,  # InvalidJsonText DataError
    '22033': None,  # InvalidSqlJsonSubscript DataError
    '22034': None,  # MoreThanOneSqlJsonItem DataError
    '22035': None,  # NoSqlJsonItem DataError
    '22036': None,  # NonNumericSqlJsonItem DataError
    '22037': None,  # NonUniqueKeysInAJsonObject DataError
    '22038': None,  # SingletonSqlJsonItemRequired DataError
    '22039': None,  # SqlJsonArrayNotFound DataError
    '2203A': None,  # SqlJsonMemberNotFound DataError
    '2203B': None,  # SqlJsonNumberNotFound DataError
    '2203C': None,  # SqlJsonObjectNotFound DataError
    '2203D': None,  # TooManyJsonArrayElements DataError
    '2203E': None,  # TooManyJsonObjectMembers DataError
    '2203F': None,  # SqlJsonScalarRequired DataError
    '22P01': None,  # FloatingPointException DataError
    '22P02': 'BE_DAT_TYP',  # InvalidTextRepresentation DataError
    '22P03': None,  # InvalidBinaryRepresentation DataError
    '22P04': None,  # BadCopyFileFormat DataError
    '22P05': None,  # UntranslatableCharacter DataError
    '22P06': None,  # NonstandardUseOfEscapeCharacter DataError
    # Class 23: Integrity Constraint Violation
    '23000': None,  # IntegrityConstraintViolation IntegrityError
    '23001': None,  # RestrictViolation IntegrityError
    '23502': 'BE_ITG_NULL',  # NotNullViolation IntegrityError
    '23503': 'BE_ITG_FK',  # ForeignKeyViolation IntegrityError
    '23505': 'BE_ITG_UNQ',  # UniqueViolation IntegrityError
    '23514': 'BE_ITG_CHECK',  # CheckViolation IntegrityError
    '23P01': None,  # ExclusionViolation IntegrityError
    # Class 24: Invalid Cursor State
    '24000': None,  # InvalidCursorState InternalError
    # Class 25: Invalid Transaction State
    '25000': None,  # InvalidTransactionState InternalError
    '25001': None,  # ActiveSqlTransaction InternalError
    '25002': None,  # BranchTransactionAlreadyActive InternalError
    '25003': None,  # InappropriateAccessModeForBranchTransaction InternalError
    '25004': None,  # InappropriateIsolationLevelForBranchTransaction InternalError
    '25005': None,  # NoActiveSqlTransactionForBranchTransaction InternalError
    '25006': None,  # ReadOnlySqlTransaction InternalError
    '25007': None,  # SchemaAndDataStatementMixingNotSupported InternalError
    '25008': None,  # HeldCursorRequiresSameIsolationLevel InternalError
    '25P01': None,  # NoActiveSqlTransaction InternalError
    '25P02': None,  # InFailedSqlTransaction InternalError (If another statement is executed after an error) # noqa
    '25P03': None,  # IdleInTransactionSessionTimeout InternalError
    # Class 26: Invalid SQL Statement Name
    '26000': None,  # InvalidSqlStatementName OperationalError
    # Class 27: Triggered Data Change Violation
    '27000': None,  # TriggeredDataChangeViolation OperationalError
    # Class 28: Invalid Authorization Specification
    '28000': None,  # InvalidAuthorizationSpecification OperationalError
    '28P01': None,  # InvalidPassword OperationalError
    # Class 2B: Dependent Privilege Descriptors Still Exist
    '2B000': None,  # DependentPrivilegeDescriptorsStillExist InternalError
    '2BP01': None,  # DependentObjectsStillExist InternalError
    # Class 2D: Invalid Transaction Termination
    '2D000': None,  # InvalidTransactionTermination InternalError
    # Class 2F: SQL Routine Exception
    '2F000': None,  # SqlRoutineException InternalError
    '2F002': None,  # ModifyingSqlDataNotPermitted InternalError
    '2F003': None,  # ProhibitedSqlStatementAttempted InternalError
    '2F004': None,  # ReadingSqlDataNotPermitted InternalError
    '2F005': None,  # FunctionExecutedNoReturnStatement InternalError
    # Class 34: Invalid Cursor Name
    '34000': None,  # InvalidCursorName OperationalError
    # Class 38: External Routine Exception
    '38000': None,  # ExternalRoutineException InternalError
    '38001': None,  # ContainingSqlNotPermitted InternalError
    '38002': None,  # ModifyingSqlDataNotPermittedExt InternalError
    '38003': None,  # ProhibitedSqlStatementAttemptedExt InternalError
    '38004': None,  # ReadingSqlDataNotPermittedExt InternalError
    # Class 39: External Routine Invocation Exception
    '39000': None,  # ExternalRoutineInvocationException InternalError
    '39001': None,  # InvalidSqlstateReturned InternalError
    '39004': None,  # NullValueNotAllowedExt InternalError
    '39P01': None,  # TriggerProtocolViolated InternalError
    '39P02': None,  # SrfProtocolViolated InternalError
    '39P03': None,  # EventTriggerProtocolViolated InternalError
    # Class 3B: Savepoint Exception
    '3B000': None,  # SavepointException InternalError
    '3B001': None,  # InvalidSavepointSpecification InternalError
    # Class 3D: Invalid Catalog Name
    '3D000': None,  # InvalidCatalogName ProgrammingError
    # Class 3F: Invalid Schema Name
    '3F000': None,  # InvalidSchemaName ProgrammingError
    # Class 40: Transaction Rollback
    '40000': None,  # TransactionRollback OperationalError
    '40001': None,  # SerializationFailure OperationalError
    '40002': None,  # TransactionIntegrityConstraintViolation OperationalError
    '40003': None,  # StatementCompletionUnknown OperationalError
    '40P01': None,  # DeadlockDetected OperationalError
    # Class 42: Syntax Error or Access Rule Violation
    '42000': None,  # SyntaxErrorOrAccessRuleViolation ProgrammingError
    '42501': 'BE_PRIV',  # InsufficientPrivilege ProgrammingError
    '42601': None,  # SyntaxError ProgrammingError
    '42602': None,  # InvalidName ProgrammingError
    '42611': None,  # InvalidColumnDefinition ProgrammingError
    '42622': None,  # NameTooLong ProgrammingError
    '42701': None,  # DuplicateColumn ProgrammingError
    '42702': None,  # AmbiguousColumn ProgrammingError
    '42703': None,  # UndefinedColumn ProgrammingError
    '42704': None,  # UndefinedObject ProgrammingError
    '42710': None,  # DuplicateObject ProgrammingError
    '42712': None,  # DuplicateAlias ProgrammingError
    '42723': None,  # DuplicateFunction ProgrammingError
    '42725': None,  # AmbiguousFunction ProgrammingError
    '42803': None,  # GroupingError ProgrammingError
    '42804': None,  # DatatypeMismatch ProgrammingError
    '42809': None,  # WrongObjectType ProgrammingError
    '42830': None,  # InvalidForeignKey ProgrammingError
    '42846': None,  # CannotCoerce ProgrammingError
    '42883': None,  # UndefinedFunction ProgrammingError
    '428C9': None,  # GeneratedAlways ProgrammingError
    '42939': None,  # ReservedName ProgrammingError
    '42P01': None,  # UndefinedTable ProgrammingError
    '42P02': None,  # UndefinedParameter ProgrammingError
    '42P03': None,  # DuplicateCursor ProgrammingError
    '42P04': None,  # DuplicateDatabase ProgrammingError
    '42P05': None,  # DuplicatePreparedStatement ProgrammingError
    '42P06': None,  # DuplicateSchema ProgrammingError
    '42P07': None,  # DuplicateTable ProgrammingError
    '42P08': None,  # AmbiguousParameter ProgrammingError
    '42P09': None,  # AmbiguousAlias ProgrammingError
    '42P10': None,  # InvalidColumnReference ProgrammingError
    '42P11': None,  # InvalidCursorDefinition ProgrammingError
    '42P12': None,  # InvalidDatabaseDefinition ProgrammingError
    '42P13': None,  # InvalidFunctionDefinition ProgrammingError
    '42P14': None,  # InvalidPreparedStatementDefinition ProgrammingError
    '42P15': None,  # InvalidSchemaDefinition ProgrammingError
    '42P16': None,  # InvalidTableDefinition ProgrammingError
    '42P17': None,  # InvalidObjectDefinition ProgrammingError
    '42P18': None,  # IndeterminateDatatype ProgrammingError
    '42P19': None,  # InvalidRecursion ProgrammingError
    '42P20': None,  # WindowingError ProgrammingError
    '42P21': None,  # CollationMismatch ProgrammingError
    '42P22': None,  # IndeterminateCollation ProgrammingError
    # Class 44: WITH CHECK OPTION Violation
    '44000': None,  # WithCheckOptionViolation ProgrammingError
    # Class 53: Insufficient Resources
    '53000': None,  # InsufficientResources OperationalError
    '53100': None,  # DiskFull OperationalError
    '53200': None,  # OutOfMemory OperationalError
    '53300': None,  # TooManyConnections OperationalError
    '53400': None,  # ConfigurationLimitExceeded OperationalError
    # Class 54: Program Limit Exceeded
    '54000': None,  # ProgramLimitExceeded OperationalError
    '54001': None,  # StatementTooComplex OperationalError
    '54011': None,  # TooManyColumns OperationalError
    '54023': None,  # TooManyArguments OperationalError
    # Class 55: Object Not In Prerequisite State
    '55000': None,  # ObjectNotInPrerequisiteState OperationalError
    '55006': None,  # ObjectInUse OperationalError
    '55P02': None,  # CantChangeRuntimeParam OperationalError
    '55P03': None,  # LockNotAvailable OperationalError
    '55P04': None,  # UnsafeNewEnumValueUsage OperationalError
    # Class 57: Operator Intervention
    '57000': None,  # OperatorIntervention OperationalError
    '57014': None,  # QueryCanceled OperationalError
    '57P01': None,  # AdminShutdown OperationalError
    '57P02': None,  # CrashShutdown OperationalError
    '57P03': None,  # CannotConnectNow OperationalError
    '57P04': None,  # DatabaseDropped OperationalError
    # Class 58: System Error (errors external to PostgreSQL itself)
    '58000': None,  # SystemError OperationalError
    '58030': None,  # IoError OperationalError
    '58P01': None,  # UndefinedFile OperationalError
    '58P02': None,  # DuplicateFile OperationalError
    # Class 72: Snapshot Failure
    '72000': None,  # SnapshotTooOld DatabaseError
    # Class F0: Configuration File Error
    'F0000': None,  # ConfigFileError InternalError
    'F0001': None,  # LockFileExists InternalError
    # Class HV: Foreign Data Wrapper Error (SQL/MED)
    'HV000': None,  # FdwError OperationalError
    'HV001': None,  # FdwOutOfMemory OperationalError
    'HV002': None,  # FdwDynamicParameterValueNeeded OperationalError
    'HV004': None,  # FdwInvalidDataType OperationalError
    'HV005': None,  # FdwColumnNameNotFound OperationalError
    'HV006': None,  # FdwInvalidDataTypeDescriptors OperationalError
    'HV007': None,  # FdwInvalidColumnName OperationalError
    'HV008': None,  # FdwInvalidColumnNumber OperationalError
    'HV009': None,  # FdwInvalidUseOfNullPointer OperationalError
    'HV00A': None,  # FdwInvalidStringFormat OperationalError
    'HV00B': None,  # FdwInvalidHandle OperationalError
    'HV00C': None,  # FdwInvalidOptionIndex OperationalError
    'HV00D': None,  # FdwInvalidOptionName OperationalError
    'HV00J': None,  # FdwOptionNameNotFound OperationalError
    'HV00K': None,  # FdwReplyHandle OperationalError
    'HV00L': None,  # FdwUnableToCreateExecution OperationalError
    'HV00M': None,  # FdwUnableToCreateReply OperationalError
    'HV00N': None,  # FdwUnableToEstablishConnection OperationalError
    'HV00P': None,  # FdwNoSchemas OperationalError
    'HV00Q': None,  # FdwSchemaNotFound OperationalError
    'HV00R': None,  # FdwTableNotFound OperationalError
    'HV010': None,  # FdwFunctionSequenceError OperationalError
    'HV014': None,  # FdwTooManyHandles OperationalError
    'HV021': None,  # FdwInconsistentDescriptorInformation OperationalError
    'HV024': None,  # FdwInvalidAttributeValue OperationalError
    'HV090': None,  # FdwInvalidStringLengthOrBufferLength OperationalError
    'HV091': None,  # FdwInvalidDescriptorFieldIdentifier OperationalError
    # Class P0: PL/pgSQL Error
    'P0000': None,  # PlpgsqlError InternalError
    'P0001': None,  # RaiseException InternalError
    'P0002': None,  # NoDataFound InternalError
    'P0003': None,  # TooManyRows InternalError
    'P0004': None,  # AssertFailure InternalError
    # Class XX: Internal Error
    'XX000': None,  # InternalError_ InternalError
    'XX001': None,  # DataCorrupted InternalError
    'XX002': None,  # IndexCorrupted InternalError
    # GEOL_BIM Server Custom Errors
    'GBSIC': None,  # Data Integrity Error: Invalid Code
}
