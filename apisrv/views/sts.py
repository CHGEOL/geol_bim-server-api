
from flask import Blueprint

from .sts_info import Info


status_blueprint = Blueprint('status', __name__)  # URL-prefix, name


# A GET request will lead to this endpoint
@status_blueprint.route('/')
def index():
    """
    root endpoint
    """
    return 'Welcome to the status API of the GEOL_BIM Server.'


# Add Views to the app: register Collection first, then Single
status_blueprint.add_url_rule(
    '/info', view_func=Info.as_view('status_info_api'))
