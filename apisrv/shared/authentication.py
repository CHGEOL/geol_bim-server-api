
from typing import Union, TYPE_CHECKING
import logging
import secrets

from flask import current_app as app, render_template
import werkzeug.exceptions
from itsdangerous import (TimedJSONWebSignatureSerializer  # for Flask-HttpAuth
                          as Serializer, BadSignature, SignatureExpired)
from flask_mail import Message
import jinja2.exceptions
from psycopg2 import sql
from passlib.context import CryptContext

from .crud import CRUD
from ..schemas import UserSchema

if TYPE_CHECKING:
    # Import only for the type-checking, not on runtime
    from ..models import CurrentUserModel


logger = logging.getLogger('apisrv')


def init_pwd_context() -> CryptContext:
    """
    Add a context for managing the password hashes.
    It provides functions for hashing (``self.hash(password``)) and for validating
    (``self.verify(password, hash)``). Using the function
    ``self.verify_and_update(password, hash)`` allows to change the hashing algorithm
    without breaking the password validation.

    :return: password context
    """

    context = CryptContext(
        # Replace this list with the hash(es) you wish to support.
        # argon2 is the default hash (first in the list),
        # with adfditional support or reading legacy bcrypt hashes.
        schemes=app.config['HASHER'],
        # Automatically mark all but first hasher in list as deprecated.
        # (this will be the default in Passlib 2.0)
        # Deprecated hashes are automatically updated on login()
        deprecated="auto",
        # Optionally, set the number of rounds that should be used.
        # Appropriate values may vary for different schemes,
        # and the amount of time you wish it to take.
        # Leaving this alone is usually safe, and will use passlib's defaults.
        # bcrypt__rounds=12

        # usage: pwd_context.hash(pw), pwd_context.verify_and_update(pw, pw_hash)
    )
    return context


def init_token_verification():
    """
    Add the automatic verification of the authentication-token to the application.
    After a successful authentication, the current-user is stored during the request.
    """

    @app.auth.verify_token
    def verify_token(token: str) -> Union['CurrentUserModel', bool]:
        """
        Automatic verification of the authentication-token for a protected resource
        (``@app.auth.login_required``) with Flask-HttpAuth and itsdangerous.

        :param token: signed JWT
        :return: CurrentUser-object with all relevant data of the logged-in user
                 or raises an Unauthorized-Error with the appropriate message.
        """
        if not token:
            err = app.config['DEFAULT_MESSAGES']['AUTH_NO_TOK']
            raise werkzeug.exceptions.Unauthorized(description=err)

        db = CRUD()
        s = Serializer(app.config['SECRET_KEY'])
        # Return False --> Automatically returns 401 unauthorized with the default message.
        # If Errors are raised, then the return-message can be specified.

        # Verify JWT
        try:
            data = s.loads(token)
        except SignatureExpired:
            # valid token, but expired
            err = app.config['DEFAULT_MESSAGES']['AUTH_EXP_TOK']
            raise werkzeug.exceptions.Unauthorized(description=err)
        except BadSignature:
            # invalid token
            err = app.config['DEFAULT_MESSAGES']['AUTH_INV_TOK']
            raise werkzeug.exceptions.Unauthorized(description=err)

        # Verify Token with DB
        try:
            usr_id = data[app.config['JWT_IDENTITY_CLAIM']]  # user id in JWT identity claim

            user_schema = UserSchema()
            # Select the token, user-information and roles with the view usr.v_user_roles
            query = sql.SQL(
                "SELECT"
                " usr_id, usr_email,"
                " usr_name_first, usr_name, usr_token, "
                " id, role_code, usr_org_name "
                "FROM usr.v_user_roles "
                "WHERE usr_id={}"
                ";").format(sql.Placeholder('usr_id'))
            message, code = db.read(query, values=dict(usr_id=usr_id))

            if code == 200:  # user found
                usr = user_schema.load_current_user(message)
                if not usr.usr_app_conn:  # should not be possible: tested on login
                    err = app.config['DEFAULT_MESSAGES']['NF_USR']
                    raise werkzeug.exceptions.NotFound(description=err)
                if token == message[0]['usr_token']:  # token is valid
                    # Store the user-information (and project-ids) in the current-user
                    return usr
            # If validation with db fails:
            err = app.config['DEFAULT_MESSAGES']['AUTH_BE_TOK']
            raise werkzeug.exceptions.Unauthorized(description=err)
        except KeyError:
            return False  # DB-Error


def generate_auth_token(usr_id: int, expiration: int = None) -> bytes:
    """
    Generates an authentication token for a given User-ID. The expiration date can be set
    as a duration in seconds

    :param usr_id: ID of the subject of the token
    :param expiration: How long a token is valid in seconds
    :return: JWT
    """

    if not expiration:
        expiration = int(app.config['TOKEN_EXPIRATION'])
    s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
    return s.dumps({app.config['JWT_IDENTITY_CLAIM']: usr_id})


def generate_url_safe_token(length: int = None) -> str:
    """
    Generates a random token of a fixed length. The token can be used in an url
    as a temporal token (e.g. reset password)

    :param length: length of the token
    :return: token
    """
    return secrets.token_urlsafe(length)


def send_mail(template: str, subject: str, recipient: str,
              body: bool = True, html: bool = True, **kwargs):
    """
    Sends an email to the given address (recipient).
    The message of the email is defined in a template
    (<template>.html and <template>.txt for the body).
    The kwargs are passed to the template.

    :param template: name of the html/txt template for the email in the folder '/templates'
    :param subject: subject of the email (after `GEOL_BIM Server: `
    :param recipient: email address of the recipient
    :param body: Whether the email contains a body (<template>.txt required)
    :param html: Whether the email contains a html-message (<template>.html required)
    :param kwargs: Additional parameters for the template
    :return: None
    :raises ConnectionRefusedError: Raises, if the mail server refused the connection
    :raises Exception: Raises, in any case the sending of the mail fails
    """
    # Load the template
    try:
        if body:
            message_txt = render_template(f'{template}.txt', **kwargs)
        else:
            message_txt = None
        if html:
            message_html = render_template(f'{template}.html', **kwargs)
        else:
            message_html = None
    except jinja2.exceptions.TemplateNotFound as exc:
        logger.error(f"Template '{exc}' not found.")
        raise werkzeug.exceptions.InternalServerError()

    # subject
    subject = f'{app.config["MAIL_SUBJECT"]}: {subject}'
    sender_mail_address = app.config['SENDER_MAIL_ADDRESS']

    msg = Message(subject, recipients=[recipient, ],
                  sender=sender_mail_address,
                  body=message_txt,
                  html=message_html)
    # try except Exceptions
    app.mail.send(msg)
