
from setuptools import find_packages, setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='apisrv',
    version='0.0.1',
    author='Thomas Gafner',
    description='API for GEOL_BIM transformations',
    long_description=long_description,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask', ' flask-cors', 'flask-mail', 'flask-httpauth', 'werkzeug',
        'passlib', 'argon2_cffi',
        'itsdangerous', 'marshmallow', 'psycopg2-binary', 'flasgger', 'apispec',
        'pillow', 'requests', 'python-dotenv'],
    python_requires='>=3.7',
    url='https://gitlab.fhnw.ch/idibau/projekte/geol_bim/geol_bim-server-api',
    classifiers=[
            "Programming Language :: Python :: 3",
            "Operating System :: OS Independent",
        ],
)
