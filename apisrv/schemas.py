
import base64
from apisrv.shared.base_schema import NestedSchema
from typing import Dict, Tuple, List

from flask import current_app as app
from marshmallow import fields, post_load, post_dump, \
    validate, validates, validates_schema, \
    ValidationError, EXCLUDE

from .shared import BaseSchema, HttpStatus
from .models import CurrentUserModel


class ProjectSchema(BaseSchema):
    """
    Schema for Project
    """
    db_schema = 'trf'
    tablename = 'project'
    prefix = 'pro'

    pk_column = f'{prefix}_id'  # intern name for id column
    nested_attr = ['blocks']

    pro_id = fields.Integer(required=True)
    pro_org_id = fields.Integer(required=True)
    pro_pro_id = fields.Integer(missing=None)  # Reference project
    pro_name = fields.String(required=True, validate=validate.Length(max=50))
    pro_desc = fields.String(missing=None, validate=validate.Length(max=200))
    pro_type_code = fields.String(missing='pro-usr', validate=validate.Length(max=8))  # required
    pro_version = fields.String(missing=None, validate=validate.Length(max=50))
    pro_create_date = fields.DateTime(missing=None, dump_only=True)  # format='%Y-%m-%dT%H:%M:%S%z'
    pro_create_user = fields.String(missing=None, dump_only=True)
    pro_change_date = fields.DateTime(missing=None, dump_only=True)  # not dump_only
    pro_change_user = fields.String(missing=None, dump_only=True)

    pro_output = fields.Raw(missing=None)
    
    # Include the related org_name
    #pro_org_name = fields.String(missing=None, dump_only=True)
    # Boolean, if the project belongs to a personal organisation
    #pro_pers = fields.Boolean(missing=None, dump_only=True)
    # Statistics
    # none

    # Nested: blocks
    pro_blk_nested = fields.Nested('BlockSchema', many=True, dump_only=True, missing=None)

    @post_dump
    def encode_raw_data(self, data: Dict, **kwargs) -> Dict:
        """
        The raw data fields are automatically encoded as Base64 on ProjectSchema.dump().

        :param data: User data
        :param kwargs: Arguments passed from Schema.dump()
        :return: Data with the encoded raw fields
        """
        # dump base-64 encoded string, if there is a raw field
        if 'pro_output' in data:
            if data['pro_output']:
                data['pro_output'] = base64.b64encode(data['pro_output']).decode()
        return data

    def shortFieldsList(self):
        # Use schema.fields https://marshmallow.readthedocs.io/en/stable/api_reference.html#marshmallow.Schema.fields
        res = []
        res.append(self.fields['pro_id'])
        res.append(self.fields['pro_org_id'])
        res.append(self.fields['pro_pro_id'])
        res.append(self.fields['pro_name'])
        res.append(self.fields['pro_desc'])
        res.append(self.fields['pro_type_code'])
        res.append(self.fields['pro_version'])
        res.append(self.fields['pro_create_date'])
        res.append(self.fields['pro_create_user'])
        res.append(self.fields['pro_change_date'])
        res.append(self.fields['pro_change_user'])
        #res.append(self.fields['pro_output'])
        return res

    def __str__(self):
        return 'Project'

class ProjectCopySchema(ProjectSchema):
    """
    Schema for Project Copy
    """
    pro_blk_nested = fields.Nested('BlockSchema', many=True,
                                   partial=('blk_id',), unknown=EXCLUDE)

class BlockSchema(BaseSchema):
    """
    Schema for Block
    """
    db_schema = 'trf'
    tablename = 'block'
    prefix = 'blk'

    pk_column = f'{prefix}_id'  # intern name for id column
    nested_attr = [] 

    blk_id = fields.Integer(required=True)
    blk_pro_id = fields.Integer(required=True)
    blk_type_code = fields.String(missing='bkt-hdr', validate=validate.Length(max=8))  # required
    blk_data = fields.Raw(missing=None)
    blk_mime_type = fields.String(missing=None, validate=validate.Length(max=64))
    blk_heading = fields.String(missing=None, validate=validate.Length(max=200))
    blk_section = fields.String(missing=None, validate=validate.Length(max=8000))
    blk_state_code = fields.String(missing='bks-ini', validate=validate.Length(max=8))  # required
    blk_state_percent = fields.Integer(missing=None)
    blk_state_message = fields.String(missing=None, validate=validate.Length(max=800))
    blk_seq = fields.Integer(missing=None)
    blk_create_date = fields.DateTime(dump_only=True)
    blk_create_user = fields.String(dump_only=True)
    blk_change_date = fields.DateTime(dump_only=True)
    blk_change_user = fields.String(dump_only=True)

    @post_dump
    def encode_raw_data(self, data: Dict, **kwargs) -> Dict:
        """
        The raw data fields are automatically encoded as Base64 on BlockSchema.dump().

        :param data: User data
        :param kwargs: Arguments passed from Schema.dump()
        :return: Data with the encoded raw fields
        """
        # dump base-64 encoded string, if there is a raw field
        if 'blk_data' in data:
            if data['blk_data']:
                data['blk_data'] = base64.b64encode(data['blk_data']).decode()
        return data

    def shortFieldsList(self):
        # Use schema.fields https://marshmallow.readthedocs.io/en/stable/api_reference.html#marshmallow.Schema.fields
        res = []
        res.append(self.fields['blk_id'])
        res.append(self.fields['blk_pro_id'])
        res.append(self.fields['blk_type_code'])
        #res.append(self.fields['blk_data'])
        res.append(self.fields['blk_mime_type'])
        res.append(self.fields['blk_heading'])
        res.append(self.fields['blk_section'])
        res.append(self.fields['blk_state_code'])
        res.append(self.fields['blk_state_percent'])
        res.append(self.fields['blk_state_message'])
        res.append(self.fields['blk_seq'])
        res.append(self.fields['blk_create_date'])
        res.append(self.fields['blk_create_user'])
        res.append(self.fields['blk_change_date'])
        res.append(self.fields['blk_change_user'])
        return res

    def __str__(self):
        return 'Block'

class CodeSchema(BaseSchema):
    """
    Schema for Codes
    """
    db_schema = 'trf'
    tablename = 'code'
    prefix = 'cod'

    pk_column = f'{prefix}_id'  # intern name for id column
    nested_attr = []

    cod_id = fields.String(required=True, validate=validate.Length(max=8))
    cod_col_id = fields.String(required=True, validate=validate.Length(max=8))
    cod_code = fields.String(required=True, validate=validate.Length(max=10))
    cod_name = fields.String(missing=None, validate=validate.Length(max=50))
    cod_seq = fields.Integer(missing=None)

    cod_create_date = fields.DateTime(missing=None, dump_only=True)
    cod_create_user = fields.String(missing=None, dump_only=True)
    cod_change_date = fields.DateTime(missing=None, dump_only=True)
    cod_change_user = fields.String(missing=None, dump_only=True)

    # Include the related codelist-name
    cod_col_name = fields.String(missing=None, dump_only=True)

    def __str__(self):
        return 'Code'


class CodeListSchema(BaseSchema):
    """
    Schema for CodeList
    """
    db_schema = 'trf'
    tablename = 'codelist'
    prefix = 'col'

    pk_column = f'{prefix}_id'  # intern name for id column
    nested_attr = []

    col_id = fields.String(required=True, validate=validate.Length(max=8))
    col_name = fields.String(required=True, validate=validate.Length(max=50))

    col_create_date = fields.DateTime(missing=None, dump_only=True)
    col_create_user = fields.String(missing=None, dump_only=True)
    col_change_date = fields.DateTime(missing=None, dump_only=True)
    col_change_user = fields.String(missing=None, dump_only=True)

    def __str__(self):
        return 'CodeList'




# ------------------------------------------------------------------------------------------------
# Usr Schemas

class OrganisationSchema(BaseSchema):
    """
    Schema for Organisation
    """
    db_schema = 'usr'
    tablename = 'organisation'
    prefix = 'org'

    pk_column = f'{prefix}_id'  # intern name for id column
    nested_attr = []  # nested_attr = ['owned_projects']

    org_id = fields.Integer(required=True)
    org_name = fields.String(required=True, validate=validate.Length(max=50))
    org_desc = fields.String(missing=None, validate=validate.Length(max=200))
    org_create_date = fields.DateTime(dump_only=True)
    org_create_user = fields.String(dump_only=True)
    org_change_date = fields.DateTime(dump_only=True)
    org_change_user = fields.String(dump_only=True)

    # related attributes
    org_pers = fields.Boolean(missing=None, dump_only=True)

    # org_pro_nested_own = fields.List(fields.Nested(ProjectSchema, missing=None))

    def __str__(self):
        return 'Organisation'

    @validates('org_name')
    def validate_org_name(self, value: str):
        """
        Validation of the name of the Organisation.
        The Organisation name isn't allowed to contain the character @.
        The character @ is used for personal organisation.

        :param value: org_name
        :raises ValidationError: Raises a ValidationError, if the validation fails
        """
        if '@' in value:
            raise ValidationError("The @ character is not allowed in the organization name")


class UserSchema(BaseSchema):
    """
    Schema for an User
    """
    db_schema = 'usr'
    tablename = 'user'
    prefix = 'usr'

    pk_column = f'{prefix}_id'  # intern name for id column
    nested_attr = []

    usr_id = fields.Integer(required=True)
    usr_name = fields.String(required=True, validate=validate.Length(max=50))  # last name
    usr_name_first = fields.String(required=True, validate=validate.Length(max=50))
    usr_desc = fields.String(missing=None, validate=validate.Length(max=200))
    usr_email = fields.Email(required=True, validate=validate.Length(max=200))
    usr_pw = fields.String(required=True, load_only=True)  # write-only
    usr_token = fields.String(missing=None, load_only=True)
    usr_image = fields.Raw(missing=None)
    usr_image_type = fields.String(missing=None, validate=validate.Length(max=5))
    usr_login_date = fields.DateTime(missing=None, dump_only=True)
    usr_login_ip = fields.String(missing=None, dump_only=True, validate=validate.Length(max=50))
    # usr_status = fields.String(validate=validate.OneOf(['active', 'inactive']))

    usr_create_date = fields.DateTime(missing=None, dump_only=True)
    usr_create_user = fields.String(missing=None, dump_only=True)
    usr_change_date = fields.DateTime(missing=None, dump_only=True)
    usr_change_user = fields.String(missing=None, dump_only=True)

    # Related organisation id / name (queried with role org-employee)
    usr_org_id = fields.Integer(missing=None, dump_only=True)
    usr_org_name = fields.String(missing=None, dump_only=True)

    # Related personal organisation id / name (queried with role org-pers)
    usr_org_pers_id = fields.Integer(missing=None, dump_only=True)
    usr_org_pers_name = fields.String(missing=None, dump_only=True)

    # Include all roles of the user:
    # - Included in login, loading the current-user and GET user/users/<id>
    # - Merge manually with the user-data: {**user, **user_roles}
    usr_app_admin = fields.Boolean(missing=None, dump_only=True)
    usr_app_conn = fields.Boolean(missing=None, dump_only=True)
    usr_org_admin = fields.List(fields.Integer(), missing=None, dump_only=True)
    usr_org_pl = fields.List(fields.Integer(), missing=None, dump_only=True)
    usr_org_emp = fields.List(fields.Integer(), missing=None, dump_only=True)
    usr_pro_admin = fields.List(fields.Integer(), missing=None, dump_only=True)
    usr_org_pers = fields.List(fields.Integer(), missing=None, dump_only=True)
    usr_pro_read = fields.List(fields.Integer(), missing=None, dump_only=True)
    usr_pro_write = fields.List(fields.Integer(), missing=None, dump_only=True)

    @validates("usr_pw")
    def validate_usr_pw(self, value: str):
        """
        Validation of formal criteria on the User-Password:

        * Length: 8-72 characters
        * Upper/lower case: at least one upper and one lower

        :param value: password as a string
        :return: Raises a ValidationError, if the validation fails
        """
        # Validates formal requirements of the password: length, ...
        if len(value) < 8:
            raise ValidationError('Password is too short, at least 8 characters needed.')
        if len(value.encode('utf-8')) > 72:
            raise ValidationError('Password is too long.')
        if value.lower() == value:
            raise ValidationError('The password requires upper and lower case characters.')
        if value.upper() == value:
            raise ValidationError('The password requires upper and lower case characters.')

    @post_load
    def load_hashed_pw(self, data: Dict, **kwargs) -> Dict:
        """
        The Password is automatically replaced by its hash on UserSchema.load()

        :param data: User data
        :param kwargs: Arguments passed from Schema.load()
        :return: Data with the hashed password
        """
        # Automatically load the encrypted password
        if 'usr_pw' in data:
            data['usr_pw'] = app.pwd_context.hash(data['usr_pw'])
        return data

    @post_dump
    def encode_image(self, data: Dict, **kwargs) -> Dict:
        """
        The Image is automatically encoded as Base64 on UserSchema.dump().

        :param data: User data
        :param kwargs: Arguments passed from Schema.dump()
        :return: Data with the encoded image
        """
        # dump base-64 encoded string, if there is an image
        if 'usr_image' in data:
            if data['usr_image']:
                data['usr_image'] = base64.b64encode(data['usr_image']).decode()
        return data

    def dump_roles(self, data: List):
        """
        Dumps the roles from the DB-request.
        Only for one user (multiple rows in the DB-response because of the joins).

        :param data: Result of the DB-Query
        :return: Authenticated User as a CurrentUserModel
        """

        # Merge org-ids
        roles = {'ros-aadm': list(),
                 'ros-acon': list(),
                 'ros-oadm': list(),
                 'ros-opl': list(),
                 'ros-oemp': list(),
                 'ros-oprs': list(),
                 'ros-padm': list(),
                 'ros-prd': list(),
                 'ros-pwr': list()}

        # org-name / org-id of the employee
        org_name = [tup['usr_org_name'] for tup in data if
                    tup['usr_org_name'] and tup['role_code'] == 'ros-oemp']
        org_id = [tup['id'] for tup in data if tup['role_code'] == 'ros-oemp']
        if org_id:  # is an employee and has an organisation
            org_name = org_name[0]
            org_id = org_id[0]
        else:
            org_name, org_id = None, None

        # org-name / org-id of the personal organisation
        org_pers_name = [tup['usr_org_name'] for tup in data if
                         tup['usr_org_name'] and tup['role_code'] == 'ros-oprs']
        org_pers_id = [tup['id'] for tup in data if tup['role_code'] == 'ros-oprs']
        if org_pers_id:  # has a personal organisation
            org_pers_name = org_pers_name[0]
            org_pers_id = org_pers_id[0]
        else:
            org_pers_name, org_pers_id = None, None

        # Merge the id's of the tuples in the list for each role
        ids = [tup['id'] for tup in data]
        codes = [tup['role_code'] for tup in data]

        for i, c in zip(ids, codes):
            # There is a code: add the id to the list
            if c:
                roles[c].append(i)
            # else: no code
            # - No organisation- or project-role: i = None, c = None
            # - No application code to the user-id: i = <usr_id>, c = None
            #   -> usr_roles['usr_app_conn'] = False

        # Resulting roles
        usr_roles = {'usr_org_id': org_id,
                     'usr_org_name': org_name,
                     'usr_org_pers_id': org_pers_id,
                     'usr_org_pers_name': org_pers_name,
                     'usr_app_admin': True if roles['ros-aadm'] else False,
                     'usr_app_conn': True if roles['ros-acon'] else False,
                     'usr_org_admin': roles['ros-oadm'],
                     'usr_org_pl': roles['ros-opl'],
                     'usr_org_emp': roles['ros-oemp'],
                     'usr_org_pers': roles['ros-oprs'],
                     'usr_pro_admin': roles['ros-padm'],
                     'usr_pro_read': roles['ros-prd'],
                     'usr_pro_write': roles['ros-pwr']}
        # Dump is not necessary, it is a dump by hand
        usr_roles = self.dump(usr_roles)
        return usr_roles

    def load_current_user(self, data: List) -> CurrentUserModel:
        """
        Loads the User-Data from the DB-request.
        Only for one user (multiple rows in the DB-response because of the joins).

        :param data: Result of the DB-Query
        :return: Authenticated User as a CurrentUserModel
        """
        assert len(set([m['usr_id'] for m in data])) == 1   # only one individual user in result
        usr = self.dump(data[0], many=False)
        usr_roles = self.dump_roles(data)

        # Merge the user and his roles
        usr = {**usr, **usr_roles}
        return CurrentUserModel(**usr)

    def __str__(self):
        return 'User'


class RegisterUserSchema(UserSchema):
    """
    Schema for the data of a registration of a new user
    """
    url = fields.String(required=True)  # not fields.URL -> localhost:5000 is not a valid url

    # extended optional user information
    usr_title = fields.String(missing=None)  # salutation
    usr_company = fields.String(missing=None)
    usr_phone = fields.String(missing=None)
    usr_address_street = fields.String(missing=None)
    usr_address_postcode = fields.String(missing=None)
    usr_address_town = fields.String(missing=None)
    usr_address_country = fields.String(missing=None)
    usr_confirmed_agb = fields.Boolean(missing=False)
    usr_confirmed_query = fields.Boolean(missing=False)


class PasswordResetSchema(BaseSchema):
    """
    Schema for the data of a request to reset the password
    """
    email = fields.Email(required=True, validate=validate.Length(max=200))
    url = fields.String(required=True)  # not fields.URL -> localhost:5000 is not a valid url


class TokenSchema(BaseSchema):
    """
    Schema for the (temporary) token
    """
    db_schema = 'usr'
    tablename = 'token'
    prefix = 'tok'

    pk_column = 'tok_id'  # intern name for id column
    nested_attr = []

    tok_id = fields.Integer(required=True)
    tok_usr_id = fields.Integer(missing=None)
    tok_token = fields.String(required=True)
    tok_expire = fields.DateTime(required=True)


class UserApplicationSchema(BaseSchema):
    """
    Schema the mapping User-Application
    """
    db_schema = 'usr'
    tablename = 'user_application'
    prefix = 'uap'

    pk_column = ['uap_usr_id', 'uap_role_code']  # intern name for id column
    nested_attr = []

    uap_usr_id = fields.Integer(required=True)
    uap_role_code = fields.String(required=True, validate=validate.Length(max=8))
    uap_create_date = fields.DateTime(dump_only=True)
    uap_create_user = fields.String(dump_only=True)
    uap_change_date = fields.DateTime(dump_only=True)
    uap_change_user = fields.String(dump_only=True)

    # Include the names of the related users and project
    uap_usr_name_first = fields.String(missing=None, dump_only=True)
    uap_usr_name = fields.String(missing=None, dump_only=True)

    def __str__(self):
        return 'UserApplication'


class UserOrganisationSchema(BaseSchema):
    """
    Schema for the mapping User-Organisation
    """
    db_schema = 'usr'
    tablename = 'user_organisation'
    prefix = 'uog'

    pk_column = ['uog_usr_id', 'uog_org_id', 'uog_role_code']  # intern name for id column
    nested_attr = []

    uog_usr_id = fields.Integer(required=True)
    uog_org_id = fields.Integer(required=True)
    uog_role_code = fields.String(required=True, validate=validate.Length(max=8))
    uog_create_date = fields.DateTime(dump_only=True)
    uog_create_user = fields.String(dump_only=True)
    uog_change_date = fields.DateTime(dump_only=True)
    uog_change_user = fields.String(dump_only=True)

    # Include the names of the related users and project
    uog_usr_name_first = fields.String(missing=None, dump_only=True)
    uog_usr_name = fields.String(missing=None, dump_only=True)
    uog_org_name = fields.String(missing=None, dump_only=True)

    def __str__(self):
        return 'UserOrganisation'


class UserProjectSchema(BaseSchema):
    """
    Schema for the mapping User-Project
    """
    db_schema = 'usr'
    tablename = 'user_project'
    prefix = 'upr'

    pk_column = ['upr_usr_id', 'upr_pro_id', 'upr_role_code']  # intern name for id column
    nested_attr = []

    upr_usr_id = fields.Integer(required=True)
    upr_pro_id = fields.Integer(required=True)
    upr_role_code = fields.String(required=True, validate=validate.Length(max=8))
    upr_create_date = fields.DateTime(dump_only=True)
    upr_create_user = fields.String(dump_only=True)
    upr_change_date = fields.DateTime(dump_only=True)
    upr_change_user = fields.String(dump_only=True)

    # Include the names of the related users and project
    upr_usr_name_first = fields.String(missing=None, dump_only=True)
    upr_usr_name = fields.String(missing=None, dump_only=True)
    upr_pro_name = fields.String(missing=None, dump_only=True)

    def __str__(self):
        return 'UserProject'


# ------------------------------------------------------------------------------------------------
# Status Schemas

class InfoSchema(BaseSchema):
    """
    Holder for response of status info
    """
    api_version = fields.String(required=True)

    def __str__(self):
        return 'InfoSchema'
