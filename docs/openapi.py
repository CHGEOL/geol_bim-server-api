
from marshmallow import schema
import yaml
from pathlib import Path

from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec.ext.marshmallow.common import resolve_schema_cls

from apisrv.schemas import *


def schema_name_resolver(schema):  # Name-Resolver for nested schemas
    schema_cls = resolve_schema_cls(schema)
    return schema_cls.__name__


if __name__ == '__main__':
    # set the folder to /docs
    folder = Path.cwd()
    if folder.parts[-1] == 'geol_bim-server-api':
        folder = folder / 'docs'
    with open(folder / 'description.md', 'r') as f:
        description = f.read()

    # Information for OpenApi-specification
    VERSION = '0.0.1'
    spec = APISpec(
        title='GEOL_BIM Server',
        openapi_version='3.0.2',
        version=VERSION,
        info=dict(
            description=description,
            contact=dict(
                name='Fachhochschule Nordwestschweiz FHNW - Institut Digitales Bauen')
        ),
        plugins=[MarshmallowPlugin(schema_name_resolver=schema_name_resolver)],
    )

    # import Schemas, order is important (because of nested schemas)
    # main
    spec.components.schema('CodeSchema', schema=CodeSchema)
    spec.components.schema('ProjectSchema', schema=ProjectSchema)
    # user
    spec.components.schema('OrganisationSchema', schema=OrganisationSchema)
    spec.components.schema('UserSchema', schema=UserSchema)
    spec.components.schema('UserApplicationSchema', schema=UserApplicationSchema)
    spec.components.schema('UserOrganisationSchema', schema=UserOrganisationSchema)
    spec.components.schema('UserProjectSchema', schema=UserProjectSchema)
    # register
    spec.components.schema('RegisterUserSchema', schema=RegisterUserSchema)
    # password reset
    spec.components.schema('PasswordResetSchema', schema=PasswordResetSchema)
    # status
    spec.components.schema('InfoSchema', schema=InfoSchema)
    # Response-schemas
    spec.components.schema(
        'ID', {'type': 'object', 'properties': {
            'id': {'type': 'integer'}}, 'required': ['id']})
    spec.components.schema(
        'Error', {'type': 'object', 'properties': {
            'message': {'type': 'string'}, 'code': {'type': 'integer'},
            'internal_code': {'type': 'string'}, 'details': {'type': 'object'}},
                        'required': ['message', 'code', 'internal_code']})
    spec.components.schema(
        'Deletion', {'type': 'object', 'properties': {
            'message': {'type': 'string'}, 'count': {'type': 'integer'}},
                     'required': ['message', 'count']})

    # Add security scheme
    bearer_auth = {'type': 'http', 'scheme': 'bearer', 'bearerFormat': 'JWT'}
    spec.components.security_scheme('bearerAuth', bearer_auth)
    # Add default parameters
    projectParam = {'description': 'ID of the current project', 'required': True,
                    'schema': {'type': 'integer', 'default': 1}}
    spec.components.parameter('X-ProjectId', 'header', projectParam)

    # Add responses
    # 201 OK Created
    create_ok = {
        'description': 'Success. Object created.',
        'content': {'application/json': {'schema': {'$ref': '#/components/schemas/ID'}}}}
    spec.components.response('create_ok', create_ok)
    # 200 ok delete
    delete_ok = {
        'description': 'Success. Objects deleted.',
        'content': {'application/json': {'schema': {'$ref': '#/components/schemas/Deletion'}}}}
    spec.components.response('delete_ok', delete_ok)
    # 400 bad_request
    bad_request = {
        "description": "Bad Request.",
        "content": {"application/json": {"schema": {"$ref": "#/components/schemas/Error"}}}}
    spec.components.response('bad_request', bad_request)
    # 401 unauthorized
    unauthorized = {
        "description": "Unauthorized.",
        "content": {"application/json": {"schema": {"$ref": "#/components/schemas/Error"}}}}
    spec.components.response('unauthorized', unauthorized)
    not_found = {
        "description": "Object not found.",
        "content": {"application/json": {"schema": {"$ref": "#/components/schemas/Error"}}}}
    spec.components.response('not_found', not_found)
    unexpected_error = {
        "description": "Unexpected Error.",
        "content": {"application/json": {"schema": {"$ref": "#/components/schemas/Error"}}}}
    spec.components.response('unexpected_error', unexpected_error)

    # Add requestBody

    # Store template to dict
    swagger_template = spec.to_dict()

    # Add missing security to template (endpoints are protected by default)
    swagger_template['security'] = [{'bearerAuth': []}]
    swagger_template['servers'] = [
        {'url': 'http://127.0.0.1:5080/', 'description': 'Testing, localy 127.0.0.1'},
        {'url': 'http://localhost:5080/', 'description': 'Testing, localy localhost'},
        {'url': 'http://localhost:5000/api/0.0/', 'description': 'Docker-Test, localhost'},
        {'url': 'https://TODO.fhnw.ch/api/v0_8/', 'description': 'FHNW Production, online'},
        {'url': 'https://geol_bim-server.customer.TODO.ch/1.2/', 'description': 'customer DEV, requires customer VPN'},
        {'url': 'https://geol_bim-server.customer.TODO.ch/1.2/', 'description': 'customer TEST , requires customer VPN'},
        {'url': 'https://geol_bim-server.customer.TODO.ch/1.2/', 'description': 'customer PROD, online'}]
    swagger_template.pop('paths')

    # write template as yaml
    with open(folder / 'openapi_template.yaml', 'w', encoding='utf-8') as f:
        f.write((yaml.dump(swagger_template)))
