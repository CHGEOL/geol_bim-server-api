
import logging

from flask import request, make_response, jsonify, current_app as app
from flasgger import SwaggerView  # SwaggerView inherits flask.views.MethodView
from psycopg2 import sql, Binary
import psycopg2

from ..shared import (HttpStatus, CRUD, CreateQueryBuilder, ReadQueryBuilder,
                      UpdateQueryBuilder)
from ..shared import authorization
from ..schemas import UserSchema

__all__ = ['User', 'UserImage', 'UserCollection']


class User(SwaggerView):
    """
    /user/users/<int:usr_id>
    """

    logger = logging.getLogger('apisrv')
    schema = UserSchema()
    db = CRUD()

    @app.auth.login_required
    def get(self, usr_id: int):
        """
        Get one single user and its roles and permissions.
        The related organisation is queried on the employee-role.
        Get data about one single object (filtering parameter: id).

        The user can only access his data.
        ---
        operationId: user-id-get
        tags:
          - user
        parameters:
          - in: path
            name: usr_id
            schema:
              type: integer
              example: 1
            required: true
            description: Filtering criterion
          - in: query
            name: partial
            schema:
              type: boolean
            required: false
            description: Minimal response, including name, email, roles
        responses:
          200:
            description: One single object (user)
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/UserSchema'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          404:
            $ref: '#/components/responses/not_found'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.method}: {request.full_path}')
        message, code = {}, HttpStatus.not_found_404.value

        # Return only the attributes of the CurrentUserModel, no image, audit, pw
        request_data = dict(request.args)  # Arguments from URL
        if 'partial' in request_data and request_data['partial'] == 'true':
            message = self.schema.jsonify(app.auth.current_user())
            code = HttpStatus.ok_200.value
            return make_response(message, code)

        # Return all attributes and roles
        # TODO: Who can see what?
        attr_filter = {self.schema.pk_column: usr_id}

        # Authorize: select, check usr
        authorization.select_usr_org_role_id(self.db, 'user', attr_filter)
        authorization.check_usr_id(usr_id)

        try:
            # A similar request as in authorization
            query = sql.SQL(
                "SELECT"
                " * "
                "FROM"
                " usr.v_user_roles "
                "WHERE"
                " usr_id={}"
                ";").format(sql.Placeholder('usr_id'))

            message, code = self.db.read(query, values=attr_filter)

            if code != 200:  # User gets queried and verified on authentication
                message = jsonify(message)
            else:
                usr = self.schema.dump(message[0], many=False)
                usr_roles = self.schema.dump_roles(message)
                # Merge the user and his roles
                message = jsonify({**usr, **usr_roles})

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(message, code)

    @app.auth.login_required
    def patch(self, usr_id: int):
        """
        Update one single user.
        Update data of one single object. Only valid attributes can be updated.
        No validation implemented (confirm old password, not all attributes, ...).
        Use this endpoint to remove a user-image.
        To set a new user-image, please use the specific endpoint PATCH /user/users/<id>/img
        ---
        operationId: user-id-patch
        tags:
          - user
        parameters:
          - in: path
            name: usr_id
            schema:
              type: integer
              example: 1
            required: true
            description: Filtering criterion
        requestBody:
          required: true
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserSchema'
              example: {"usr_image": null, "usr_image_type": null}
        responses:
          204:
            description: No content, updating the object was successful.
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          404:
            $ref: '#/components/responses/not_found'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.not_found_404.value
        usr = app.auth.current_user()

        # Get the JSON from the request body and validate the data
        request_data = request.get_json()
        request_params = self.schema.parse_value_param(request_data, partial=True)

        attr_values = request_params.values
        attr_filter = {self.schema.pk_column: usr_id}  # for selection

        # Authorize: select, check usr (usr_org_id is dump_only)
        authorization.select_usr_org_role_id(self.db, 'user', attr_filter)
        if not usr.usr_app_admin:
            authorization.check_usr_id(usr_id)

        try:
            attr_values['usr_change_user'] = usr_id

            qb = UpdateQueryBuilder()
            qb.update_sql = sql.SQL('UPDATE usr.user')
            qb.build_set_sql(attr_values)
            qb.build_where_sql(attr_filter)

            query, values = qb.update_query()

            message, code = self.db.update(query, values)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(jsonify(message), code)

    @app.auth.login_required
    def delete(self, usr_id: int):
        """
        Deactivate one single user.
        A user gets deactivated, not deleted.

        A user can remove himself, app-admins can remove any user.
        ---
        operationId: user-id-delete
        tags:
          - user
        parameters:
          - in: path
            name: usr_id
            schema:
              type: integer
              example: 1
            required: true
            description: Filtering criterion
        responses:
          200:
            $ref: '#/components/responses/delete_ok'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          404:
            $ref: '#/components/responses/not_found'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.not_found_404.value
        # Verify, that the requested user-id is the id of the authenticated user
        usr = app.auth.current_user()

        attr_filter = {self.schema.pk_column: usr_id}

        # Authorize: select, check usr
        authorization.select_usr_org_role_id(self.db, 'user', attr_filter)
        if not usr.usr_app_admin:
            authorization.check_usr_id(usr_id)

        try:
            # Delete the role 'ros-acon'
            query = sql.SQL(
                "DELETE FROM usr.user_application "
                "WHERE uap_usr_id={} AND uap_role_code='ros-acon'"
            ).format(sql.Placeholder('usr_id'))
            values = attr_filter

            message, code = self.db.delete(query, values)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(jsonify(message), code)


class UserImage(SwaggerView):
    """
    /user/users/<int:usr_id>/img
    """

    logger = logging.getLogger('apisrv')
    schema = UserSchema()
    db = CRUD()

    @app.auth.login_required
    def patch(self, usr_id: int):
        """
        User-Image upload
        To store an image in the database, the file or URL must be provided.
        The image must not be bigger than 400 KB. The file or url can be sent with the mimetypes:
        * multipart/form-data: File (usr_image) or URL (usr_image_url)
        * application/json: URL (usr_image_url)

        To remove an image, use the endpoint PATCH /user/users/<id>
        ---
        operationId: user-id-img-patch
        tags:
          - user
        parameters:
          - in: path
            name: usr_id
            schema:
              type: integer
              example: 1
            required: true
            description: User-ID
        requestBody:
          required: true
          content:
            multipart/form-data:
              schema:
                type: object
                properties:
                  usr_image_url:
                    type: string
                  usr_image:
                    type: string
                    format: binary
            application/json:
              schema:
                type: object
                properties:
                  usr_image_url:
                    type: string
              example: {"usr_image_url": "https://www.fhnw.ch/test/user/image.png"}
        responses:
          204:
            description: No content, updating the object was successful.
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          404:
            $ref: '#/components/responses/not_found'
          default:
            $ref: '#/components/responses/unexpected_error'
        """

        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.bad_request_400.value
        usr = app.auth.current_user()

        # Authorize: select, check usr (usr_org_id is dump_only)
        authorization.select_usr_org_role_id(self.db, 'user', {'usr_id': usr_id})
        if not usr.usr_app_admin:
            authorization.check_usr_id(usr_id)

        # Get image file
        img_data = self.schema.get_img_data_from_request(
            img_attr='usr_image', url_attr='usr_image_url')
        # Validate file and get image type
        img_type = self.schema.validate_img_data(img_data)

        try:
            # Build Query with the new values and filter by user-id
            attr_values = {'usr_image': Binary(img_data), 'usr_image_type': img_type,
                           'usr_change_user': usr_id}
            attr_filter = {'usr_id': usr_id}
            qb = UpdateQueryBuilder()
            qb.update_sql = sql.SQL('UPDATE usr.user')
            qb.build_set_sql(attr_values)
            qb.build_where_sql(attr_filter)

            query, values = qb.update_query()

            message, code = self.db.update(query, values)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(jsonify(message), code)


class UserCollection(SwaggerView):
    """
    /user/users
    """

    logger = logging.getLogger('apisrv')
    schema = UserSchema(many=True)
    db = CRUD()

    @app.auth.login_required
    def get(self):
        """
        Get all users.
        Get a list of all users.
        The related organisation is queried on the employee-role.

        Not yet decided, who can read what.
        * app-admin: no parameter required (also disconnected users are visible)
        * org-emp: usr_org_id required
        * org-admin (any org): usr_email required
        * pro-admin (any pro): usr_email required
        ---
        operationId: user-get
        tags:
          - user
        parameters:
          - in: query
            name: usr_name_first
            required: false
            schema:
              type: string
            required: false
            description: Filtering by first name
          - in: query
            name: usr_name
            required: false
            schema:
              type: string
            required: false
            description: Filtering by last name
          - in: query
            name: usr_email
            required: false
            schema:
              type: string
            required: false
            description: Filtering by email address
          - in: query
            name: usr_org_id
            required: false
            schema:
              type: string
            required: false
            description: Filtering the assigned organisation (organisation employee)
        responses:
          200:
            description: Multiple objects (user)
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/UserSchema'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.bad_request_400.value
        usr = app.auth.current_user()

        request_data = dict(request.args)  # Arguments from URL

        # Valid Parameter: attr_filter
        request_params = self.schema.parse_filter_param(
            request_data, attr_filter=True, related_filter={'usr_org_id': 'uog_org_id'})

        attr_filter = request_params.values
        pk_dict = request_params.pk_dict

        # TODO: Authorization (who can read what?)
        # App-admin can query any user (also deactivated)
        if not usr.usr_app_admin:
            # No deactivated users
            attr_filter['uap_role_code'] = 'ros-acon'
            # Org-employees can see all users of the organisation
            if 'org_id' in pk_dict:
                org_id = pk_dict['org_id']
                authorization.select_usr_org_role_id(self.db, 'organisation',
                                                     {'org_id': org_id})
                authorization.check_organisation_roles_or(['org-emp', 'org-pers'], org_id, usr)
            # org-admin or pro-admin required
            elif 'usr_email' in attr_filter:  # check, if user exists (connected)
                authorization.select_usr_org_role_id(
                    self.db, 'user', {'usr_email': attr_filter['usr_email']})
                # org-admin or pro-admin required (any org/pro)
                if not (any(usr.usr_pro_admin) or any(usr.usr_org_admin)):
                    err = app.config['DEFAULT_MESSAGES']['AUTH_USR']
                    return make_response(jsonify(err), err.code)
            else:
                err = app.config['DEFAULT_MESSAGES']['INP_VAL_MULT']
                err.details = {'usr_org_id': 'Missing data for required field.',
                               'usr_email': 'Missing data for required field.'}
                return make_response(jsonify(err), err.code)

        try:
            qb = ReadQueryBuilder()
            qb.select_sql = sql.SQL(
                "SELECT usr.*,"
                " org.org_id AS usr_org_id, org.org_name AS usr_org_name, "
                "case"
                " when uap.uap_role_code is null then false"
                " else true "
                "end usr_app_conn")
            qb.from_sql = sql.SQL(
                "FROM usr.\"user\" AS usr"
                " LEFT JOIN usr.user_organisation AS uog"
                "  ON uog.uog_usr_id = usr.usr_id AND uog.uog_role_code = 'ros-oemp'"
                " LEFT JOIN usr.organisation AS org"
                "  ON org.org_id = uog.uog_org_id"
                " LEFT JOIN usr.user_application AS uap"
                "  ON uap.uap_usr_id = usr.usr_id AND uap.uap_role_code = 'ros-acon'")
            qb.build_where_sql(filter_equal=attr_filter)
            qb.order_sql = sql.SQL(
                "ORDER BY usr_id")

            query, values = qb.read_query()

            message, code = self.db.read(query, values, many=True)

            if code != 200:
                message = jsonify(message)
            else:
                message = self.schema.jsonify(message, many=True)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())

        finally:
            return make_response(message, code)

    @app.auth.login_required
    @authorization.application_role_required('app-admin')
    def post(self):
        """
        Create a new User.
        Creating a new object will automatically produce a new id. The attributes must be valid.
        The password must be at least 8 characters long.

        Only app-admins can use this endpoint. For registering, use the /auth/register-endpoint.
        ---
        operationId: user-post
        tags:
          - user
        requestBody:
          required: true
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserSchema'
              example: {"usr_name": "Test1", "usr_name_first": "Kunibert",
                        "usr_pw": "Password", "usr_email": "test@example.com"}
        responses:
          201:
            description: Created.
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/ID'
          400:
            $ref: '#/components/responses/bad_request'
          401:
            $ref: '#/components/responses/unauthorized'
          default:
            $ref: '#/components/responses/unexpected_error'
        """
        self.logger.info(f'{request.remote_addr} requested {request.url}')
        message, code = {}, HttpStatus.bad_request_400.value

        # Get the JSON from the request body and validate the data
        request_data = request.get_json()
        request_params = self.schema.parse_value_param(request_data, partial=('usr_id',))
        values = request_params.values

        conn, cur = self.db.get_cursor()
        try:
            # Insert user
            qb = CreateQueryBuilder()
            qb.build_insert_sql(sql.SQL('usr.user'), values)
            qb.build_values_sql(values)
            qb.returning_sql = sql.SQL('RETURNING usr_id')

            query, values = qb.create_query()

            message, code = self.db._create(cur, query, 'usr_id', values)

            if code == 201:
                # insert role 'ros-acon'
                query = sql.SQL(
                    'INSERT INTO usr.user_application (uap_usr_id, uap_role_code) '
                    'VALUES ({}, {}) RETURNING uap_usr_id;'
                ).format(sql.Placeholder(), sql.Placeholder())
                values = [message['usr_id'], 'ros-acon']
                ros_message, code = self.db._create(cur, query, 'uap_usr_id', values)

                if code == 201:  # both created
                    conn.commit()
                else:  # role failed
                    message = ros_message
                    conn.rollback()
            else:  # user failed
                conn.rollback()
            cur.close()

            message = jsonify(message)

        except (ValueError, TypeError):  # pragma: no cover
            # QueryBuilder: missing / wrong statements. Raises Exception if not correctly used
            message = app.config['DEFAULT_MESSAGES']['INT_ERR']
            code = message.code
            message = jsonify(message.to_dict())
        # Catch DB-Exceptions
        except psycopg2.Error as exc:
            self.logger.info('Rolling back.')
            conn.rollback()
            cur.close()
            err = self.db.get_error_message(exc)
            code = err.code
            message = err.to_dict()

        finally:
            return make_response(message, code)
