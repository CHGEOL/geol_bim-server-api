import os
from logging.config import dictConfig

from flask import Flask
from flask.logging import default_handler
from flask_cors import CORS
from flask_httpauth import HTTPTokenAuth
from flask_mail import Mail
from flasgger import Swagger
import psycopg2
import psycopg2.pool
import psycopg2.extras

from .config import app_config
from .shared.crud import init_close_db_connection, LoggingCursor, CRUD
from .shared.errormessage import init_errorhandler
from .shared.authentication import init_token_verification, init_pwd_context


__all__ = ['create_app']


def create_app():
    """
    Application factory: Create Application and DB-connection pool,
    configuration depending on Flask-Environment.

    :return: Flask application
    """
    # development / production / testing development set in environment variable
    conf = app_config[os.getenv('FLASK_ENV')]()
    app = Flask('apisrv', instance_relative_config=True)
    # Load config
    app.config.from_object(conf)
    load_from_flask_config = os.getenv('LOAD_FROM_FLASK_CONFIG', '').lower() in ['true', '1']
    if load_from_flask_config and conf.instance_config:
        app.config.from_pyfile(conf.instance_config, silent=True)

    cors_allowed_origin = app.config['CORS_ALLOWED_ORIGIN']
    CORS(app, resources={"*": {"origins": cors_allowed_origin}})

    app.db = CRUD()
    app.auth = HTTPTokenAuth(scheme='Bearer')  # Token-Based authentication for the app
    app.mail = Mail(app)

    # Swagger documentation: configured in public config
    if 'OPENAPI_TEMPLATE' in app.config and app.config['OPENAPI_TEMPLATE']:
        Swagger(app, template_file=str(app.config['OPENAPI_TEMPLATE']))

    # Logging: configured in public config
    logger = app.logger
    logger.removeHandler(default_handler)
    dictConfig(conf.LOG_CONFIG)

    # DB-connection pool
    psycopg2.extras.register_uuid()
    minconn, maxconn = [10, 1000]
    # https://github.com/psycopg/psycopg2/issues/563
    # https://github.com/Changaco/psycopg2-pool
    app.config['postgreSQL_pool'] = psycopg2.pool.SimpleConnectionPool(
        minconn, maxconn,
        user=app.config['DB_USER'],
        password=app.config['DB_PASS'],
        host=app.config['DB_HOST'],
        port=app.config['DB_PORT'],
        database=app.config['DB_NAME'],
        cursor_factory=LoggingCursor)

    # Versioned views
    version = app.config['VERSION']

    # A GET request will lead to this endpoint
    @app.route('/', methods=['GET'])
    def index():
        """
        example endpoint
        """
        return f'Welcome to the API Server (Version {version}).'

    with app.app_context():
        # Initialize the password hasher
        app.pwd_context = init_pwd_context()
        # Initialize automatic closing of db-connections after a request
        init_close_db_connection()
        # Initialize automatic responses for raised HTTP-Errors
        init_errorhandler()  # shared.errormessage.init_errorhandler()
        # Initialize automatic token verification
        init_token_verification()

        # Register Blueprints
        from apisrv.views import (transform_blueprint, user_blueprint, auth_blueprint, status_blueprint)

        app.register_blueprint(transform_blueprint, url_prefix='/transform')
        app.register_blueprint(user_blueprint, url_prefix='/user')
        app.register_blueprint(auth_blueprint, url_prefix='/auth')
        app.register_blueprint(status_blueprint, url_prefix='/status')

        return app
