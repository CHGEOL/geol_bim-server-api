
from os import environ
from pathlib import Path
import logging

from .shared import HttpStatus, ErrorMessage

logger = logging.getLogger('apisrv')


class Config:
    """
    Environment Configurations: Default values for Parameter, Logger, Documentation
    """

    DEBUG = False
    TESTING = False
    VERSION = '0.0.1'

    # Mail
    # Mailserver on localhost (local development)
    # >python -m smtpd -c DebuggingServer -n localhost:25
    MAIL_SUBJECT = 'GEOL_BIM Server'  # subject of an email: 'GEOL_BIM Server: ...'
    MAIL_DEFAULT_SENDER = 'geol_bim@customer.TODO.ch'
    # sensitive mail-config in .env
    PASSWORD_RESET_TOKEN_DURATION = environ.get('PASSWORD_RESET_TOKEN_DURATION')
    MAIL_SERVER = environ.get('MAIL_SERVER')
    MAIL_PORT = environ.get('MAIL_PORT')
    MAIL_USE_TLS = environ.get('MAIL_USE_TLS', '').lower() in ['true', '1']
    MAIL_USE_SSL = environ.get('MAIL_USE_SSL', '').lower() in ['true', '1']
    MAIL_USERNAME = environ.get('MAIL_USERNAME', None)
    MAIL_PASSWORD = environ.get('MAIL_PASSWORD', None)
    SENDER_MAIL_ADDRESS = environ.get('SENDER_MAIL_ADDRESS', 'non-reply@customer.TODO.ch')

    # Security
    hasher_env = environ.get('HASHER')
    if hasher_env:
        HASHER = environ.get('HASHER').split(',')  # password hasher
    else:
        HASHER = None
    JWT_IDENTITY_CLAIM = 'sub'  # Identity claim, standardized to 'sub'y

    # Images
    IMAGE_TYPES = ['jpg', 'jpeg', 'png']
    MAX_IMAGE_SIZE = 1024 * 400  # 400 kb

    # JasperServer: Timeout for a request (generating a report)
    J_TIMEOUT = 5

    # Load from environment variables
    # Database configuration
    DB_USER = environ.get('DB_USER')
    DB_PASS = environ.get('DB_PASS')
    DB_HOST = environ.get('DB_HOST')
    DB_PORT = environ.get('DB_PORT')
    DB_NAME = environ.get('DB_NAME')

    # Security (for JWT): import os; print(os.urandom(24).hex())
    SECRET_KEY = environ.get('SECRET_KEY')
    # expiration in seconds for JWT
    TOKEN_EXPIRATION = environ.get('TOKEN_EXPIRATION')

    # CORS
    cors_env = environ.get('CORS_ALLOWED_ORIGIN')
    if cors_env:
        if ',' in cors_env:
            # multiple allowed origins
            CORS_ALLOWED_ORIGIN = cors_env.split(',')
        else:
            CORS_ALLOWED_ORIGIN = cors_env
    else:
        # not configured. Default '*'
        CORS_ALLOWED_ORIGIN = '*'

    # The default reference project will be added to every new registered user
    REGISTER_DEFAULT_REFERENCE_PROJECT_ID = environ.get('REGISTER_DEFAULT_REFERENCE_PROJECT_ID')

    # Jasper
    J_HOST = environ.get('J_HOST')
    J_PORT = environ.get('J_PORT')
    J_REST_URL = environ.get('J_REST_URL')
    J_USERNAME = environ.get('J_USERNAME')
    J_PASSWORD = environ.get('J_PASSWORD')

    # create logfile
    logfile_folder = Path('logfiles')
    logfile_path = logfile_folder / 'app.log'
    if not Path.exists(logfile_folder):  # pragma: no cover
        logger.info('no logfile path')
        Path.mkdir(logfile_folder)
    if not Path.exists(logfile_path):  # pragma: no cover
        logger.info('no file')
        logfile_path.touch()

    # logging configuration
    LOG_CONFIG = {
        'version': 1,
        'formatters': {'default': {
            'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
        }},
        'handlers': {
            'wsgi': {
                'class': 'logging.StreamHandler',
                'stream': 'ext://flask.logging.wsgi_errors_stream',
                'formatter': 'default',
                'level': 'INFO'
            },
            'file_handler': {
                'class': 'logging.FileHandler',
                'filename': logfile_path,
                'mode': 'a',
                'encoding': 'utf-8',
                'formatter': 'default',
                'level': 'DEBUG'
            }
        },
        'loggers': {
            'apisrv': {
                'handlers': ['wsgi', 'file_handler']
            },
            'sql_debug': {
                'handlers': ['wsgi', 'file_handler']
            },
            'werkzeug': {
                'handlers': ['wsgi', 'file_handler']
            },
        },
        'root': {
            'level': 'INFO',
            'propagate': True
        }
    }

    DEFAULT_MESSAGES = {
        # 400 - BadRequest
        'BAD_REQ': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                message='The browser (or proxy) sent a request that this '
                                        'server could not understand.',
                                internal_code='BAD_REQ'),
        # Marshmallow
        'INP_VAL': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                message='Input validation failed.',
                                internal_code='INP_VAL'),
        'INP_VAL_ND': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                   message='No (valid) data given.',
                                   internal_code='INP_VAL_ND'),
        'INP_VAL_MULT': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                     message='Validation of multiple fields failed.',
                                     internal_code='INP_VAL_MULT'),
        'INP_VAL_INV_FLD': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                        message='Invalid field-name.',
                                        internal_code='INP_VAL_INV_FLD'),
        'INP_VAL_REQ_FLD': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                        message='A required field is missing.',
                                        internal_code='INP_VAL_REQ_FLD'),
        'INP_VAL_NULL_FLD': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                         message='A field may not be empty.',
                                         internal_code='INP_VAL_NULL_FLD'),
        'INP_VAL_STR_LEN': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                        message='The input is too short or too long.',
                                        internal_code='INP_VAL_STR_LEN'),
        'INP_VAL_TYP': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                    message='Wrong input type.',
                                    internal_code='INP_VAL_TYP'),
        'INP_VAL_MAIL': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                     message='Not a valid email address.',
                                     internal_code='INP_VAL_MAIL'),
        # Errors on data input: input validation and data validation (e.g. blk_data)
        'DTA_VAL_REQ_FLD': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                        message='No file given.',
                                        internal_code='DTA_VAL_REQ_FLD'),
        'DTA_VAL_MTYP': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                     message='Invalid mimetype.',
                                     internal_code='DTA_VAL_MTYP'),
        # Errors on image input: input validation and image validation
        'IMG_VAL': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                message='Image validation failed.',
                                internal_code='IMG_VAL'),
        'IMG_VAL_REQ_FLD': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                        message='No file and no URL given.',
                                        internal_code='IMG_VAL_REQ_FLD'),
        'IMG_VAL_MTYP': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                     message='Invalid mimetype.',
                                     internal_code='IMG_VAL_MTYP'),
        'IMG_VAL_INV_URL': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                        message='Invalid URL: The file could not be '
                                                'fetched from the given URL.',
                                        internal_code='IMG_VAL_INV_URL'),
        'IMG_VAL_SIZE': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                     message='The file is too big.',
                                     internal_code='IMG_VAL_SIZE'),
        'IMG_VAL_TYP': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                    message='Invalid file type.',
                                    internal_code='IMG_VAL_TYP'),
        'IMG_VAL_BOMB': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                     message='Image validation failed.',
                                     internal_code='IMG_VAL_BOMB'),
        # DB-Errors
        # Data Errors: should be cought by previous validation (marshmallow)
        # Default
        'BE_DAT': ErrorMessage(code=HttpStatus.bad_request_400.value,
                               message='Error in the given data.',
                               internal_code='BE_DAT'),
        # StringDataRightTruncation: 22001
        'BE_DAT_STR_LEN': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                       message='The input is too short or too long.',
                                       internal_code='BE_DAT_STR_LEN'),
        # InvalidTextRepresentation: 22P02
        'BE_DAT_TYP': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                   message='Wrong input type.',
                                   internal_code='BE_DAT_TYP'),
        # Integrity Errors
        # default
        'BE_ITG': ErrorMessage(code=HttpStatus.bad_request_400.value,
                               message='Constrains in DB',
                               internal_code='BE_ITG'),
        # UniqueViolation: 23505
        'BE_ITG_UNQ': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                   message='The same values already exist.',
                                   internal_code='BE_ITG_UNQ'),
        # NotNullViolation: 23502
        'BE_ITG_NULL': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                    message='A required field is missing.',
                                    internal_code='BE_ITG_NULL'),
        # ForeignKeyViolation: 23503
        'BE_ITG_FK': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                  message='The object has related objects '
                                          'or the given relation does not exist.',
                                  internal_code='BE_ITG_FK'),
        # CheckViolation: 23514
        'BE_ITG_CHECK': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                     message='The object fails on integrity checks.',
                                     internal_code='BE_ITG_CHECK'),
        # Manual Checks in Database
        'BE_ITG_AUTH': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                    message='The last administrator cannot be deleted.',
                                    internal_code='BE_ITG_AUTH'),
        # JasperServer
        'RPT_INV': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                message='Generating the report failed.',
                                internal_code='RPT_INV'),
        # JasperServer
        'RPT_INV_TO': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                   message='Generating the report failed.',
                                   internal_code='RPT_INV_TO'),
        # 401 - Unauthorized
        # default
        'AUTH': ErrorMessage(code=HttpStatus.unauthorized_401.value,
                             message='The server could not verify that you are '
                                     'authorized to access the URL requested.',
                             internal_code='AUTH'),
        'AUTH_NO_TOK': ErrorMessage(code=HttpStatus.unauthorized_401.value,
                                    message='No authentication token given.',
                                    internal_code='AUTH_NO_TOK'),
        'AUTH_INV_TOK': ErrorMessage(code=HttpStatus.unauthorized_401.value,
                                     message='Invalid authentication token.',
                                     internal_code='AUTH_INV_TOK'),
        'AUTH_EXP_TOK': ErrorMessage(code=HttpStatus.unauthorized_401.value,
                                     message='The authentication token is expired.',
                                     internal_code='AUTH_EXP_TOK'),
        'AUTH_BE_TOK': ErrorMessage(code=HttpStatus.unauthorized_401.value,
                                    message='The authentication token does not match.',
                                    internal_code='AUTH_BE_TOK'),
        'AUTH_BE_CRED': ErrorMessage(code=HttpStatus.unauthorized_401.value,
                                     message='No matching password and email found.',
                                     internal_code='AUTH_BE_CRED'),
        'AUTH_RPT_CRED': ErrorMessage(code=HttpStatus.unauthorized_401.value,
                                      message='The reporting-server is not available.',
                                      internal_code='AUTH_RPT_CRED'),
        'AUTH_PRO': ErrorMessage(code=HttpStatus.unauthorized_401.value,
                                 message="You don't have the permission to access this resource.",
                                 internal_code='AUTH_PRO'),
        'AUTH_PRO_MULT': ErrorMessage(code=HttpStatus.unauthorized_401.value,
                                      message='Multiple project-references in the request '
                                              '(only one allowed).',
                                      internal_code='AUTH_PRO_MULT'),
        'AUTH_ORG': ErrorMessage(code=HttpStatus.unauthorized_401.value,
                                 message="You don't have the permission to access this resource.",
                                 internal_code='AUTH_ORG'),
        'AUTH_ORG_MULT': ErrorMessage(code=HttpStatus.unauthorized_401.value,
                                      message="A user can only be the employee "
                                              "of one organisation.",
                                      internal_code='AUTH_ORG_MULT'),
        'AUTH_ORGPERS_MULT': ErrorMessage(code=HttpStatus.unauthorized_401.value,
                                          message="A user can only be member of "
                                                  "one personal organisation.",
                                          internal_code='AUTH_ORGPERS_MULT'),
        'AUTH_APP': ErrorMessage(code=HttpStatus.unauthorized_401.value,
                                 message="You don't have the permission to access this resource.",
                                 internal_code='AUTH_APP'),
        'AUTH_USR': ErrorMessage(code=HttpStatus.unauthorized_401.value,
                                 message="You don't have the permission to access this resource.",
                                 internal_code='AUTH_USR'),
        # 404 - NotFound
        # default: URL not found
        'NF_URL': ErrorMessage(code=HttpStatus.not_found_404.value,
                               message='The requested URL was not found on the server.',
                               internal_code='NF_URL'),
        # object in Database not found
        'NF_OBJ': ErrorMessage(code=HttpStatus.not_found_404.value,
                               message='Object not found for the given parameters.',
                               internal_code='NF_OBJ'),
        # user not connected to the application
        'NF_USR': ErrorMessage(code=HttpStatus.not_found_404.value,
                               message='User not found for the given parameters.',
                               internal_code='NF_USR'),
        # Reporting-Server not found
        'NF_RPT_SRV': ErrorMessage(code=HttpStatus.not_found_404.value,
                                   message='The reporting-server is not available.',
                                   internal_code='NF_RPT_SRV'),
        # Report was not found on the reporting-server
        'NF_RPT_RPT': ErrorMessage(code=HttpStatus.not_found_404.value,
                                   message='The reporting-server is not available.',
                                   internal_code='NF_RPT_RPT'),
        # 405 - NotAllowed
        # default
        'NOT_ALL': ErrorMessage(code=HttpStatus.method_not_allowed_405.value,
                                message='The method is not allowed for the requested URL.',
                                internal_code='NOT_ALL'),
        # 500 - InternalServerError
        # default
        'INT_ERR': ErrorMessage(code=HttpStatus.internal_server_error_500.value,
                                message='Error occurred. Please contact the support.',
                                internal_code='INT_ERR'),
        # DB-Errors:
        # InterfaceError
        'BE_IF': ErrorMessage(code=HttpStatus.internal_server_error_500.value,
                              message='Error occurred. Please contact the support.',
                              internal_code='BE_IF'),
        # DatabaseError
        'BE_DB': ErrorMessage(code=HttpStatus.internal_server_error_500.value,
                              message='Error occurred. Please contact the support.',
                              internal_code='BE_DB'),
        # InternalError
        'BE_INT': ErrorMessage(code=HttpStatus.internal_server_error_500.value,
                               message='Error occurred. Please contact the support.',
                               internal_code='BE_INT'),
        # OperationalError
        'BE_OP': ErrorMessage(code=HttpStatus.internal_server_error_500.value,
                              message='Error occurred. Please contact the support.',
                              internal_code='BE_OP'),
        # NotSupportedError
        'BE_NOT_SUPP': ErrorMessage(code=HttpStatus.internal_server_error_500.value,
                                    message='Error occurred. Please contact the support.',
                                    internal_code='BE_NOT_SUPP'),
        # ProgrammingError: Syntax error in the sql, should never happen on live
        'BE_SYNT': ErrorMessage(code=HttpStatus.internal_server_error_500.value,
                                message='Error occurred. Please contact the support.',
                                internal_code='BE_SYNT'),
        'BE_PRIV': ErrorMessage(code=HttpStatus.internal_server_error_500.value,
                                message='Error occurred. Please contact the support.',
                                internal_code='BE_PRIV'),
        # Mail Errors: Mailserver not found
        'MAIL_NA': ErrorMessage(code=HttpStatus.internal_server_error_500.value,
                                message='Error occurred. Please contact the support.',
                                internal_code='MAIL_NA'),
        # Token
        'TOK_EXP': ErrorMessage(code=HttpStatus.unauthorized_401.value,
                                message='The token is expired.',
                                internal_code='TOK_EXP'),
        # Register
        'REG_MAIL_DUP': ErrorMessage(code=HttpStatus.bad_request_400.value,
                                     message='The email address already exists',
                                     internal_code='REG_MAIL_DUP'),

    }


class Development(Config):
    """
    Development environment configuration
    """
    DEBUG = True

    # Swagger Configuration: /apisrv/docs/openapi_template.yaml
    folder = Path(__file__).parent.parent
    OPENAPI_TEMPLATE = folder / 'docs' / 'openapi_template.yaml'

    # Additional Swagger configurations for the HTML
    SWAGGER = {
        'title': 'GEOL_BIM Server',
        'openapi': '3.0.2',
        'uiversion': 3,
        'favicon': "data:image/png;base64,TODO_TODO_TODO",  # noqa
        'hide_top_bar': True,
        'specs_route': '/docs/',
        'specs': [{
            'endpoint': 'spec',
            'route': '/spec.json',
        }]
    }


class Production(Config):
    """
    Production environment configuration
    """
    instance_config = 'config_prod.cfg'


class Testing(Config):
    """
    Testing environment configureatoin
    """
    TESTING = True

    # logging configuration: No handlers for testing
    def __init__(self):
        super().__init__()
        self.LOG_CONFIG['loggers'] = {
            'apisrv': {
                'handlers': []
            },
            'sql_debug': {
                'handlers': []
            },
            'werkzeug': {
                'handlers': []
            },
        }


app_config = {
    'development': Development,
    'production': Production,
    'testing': Testing
}
