
from typing import List


class CurrentUserModel:
    """
    ORM-Model for the logged-in user.
    """
    usr_id: int
    usr_name_first: str
    usr_name: str
    usr_email: str
    # related organisation (user is an employee)
    usr_org_id: int
    usr_org_name: str
    # related personal organisation of user
    usr_org_pers_id: int
    usr_org_pers_name: str
    # Authorization
    usr_app_admin: bool  # Whether the user is an application-admin or not
    usr_app_conn: bool  # Whether the user is connected (active) or not
    usr_org_pers: List[int]  # Wether the user has a personal organisation, but only one
    usr_org_admin: List[int]  # List of all org_id, where the user is an org-admin
    usr_org_pl: List[int]  # List of all org_id, where the user is an org-project-leader
    usr_org_emp: List[int]  # List of all org_id, where the user is an org-employee
    usr_pro_admin: List[int]  # List of all pro_id, where the user is a project-leader
    usr_pro_read: List[int]  # List of all pro_id, where the user can read
    usr_pro_write: List[int]  # List of all pro_id, where the user can write

    def __init__(self, **data):
        self.usr_id = data.get('usr_id')
        self.usr_name_first = data.get('usr_name_first')
        self.usr_name = data.get('usr_name')
        self.usr_email = data.get('usr_email')
        self.usr_org_id = data.get('usr_org_id')
        self.usr_org_name = data.get('usr_org_name')
        self.usr_org_pers_id = data.get('usr_org_pers_id')
        self.usr_org_pers_name = data.get('usr_org_pers_name')
        # Authorization
        self.usr_app_admin = data.get('usr_app_admin')
        self.usr_app_conn = data.get('usr_app_conn')
        self.usr_org_pers = data.get('usr_org_pers')
        self.usr_org_admin = data.get('usr_org_admin')
        self.usr_org_pl = data.get('usr_org_pl')
        self.usr_org_emp = data.get('usr_org_emp')
        self.usr_pro_admin = data.get('usr_pro_admin')
        self.usr_pro_read = data.get('usr_pro_read')
        self.usr_pro_write = data.get('usr_pro_write')

    def __repr__(self):
        # <CurrentUserModel(usr_id=1, usr_name_first=Toni, usr_name, Tester, ...)
        attr_string = ', '.join([f'{k}={v}' for k, v in self.__dict__.items()])
        return f'<CurrentUserModel({attr_string})'
