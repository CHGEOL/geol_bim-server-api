
import logging
import time

import pandas as pd
import euclid 

from .common import *

__all__ = ['bhl2ifc']


# Borehole specific

BHL_DF_NAME = 'Borehole'
GEO_DF_NAME = 'Geometry (Borehole)'
ITV_DF_NAME = 'Interval_GeologicFeature'
DRL_DF_NAME = 'Interval_DrillingCompletion'

BHL_ID_CFG_KEY = 'BoreholeID'
BHL_NAME_CFG_KEY = 'BohrholeName'
BHL_X_CFG_KEY = 'XCoord'
BHL_Y_CFG_KEY = 'YCoord'
BHL_Z_CFG_KEY = 'ZCoord'
BHL_TYPE_CFG_KEY = 'BoreholeType'
BHL_INCL_TYPE_CFG_KEY = 'InclinationType'
BHL_DT_DRILL_CFG_KEY = 'DateOfDrilling'
BHL_PURPOSE_CFG_KEY = 'Purpose'
BHL_START_PNT_CFG_KEY = 'StartPoint'
BHL_DT_CUST_CFG_KEY = 'DataCustodian'

BHL_CFG_SET = {
    BHL_ID_CFG_KEY, BHL_NAME_CFG_KEY, BHL_X_CFG_KEY, BHL_Y_CFG_KEY, BHL_Z_CFG_KEY, 
    BHL_TYPE_CFG_KEY, BHL_INCL_TYPE_CFG_KEY, BHL_DT_DRILL_CFG_KEY, BHL_PURPOSE_CFG_KEY, BHL_START_PNT_CFG_KEY, BHL_DT_CUST_CFG_KEY
}

GEO_BHL_ID_CFG_KEY = 'BoreholeID'
GEO_X_CFG_KEY = 'XCoord'
GEO_Y_CFG_KEY = 'YCoord'
GEO_Z_CFG_KEY = 'ZCoord'
GEO_CFG_SET = {
    GEO_BHL_ID_CFG_KEY, GEO_X_CFG_KEY, GEO_Y_CFG_KEY, GEO_Z_CFG_KEY
}

ITV_BHL_ID_CFG_KEY = 'BoreholeID'
ITV_DEPTH_FROM_CFG_KEY = 'IntervalBegin'
ITV_DEPTH_TO_CFG_KEY = 'IntervalEnd'
ITV_BASE_TYPE_CFG_KEY = GF_BASE_TYPE_CFG_KEY
ITV_PERSPECTIVE_CFG_KEY = GF_PERSPECTIVE_CFG_KEY
ITV_SUB_TYPE_CFG_KEY = GF_SUB_TYPE_CFG_KEY
ITV_CLASS_CFG_KEY = GF_CLASS_CFG_KEY
ITV_OBS_MTH_CFG_KEY = GF_OBS_MTH_CFG_KEY
ITV_OBS_MTH_GEO_CFG_KEY = GF_OBS_MTH_GEO_CFG_KEY
ITV_PURPOSE_CFG_KEY = GF_PURPOSE_CFG_KEY
ITV_DESC_CFG_KEY = GF_DESC_CFG_KEY
ITV_CFG_SET = {
    ITV_BHL_ID_CFG_KEY, ITV_DEPTH_FROM_CFG_KEY, ITV_DEPTH_TO_CFG_KEY, ITV_BASE_TYPE_CFG_KEY, 
    ITV_PERSPECTIVE_CFG_KEY, ITV_SUB_TYPE_CFG_KEY, ITV_CLASS_CFG_KEY, ITV_OBS_MTH_CFG_KEY, 
    ITV_OBS_MTH_GEO_CFG_KEY, ITV_PURPOSE_CFG_KEY, ITV_DESC_CFG_KEY
}

DRL_BHL_ID_CFG_KEY = 'BoreholeID'
DRL_DEPTH_FROM_CFG_KEY = 'IntervalBegin'
DRL_DEPTH_TO_CFG_KEY = 'IntervalEnd'
DRL_MTH_CFG_KEY = 'DrillingMethod'
DRL_COMPL_CFG_KEY = 'CompletionType'
DRL_DIA_CFG_KEY = 'BoreholeDiameter'
DRL_DESC_CFG_KEY = 'Description'
DRL_CFG_SET = {
    DRL_BHL_ID_CFG_KEY, DRL_DEPTH_FROM_CFG_KEY, DRL_DEPTH_TO_CFG_KEY,
    DRL_MTH_CFG_KEY, DRL_COMPL_CFG_KEY, DRL_DIA_CFG_KEY, DRL_DESC_CFG_KEY
}

COMMON_OWN_PTY_SET = 'CHGLG_BoreholeCommon'
COMMON_IFC_PTY_SET = 'Pset_BoreholeCommon'

COMMON_OWN_ITV_PTY_SET = 'CHGLG_PipeSegmentTypeCommon'
COMMON_IFC_ITV_PTY_SET = 'Pset_PipeSegmentTypeCommon'

DRL_MIN_DIA = 10 # 1cm = more narrow than any driller TODO what is a sensible fix value
DRL_MAX_DIA = 1000 # 1m = wider than any driller TODO what is a sensible fix value?

# Eventhough GEOMETRY_TYPE_CFG_KEY is common, the possible values are specific to the adapter.
SGT_V_EX_AREA_SOLID = 'ExtrudedAreaSolid'
SGT_V_EX_AREA_SEG = 'ExtrudedAreaSolidSegmented'
SGT_V_SEC_SOLID_HOR = 'SectionedSolidHorizontal'
SEGMENT_GEOMETRY_TYPE_VALUE_SET = {SGT_V_EX_AREA_SEG, SGT_V_SEC_SOLID_HOR} # SGT_V_EX_AREA_SOLID

def pointArrLen(pntArr):
    nofPnts = len(pntArr)
    if 1 < nofPnts:
        ttlLen = 0.0
        for i in range(1, nofPnts):
            p1 = euclid.Point3(pntArr[i-1][0], pntArr[i-1][1], pntArr[i-1][2])
            p2 = euclid.Point3(pntArr[i][0], pntArr[i][1], pntArr[i][2])
            segment = euclid.LineSegment3(p1, p2)
            ttlLen += segment.length
        return ttlLen
    else:
        return 0.0

def splitLine(coords, distance):
    # splits a line (coords) at a distance
    # returns [linesplit1, linesplit2] if 0 < distance < len(coords))
    # returns [None, coords] if distance < 0
    # returns [coords, None] if distance > len(coords)
    if distance <= 0.0: 
        return [None, coords]
    
    cumdist = 0
    for i,p in enumerate(coords):
        if i < len(coords) - 1:
            # create a segment with 2 verteces
            p1 = euclid.Point3(coords[i][0], coords[i][1], coords[i][2])
            p2 = euclid.Point3(coords[i+1][0], coords[i+1][1], coords[i+1][2])
            segment = euclid.LineSegment3(p1, p2)
            if distance <= cumdist + segment.length:
                # splitting the line on the current segment
                sphere = euclid.Sphere(p1, distance - cumdist)
                segmentPart = segment.intersect(sphere)
                splittingPoint = (round(segmentPart.p1.x,2), 
                                round(segmentPart.p1.y,2), 
                                round(segmentPart.p1.z,2))
                
                lineSplit1 = coords[:i+1] + [splittingPoint]
                lineSplit2 = [splittingPoint] + coords[i+1:]

                return [lineSplit1, lineSplit2]
            cumdist = cumdist + segment.length

    return [coords, None] # distance > line.length


def getLinesegment(coords, distanceFrom, distanceTo):
    segment = splitLine(coords, distanceTo)
    if segment[0] is None:
        # distanceTo < 0 
        return None
    segment = splitLine(segment[0], distanceFrom)
    if segment[1] is None:
        # distanceFrom > line.length
        return None
    return segment[1]

def bhlAggregateGeometryPointsPerBorehole(ifcw, dfGeometry):
    #tmp = [xyz for xyz in zip(dfGeometry[GEO_X_CFG_KEY],dfGeometry[GEO_Y_CFG_KEY],dfGeometry[GEO_Z_CFG_KEY])]
    bhId2Points = {}
    for i, val in dfGeometry.iterrows():
        bhId = val[GEO_BHL_ID_CFG_KEY]

        # No need to validate here since grantColumnValuesNumericMany is used
        if hasCoordErrors(i+2, val, ifcw,  GEO_DF_NAME, GEO_X_CFG_KEY, GEO_Y_CFG_KEY, GEO_Z_CFG_KEY, 
                GroupableError('Im Excel in Blatt {} in Zeile {} wurde die Geometrie für {} ignoriert, weil die Koordinaten ungültig sind.', GEO_DF_NAME, i+2, bhId)
            ): continue

        coord = (val[GEO_X_CFG_KEY], val[GEO_Y_CFG_KEY], val[GEO_Z_CFG_KEY])
        if bhId in bhId2Points:
            bhId2Points[bhId].append(coord)
        else:
            bhId2Points[bhId] = [coord]
    return bhId2Points

class ItvDta(EltData):

    def __init__(self):
        self.dFrom = None
        self.dTo = None
        super().__init__()

    def validateFromTo(self, dfName, rowNo, fromName, toName):
        valNo(self.dFrom, GroupableError('Im Excel hat Blatt {} in Zeile {} für den Segment-Start {} einen unültigen Wert {} .', dfName, rowNo, fromName, self.dFrom), self.errors)
        valNo(self.dTo, GroupableError('Im Excel hat Blatt {} in Zeile {} für das Segment-Ende {} einen unültigen Wert {} .', dfName, rowNo, toName, self.dTo), self.errors)
        if self.dTo < self.dFrom:
            self.errors.append(GroupableError('Im Excel in Blatt {} in Zeile {} liegt der Segment-Start {} ({}) nach dem Segment-Ende {} ({}).', dfName, rowNo, fromName, self.dFrom, toName, self.dTo))

    def setGfRow(self, row, rowNo, pty2ptySetName):
        self.dFrom = row[ITV_DEPTH_FROM_CFG_KEY]
        self.dTo = row[ITV_DEPTH_TO_CFG_KEY]
        self.validateFromTo(ITV_DF_NAME, rowNo, ITV_DEPTH_FROM_CFG_KEY, ITV_DEPTH_TO_CFG_KEY)

        self.assignGfProps(EXCEL_DOC_NAME, row, rowNo, pty2ptySetName) # can only be used if ITV_BASE_TYPE_CFG_KEY == GF_BASE_TYPE_CFG_KEY, etc.
        # Do intervalBegin/End here, because common GF are not segments and assignGfProps cannot know dFrom nor dTo.
        self.setPty(COMMON_OWN_ITV_PTY_SET, 'intervalBegin', self.dFrom)
        self.setPty(COMMON_OWN_ITV_PTY_SET, 'intervalEnd', self.dTo)

    def setDrlRow(self, row, rowNo, pty2ptySetName):
        # pty2ptySetName is unused
        self.dFrom = row[DRL_DEPTH_FROM_CFG_KEY]
        self.dTo = row[DRL_DEPTH_TO_CFG_KEY]
        self.validateFromTo(DRL_DF_NAME, rowNo, DRL_DEPTH_FROM_CFG_KEY, DRL_DEPTH_TO_CFG_KEY)
        self.diameter = float(row[DRL_DIA_CFG_KEY])
        # Validate diameter not nan & DRL_MIN_DIA < self.diameter < DRL_MAX_DIA
        valNo(self.diameter, GroupableError('Im Excel hat Blatt {} in Zeile {} für den Durchmesser {} einen unültigen Wert {} .', DRL_DF_NAME, rowNo, DRL_DIA_CFG_KEY, self.diameter), self.errors)
        if self.diameter < DRL_MIN_DIA:
            self.errors.append(GroupableError('Im Excel in Blatt {} in Zeile {} liegt der Durchmesser {} ({}) unterhalb der Untergrenze für sinnvolle Werte {}.', DRL_DF_NAME, rowNo, DRL_DIA_CFG_KEY, self.diameter, DRL_MIN_DIA))
        if DRL_MAX_DIA < self.diameter:
            self.errors.append(GroupableError('Im Excel in Blatt {} in Zeile {} liegt der Durchmesser {} ({}) oberhalb der Obergrenze für sinnvolle Werte {}.', DRL_DF_NAME, rowNo, DRL_DIA_CFG_KEY, self.diameter, DRL_MAX_DIA))

        self.desc = row[DRL_DESC_CFG_KEY]
        self.setPty(COMMON_IFC_ITV_PTY_SET, 'OuterDiameter', row[DRL_DIA_CFG_KEY])
        self.setPty(COMMON_OWN_ITV_PTY_SET, 'drillingMethod', row[DRL_MTH_CFG_KEY])
        self.setPty(COMMON_OWN_ITV_PTY_SET, 'completionType', row[DRL_COMPL_CFG_KEY])

        self.setPty(COMMON_OWN_ITV_PTY_SET, 'intervalBegin', self.dFrom)
        self.setPty(COMMON_OWN_ITV_PTY_SET, 'intervalEnd', self.dTo)

    @staticmethod
    def defaultSortValue(data):
        return data.dFrom
    
    @staticmethod
    def handleGfRow(data, row, rowNo, pty2ptySetName):
        data.setGfRow(row, rowNo, pty2ptySetName)

    @staticmethod
    def handleDrlRow(data, row, rowNo, pty2ptySetName):
        data.setDrlRow(row, rowNo, pty2ptySetName)

    @staticmethod
    def aggrPerBh(df, pty2ptySetName, bhIdKey, handleFct):
        bhId2Aggr = {}
        for i, val in df.iterrows():
            bhId = val[bhIdKey]
            data = ItvDta()
            handleFct(data, val, i+1, pty2ptySetName)
            if bhId in bhId2Aggr:
                bhId2Aggr[bhId].append(data)
            else:
                bhId2Aggr[bhId] = [data]
        for bhId, dataList in bhId2Aggr.items():
            dataList.sort(key=ItvDta.defaultSortValue)
        return bhId2Aggr

class BhDta(EltData):

    def __init__(self):
        self.dFrom = None
        self.dTo = None
        super().__init__()

    def setRow(self, row, dFrom, dTo, length):
        self.dFrom = dFrom
        self.dTo = dTo

        self.name = row[BHL_NAME_CFG_KEY]
        self.desc = None

        self.setPty(COMMON_IFC_PTY_SET, 'CapDepth', length)
        # TODO BoreholeMaterialCustodian does not exist in xlsx
        self.setPty(COMMON_OWN_PTY_SET, 'BoreholeType', row[BHL_TYPE_CFG_KEY])
        self.setPty(COMMON_OWN_PTY_SET, 'InclinationType', row[BHL_INCL_TYPE_CFG_KEY])
        self.setPty(COMMON_OWN_PTY_SET, 'DateOfDrill', row[BHL_DT_DRILL_CFG_KEY])
        # TODO Driller does not exist in xlsx
        # TODO Operator does not exist in xlsx
        self.setPty(COMMON_OWN_PTY_SET, 'XCoord', row[BHL_X_CFG_KEY])
        self.setPty(COMMON_OWN_PTY_SET, 'YCoord', row[BHL_Y_CFG_KEY])
        self.setPty(COMMON_OWN_PTY_SET, 'ZCoord', row[BHL_Z_CFG_KEY])
        self.setPty(COMMON_OWN_PTY_SET, 'Purpose', row[BHL_PURPOSE_CFG_KEY])
        self.setPty(COMMON_OWN_PTY_SET, 'StartPoint', row[BHL_START_PNT_CFG_KEY])
        self.setPty(COMMON_OWN_PTY_SET, 'DataCustodian', row[BHL_DT_CUST_CFG_KEY])
        # Pset_GeotechnicalAssemblyCommon and CHGLG_GeotechnicalAssemblyCommon are not filled

def createExtrudedAreaSolidElements(ifcw, bhId, contPlcId, origin, coords, intervals, resEltIds, getItvDiameterFct, getItvPointsFct, createElementFct, segVal = SGT_V_EX_AREA_SEG):

    for itv in (intervals or []): # Remember this is also used for a single borehole as one interval
        diameterInMm = getItvDiameterFct(itv) * 0.001

        ifcw.nttCartesianPointList3D(((0.0,0.0-diameterInMm,0.0), (0.0+diameterInMm,0.0,0.0), (0.0,0.0+diameterInMm,0.0), (0.0-diameterInMm,0.0,0.0)))
        cplId = ifcw.lastCartesianPointList3DId
        ifcw.nttIndexedPolyCurve(cplId, (ifcw.tArcIndex((1,2,3)), ifcw.tArcIndex((3,4,1))), False)
        outerCurveId = ifcw.lastIndexedPolyCurveId
        ifcw.nttArbitraryClosedProfileDef('AREA', None, outerCurveId)
        areaId = ifcw.lastArbitraryClosedProfileDefId

        itvPoints = getItvPointsFct(coords,itv.dFrom,itv.dTo)
        if itvPoints is not None:
            # logger.info('seg {} {}'.format(i, len(coords)))
            segEltIds = []

            rng = None
            coordsLen = len(itvPoints)-1
            if segVal == SGT_V_EX_AREA_SOLID:
                rng = range(0, 1) # only first
            elif segVal == SGT_V_EX_AREA_SEG:
                rng = range(0, coordsLen)

            for iii in rng:
                # logger.info('iii {}'.format(iii))
                coord1 = itvPoints[iii]
                if segVal == SGT_V_EX_AREA_SOLID:
                    coord2 = itvPoints[coordsLen]
                elif segVal == SGT_V_EX_AREA_SEG:
                    coord2 = itvPoints[iii+1]
                p1 = euclid.Point3(coord1[0]-origin[0], coord1[1]-origin[1],coord1[2]-origin[2])
                p2 = euclid.Point3(coord2[0]-origin[0], coord2[1]-origin[1],coord2[2]-origin[2])
                
                bhSegmentSingel = euclid.LineSegment3(p1,p2)
                vector = bhSegmentSingel.v
                
                ifcw.ncpLocalPlacement(relativeToId=contPlcId)
                spacePlacementId = ifcw.lastLocalPlacementId
                ifcw.ncpAxis2placement3D((p1.x, p1.y, p1.z), (vector.x, vector.y, vector.z), (1.0, 0.0, 0.0))
                extrusionPlacementId = ifcw.lastAxis2placement3DId
                extrusion = bhSegmentSingel.length

                ifcw.nttDirection((0., 0., 1.))
                ifcw.nttExtrudedAreaSolid(areaId, extrusionPlacementId, ifcw.lastDirectionId, extrusion)
                ifcw.nttShapeRepresentation(ifcw.contextId, 'Body', 'AdvancedSweptSolid', [ifcw.lastExtrudedAreaSolidId])
                ifcw.nttProductDefinitionShape(None, None, [ifcw.lastShapeRepresentationId])
                productId = ifcw.lastProductDefinitionShapeId

                # ifcw.nttSpace(ifcw.createGuid(), ifcw.ownerHistoryId, bhId, 'BOREHOLE_SEGMENT', 'BoreholeSegment', 
                #     spacePlacementId, productId, None, 'ELEMENT', 'USERDEFINED', None)
                # spaceId = ifcw.lastSpaceId
                eltId = createElementFct(ifcw, bhId, itv, spacePlacementId, productId)

                segEltIds.append(eltId)
                resEltIds.append(eltId)

            createIfcProperties(itv.pts, ifcw, segEltIds)

def createSectionedSolidHorizontal(ifcw, bhId, contPlcId, alignmentCurveId, intervals, resEltIds, getItvDiameterFct, createElementFct):

    for itv in (intervals or []): # Remember this is also used for a single borehole as one interval
        diameterInMm = getItvDiameterFct(itv) * 0.001

        ifcw.nttCartesianPointList2D(((diameterInMm,0.0), (2*diameterInMm,diameterInMm), (diameterInMm,2*diameterInMm), (0.0,diameterInMm)))
        cplId = ifcw.lastCartesianPointList2DId
        ifcw.nttIndexedPolyCurve(cplId, (ifcw.tArcIndex((1,2,3)), ifcw.tArcIndex((3,4,1))), False)
        outerCurveId = ifcw.lastIndexedPolyCurveId
        ifcw.nttArbitraryClosedProfileDef('AREA', None, outerCurveId)
        closedProfileId = ifcw.lastArbitraryClosedProfileDefId

        if ifcw.getVersion() != 'IFC4x3':
            ifcw.nttDistanceExpression(itv.dFrom, 0.0, -1.0, None, True)
            xSecPosStartId = ifcw.lastDistanceExpressionId
            ifcw.nttDistanceExpression(itv.dTo, 0.0, -1.0, None, True)
            xSecPosEndId = ifcw.lastDistanceExpressionId
            fixedAxisVertical = True
        else:
            # IFC4x3 does no longer have IfcDistanceExpression.
            # IFX4x3 does no longer support IfcSectionedSolidHorizontal.FixedAxisVertical .
            # TODO somehow verify the following
            ifcw.ncpAxis2placementLinear(dirZ=(itv.dFrom, 0.0, 0.0)) # point defaults to 0,0,0 and dirX to 1,0,0
            xSecPosStartId = ifcw.lastAxis2placementLinearId
            ifcw.ncpAxis2placementLinear(dirZ=(itv.dTo, 0.0, 0.0)) # point defaults to 0,0,0 and dirX to 1,0,0
            xSecPosEndId = ifcw.lastAxis2placementLinearId
            fixedAxisVertical = None # No longer defined in IFC4x3
        ifcw.nttSectionedSolidHorizontal(alignmentCurveId, (closedProfileId, closedProfileId), (xSecPosStartId, xSecPosEndId), fixedAxisVertical)
        ifcw.nttShapeRepresentation(ifcw.contextId, 'Body', 'AdvancedSweptSolid', [ifcw.lastSectionedSolidHorizontalId])
        ifcw.nttProductDefinitionShape(None, None, [ifcw.lastShapeRepresentationId])
        productId = ifcw.lastProductDefinitionShapeId

        ifcw.ncpLocalPlacement(relativeToId=contPlcId)
        spacePlacementId = ifcw.lastLocalPlacementId

        # ifcw.nttSpace(ifcw.createGuid(), ifcw.ownerHistoryId, bhId, 'BOREHOLE_SEGMENT', 'BoreholeSegment', 
        #     spacePlacementId, ifcw.lastProductDefinitionShapeId, None, 'ELEMENT', 'USERDEFINED', None)
        # spaceId = ifcw.lastSpaceId
        eltId = createElementFct(ifcw, bhId, itv, spacePlacementId, productId)

        segmentSpaceIds = [eltId]
        resEltIds.append(eltId)

        createIfcProperties(itv.pts, ifcw, segmentSpaceIds)

def getItvExtrema(intervals, extr=None):
    # dFrom resp. dTo not Nan nor None and dFrom < dTo is granted
    if extr is None:
        extr = [0, 0]
    for itv in (intervals or []):
        extr[0] = min(extr[0], itv.dFrom)
        extr[1] = max(extr[1], itv.dTo)
    return extr

def filterItvExtrema(warns, dfName, bhId, intervals, length):
    # dFrom resp. dTo not Nan nor None and dFrom < dTo is granted
    res = []
    moreFlexibleLength = length * (1.00 + NUMERIC_EXACT) # allow 1% error in 3D length
    for itv in (intervals or []):
        if itv.dFrom < 0.0:
            warns.append(GroupableError('Im Excel hat Blatt "{}" für Bohrung "{}" eine negative Segment-Längenangabe von {}.', dfName, bhId, itv.dFrom))
        elif moreFlexibleLength < itv.dTo:
            warns.append(GroupableError('Im Excel hat Blatt "{}" für Bohrung "{}" eine Segment-Längenangabe von {}, welche länger ist als die Geometrie der Bohrung {}.', dfName, bhId, itv.dTo, length))
        else:
            res.append(itv)
    return res

def bhl2ifc(ifcw, origShift, xlsxStream):

    logger = logging.getLogger('apisrv')

    logger.info('Converting Borehole Data to IFC.')
    startTime = time.time()

    dfBh = pd.read_excel(xlsxStream,sheet_name=BHL_DF_NAME, skiprows=[1],converters={BHL_ID_CFG_KEY:str})
    dfGeometry = pd.read_excel(xlsxStream,sheet_name=GEO_DF_NAME, skiprows=[1],converters={GEO_BHL_ID_CFG_KEY:str})
    dfGf = pd.read_excel(xlsxStream,sheet_name=ITV_DF_NAME, skiprows=[0,2],converters={ITV_BHL_ID_CFG_KEY:str})
    dfDrill = pd.read_excel(xlsxStream,sheet_name=DRL_DF_NAME, skiprows=[0,2],converters={DRL_BHL_ID_CFG_KEY:str})

    pty2ptySetName = readPtyToPtySetName(xlsxStream, ITV_DF_NAME, 11) # does no validation

    # #################################
    # Do various validations and checks

    # It is not too inefficient to open the same project config again, since it is a small tab.
    dfProMeta = pd.read_excel(xlsxStream,sheet_name=PRO_DF_NAME, index_col=None, header=None)
    proCfg = readProjectConfig(dfProMeta) # does also validate
    segVal = proCfg[GEOMETRY_TYPE_CFG_KEY]
    if not segVal in SEGMENT_GEOMETRY_TYPE_VALUE_SET:
        raise ValueError('Fehlerhafte Konfiguration für {} mit "{}". Erlaubte Werte sind: {}'.format(GEOMETRY_TYPE_CFG_KEY, segVal, ', '.join(SEGMENT_GEOMETRY_TYPE_VALUE_SET)))

    grantColumnsPresent(BHL_DF_NAME, dfBh, BHL_CFG_SET)
    grantColumnsPresent(GEO_DF_NAME, dfGeometry, GEO_CFG_SET) # TODO ev. remove the ' (Borehole)'
    grantColumnsPresent(ITV_DF_NAME, dfGf, ITV_CFG_SET)
    grantColumnsPresent(DRL_DF_NAME, dfDrill, DRL_CFG_SET)
    
    grantColumnValuesUnique(BHL_DF_NAME, dfBh, BHL_ID_CFG_KEY)
    # bhIds = set(map(str, dfBh[BHL_ID_CFG_KEY]))
    bhIds = set(dfBh[BHL_ID_CFG_KEY])
    grantColumnValuesWithin(GEO_DF_NAME, dfGeometry, GEO_BHL_ID_CFG_KEY, bhIds, rowNamingOffset=3)
    grantColumnValuesWithin(ITV_DF_NAME, dfGf, ITV_BHL_ID_CFG_KEY, bhIds, rowNamingOffset=4)
    grantColumnValuesWithin(DRL_DF_NAME, dfDrill, DRL_BHL_ID_CFG_KEY, bhIds, rowNamingOffset=4)

    grantColumnValuesNumericMany(BHL_DF_NAME, dfBh, {BHL_X_CFG_KEY, BHL_Y_CFG_KEY, BHL_Z_CFG_KEY}, rowNamingOffset=3)
    grantColumnValuesNumericMany(GEO_DF_NAME, dfGeometry, {GEO_X_CFG_KEY, GEO_Y_CFG_KEY, GEO_Z_CFG_KEY}, rowNamingOffset=3)
    grantColumnValuesNumericMany(ITV_DF_NAME, dfGf, {ITV_DEPTH_FROM_CFG_KEY, ITV_DEPTH_TO_CFG_KEY}, rowNamingOffset=4)
    grantColumnValuesNumericMany(DRL_DF_NAME, dfDrill, {DRL_DEPTH_FROM_CFG_KEY, DRL_DEPTH_TO_CFG_KEY, DRL_DIA_CFG_KEY}, rowNamingOffset=4)

    grantColumnValuesDatetime(BHL_DF_NAME, dfBh, BHL_DT_DRILL_CFG_KEY, rowNamingOffset=3)

    # Prepare GF and drilling per borehole dictionaries
    bhId2Bh = {}
    for i, row in dfBh.iterrows():
        key = row[BHL_ID_CFG_KEY]
        bhId2Bh[key] = row
    bhId2Points = bhlAggregateGeometryPointsPerBorehole(ifcw, dfGeometry)
    # TODO if we set up bhId2Len (using pointArrLen) and passed it to aggrPerBh we could do the 
    #   segment-to-geometry-validations the same way the other are also done. (Warning the exact xlsx row would be possible)
    #   This would mean moving the filterItvExtrema code to ItvDta.setGfRow resp. ItvDta.setDrlRow.
    #   But be aware, that if we do so, we have to move two validations "bhId not in bhId2Points" and "len(coords) < 2" up here.
    bhId2Gf = ItvDta.aggrPerBh(dfGf, pty2ptySetName, ITV_BHL_ID_CFG_KEY, ItvDta.handleGfRow)
    bhId2Drl = ItvDta.aggrPerBh(dfDrill, None, DRL_BHL_ID_CFG_KEY, ItvDta.handleDrlRow)

    for bhId in bhId2Bh:
        if bhId not in bhId2Points:
            raise ValueError(f"""Im Excel hat Blatt "{BHL_DF_NAME}" eine Bohrung "{bhId}" ohne Linenführung (ohne Stützpunkte).""")
        if bhId not in bhId2Gf:
            raise ValueError(f"""Im Excel hat Blatt "{BHL_DF_NAME}" eine Bohrung "{bhId}" ohne Geologic Features.""")
        # if bhId not in bhId2Drl: Drilling Completion is not mandatory.
    # Also validate the points
    for bhId, coords in bhId2Points.items():
        if len(coords) < 2:
            raise ValueError(f"""Im Excel hat Blatt "{GEO_DF_NAME}" eine Linenführung "{bhId}" mit zu wenigen Stützpunkten.""")

        firstPoint = coords[0]
        bhlRow = bhId2Bh[bhId]
        if not ( bhlRow[BHL_X_CFG_KEY] * (1.0 - NUMERIC_EXACT_COORDS) < firstPoint[0] and firstPoint[0] < bhlRow[BHL_X_CFG_KEY] * (1.0 + NUMERIC_EXACT_COORDS) ):
            raise ValueError(f"""Im Excel Blatt "{GEO_DF_NAME}" beginnt die Linenführung "{bhId}" mit einem Stützpunkt, dessen X-Koordinate {firstPoint[0]} nicht der der Bohrung "{bhId}" (in Blatt {BHL_DF_NAME}, mit Wert {bhlRow[BHL_X_CFG_KEY]}) entspricht.""")
        if not ( bhlRow[BHL_Y_CFG_KEY] * (1.0 - NUMERIC_EXACT_COORDS) < firstPoint[1] and firstPoint[1] < bhlRow[BHL_Y_CFG_KEY] * (1.0 + NUMERIC_EXACT_COORDS) ):
            raise ValueError(f"""Im Excel Blatt "{GEO_DF_NAME}" beginnt die Linenführung "{bhId}" mit einem Stützpunkt, dessen Y-Koordinate {firstPoint[1]} nicht der der Bohrung "{bhId}" (in Blatt {BHL_DF_NAME}, mit Wert {bhlRow[BHL_Y_CFG_KEY]}) entspricht.""")
        if not ( bhlRow[BHL_Z_CFG_KEY] * (1.0 - NUMERIC_EXACT) < firstPoint[2] and firstPoint[2] < bhlRow[BHL_Z_CFG_KEY] * (1.0 + NUMERIC_EXACT) ):
            raise ValueError(f"""Im Excel Blatt "{GEO_DF_NAME}" beginnt die Linenführung "{bhId}" mit einem Stützpunkt, dessen Z-Koordinate {firstPoint[2]} nicht der der Bohrung "{bhId}" (in Blatt {BHL_DF_NAME}, mit Wert {bhlRow[BHL_Z_CFG_KEY]}) entspricht.""")

    # Handle errors on the individual intervals (created during aggrPerBh)
    # Including NaN, dTo < dFrom (and BaseType or Perspective for GF)
    def handleIntevals(bhId2Intervals):
        errs = []
        warns = []
        for bhId, intervals in bhId2Intervals.items():
            for itv in intervals:
                errs += itv.errors
                warns += itv.warnings
        hasErrorsSoHandle(None, errs)
        hasWarningsSoHandle(ifcw, warns) # may write comments to ifc
    handleIntevals(bhId2Gf)
    handleIntevals(bhId2Drl)

    # Validate GF and DRL segments are within the geometry
    warns = []
    for bhId, coords in bhId2Points.items():
        pLen = pointArrLen(coords)
        bhId2Gf[bhId] = filterItvExtrema(warns, ITV_DF_NAME, bhId, bhId2Gf[bhId], pLen)
        bhId2Drl[bhId] = filterItvExtrema(warns, DRL_DF_NAME, bhId, bhId2Drl[bhId], pLen)
    hasWarningsSoHandle(ifcw, warns) # max write comments to ifc

    # Validate GF and DRL segments do not intersect mutually as errors
    # and gaps as warnings.
    def checkIntevalsIntersection(dfName, bhId2Intervals):
        errs = []
        warns = []
        for bhId, intervals in bhId2Intervals.items():
            lastItv = ItvDta() # a dummy one
            lastItv.dFrom = intervals[-1].dTo
            for itv in reversed(intervals): # they are ordered according to dFrom
                if lastItv.dFrom < itv.dTo:
                    itvName = f'[{itv.dFrom}:{itv.dTo}]'
                    errs.append(GroupableError('Im Excel hat Blatt "{}" für Bohrung "{}" ein Segment {}, welches den Angfang des nächsten Segment überschneidet.', dfName, bhId, itvName))
                lastItv = itv
            lastItv = ItvDta() # a dummy one
            lastItv.dTo = intervals[0].dFrom
            for itv in intervals: # they are ordered according to dFrom
                if lastItv.dTo < itv.dFrom:
                    itvName = f'[{itv.dFrom}:{itv.dTo}]'
                    warns.append(GroupableError('Im Excel hat Blatt "{}" für Bohrung "{}" ein Segment {}, welches nicht direkt anschliesst an das Ende des vorhergehenden Segments.', dfName, bhId, itvName))
                lastItv = itv
        hasErrorsSoHandle(None, errs)
        hasWarningsSoHandle(ifcw, warns) # may write comments to ifc
    checkIntevalsIntersection(ITV_DF_NAME, bhId2Gf)
    checkIntevalsIntersection(DRL_DF_NAME, bhId2Drl)

    # #####################################################
    # Start the 'normal' (not comments) writing of the file

    spatialContainerId = ifcw.externalSpatialElementId
    containerPlacementId = ifcw.externalSpatialElementPlacementId

    if segVal == SGT_V_SEC_SOLID_HOR: # TODO Check if this is really needed
        ifcw.ncpLocalPlacement(relativeToId=containerPlacementId)

    # #####################
    # The actual processing

    def gfDiameterFct(itv):
        return DRL_MAX_DIA
    def gfCreateEltFct(ifcw, bhId, itv, placementId, productId):
        # We do not attache these elements directly to the spacial element (by spatialContainerId)
        return createGf(ifcw, itv.name, itv.desc, itv.baseType, itv.perspective, itv.objType, itv.tag, placementId, productId)

    def drlDiameterFct(itv):
        return itv.diameter
    def drlCreateEltFct(ifcw, bhId, itv, spacePlacementId, productId):
        ifcw.nttPipeSegment(ifcw.createGuid(), ifcw.ownerHistoryId, None, 'BoreholeSegment', 'BOREHOLESEGMENT', 
            spacePlacementId, productId, None, None)
        return ifcw.lastPipeSegmentId
        # ifcw.nttSpace(ifcw.createGuid(), ifcw.ownerHistoryId, bhId, 'BOREHOLE_SEGMENT', 'BoreholeSegment', 
        #     spacePlacementId, productId, None, 'ELEMENT', 'USERDEFINED', None)
        # return ifcw.lastSpaceId

    def bhlDiameterFct(itv):
        return DRL_MIN_DIA
    def bhlCreateEltFct(ifcw, bhId, itv, spacePlacementId, productId):
        if ifcw.getVersion() != 'IFC4x3':
            # ifcw.nttSystem(ifcw.createGuid(), ifcw.ownerHistoryId, itv.name + ' System', None, 'BOREHOLE')
            ifcw.nttBuildingElementProxy(ifcw.createGuid(), ifcw.ownerHistoryId, itv.name, itv.desc, 'BOREHOLE', spacePlacementId, productId, None, None)
            # ifcw.nttRelDefinesByType(ifcw.createGuid(), ifcw.ownerHistoryId, 'System - BuildingElementProxy Duality', None, [ifcw.lastSystemId], ifcw.lastBuildingElementProxyId)
            # The following link suggests that the product type is the relating one.
            # https://standards.buildingsmart.org/IFC/RELEASE/IFC4/ADD2_TC1/HTML/schema/ifckernel/lexical/ifcreldefinesbytype.htm
            return ifcw.lastBuildingElementProxyId # This id does the spacial attachment and gets all the properties attached.
        else:
            ifcw.nttBorehole(ifcw.createGuid(), ifcw.ownerHistoryId, itv.name, itv.desc, 'BOREHOLE', spacePlacementId, productId, None)
            return ifcw.lastBoreholeId

    assemblyName = proCfg[PRO_NAME_CFG_KEY] # It is not correct, but we do not have any other source for the name.
    assemblyId = createAssemblyWithProps(ifcw, assemblyName, '3DModel', proCfg)

    bhEltIds = []

    for bhId, coords in bhId2Points.items():
        pLen = pointArrLen(coords)
        extr = getItvExtrema(bhId2Gf[bhId])
        extr = getItvExtrema(bhId2Drl[bhId], extr)
        bhlRow = bhId2Bh[bhId]
        bh = BhDta()
        bh.setRow(bhlRow, extr[0], extr[1], pLen)
        bhChildIds = [] # It is not only spaces but also IfcSolidStratum or IfcPipeSegment

        newBhIds = [] # Expected to contain exaclty one id
        if segVal == SGT_V_SEC_SOLID_HOR:

            # Prepare alignment curve (as polyline from BoreholeGeometry)
            pts = []
            for p in coords:
                pts.append((p[0]-origShift[0],p[1]-origShift[1],p[2]-origShift[2]))
            ifcw.nttPolyline(pts)
            alignmentCurveId = ifcw.lastPolylineId

            createSectionedSolidHorizontal(ifcw, bhId, containerPlacementId, 
                alignmentCurveId, bhId2Gf[bhId], bhChildIds,
                gfDiameterFct, gfCreateEltFct)

            createSectionedSolidHorizontal(ifcw, bhId, containerPlacementId, 
                alignmentCurveId, bhId2Drl[bhId], bhChildIds,
                drlDiameterFct, drlCreateEltFct)

            createSectionedSolidHorizontal(ifcw, bhId, containerPlacementId, 
                alignmentCurveId, [bh], newBhIds,
                bhlDiameterFct, bhlCreateEltFct)

        else:
            createExtrudedAreaSolidElements(ifcw, bhId, containerPlacementId, origShift, 
                coords, bhId2Gf[bhId], bhChildIds, 
                gfDiameterFct, getLinesegment, gfCreateEltFct, segVal)

            createExtrudedAreaSolidElements(ifcw, bhId, containerPlacementId, origShift, 
                coords, bhId2Drl[bhId], bhChildIds, 
                drlDiameterFct, getLinesegment, drlCreateEltFct, segVal)

            createExtrudedAreaSolidElements(ifcw, bhId, containerPlacementId, origShift, 
                coords, [bh], newBhIds, 
                bhlDiameterFct, getLinesegment, bhlCreateEltFct, segVal)

        ifcw.nttRelAggregates(ifcw.createGuid(), ifcw.ownerHistoryId, 'Borehole Containment', None, newBhIds[0], bhChildIds)
        bhEltIds.append(newBhIds[0])

    ifcw.nttRelAggregates(ifcw.createGuid(), ifcw.ownerHistoryId, 'Assembly Attachement', None, assemblyId, bhEltIds)
    ifcw.nttRelAggregates(ifcw.createGuid(), ifcw.ownerHistoryId, 'Spatial Attachement', None, spatialContainerId, [assemblyId])

    logger.info('Successfully done!')

    # time duration to calculate segments
    def timecalc(calculatedSeconds):
        secondsInDay = 60 * 60 * 24
        secondsInHour = 60 * 60
        secondsInMinute = 60
        days = calculatedSeconds // secondsInDay
        hours = (calculatedSeconds - (days * secondsInDay)) // secondsInHour
        minutes = (calculatedSeconds - (days * secondsInDay) - (hours * secondsInHour)) // secondsInMinute
        return(minutes)
    calcSecs = time.time() - startTime
    logger.info('The script finished in {} minutes'.format(timecalc(calcSecs)))

