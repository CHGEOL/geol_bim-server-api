
from flask import Blueprint

from .usr_organisation import Organisation, OrganisationCollection
from .usr_user import User, UserImage, UserCollection
from .usr_user_application import UserApplication, UserApplicationCollection
from .usr_user_organisation import UserOrganisation, UserOrganisationCollection
from .usr_user_project import UserProject, UserProjectCollection


user_blueprint = Blueprint('user', __name__)  # URL-prefix, name


# A GET request will lead to this endpoint
@user_blueprint.route('/')
def index():
    """
    example endpoint
    """
    return 'Welcome to the user API of the GEOL_BIM Server.'


# Add Views to the app: register Collection first, then Single
user_blueprint.add_url_rule(
    '/organisations', view_func=OrganisationCollection.as_view('organisation_collection_api'))
user_blueprint.add_url_rule(
    '/organisations/<int:org_id>', view_func=Organisation.as_view('organisation_api'))

user_blueprint.add_url_rule(
    '/users', view_func=UserCollection.as_view('user_collection_api'))
user_blueprint.add_url_rule(
    '/users/<int:usr_id>', view_func=User.as_view('user_api'))
user_blueprint.add_url_rule(
    '/users/<int:usr_id>/img', view_func=UserImage.as_view('usr_img_api'))

user_blueprint.add_url_rule(
    '/user_applications',
    view_func=UserApplicationCollection.as_view('user_application_collection_api'))
user_blueprint.add_url_rule(
    '/user_applications/<int:uap_usr_id>/<string:uap_role_code>',
    view_func=UserApplication.as_view('user_application_api'))

user_blueprint.add_url_rule(
    '/user_organisations',
    view_func=UserOrganisationCollection.as_view('user_organisation_collection_api'))
user_blueprint.add_url_rule(
    '/user_organisations/<int:uog_usr_id>/<int:uog_org_id>/<string:uog_role_code>',
    view_func=UserOrganisation.as_view('user_organisation_api'))

user_blueprint.add_url_rule(
    '/user_projects',
    view_func=UserProjectCollection.as_view('user_project_collection_api'))
user_blueprint.add_url_rule(
    '/user_projects/<int:upr_usr_id>/<int:upr_pro_id>/<string:upr_role_code>',
    view_func=UserProject.as_view('user_project_api'))
