# GEOL_BIM Server API

REST API Server (Backend) für den GEOL_BIM Server

## Installation

See Frontend README.

## Input formats

In most cases the template/ and demoProject/ xlsx give enough guidance on how to fill the data in.

### Voxel

This section concerns the tab Voxel_GeologicFeature in the xlsx. It describes how it is expected to be filled in normally but also mentions some edge cases and limitations.

Normally the "Voxel Definition" and Pset_SolidStratumCapacity properties come from the CSV file and hence the mapping must be defined by starting the cell name with a colon followed by the column name in the CSV.
If the colon is omitted, the content of the cell is taken as a constant for all voxels of that file. E.g. for ExtX, ExtY, ExtZ there could be such constant values for the dimension of each voxel. Normally all voxels of one file share the "GeologicFeature Objecttype Definition" and belong to one slice in the IFC.

At the other hand it is also possible to specify voxels directly in the xlsx. If in a row of the tab Voxel_GeologicFeature there is no GeoFileName mentioned, such a row is considered as one single voxel. All such voxels belong in the IFC to one general slice.

Furthermore the presumably constant columns of the "GeologicFeature Objecttype Definition" can also be read from the CSV and hence appear prefixed by a colon in the xlsx. When doing so, it must be granted that the values in the CSV for BaseType and Perspective are conforming with what is allowed. The algorithm is flexible to some extent, but there are limits and if the values in the CSV are just not usable such lines will be ommitted an no voxel appears in the IFC.

Voxel definitions by CSV and direct ones can be mixed in the xlsx.

## Log warnings when creating the IFC file

Any warnings, if for example input data is interpreted of some sort or even ignored, are written to the IFC file as comments in order to appear as close to the line of the problem as possible. Like that they get automatically downloaded allong with the result IFC. At the same time these warnings can also be downloaded separatly as a log file in the UI.

## User registration

Single user registration is not acitvated (nor tested) in the UI.

Instead there is a standard procedure to create many logins at once. It is presumed, that we have got an xlsx with one row for each new user. Such a row contains first name, last name, email and organisation name for a user (Column A to D in our example below).

In the code /apisrv/views/trf_block.py there is the function sdFxCreatePws that can be used to create a number of strong passwords for a number of users. It can be uncommented in the code an the number of users set. Then as a side effect of creating an IFC on the local dev environment, these password-hash pairs are written to the console and from there can be copy-pasted to the xlsx (Column F and G in our example below).

Then we also add a formula as the following to the xlsx, in order to generate a series of createUser statements: 
```
="self.createUser(usr, '" & A2 & "', '" & B2 & "', '" & C2 & "', '" & G2 & "', '" & D2 & "')"
```
These statements are copy-pasted from the xlsx to the body of the sdFxCreateUsers funciton, which then is activated in the code instead of sdFxCreatePws. Its execution can be tested locally and if it runs successfully, it can be repeated on the test or prod environment by temporarily moving the trf_block.py file there and redeploy like:
```
sudo docker-compose -f docker-compose-test.yml -p geol_bim-apisrv up -d --build --force-recreate
```
One should not forget to reset trf_block.py after creating the logins.

Remind: Both side effects in the code are restricted to work only for a certain user to be logged in (Number 4 in our case). If your developer login has another user number you need to change that in the code.

The createUser function also does sort of a lazzy creation of the organisation if the name (column D in the example) does not yet exist. Otherwise the exising organisation is linked to the user.
