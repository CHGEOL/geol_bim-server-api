
from typing import List, Dict, Tuple, Optional, Union, TYPE_CHECKING
import logging

import psycopg2
from psycopg2 import sql
from psycopg2.extras import RealDictCursor
from flask import g, current_app as app
import werkzeug.exceptions
from collections import OrderedDict

from .http_status import HttpStatus
from .errormessage import error_mapping, ErrorMessage

if TYPE_CHECKING:
    # Import only for the type-checking, not on runtime
    import psycopg2.extensions


__all__ = ['init_close_db_connection', 'LoggingCursor', 'CRUD']


def init_close_db_connection():
    """
    Add the automatic closing of DB-connections to the application
    """
    @app.teardown_appcontext
    def close_conn(_):
        """
        Closes the connection automatically when the app-context is removed after each request.

        :param _: dummy var
        :return: None
        """
        app.logger.debug('CLOSING CONNECTION')
        db = g.pop('db', None)
        if db is not None:
            app.config['postgreSQL_pool'].putconn(db)


class LoggingCursor(RealDictCursor):
    """
    Log the SQL-Queries with the values. Only use in debug-mode.
    """
    def execute(self, query, args=None):  # pragma: no cover
        logger = logging.getLogger('sql_debug')
        # Todo: only on debug/testing? Remove completly?
        argsWoData = args
        if type(args) == OrderedDict:
            argsWoData = OrderedDict()
            for key, value in args.items():
                if type(value) != psycopg2.extensions.Binary:
                    argsWoData[key] = value
                else:
                    binLen = len(value.adapted)
                    argsWoData[key] = f'..BINARY({binLen})..'
        logger.info(self.mogrify(query, argsWoData))

        try:
            RealDictCursor.execute(self, query, args)
        except psycopg2.Error as exc:
            logger.info(
                f'{type(exc).__name__} pgcode {exc.pgcode}: {exc}')
            raise


class CRUD:
    """
    Base class for DB-Operations
    """
    logger = logging.getLogger('sql_debug')

    @staticmethod
    def get_error_message(exc: 'Exception') -> 'ErrorMessage':
        """
        Generates an error-message-Object, based on the type of the exception.
        The mapping from DB-Errorcodes to the Internal Codes is defined in
        ``errormessage.error_mapping``.

        :param exc: raised Exception
        :return: Object with the message and code
        """
        # The error is logged with the LoggingCursor
        default_messages = app.config['DEFAULT_MESSAGES']

        if isinstance(exc, psycopg2.InterfaceError):
            err_message = default_messages['BE_IF']
        elif isinstance(exc, psycopg2.DatabaseError):
            err_message = default_messages['BE_DB']
            if exc.pgcode.startswith('GBS'):
                # Custom GBS DataIntegrity Error
                # Get Error Message from Database Exception
                err_message = ErrorMessage(
                    code=HttpStatus.bad_request_400.value,
                    message=exc.diag.message_primary,
                    details={'hint': exc.diag.message_hint},
                    internal_code=exc.pgcode)
            else:
                if isinstance(exc, psycopg2.InternalError):
                    err_message = default_messages['BE_INT']
                elif isinstance(exc, psycopg2.OperationalError):
                    err_message = default_messages['BE_OP']
                elif isinstance(exc, psycopg2.NotSupportedError):
                    err_message = default_messages['BE_NOT_SUPP']
                elif isinstance(exc, psycopg2.ProgrammingError):
                    err_message = default_messages['BE_SYNT']
                elif isinstance(exc, psycopg2.DataError):
                    err_message = default_messages['BE_DAT']
                elif isinstance(exc, psycopg2.IntegrityError):
                    err_message = default_messages['BE_ITG']
        else:
            # Default 500 error for any other exception
            err_message = default_messages['INT_ERR']

        # If the subclass of the error is defined in the Error-Dictionary
        if isinstance(exc, psycopg2.Error) and error_mapping[exc.pgcode] in default_messages:
            err_message = default_messages[error_mapping[exc.pgcode]]
            if exc.diag.column_name:
                err_message.details = {exc.diag.column_name: None}
            elif exc.diag.constraint_name:
                err_message.details = {exc.diag.table_name: None}

        return err_message

    def get_db(self) -> 'psycopg2.extensions.connection':
        """
        Gets called during a request, the app context is already established

        :return: DB-Connection from the connection-pool
        """
        if 'db' not in g:
            app.logger.debug('CREATING CONNECTION')
            try:
                g.db = app.config['postgreSQL_pool'].getconn()
            except psycopg2.Error as exc:
                g.pop('db', None)
                err = self.get_error_message(exc)
                raise werkzeug.exceptions.InternalServerError(err)
        return g.db

    def get_cursor(self, conn: 'psycopg2.extensions.connection' = None
                   ) -> Tuple['psycopg2.extensions.connection', 'psycopg2.extensions.cursor']:
        """
        Gets a cursor from the connection, either given or
        from the request-context with the function ``get_db()``.

        :param conn: (optional) DB-Connection
        :return: Connection, Cursor
        """
        if not conn:
            conn = self.get_db()
        cursor = conn.cursor()
        return conn, cursor

    @staticmethod
    def _execute(cur: 'psycopg2.extensions.cursor', query: 'sql.Composable',
                 values: Union[Dict, List] = None) -> 'psycopg2.extensions.cursor':
        """
        Example-function for executing one query.

        .. warning::
           The connection is NOT committed and the cursor (and connection)
           stay open and have to be closed manually.

        :param cur: DB-Cursor
        :param query: Query to execute
        :param values: Dict or List of values matching the placeholders in the query
        :return: DB-Cursor containing the result
        """
        cur.execute(query, values)
        return cur

    def execute(self, queries: List['sql.Composable']) -> Tuple[Dict, int]:
        """
        Example-function for executing multiple queries
        and returning the appropriate message /code

        :param queries: List of tuples with the query and optional values.
        :return: (error-)message and code
        """
        message, code = {}, HttpStatus.internal_server_error_500.value  # returning values

        conn, cur = self.get_cursor()

        try:
            # Execute statements
            for query in queries:
                cur = self._execute(cur, query)
                # If necessary, do stuff with the cursor

            conn.commit()
            message = None
            code = HttpStatus.ok_200.value
        except Exception as exc:
            self.logger.info('Rolling back.')
            conn.rollback()
            err = self.get_error_message(exc)
            code = err.code
            message = err.to_dict()
        finally:
            cur.close()
            return message, code

    def _create(self, cur: 'psycopg2.extensions.cursor',
                query: 'sql.Composable',
                pk: Union[str, List[str]],
                values: Optional[Union[Dict, List]] = None) -> Tuple[Dict, int]:
        """
        Base-function for creating a new tuple.

        .. warning::
           The connection is NOT committed and the cursor (and connection)
           stay open and have to be closed manually.

        :param cur:
        :param query: SQL-Query
        :param pk: Name of the primary key as string or List of strings
        :param values: Dict or List of values matching the placeholders in the query
        :return: (ID or error message), HTTP Status Code
        """
        if not isinstance(pk, (str, tuple, list)):
            err = app.config['DEFAULT_MESSAGES']['INT_ERR']
            self.logger.warning(f'Invalid type for the keys of the returning dictionary.'
                                f'One of (str, list, tuple) required, {type(pk)} given.')

            return err.to_dict(), err.code
        # execute SQL
        cur.execute(query, values)
        new_ids = cur.fetchone()

        if isinstance(pk, str):
            code = HttpStatus.created_201.value
            message = {pk: new_ids[pk]}

        else:
            code = HttpStatus.created_201.value
            message = {k: new_ids[k] for k in pk}
        return message, code

    def create(self, query: 'sql.Composable',
               pk: Union[str, List[str]],
               values: Optional[Union[Dict, List]] = None) -> Tuple[Dict, int]:
        """
        INSERT a new tuple.

        :param query: SQL-Query
        :param pk: Name of the primary key as string or List of strings
        :param values: Dict or List of values matching the placeholders in the query
        :return: (ID or error message), HTTP Status Code
        """
        # Create multiples of the same object: (with cursor.execute_values(...)
        # See commented section in commit 1c7d92f3

        conn, cur = self.get_cursor()  # Get the DB-connection from db connection-pool
        message, code = {}, HttpStatus.internal_server_error_500.value  # returning values

        try:
            # Excute the query
            message, code = self._create(cur, query=query, pk=pk, values=values)
            conn.commit()

        except Exception as exc:
            self.logger.info('Rolling back.')
            conn.rollback()
            err = self.get_error_message(exc)
            code = err.code
            message = err.to_dict()

        finally:
            cur.close()  # Connection is closed automatically (teardown of app.context)
            return message, code

    @staticmethod
    def _read(cur,
              query: 'sql.Composable', values: Optional[Union[Dict, List]] = None,
              many: bool = False) -> Tuple[Union[List, Dict], int]:
        """
        Base-function for reading from the database.

        .. warning::
           The connection is NOT committed and the cursor (and connection)
           stay open and have to be closed manually.

        :param cur:
        :param query: SQL-Query
        :param values: Dict or List of values matching the placeholders in the query
        :param many: Query the collection: It returns an empty list.
                     (don't use fetchone for querying a single object,
                     multiple results are possible with joins)
        :return: (Data OR Error Message), HTTP Status Code
        """
        cur.execute(query, values)
        result = cur.fetchall()  # List with psycoopg2.RealDictRow()

        if result or many:
            message = result
            code = HttpStatus.ok_200.value
        else:
            err = app.config['DEFAULT_MESSAGES']['NF_OBJ']
            err.internal_message = values
            return err.to_dict(), err.code

        return message, code

    def read(self, query: 'sql.Composable', values: Optional[Union[Dict, List]] = None,
             many: bool = False) -> Tuple[Union[List, Dict], int]:
        """
        SELECT data with a query-String and matching values

        :param query: SQL-Query
        :param values: Dict or List of values matching the placeholders in the query
        :param many: Query the collection: It returns an empty list.
                     (don't use fetchone for querying a single object,
                     multiple results are possible with joins)
        :return: (Data OR Error Message), HTTP Status Code
        """
        conn, cur = self.get_cursor()  # Get the DB-connection from db connection-pool
        message, code = {}, HttpStatus.internal_server_error_500.value  # returning values

        try:  # execute SQL
            message, code = self._read(cur, query=query, values=values, many=many)
            # No commit necessary, only reading

        except Exception as exc:
            conn.rollback()  # Rollback needed for reading (if InternalError happens)
            err = self.get_error_message(exc)
            code = err.code
            message = err.to_dict()

        finally:
            cur.close()  # Connection is closed automatically (teardown of app.context)
            return message, code

    @staticmethod
    def _update(cur: 'psycopg2.extensions.cursor',
                query: 'sql.Composable',
                values: Optional[Union[Dict, List]] = None) -> Tuple[Dict, int]:
        """
        Base-function for updating a tuple.

        .. warning::
           The connection is NOT committed and the cursor (and connection)
           stay open and have to be closed manually.

        :param cur:
        :param query: SQL Query
        :param values: Dict or List of values matching the placeholders in the query; WHERE, VALUES
        :return: (None or error message), HTTP Status Code
        """
        cur.execute(query, values)
        count = cur.rowcount

        if count > 0:
            code = HttpStatus.no_content_204.value
            message = {}
        else:
            err = app.config['DEFAULT_MESSAGES']['NF_OBJ']
            err.internal_message = values
            return err.to_dict(), err.code

        return message, code

    def update(self, query: 'sql.Composable',
               values: Optional[Union[Dict, List]] = None) -> Tuple[Dict, int]:
        """
        UPDATE existing tuples

        :param query: SQL Query
        :param values: Dict or List of values matching the placeholders in the query; WHERE, VALUES
        :return: (None or error message), HTTP Status Code
        """
        conn, cur = self.get_cursor()  # Get the DB-connection from db connection-pool
        message, code = {}, HttpStatus.internal_server_error_500.value  # returning values

        try:  # execute SQL
            message, code = self._update(cur, query=query, values=values)
            conn.commit()

        except Exception as exc:
            self.logger.info('Rolling back.')
            conn.rollback()
            err = self.get_error_message(exc)
            code = err.code
            message = err.to_dict()

        finally:
            cur.close()  # Connection is closed automatically (teardown of app.context)
            return message, code

    @staticmethod
    def _delete(cur: 'psycopg2.extensions.cursor',
                query: 'sql.Composable',
                values: Optional[Union[Dict, List]] = None) -> Tuple[Dict, int]:
        """
        Base-function for deleting tuples.

        .. warning::
           The connection is NOT committed and the cursor (and connection)
           stay open and have to be closed manually.

        :param cur:
        :param query: SQL Query
        :param values: Dict or List of values matching the placeholders in the query; WHERE
        :return: (None or error message), HTTP Status Code
        """
        cur.execute(query, values)
        count = cur.rowcount

        if count == 1:
            code = HttpStatus.ok_200.value
            message = dict(message=f'{count} object deleted', count=count)
        elif count > 1:
            code = HttpStatus.ok_200.value
            message = dict(message=f'{count} objects deleted', count=count)
        else:
            err = app.config['DEFAULT_MESSAGES']['NF_OBJ']
            err.internal_message = values
            return err.to_dict(), err.code

        return message, code

    def delete(self, query: 'sql.Composable',
               values: Optional[Union[Dict, List]] = None) -> Tuple[Dict, int]:
        """
        DELETE tuples

        :param query: SQL Query
        :param values: Dict or List of values matching the placeholders in the query; WHERE
        :return: (None or error message), HTTP Status Code
        """
        conn, cur = self.get_cursor()  # Get the DB-connection from db connection-pool
        message, code = {}, HttpStatus.internal_server_error_500.value  # returning values

        try:  # execute SQL
            message, code = self._delete(cur, query=query, values=values)
            conn.commit()

        except Exception as exc:
            self.logger.info('Rolling back.')
            conn.rollback()
            err = self.get_error_message(exc)
            code = err.code
            message = err.to_dict()

        finally:
            cur.close()  # Connection is closed automatically (teardown of app.context)
            return message, code
